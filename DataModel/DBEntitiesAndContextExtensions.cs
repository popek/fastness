﻿using Fastness.Common;
using System.Data.Entity;

namespace Fastness.DataModel
{
   public partial class fastnessEntities
   {
      // so set modified is unit testable
      public virtual void SetAsModified(object entity)
      {
         Entry(entity).State = EntityState.Modified;
      }
      public virtual void ForceDetectChanges()
      {
         ChangeTracker.DetectChanges();
      }
   }

   //
   public partial class User
   {
      public static readonly int ADMIN = 0;
      public static readonly int WRITER = 1;
      public static readonly int READER = 2;

      public bool IsValidPassword(string plain)
      {
         return PasswordUtils.IsPasswordOk(plain, Password);
      }
   }

   //
   public partial class ItemDetail
   {
      public static string DestructionDateFormatYYYY()
      {
         return "yyyy";
      }
      public static string DestructionDateFormatMMYYYY()
      {
         return "MM\\/yyyy";
      }
      public static string DestructionDateFormatDDMMYYYY()
      {
         return "dd\\/MM\\/yyyy";
      }
   }
}
