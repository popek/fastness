﻿using Fastness.Common;
using Fastness.DataModel;
using Fastness.DataModel.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fastness.DataModel.Utils
{
   public class ItemsAndPercentsCount
   {
      public int ItemsCount = 0;
      public int ItemsPercentsCount = 0;
   }

   public class FetchFromDb
   {
      fastnessEntities dbCtx;

      public FetchFromDb(fastnessEntities dbCtx)
      {
         this.dbCtx = dbCtx;
      }

      public ItemDetail ItemBasedOnItemNo(string itemNo)
      {
         var result = (from item in dbCtx.ItemDetail where item.ItemNumber == itemNo select item).ToList();

         if (result.Count > 1)
         {
            throw new DataNotUniqueException(string.Format("There are {0} Items with ItemNumber = {1}!", result.Count, itemNo));
         }
         if (result.Count == 0)
         {
            throw new DataDoesNotExistException(string.Format("Item with ItemNumber = {0} does not exists!"));
         }
         return result[0];
      }

      public Contract ContractBasedOnContractId(int contractId)
      {
         var contracts = (from ctr in dbCtx.Contract where ctr.ContractID == contractId select ctr).ToList<Contract>();

         if (contracts.Count > 1)
         {
            throw new DataNotUniqueException(string.Format("contractId({0}) does not match exactly single contract! ({1} matches found)", contractId, contracts.Count));
         }
         if (contracts.Count == 0)
         {
            throw new DataDoesNotExistException(string.Format("contractId({0}) does not match exactly single contract! (0 matches found)", contractId));
         }
         return contracts[0];
      }

      public double ConsignmentChargeBasedOnContractId(int contractId)
      {
         var price = (from ca in dbCtx.ContractAction
                      where ca.ContractID == contractId && ca.Description == ActionUtils.ACTION_DESCRIPTION_CONSIGNMENT
                      select ca.Charge).ToList<double>();

         if (price.Count > 1)
         {
            throw new DataNotUniqueException(string.Format("consignment action for contractId {0}, is not unique! ({1} matches found)", contractId, price.Count));
         }
         if (price.Count == 0)
         {
            throw new DataDoesNotExistException(string.Format("consignment action for contractId {0} does not exists!", contractId));
         }
         return price[0];
      }

      public ItemsAndPercentsCount ContractItemsCountBasedOnContractId(int contractId)
      {
         var itemsCountAndTotalPercentageQuery =
            from item in dbCtx.ItemDetail
            where item.ContractID == contractId && item.InStock == true
            group item by item.ContractID into contractItems
            select new ItemsAndPercentsCount
            {
               ItemsCount = contractItems.Count(),
               ItemsPercentsCount = contractItems.Sum(i => i.Percentage ?? 100)
            };

         if (itemsCountAndTotalPercentageQuery.Count() == 0)
         {
            return new ItemsAndPercentsCount { ItemsCount = 0, ItemsPercentsCount = 0 };
         }
         return itemsCountAndTotalPercentageQuery.First();
      }

      public InvoiceHistory ContractStoredInvoice(Contract ctr, DateTime yearMonthOfInvoices)
      {
         var query = from ih in dbCtx.InvoiceHistory
                     where ih.Year == yearMonthOfInvoices.Year &&
                           ih.Month == yearMonthOfInvoices.Month &&
                           ih.ContractID == ctr.ContractID
                     select ih;

         var historyList = query.ToList<InvoiceHistory>();

         if (historyList.Count > 1)
         {
            throw new DataNotUniqueException(string.Format("There are {0} ivnoices stored for contract {1}, and date {2}/{3} which indicated data consistency error (can be only one invoice per contract and date)!",
               historyList.Count,
               ctr.LogDesc(),
               yearMonthOfInvoices.Year,
               yearMonthOfInvoices.Month));
         }
         else if (historyList.Count == 1)
         {
            return historyList[0];
         }
         else
         {
            return null;
         }
      }

      public IEnumerable<InvoiceHistory> AllStoredInvoices(DateTime yearMonthOfInvoices)
      {
         var query = from ih in dbCtx.InvoiceHistory
                     where ih.Year == yearMonthOfInvoices.Year &&
                           ih.Month == yearMonthOfInvoices.Month
                     select ih;
         return query;
      }

      public IEnumerable<Contract> AvailableContracts()
      {
         var query = from ctr in dbCtx.Contract
                     where ctr.ContractFinished == false
                     select ctr;
         return query;
      }

      public IEnumerable<ItemDetail> AllContractItems(Contract ctr)
      {
         return
            from item in dbCtx.ItemDetail
            where item.ContractID == ctr.ContractID
            select item;
      }
      public IEnumerable<ItemDetail> InStockContractItems(Contract ctr)
      {
         var query = from i in dbCtx.ItemDetail
                     where (i.InStock ?? true) == true && i.ContractID == ctr.ContractID
                     select i;
         return query;
      }

      public int NextInvoiceNo()
      {
         var query = from mp in dbCtx.MiscParams select mp;
         if (query.Count() != 1)
         {
            throw new InvalidOperationException(string.Format(
               "MiscParams MUST have only single record! Currently there are {0} records in this table", 
               query.Count()));
         }
         return query.First().NextInvoiceNo;
      }
   }
}
