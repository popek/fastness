﻿using Fastness.DataModel;
using System.Linq;
using Fastness.Common;

namespace Fastness.DataModel.Utils
{
   public class AskDb
   {
      fastnessEntities dbCtx;

      public AskDb(fastnessEntities dbCtx)
      {
         this.dbCtx = dbCtx;
      }

      public bool LocationOccupied(string location)
      {
         var query = from item in dbCtx.ItemDetail where item.Location == location select item;
         return query.ToList<ItemDetail>().Count > 0;
      }

      public bool ItemAlreadyExists(string itemNo)
      {
         var query = from item in dbCtx.ItemDetail where item.ItemNumber == itemNo select item;
         return query.ToList<ItemDetail>().Count > 0;
      }

      public bool AreInvoicesAlreadyGenerated(int year, int month)
      {
         var alreadyExistingInvoicesForRequestMonth =
            from ih in dbCtx.InvoiceHistory
            where ih.Month == month && ih.Year == year
            group ih by ih.Year into invoices
            select invoices.Count();

         return alreadyExistingInvoicesForRequestMonth.Count() > 0 && // will be zero in case when invoiceHistory table is empty
                alreadyExistingInvoicesForRequestMonth.First() > 0;
      }

      public bool IsItLatestReconsignment(ActionHistory reconHistoryEntry)
      {
         // get all item reconsignments and check if this is the last one
         var lastReconId = (from ah in dbCtx.ActionHistory
                            where (ah.Description == ActionUtils.ACTION_DESCRIPTION_RECONSIGNMENT &&
                                  ah.ItemNumber == reconHistoryEntry.ItemNumber)
                            orderby ah.ActionHistoryID
                            select ah.ActionHistoryID).ToArray().Last();

         return reconHistoryEntry.ActionHistoryID == lastReconId;
      }
   }
}
