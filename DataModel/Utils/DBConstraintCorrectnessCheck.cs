﻿using Fastness.Common;
using Fastness.DataModel.Exceptions;
using log4net;
using System;
using System.IO;
using System.Linq;

namespace Fastness.DataModel.Utils
{
   public class DBConstraintCorrectnessCheck
   {
      static readonly ILog log = LogManager.GetLogger(typeof(DBConstraintCorrectnessCheck));

      Action<string> reportError = new Action<string>(err => log.Error(err));

      public void Run()
      {
         CheckIfConsignmetsAndReconsignmentsChargeIsTheSame();

         CheckContractItemsStatsCorrectness();

      }

      private void CheckIfConsignmetsAndReconsignmentsChargeIsTheSame()
      {
         CheckIfConsignmetsAndReconsignmentsChargeAreTheSameInActionTemplate();

         using (var ctx = new fastnessEntities())
         {
            foreach (var ctr in ctx.Contract)
            {
               CheckIfConsignmetsAndReconsignmentsChargeAreTheSame(ctr.ContractID);
            }
         }
      }
      private void CheckIfConsignmetsAndReconsignmentsChargeAreTheSameInActionTemplate()
      {
         using (var ctx = new fastnessEntities())
         {
            var conQuery = from a in ctx.ActionTemplate
                        where a.Description == ActionUtils.ACTION_DESCRIPTION_CONSIGNMENT
                        orderby a.ServiceLevel
                        select a;

            var reconQuery = from a in ctx.ActionTemplate
                             where a.Description == ActionUtils.ACTION_DESCRIPTION_RECONSIGNMENT
                             orderby a.ServiceLevel
                             select a;

            if (conQuery.Count() != reconQuery.Count())
            {
               reportError(string.Format("ActionTemplate table error: consignments count {0} != reconsignemtns count {1}", 
                  conQuery.Count(), reconQuery.Count()));
            }

            var conList = conQuery.ToList();
            var reconList = reconQuery.ToList();

            for (var i = 0; i < Math.Max(conList.Count, reconList.Count); ++i)
            {
               if (conList[i].ServiceLevel != reconList[i].ServiceLevel)
               {
                  reportError("ActionTemplate table: service levels of con and recon should be the same!");
               }
               if (conList[i].Charge != reconList[i].Charge)
               {
                  reportError("ActionTemplate table: charge of con and recon of the same service level should be the same!");
               }
            }
         }
      }
      private void CheckIfConsignmetsAndReconsignmentsChargeAreTheSame(int contractId)
      {
         using (var ctx = new fastnessEntities())
         {
            var conCharge = from ca in ctx.ContractAction
                            where ca.Description == ActionUtils.ACTION_DESCRIPTION_CONSIGNMENT && ca.ContractID == contractId
                            select ca.Charge;

            var reconCharge = from ca in ctx.ContractAction
                              where ca.Description == ActionUtils.ACTION_DESCRIPTION_RECONSIGNMENT && ca.ContractID == contractId
                              select ca.Charge;

            if (conCharge.Count() != 1)
            {
               reportError(string.Format("ContractAction table error: contract {0}, has {1} consignment actions! (should have only one)", 
                  contractId, conCharge.Count()));
            }
            if (reconCharge.Count() != 1)
            {
               reportError(string.Format("ContractAction table error: contract {0}, has {1} re-consignment actions! (should have only one)", 
                  contractId, reconCharge.Count()));
            }

            if (conCharge.FirstOrDefault() != reconCharge.FirstOrDefault())
            {
               reportError(string.Format(
                  "ContractAction table: charge of con and recon for contract {0} should be the same! (they are not: con: {1}, recon: {2}", 
                  contractId, conCharge.FirstOrDefault(), reconCharge.FirstOrDefault()));
            }
         }
      }

      private void CheckContractItemsStatsCorrectness()
      {
         using (var dbCtx = new fastnessEntities())
         {
            foreach (var ctr in dbCtx.Contract)
            {
               // check if items count equals to in stock items
               var itemsCount = new FetchFromDb(dbCtx).InStockContractItems(ctr).Count();
               if (itemsCount != ctr.Total)
               {
                  reportError(string.Format("contract {0} items count differs! ({1} != {2})",
                     ctr.LogDesc(), ctr.Total, itemsCount));
               }

               // check item stats arithmetic
               int itemsTakenToStock = ctr.ConLastMonth + ctr.ConThisMonth + ctr.ReConThisMonth;
               int itemsReleaseFromStock = ctr.DestroyedThisMonth + ctr.ReturnedThisMonth;
               if (itemsTakenToStock - itemsReleaseFromStock != ctr.Total)
               {
                  reportError(string.Format("contract({0}) in-stock items count track is corrupted! {1}", 
                     ctr.LogDesc(), ctr.ItemsCountLogDesc()));
               }
            }
         }

      }
   }
}
