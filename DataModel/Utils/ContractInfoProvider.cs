﻿using Fastness.Common;
using Fastness.DataModel.Exceptions;
using System;
using System.Linq;

namespace Fastness.DataModel.Utils
{
   public class ContractInfoProvider
   {
      fastnessEntities dbCtx;

      public ContractInfoProvider(fastnessEntities dbCtx)
      {
         this.dbCtx = dbCtx;
      }

      public Contract GetContract(int contractId)
      {
         return new FetchFromDb(dbCtx).ContractBasedOnContractId(contractId);
      }

      public ItemsAndPercentsCount GetContractItemsCount(int contractId)
      {
         return new FetchFromDb(dbCtx).ContractItemsCountBasedOnContractId(contractId);
      }

      public double GetContractItemPrice(int contractId)
      {
         return new FetchFromDb(dbCtx).ConsignmentChargeBasedOnContractId(contractId);
      }
   }
}
