﻿using Fastness.DataModel.Transaction;
using System;
using System.Linq;

namespace Fastness.DataModel.Utils
{
   public static class Misc
   {
      public enum AddressStatus
      {
         ADDRESS_OK,
         ADDRESS_TO_LONG,
         ADDRESS_TO_MUCH_LINES,
         ADDRESS_IS_EMPTY
      }

      public static double CurrentVAT
      {
         get 
         {
            using (var dbCtx = new fastnessEntities())
            {
               var query = from mp in dbCtx.MiscParams select mp;
               if (query.Count() != 1)
               {
                  throw new InvalidOperationException(string.Format("MiscParams MUST have only single record! Currently there are {0} records in this table", query.Count()));
               }
               return query.First().VAT;
            }
         }
         set
         {
            ModifyDb.RunTransaction(dbCtx => new UpdateVAT(value, dbCtx).Run());
         }
      }

      public static string FixAddress(string inAddress)
      {
         return string.Join(Environment.NewLine, ToTrimmedLines(inAddress));
      }
      private static string[] ToTrimmedLines(string multiLineText)
      {
         return (from line in multiLineText.Split(new string[] { Environment.NewLine }, StringSplitOptions.None)
                 where line.Trim() != string.Empty
                 select line.Trim()).ToArray();
      }

      public static AddressStatus ValidateAddress(string address, int maxLength, int maxLines)
      {
         var fixedAddress = FixAddress(address);

         if (string.IsNullOrEmpty(fixedAddress))
         {
            return AddressStatus.ADDRESS_IS_EMPTY;
         }
         if (fixedAddress.Length > maxLength)
         {
            return AddressStatus.ADDRESS_TO_LONG;
         }
         if (fixedAddress.Split(new string[] { Environment.NewLine }, StringSplitOptions.None).Length > maxLines)
         {
            return AddressStatus.ADDRESS_TO_MUCH_LINES;
         }
         return AddressStatus.ADDRESS_OK;
      }

      public static string[] EmailAddressesListToArray(string emailAddressesList)
      {
         return ToTrimmedLines(emailAddressesList);
      }
   }
}
