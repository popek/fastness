﻿
namespace Fastness.DataModel.Utils
{
   public class DBHandle
   {
      object sync = new object();
      
      bool _needReload = true;
      public bool NeedReload 
      {
         set 
         {
            lock(sync)
            {
               _needReload = value;
            }
         }
      }
      
      fastnessEntities _dbCtx = null;
      public fastnessEntities DBCtx 
      {
         get 
         {
            lock (sync)
            { 
               if(_needReload)
               {
                  if (_dbCtx != null)
                  {
                     _dbCtx.Dispose();
                  }
                  _dbCtx = new fastnessEntities();
                  _needReload = false;
               }
               return _dbCtx;
            }
         }
      }
   }
}
