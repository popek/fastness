﻿using Fastness.DataModel.Exceptions;

namespace Fastness.DataModel.Utils
{
   public class DBFieldsConstraint
   {
      public static readonly int ITEM_NUMBER_LENGTH = 8;
      public static readonly int LOCATION_NUMBER_LENGTH = 8;
      public static readonly int CLIENT_ITEM_REF_MAX_LENGTH = 30;
      public static readonly int ITEM_DESCRIPTION_MAX_LENGTH = 255;
      public static readonly int ITEM_PERCENTAGE_MAX_LENGTH = 3;
      public static readonly int ACTION_HISTORY_NOTES_MAX_LENGHT = 255;
      public static readonly int ACTION_HISTORY_AUTHORIZED_BY_MAX_LENGHT = 30;

      public static readonly int INVOICE_HISTORY_DETAILS_MAX_LENGHT = 1024;
   }

   //
   internal class ItemConsistencyValidator
   { 
      ItemDetail item;

      public ItemConsistencyValidator(ItemDetail item)
      {
         this.item = item;
      }

      public void Validate()
      {
         if (item.ItemNumber.Length != DBFieldsConstraint.ITEM_NUMBER_LENGTH)
         {
            throw new BusinessRuleException(string.Format("item({0}) is corrupted! item number MUST be 8 digits long", item.LogDesc()));
         }

         if (item.DateRecieved == null)
         {
            throw new BusinessRuleException(string.Format("item({0} is corrupted! there is no consign date!)", item.LogDesc()));
         }

         if (item.InStock ?? true)
         {
            ValidateInStockState();
         }
         else
         {
            ValidateOutOfStockState();
         }

         if (item.Destroyed ?? false)
         {
            ValidateDestroyedState();
         }
         else
         {
            ValidateNotDestroyedState();
         }

         if (item.DateRetrieved != null)
         {
            ValidateOutOfStockState();
            ValidateNotDestroyedState();
         }

         if (item.DateReturned != null)
         {
            ValidateInStockState();
            ValidateNotDestroyedState();
         }
      }

      private void ValidateInStockState()
      {
         if (!(item.InStock ?? true))
         {
            throw new BusinessRuleException(string.Format(
               "item({0}) is corrupted! in stock item in-stock flag must be true(or null). Currently flag is set to false", item.LogDesc()));
         }
         if (item.Location.Length != DBFieldsConstraint.LOCATION_NUMBER_LENGTH)
         {
            throw new BusinessRuleException(string.Format(
               "item({0}) is corrupted! in stock item location MUST be {1} digits long", item.LogDesc(), DBFieldsConstraint.LOCATION_NUMBER_LENGTH));
         }
         
         ValidateNotDestroyedState();

         if (item.DateRetrieved != null)
         {
            throw new BusinessRuleException(string.Format(
               "item({0}) is corrupted! in stock item retrieve date is set ({1}) which indicates that item had been retrieved to client. When item is retireved it must be marked as out of stock", item.LogDesc(), item.DateRetrieved));
         }
      }
      private void ValidateOutOfStockState()
      {
         if (item.InStock ?? true)
         {
            throw new BusinessRuleException(string.Format(
               "item({0}) is corrupted! out of stock item in-stock flag must be false. Currently flag is set to true(or null)", item.LogDesc()));
         }
         if (item.DateReturned != null)
         {
            throw new BusinessRuleException(string.Format(
               "item({0}) is corrupted! out of stock item return(reconsign) date is set ({1}) which indicates that item had been taken back from client. When item is reconsigned it must be marked as in stock", item.LogDesc(), item.DateReturned));
         }
         if (item.Location != "")
         {
            throw new BusinessRuleException(string.Format(
               "item({0}) is corrupted! out of stock item location MUST empty string. Current location is set to: {1}", item.LogDesc(), item.Location));
         }
      }
      private void ValidateDestroyedState()
      {
         if (!(item.Destroyed ?? false))
         {
            throw new BusinessRuleException(string.Format(
               "item({0}) is corrupted! destroyed item destroy flag must be true. Currently flag is set to false(or null)", item.LogDesc()));
         }
         if (item.DateDestroyed == null)
         {
            throw new BusinessRuleException(string.Format(
               "item({0}) is corrupted! destroyed item destroy date must be set. Currently destroy date is null", item.LogDesc()));
         }
         if (item.DateRetrieved != null)
         {
            throw new BusinessRuleException(string.Format(
               "item({0}) is corrupted! destroyed item retrieve date must be null. Current retrieve date: {1}", item.LogDesc(), item.DateRetrieved));
         }
         ValidateOutOfStockState();
      }
      private void ValidateNotDestroyedState()
      {
         if ((item.Destroyed ?? false))
         {
            throw new BusinessRuleException(string.Format(
               "item({0}) is corrupted! not destroyed item destroy flag must be false(or null). Currently flag is set to true", item.LogDesc()));
         }
         if (item.DateDestroyed != null)
         {
            throw new BusinessRuleException(string.Format(
               "item({0}) is corrupted! not destroyed item destroy date must be null. Currently destroy date is {1}", item.LogDesc(), item.DateDestroyed));
         }
      }
   }

   //
   public static class FastnessBusinessRules
   {
      public static void ValidateItemCosistency(ItemDetail item)
      {
         new ItemConsistencyValidator(item).Validate();
      }

      public static void ValidateContractConsistency(Contract ctr)
      {
         int itemsTakenToStock = ctr.ConLastMonth + ctr.ConThisMonth + ctr.ReConThisMonth;

         int itemsReleaseFromStock = ctr.DestroyedThisMonth + ctr.ReturnedThisMonth;

         if (itemsTakenToStock - itemsReleaseFromStock != ctr.Total)
         {
            throw new BusinessRuleException(string.Format("contract({0}) in-stock items count track is corrupted! {1}", ctr.LogDesc(), ctr.ItemsCountLogDesc()));
         }
      }
   }

}
