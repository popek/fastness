﻿using System;
using System.Data;
using System.Linq;
using Fastness.DataModel.Transaction;

namespace Fastness.DataModel.Utils
{
   public static class ModifyDb
   {
      public static void RunTransaction(Action<fastnessEntities> transaction)
      {
         /*
         sync algo works as follows:
         - sync record is being red, changed, and saved - result sync row is not commited 
           (since we are in transaction scope). Thus other thread (in particular other fastness application)
           will hang during either reading sync row (when other thread/app has already saved sync row),
           or this thread/app will hang on saving sync row (when other thread/app was first to save the sync row)
         - proper DB modificationis performed
         - sync record is being decremented (step is not needed but just for clarity sync record data is 'rolled back')
         */
         using (var dbCtx = new fastnessEntities())
         using (var ts = dbCtx.Database.BeginTransaction(IsolationLevel.RepeatableRead))
         {
            // lock DB by locking sync record
            var oneAndOnlySyncRecord = dbCtx.MiscParams.First();
            oneAndOnlySyncRecord.DbWriteSync += 1;
            dbCtx.MiscParams.Attach(oneAndOnlySyncRecord);
            dbCtx.SetAsModified(oneAndOnlySyncRecord);
            dbCtx.SaveChanges();

            // perform DB modification
            transaction(dbCtx);

            // 'rollback' sync record data (step not needed. Introduced for clarity)
            oneAndOnlySyncRecord.DbWriteSync -= 1;
            dbCtx.MiscParams.Attach(oneAndOnlySyncRecord);
            dbCtx.SetAsModified(oneAndOnlySyncRecord);
            dbCtx.SaveChanges();

            ts.Commit();
         }

      }
   }
}
