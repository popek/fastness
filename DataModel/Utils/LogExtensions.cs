﻿
using System.Text;
namespace Fastness.DataModel.Utils
{
   public static class LogExtensions
   {
      public static string LogDesc(this Contract ctr)
      {
         return string.Format("[{0} '{1}' '{2}']", ctr.ContractID, ctr.Name, ctr.AccountRef);      
      }
      public static string LogDesc(this ItemDetail item)
      {
         return string.Format("[{0}, no: {1}, loc: {2}, perc: {3}, ownerId: {4}]", 
            item.ItemID, item.ItemNumber, item.Location, item.Percentage??100, item.ContractID);
      }
      public static string LogDesc(this ActionHistory actionHistory)
      {
         return string.Format("[{0}, desc: {1}, itemNo: {2}, itemLoc: {3}, dateCompleted: {4}]", 
            actionHistory.ActionHistoryID, 
            actionHistory.Description,
            actionHistory.ItemNumber,
            actionHistory.Location, 
            actionHistory.DateCompleted);
      }
      public static string LogDesc(this ContractAction ctrAction)
      {
         return string.Format("[{0}, ctrId: {1}, code: {2}, desc: {3}, charge: {4}, per. free: {5}]",
            ctrAction.ContractActionID,
            ctrAction.ContractID,
            ctrAction.Code,
            ctrAction.Description,
            ctrAction.Charge,
            ctrAction.PerFreeAction);
      }
      public static string LogDesc(this ActionTemplate at)
      {
         return string.Format("[{0}, code: {1}, desc: {2}, charge: {3}, per. free: {4}]",
            at.ActionTemplateID,
            at.Code,
            at.Description,
            at.Charge,
            at.PerFreeAction);
      }
      public static string LogDesc(this InvoiceHistory ih)
      {
         return string.Format("[no.:{0}, date:{1}/{2}, ctrId:{3}]", ih.InvoiceNo, ih.Year, ih.Month, ih.ContractID);
      }

      public static string ItemsCountLogDesc(this Contract ctr)
      { 
         return string.Format("con. last month: {0}, con.: {1}, ret.: {2}, recon.: {3}, des.: {4}, tot.: {5}",
            ctr.ConLastMonth,
            ctr.ConThisMonth,
            ctr.ReturnedThisMonth,
            ctr.ReConThisMonth,
            ctr.DestroyedThisMonth,
            ctr.Total);
      }

      public static string StateLogDesc(this ItemDetail item)
      {
         var sb = new StringBuilder();
         if(item.DestructionDate != null)
         {
            sb.AppendFormat("destruction: {0}, ", item.DestructionDate);
         }
         if(item.DateRetrieved != null)
         {
            sb.AppendFormat("retrieved at: {0}, ", item.DateRetrieved);
         }
         if(item.DateReturned != null)
         {
            sb.AppendFormat("returned at: {0}, ", item.DateReturned);
         }
         if(item.DateDestroyed != null)
         {
            sb.AppendFormat("destroyed at: {0}, ", item.DateDestroyed);
         }
         if(item.Destroyed != null)
         {
            sb.AppendFormat("destroyed: {0}, ", item.Destroyed);
         }
         sb.AppendFormat("loc: {0}, ", item.Location);
         if(item.InStock != null)
         {
            sb.AppendFormat("in stock: {0}, ", item.InStock);
         }
         return sb.ToString();
      }
   }
}
