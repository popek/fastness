﻿using Fastness.Common;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fastness.DataModel.Transaction
{
   public class UpdateEmailedInvoiceDetails
   {
      static readonly ILog log = LogManager.GetLogger(typeof(UpdateEmailedInvoiceDetails));

      List<int> contractIdsForWhichInvoiceMailHadBeenSent;
      DateTime invoiceDate;
      fastnessEntities dbCtx;

      public UpdateEmailedInvoiceDetails(List<int> contractIdsForWhichInvoiceMailHadBeenSent, DateTime invoiceDate, fastnessEntities dbCtx)
      { 
         this.contractIdsForWhichInvoiceMailHadBeenSent = contractIdsForWhichInvoiceMailHadBeenSent;
         this.invoiceDate = invoiceDate;
         this.dbCtx = dbCtx;
      }

      public void Run()
      {
         try
         {
            RunInternal();
         }
         catch (Exception e)
         {
            log.ErrorFormat("error while trying to update e-mailed invoice details: {0}", e);
            throw;
         }
      }

      private void RunInternal()
      {
         log.DebugFormat("updating last e-mailed invoices date ({0}) for contracts: [{1}]", 
            DateUtils.ToString(invoiceDate),
            contractIdsForWhichInvoiceMailHadBeenSent.Aggregate("", (id, result) => result + string.Format("{0}, ", id)));

         foreach (int contractId in contractIdsForWhichInvoiceMailHadBeenSent)
         {
            var ctr = dbCtx.Contract.Where(c => c.ContractID == contractId).First();

            ctr.LastSendInvoice = invoiceDate;
            
            dbCtx.Contract.Attach(ctr);
            dbCtx.SetAsModified(ctr);
         }

         dbCtx.SaveChanges();
      }
   }
}
