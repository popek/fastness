﻿using log4net;
using System;
using System.Linq;
using Fastness.DataModel.Utils;

namespace Fastness.DataModel.Transaction
{
   public class AddContract
   {
      static readonly ILog log = LogManager.GetLogger(typeof(AddContract));

      Contract ctr;
      fastnessEntities dbCtx;

      public AddContract(Contract ctr, fastnessEntities dbCtx)
      {
         this.ctr = ctr;
         this.dbCtx = dbCtx;
      }

      public void Run()
      {
         try
         {
            RunInternal();
         }
         catch (Exception e)
         {
            log.ErrorFormat("error while trying to add new contract: {0}", e);
            throw;
         }
      }

      private void RunInternal()
      {
         ctr.ContractID = dbCtx.Contract.Count() == 0 ? 1 : dbCtx.Contract.Max(c => c.ContractID) + 1;

         ctr.ConLastMonth = 0;
         ctr.ConThisMonth = 0;
         ctr.DestroyedThisMonth = 0;
         ctr.ReturnedThisMonth = 0;
         ctr.ReConThisMonth = 0;

         log.InfoFormat("adding new contract: {0}. Service Level: {1}", ctr.LogDesc(), ctr.ServiceLevel);

         AttachContractActions(dbCtx, ctr);

         FastnessBusinessRules.ValidateContractConsistency(ctr);

         dbCtx.Contract.Add(ctr);

         dbCtx.SaveChanges();
      }

      public static void AttachContractActions(fastnessEntities dbCtx, Contract ctr)
      {
         var serviceLevelAction =
            from a in dbCtx.ActionTemplate
            where a.ServiceLevel == ctr.ServiceLevel
            select a;

         foreach (var a in serviceLevelAction)
         {
            dbCtx.ContractAction.Add(
               new ContractAction
               {
                  Description = a.Description,
                  Code = a.Code,
                  Charge = a.Charge,
                  PerFreeAction = a.PerFreeAction,
                  ContractID = ctr.ContractID,
               });
         }
      }      
   }
}
