﻿using log4net;
using System;
using System.Linq;
using Fastness.DataModel.Utils;
using Fastness.Common;
using Fastness.DataModel.Exceptions;

namespace Fastness.DataModel.Transaction
{
   public class UpdateActionHistoryEntry
   {
      static readonly ILog log = LogManager.GetLogger(typeof(UpdateActionHistoryEntry));

      ActionHistory entryToUpdate;
      
      string newLocation;
      double newCharge;
      DateTime newDate;
      string newAuthorization;
      string newNotes;

      string actionCode;
      fastnessEntities dbCtx;

      ItemDetail itemLinkedWithHistoryEntry;
      ItemDetail LinkedItem 
      { 
         get 
         {
            if (itemLinkedWithHistoryEntry == null)
            {
               itemLinkedWithHistoryEntry = new FetchFromDb(dbCtx).ItemBasedOnItemNo(entryToUpdate.ItemNumber);
            }
            return itemLinkedWithHistoryEntry;
         } 
      }

      public UpdateActionHistoryEntry(ActionHistory entryToUpdate, string newLocation, double newCharge, DateTime newDate, string newAuthorization, string newNotes, string actionCode, fastnessEntities dbCtx)
      {
         this.entryToUpdate = entryToUpdate;
         
         this.newLocation = newLocation;
         this.newCharge = newCharge;
         this.newDate = newDate;
         this.newAuthorization = string.IsNullOrEmpty(newAuthorization) ? null : newAuthorization;
         this.newNotes = string.IsNullOrEmpty(newNotes) ? null : newNotes;

         this.actionCode = actionCode;
         this.dbCtx = dbCtx;
      }
      public void Run()
      {
         try
         {
            RunInternal();
         }
         catch (Exception e)
         {
            log.ErrorFormat("error while trying to update history entry {0}: {1}", entryToUpdate.LogDesc(), e);
            throw;
         }
      }
      private bool UpdateItemDateIfRequired()
      {
         if (ActionUtils.IsConsignment(actionCode))
         {
            log.DebugFormat("updating item received date ({0} -> {1})", LinkedItem.DateRecieved, newDate);
            LinkedItem.DateRecieved = newDate;
            return true;
         }

         if (ActionUtils.IsReconsignment(actionCode) && 
             LinkedItem.DateReturned.HasValue && 
             LinkedItem.DateReturned == entryToUpdate.DateCompleted)
         {
            log.DebugFormat("updating item returned(reconsignment) date ({0} -> {1})", LinkedItem.DateReturned, newDate);
            LinkedItem.DateReturned = newDate;
            return true;
         }

         if (ActionUtils.IsAnyKindOfRetrieval(actionCode) &&
             LinkedItem.DateRetrieved.HasValue &&
             LinkedItem.DateRetrieved == entryToUpdate.DateCompleted)
         {
            log.DebugFormat("updating item retrieval date ({0} -> {1})", LinkedItem.DateRetrieved, newDate);
            LinkedItem.DateRetrieved = newDate;
            return true;
         }

         if (ActionUtils.IsShreading(actionCode) &&
             LinkedItem.DateDestroyed.HasValue &&
             LinkedItem.DateDestroyed == entryToUpdate.DateCompleted)
         {
            log.DebugFormat("updating item destroy date ({0} -> {1})", LinkedItem.DateDestroyed, newDate);
            LinkedItem.DateDestroyed = newDate;
            return true;
         }
         return false;
      }
      private void RunInternal()
      {
         CheckDataConstraintAndBusinessRules();

         bool itemNeedsUpdate = false;

         if (newDate != entryToUpdate.DateCompleted)
         {
            itemNeedsUpdate = UpdateItemDateIfRequired();
            log.DebugFormat("updating history entry date ({0} -> {1})", entryToUpdate.DateCompleted, newDate);
            entryToUpdate.DateCompleted = newDate;
         }
         if (newLocation != entryToUpdate.Location)
         {
            log.DebugFormat("updating item location and history entry location ({0} -> {1})", entryToUpdate.Location, newLocation);
            LinkedItem.Location = newLocation;
            entryToUpdate.Location = newLocation;
            itemNeedsUpdate = true;
         }
         if (newCharge != entryToUpdate.Charge)
         {
            log.DebugFormat("updating history entry charge ({0} -> {1})", entryToUpdate.Charge, newCharge);
            entryToUpdate.Charge = newCharge;
         }
         if (newAuthorization!= entryToUpdate.AuthorisedBy)
         {
            log.DebugFormat("updating history entry authorization ({0} -> {1})", entryToUpdate.AuthorisedBy, newAuthorization);
            entryToUpdate.AuthorisedBy = newAuthorization;
         }
         if (newNotes != entryToUpdate.Notes)
         {
            log.DebugFormat("updating history entry notes ({0} -> {1})", entryToUpdate.Notes, newNotes);
            entryToUpdate.Notes = newNotes;
         }

         log.DebugFormat("updating LastAmend date to ({0} -> {1})", entryToUpdate.LastAmended, DateTime.Today);
         entryToUpdate.LastAmended = DateTime.Today;

         if (itemNeedsUpdate)
         {
            dbCtx.ItemDetail.Attach(LinkedItem);
            dbCtx.SetAsModified(LinkedItem);
         }
         dbCtx.ActionHistory.Attach(entryToUpdate);
         dbCtx.SetAsModified(entryToUpdate);

         dbCtx.SaveChanges();

         log.DebugFormat("history entry '{0}' updated succesfully.", entryToUpdate.LogDesc());
      }

      private void CheckDataConstraintAndBusinessRules()
      {
         if(ActionUtils.IsConsignment(actionCode))
         {
            if (newLocation != entryToUpdate.Location)
            {
               if (LinkedItem.DateDestroyed != null || LinkedItem.DateReturned != null || LinkedItem.DateRetrieved != null)
                  throw new BusinessRuleException("changing location of consignment while item had been already destroyed, returned or reconsinged does not make sense!");

               if (new AskDb(dbCtx).LocationOccupied(newLocation))
                  throw new BusinessRuleException("location '{0}' is already occupied and can't be used!");
            }
         }
         else if (ActionUtils.IsReconsignment(actionCode))
         {
            if (newLocation != entryToUpdate.Location)
            {
               if (LinkedItem.DateDestroyed != null || LinkedItem.DateRetrieved != null)
                  throw new BusinessRuleException("changing location of re-consignment while item had been already destroyed, or returned to the client, does not make sense!");

               if (new AskDb(dbCtx).LocationOccupied(newLocation))
                  throw new BusinessRuleException(string.Format("location '{0}' is already occupied and can't be used!", newLocation));

               if (!new AskDb(dbCtx).IsItLatestReconsignment(entryToUpdate))
                  throw new BusinessRuleException(
                     string.Format("changing location of re-consignment when it is not last item '{0}' reconsignment is forbidden!\n" +
                                   "If you want to change item's location, then do it by changing item's last re-consignment action location", 
                                   LinkedItem.ItemNumber));
            }
         }
         else if (newLocation != entryToUpdate.Location)
         {
            throw new BusinessRuleException("changing location of history entry which is not consignment or re-consignment does not make any sense!");
         }

         CheckNewDateContraints();
      }

      private void CheckNewDateContraints()
      {
         if (newDate == entryToUpdate.DateCompleted)
            return;

         if (ActionUtils.IsConsignment(actionCode))
         {
            var otherItemDate = LinkedItem.DateDestroyed ?? (LinkedItem.DateRetrieved ?? LinkedItem.DateReturned);
            if (otherItemDate.HasValue && newDate > otherItemDate)
               throw new BusinessRuleException
                  (string.Format("New consignment date ({0}) must not exceed other Item action (reconsign, retrieve or destroy) date ({1})",
                  DateUtils.ToString(newDate), DateUtils.ToString(otherItemDate)));
         }

         if (ActionUtils.IsReconsignment(actionCode))
         {
            if (newDate < LinkedItem.DateRecieved)
               throw new BusinessRuleException
                  (string.Format("New re-consign date ({0}) must be grater or equal to consignment date ({1})",
                  DateUtils.ToString(newDate), DateUtils.ToString(LinkedItem.DateRecieved)));

            // when after re-consign there have been destruction
            if (LinkedItem.DateDestroyed.HasValue && newDate > LinkedItem.DateDestroyed)
               throw new BusinessRuleException
                  (string.Format("New re-consign date ({0}) must not exceed destruction date ({1})",
                  DateUtils.ToString(newDate), DateUtils.ToString(LinkedItem.DateDestroyed)));

            // when after re-consign there have been retrieval
            if (LinkedItem.DateRetrieved.HasValue && newDate > LinkedItem.DateRetrieved)
               throw new BusinessRuleException
                  (string.Format("New re-consign date ({0}) must not exceed last retrieval date ({1})",
                  DateUtils.ToString(newDate), DateUtils.ToString(LinkedItem.DateRetrieved)));
         }

         if (ActionUtils.IsAnyKindOfRetrieval(actionCode))
         {
            if (newDate < LinkedItem.DateRecieved)
               throw new BusinessRuleException
                  (string.Format("New retrieval date ({0}) must be grater or equal to consignment date ({1})",
                  DateUtils.ToString(newDate), DateUtils.ToString(LinkedItem.DateRecieved)));

            // when after retrieval there have been destruction
            if (LinkedItem.DateDestroyed.HasValue && newDate > LinkedItem.DateDestroyed)
               throw new BusinessRuleException
                  (string.Format("New retrieval date ({0}) must not exceed destruction date ({1})",
                  DateUtils.ToString(newDate), DateUtils.ToString(LinkedItem.DateDestroyed)));

            // when after retrieval there have been re-consignment
            if (LinkedItem.DateReturned.HasValue && newDate > LinkedItem.DateReturned)
               throw new BusinessRuleException
                  (string.Format("New retrieval date ({0}) must not exceed latest re-consign date ({1})",
                  DateUtils.ToString(newDate), DateUtils.ToString(LinkedItem.DateReturned)));
         }

         if (ActionUtils.IsShreading(actionCode))
         {
            if (newDate < LinkedItem.DateRecieved)
               throw new BusinessRuleException
                  (string.Format("New ({0}) date must be grater or equal to consignment date ({1})",
                  DateUtils.ToString(newDate), DateUtils.ToString(LinkedItem.DateRecieved)));
         }
      }

   }
}
