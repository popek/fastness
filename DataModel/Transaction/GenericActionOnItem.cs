﻿using log4net;
using System.Linq;
using System;
using Fastness.Common;
using Fastness.DataModel.Utils;
using Fastness.DataModel.Exceptions;
using System.Data.Entity;


namespace Fastness.DataModel.Transaction
{
   public class GenericActionOnItem
   {
      static readonly ILog log = LogManager.GetLogger(typeof(RetrieveItem));

      FastnessActionContext ctx;
      string notes;

      public GenericActionOnItem(FastnessActionContext transactionCtx, string notes)
      {
         this.ctx = transactionCtx;
         this.notes = notes;
      }

      public void Run()
      {
         try
         {
            RunInternal();
         }
         catch (Exception e)
         {
            log.ErrorFormat("error while trying to '{0}' item({1}): {2}", ctx.CtrAction.Description, ctx.Item.LogDesc(), e);
            throw;
         }
      }

      private void RunInternal()
      {
         log.InfoFormat("trying to '{0}' on item: {0}", ctx.CtrAction.Description, ctx.Item.LogDesc());

         CheckDataConstraintAndBusinessRules();

         UpdateItemHistory();

         ctx.DbCtx.SaveChanges();

         log.InfoFormat("succesfully done '{1}' on item {0}  ", ctx.Item.LogDesc(), ctx.CtrAction.Description);
      }

      private void CheckDataConstraintAndBusinessRules()
      {
         int itemsCount = ctx.DbCtx.ItemDetail.Count(i => i.ItemNumber == ctx.Item.ItemNumber);
         if (itemsCount == 0)
         {
            throw new DataDoesNotExistException(string.Format("item which is '{0}' MUST exist in db! item {1} was not found in db.", ctx.CtrAction.Description, ctx.Item.LogDesc()));
         }
         if (itemsCount > 1)
         {
            throw new DataNotUniqueException(string.Format("trying to '{0}' item({1}) which is not uniqe! this indicates that Db is corrupted!", ctx.CtrAction.Description, ctx.Item.LogDesc()));
         }
         if (!(ctx.Item.InStock ?? true))
         {
            throw new BusinessRuleException(string.Format(
               "InStock flag is not set which indicates item({0}) is out of stock already! '{1}' is possible for items which are still in stock.", ctx.Item.LogDesc(), ctx.CtrAction.Description));
         }
      }

      private void UpdateItemHistory()
      {
         var itemHistoryEntry = TransactionUtils.CreateBasicActionHistoryEntry(ctx);
         itemHistoryEntry.Notes = notes;
         itemHistoryEntry.Location = ctx.Item.Location;
         itemHistoryEntry.Percentage = null;

         log.InfoFormat("adding '{0}' to item actions history: {1}", ctx.CtrAction.Description, itemHistoryEntry.LogDesc());

         ctx.DbCtx.ActionHistory.Add(itemHistoryEntry);
      }
   }
}
