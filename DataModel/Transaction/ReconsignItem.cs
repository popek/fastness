﻿using log4net;
using System.Linq;
using System;
using Fastness.DataModel.Utils;
using Fastness.DataModel.Exceptions;
using System.Data.Entity;


namespace Fastness.DataModel.Transaction
{
   public class ReconsignItem
   {
      static readonly ILog log = LogManager.GetLogger(typeof(ReconsignItem));

      FastnessActionContext ctx;
      string location;

      public ReconsignItem(FastnessActionContext transactionCtx, string location)
      {
         this.ctx = transactionCtx;
         this.location = location;
      }

      public void Run()
      {
         try
         {
            RunInternal();
         }
         catch (Exception e)
         {
            log.ErrorFormat("error while trying to reconsign item({0}): {1}", ctx.Item.LogDesc(), e);
            throw;
         }
      }

      private void RunInternal()
      {
         log.InfoFormat("re-consigning item {0}", ctx.Item.LogDesc());

         CheckDataConstraintAndBusinessRules();

         UpdateItem();

         UpdateItemHisotry();

         UpdateContract();

         ctx.DbCtx.SaveChanges();

         log.InfoFormat("item {0} re-consigned succesfully", ctx.Item.LogDesc());
      }

      private void CheckDataConstraintAndBusinessRules()
      {
         int itemsCount = ctx.DbCtx.ItemDetail.Count(i => i.ItemNumber == ctx.Item.ItemNumber);
         if (itemsCount == 0)
         {
            throw new DataDoesNotExistException(string.Format(
               "item which is re-consigned MUST exist in db! Item {0} was not found in db.", ctx.Item.LogDesc()));
         }
         if (itemsCount > 1)
         {
            throw new DataNotUniqueException(string.Format(
               "trying to re-consign item({0}) which is not uniqe! this indicates that Db is corrupted!", ctx.Item.LogDesc()));
         }
         if (ctx.Item.InStock ?? true)
         {
            throw new BusinessRuleException(string.Format(
               "InStock item({0}) flag is set which indicates that item is already in stock! only items which are out of stock can be re-consigned.", ctx.Item.LogDesc()));
         }
         if (!string.IsNullOrEmpty(ctx.Item.Location))
         {
            throw new BusinessRuleException(string.Format(
               "item({0}) has already location which indicates it is in the stock! only items which are out of stock can be re-consigned.", ctx.Item.LogDesc()));
         }
         if (ctx.Item.DateRetrieved == null)
         {
            throw new BusinessRuleException(string.Format(
               "item({0}) has no retrieval date which indicates it hans't been retrieved to the client yet! only items which were retrieved to client can be re-consigned.", ctx.Item.LogDesc()));
         }
         if (ctx.Item.DateDestroyed != null)
         {
            throw new BusinessRuleException(string.Format(
               "item({0}) has destroy date which indicates it had been destroyed! only existing items can be re-consigned.", ctx.Item.LogDesc()));
         }
      }

      private void UpdateItem()
      {
         log.InfoFormat("item state before reconsignment: {0}", ctx.Item.StateLogDesc());

         ctx.Item.DateReturned = ctx.Date;
         ctx.Item.DateRetrieved = null;
         ctx.Item.Location = location;
         ctx.Item.InStock = true;

         log.InfoFormat("item state after reconsignment: {0}", ctx.Item.StateLogDesc());

         FastnessBusinessRules.ValidateItemCosistency(ctx.Item);

         ctx.DbCtx.ItemDetail.Attach(ctx.Item);
         ctx.DbCtx.SetAsModified(ctx.Item);
      }

      private void UpdateItemHisotry()
      {
         var itemHistoryEntry = TransactionUtils.CreateBasicActionHistoryEntry(ctx);
         itemHistoryEntry.Notes = "";
         itemHistoryEntry.Location = location;
         itemHistoryEntry.Percentage = ctx.Item.Percentage;

         log.InfoFormat("adding re-consign to item actions history: {0}", itemHistoryEntry.LogDesc());

         ctx.DbCtx.ActionHistory.Add(itemHistoryEntry);
      }

      private void UpdateContract()
      {
         log.InfoFormat("contract items count before re-consignment: {0}", ctx.Ctr.ItemsCountLogDesc());

         ctx.Ctr.ReConThisMonth += 1;
         ctx.Ctr.Total += 1;

         log.InfoFormat("contract items count after re-consignment: {0}", ctx.Ctr.ItemsCountLogDesc());

         FastnessBusinessRules.ValidateContractConsistency(ctx.Ctr);

         ctx.DbCtx.Contract.Attach(ctx.Ctr);
         ctx.DbCtx.SetAsModified(ctx.Ctr);
      }
   }
}
