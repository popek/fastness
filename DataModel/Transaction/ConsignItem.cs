﻿using log4net;
using System.Linq;
using System;
using Fastness.DataModel.Utils;
using Fastness.DataModel.Exceptions;
using System.Data.Entity;


namespace Fastness.DataModel.Transaction
{
   public class ConsignItem
   {
      static readonly ILog log = LogManager.GetLogger(typeof(ConsignItem));

      FastnessActionContext ctx;

      public ConsignItem(FastnessActionContext transactionCtx)
      {
         this.ctx = transactionCtx;
      }

      public void Run()
      {
         try
         {
            RunInternal();
         }
         catch (Exception e)
         {
            log.ErrorFormat("error while trying to consign item({0}): {1}", ctx.Item.LogDesc(), e);
            throw;
         }
      }

      private void CheckDataConstraintAndBusinessRules()
      {
         int itemsCount = ctx.DbCtx.ItemDetail.Count(i => i.ItemNumber == ctx.Item.ItemNumber);
         if (itemsCount != 0)
         {
            throw new DataConstraintBrokenException(string.Format(
               "item already found in db! item({0}) which is consigned MUST NOT exist in db.", ctx.Item.LogDesc()));
         }
         int itemHistoryEntriesCount = ctx.DbCtx.ActionHistory.Count(ah => ah.ItemNumber == ctx.Item.ItemNumber);
         if (itemHistoryEntriesCount != 0)
         {
            throw new DataConstraintBrokenException(string.Format(
               "item about to be consigned has already history! item({0}) which is not yet in consigned, MUST NOT have history.", ctx.Item.LogDesc()));
         }

         if (string.IsNullOrEmpty(ctx.Item.ItemNumber))
         {
            throw new BusinessRuleException(string.Format(
               "item has no item number! item({0}) which is being consigned must have valid itemNumber.", ctx.Item.LogDesc()));
         }
         if (string.IsNullOrEmpty(ctx.Item.Location))
         {
            throw new BusinessRuleException(string.Format(
               "item has no location! item({0}) which is being consigned MUST have a proper location.", ctx.Item.LogDesc()));
         }
      }
      
      private void RunInternal()
      {
         log.InfoFormat("trying to consign item: {0}", ctx.Item.LogDesc());

         CheckDataConstraintAndBusinessRules();

         UpdateItem();

         UptadeItemHistory();

         UpdateContract();

         ctx.DbCtx.SaveChanges();

         log.InfoFormat("item({0}) ocnsigned succesfully", ctx.Item.LogDesc());
      }

      private void UpdateItem()
      {
         ctx.Item.DateRecieved = ctx.Date;

         ctx.Item.InStock = true;

         FastnessBusinessRules.ValidateItemCosistency(ctx.Item);

         ctx.DbCtx.ItemDetail.Add(ctx.Item);
      }

      private void UptadeItemHistory()
      {
         var itemHistoryEntry = TransactionUtils.CreateBasicActionHistoryEntry(ctx);
         itemHistoryEntry.Notes = "";
         itemHistoryEntry.Location = ctx.Item.Location;
         itemHistoryEntry.Percentage = ctx.Item.Percentage;

         log.InfoFormat("adding consign to item actions history: {0}", itemHistoryEntry.LogDesc());

         ctx.DbCtx.ActionHistory.Add(itemHistoryEntry);
      }

      private void UpdateContract()
      {
         log.InfoFormat("contract items count before consignment: {0}", ctx.Ctr.ItemsCountLogDesc());

         ctx.Ctr.ConThisMonth += 1;
         ctx.Ctr.Total += 1;

         log.InfoFormat("contract items count after consignment: {0}", ctx.Ctr.ItemsCountLogDesc());

         FastnessBusinessRules.ValidateContractConsistency(ctx.Ctr);

         ctx.DbCtx.Contract.Attach(ctx.Ctr);
         ctx.DbCtx.SetAsModified(ctx.Ctr);
      }
   }
}
