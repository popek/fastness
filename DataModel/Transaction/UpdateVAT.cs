﻿using Fastness.DataModel.Exceptions;
using log4net;
using System;
using System.Linq;

namespace Fastness.DataModel.Transaction
{
   public class UpdateVAT
   {
      static readonly ILog log = LogManager.GetLogger(typeof(UpdateVAT));

      double newValue;
      fastnessEntities dbCtx;

      public UpdateVAT(double newValue, fastnessEntities dbCtx)
      {
         this.newValue = newValue;
         this.dbCtx = dbCtx;
      }

      public void Run()
      {
         try
         {
            RunInternal();
         }
         catch (Exception e)
         {
            log.ErrorFormat("error while trying to update VAT value : {0}", e);
            throw;
         }
      }
      private void RunInternal()
      {
         CheckDataConstraintAndBusinessRules();

         var miscParams = dbCtx.MiscParams.First();

         log.DebugFormat("updating VAT ({0} -> {1})", miscParams.VAT, newValue);

         miscParams.VAT = newValue;

         dbCtx.MiscParams.Attach(miscParams);
         dbCtx.SetAsModified(miscParams);
         dbCtx.SaveChanges();
      }

      private void CheckDataConstraintAndBusinessRules()
      {
         if (dbCtx.MiscParams.Count() != 1)
         {
            throw new BusinessRuleException("miscelaneous params table (in which VAT is) must contains only 1 record!");
         }
         if (newValue > 1)
         {
            throw new BusinessRuleException("new VAT value is grater then 100% which is not logical!");
         }
      }
   }
}
