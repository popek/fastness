﻿using Fastness.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fastness.DataModel.Transaction
{
   internal class TransactionUtils
   {
      public static ActionHistory CreateBasicActionHistoryEntry(FastnessActionContext transactionCtx)
      {
         var ah = new ActionHistory();
         
         ah.ItemNumber = transactionCtx.Item != null ? 
            transactionCtx.Item.ItemNumber : 
            ActionUtils.ITEM_NUMBER_FOR_NO_ITEM_ACTION_HITORY;

         ah.Description = transactionCtx.CtrAction.Description;
         ah.Charge = transactionCtx.Charge;
         ah.Date = DateTime.Today;
         ah.DateCompleted = transactionCtx.Date;
         ah.AuthorisedBy = transactionCtx.Author;
         ah.ContractActionID = transactionCtx.CtrAction.ContractActionID;
         return ah;
      }
   }
}
