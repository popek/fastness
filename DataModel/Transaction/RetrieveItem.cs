﻿using log4net;
using System.Linq;
using System;
using Fastness.DataModel.Utils;
using Fastness.DataModel.Exceptions;
using System.Data.Entity;


namespace Fastness.DataModel.Transaction
{
   public class RetrieveItem
   {
      static readonly ILog log = LogManager.GetLogger(typeof(RetrieveItem));

      FastnessActionContext ctx;
      string notes;
      
      public RetrieveItem(FastnessActionContext transactionCtx, string notes)
      {
         this.ctx = transactionCtx;
         this.notes = notes;
      }

      public void Run()
      {
         try
         {
            RunInternal();
         }
         catch (Exception e)
         {
            log.ErrorFormat("error while trying to '{0}' item({1}): {2}", ctx.CtrAction.Description, ctx.Item.LogDesc(), e);
            throw;
         }
      }

      private void RunInternal()
      {
         log.InfoFormat("trying to '{0}' item {1}", ctx.CtrAction.Description, ctx.Item.LogDesc());

         CheckDataConstraintAndBusinessRules();

         UpdateItemHistory();

         UpdateItem();

         UpdateContract();

         ctx.DbCtx.SaveChanges();

         log.InfoFormat("item {0} '{1}' succesfully", ctx.Item.LogDesc(), ctx.CtrAction.Description);
      }

      private void CheckDataConstraintAndBusinessRules()
      {
         int itemsCount = ctx.DbCtx.ItemDetail.Count(i => i.ItemNumber == ctx.Item.ItemNumber);
         if (itemsCount == 0)
         {
            throw new DataDoesNotExistException(string.Format("item which is retireved MUST exist in db! item {0} was not found in db.", ctx.Item.LogDesc()));
         }
         if (itemsCount > 1)
         {
            throw new DataNotUniqueException(string.Format("trying to retrieve item({0}) which is not uniqe! this indicates that Db is corrupted!", ctx.Item.LogDesc()));
         }
         if (!(ctx.Item.InStock ?? true))
         {
            throw new BusinessRuleException(string.Format(
               "InStock flag is not set which indicates item({0}) is out of stock already! Retrieving is possible for items which are still in stock.", ctx.Item.LogDesc()));
         }
         if (ctx.Item.DateRetrieved != null)
         {
            throw new BusinessRuleException(string.Format(
               "date retrieved is already set ({0}) which indicates item({1}) had bee retrieved already!", ctx.Item.DateRetrieved, ctx.Item.LogDesc()));
         }
         if (ctx.Item.DateDestroyed != null)
         {
            throw new BusinessRuleException(string.Format(
               "item({0}) has destroy date which indicates it had been destroyed! only existing items can be retrieved.", ctx.Item.LogDesc()));
         }
      }

      private void UpdateItem()
      {
         log.InfoFormat("item state before '{0}': {1}", ctx.CtrAction.Description, ctx.Item.StateLogDesc());

         ctx.Item.DateRetrieved = ctx.Date;
         ctx.Item.DateReturned = null;
         ctx.Item.Location = "";
         ctx.Item.InStock = false;

         log.InfoFormat("item state after '{0}': {1}", ctx.CtrAction.Description, ctx.Item.StateLogDesc());

         FastnessBusinessRules.ValidateItemCosistency(ctx.Item);

         ctx.DbCtx.ItemDetail.Attach(ctx.Item);
         ctx.DbCtx.SetAsModified(ctx.Item);
      }

      private void UpdateContract()
      {
         log.InfoFormat("contract items count before '{0}': {1}", ctx.CtrAction.Description, ctx.Ctr.ItemsCountLogDesc());

         ctx.Ctr.ReturnedThisMonth += 1;
         ctx.Ctr.Total -= 1;

         log.InfoFormat("contract items count after '{0}': {1}", ctx.CtrAction.Description, ctx.Ctr.ItemsCountLogDesc());

         FastnessBusinessRules.ValidateContractConsistency(ctx.Ctr);

         ctx.DbCtx.Contract.Attach(ctx.Ctr);
         ctx.DbCtx.SetAsModified(ctx.Ctr);
      }

      private void UpdateItemHistory()
      {
         var itemHistoryEntry = TransactionUtils.CreateBasicActionHistoryEntry(ctx);
         itemHistoryEntry.Notes = notes;
         itemHistoryEntry.Location = ctx.Item.Location;
         itemHistoryEntry.Percentage = ctx.Item.Percentage;

         log.InfoFormat("adding '{0}' to item actions history: {1}", ctx.CtrAction.Description, itemHistoryEntry.LogDesc());

         ctx.DbCtx.ActionHistory.Add(itemHistoryEntry);
      }
   }
}
