﻿using log4net;
using System;
using System.Linq;
using Fastness.DataModel.Utils;
using System.Data.Entity;
using Fastness.DataModel.Exceptions;
using Fastness.Common;

namespace Fastness.DataModel.Transaction
{
   public class UpdateContract
   {
      static readonly ILog log = LogManager.GetLogger(typeof(UpdateContract));

      Contract ctr;
      fastnessEntities dbCtx;

      public UpdateContract(Contract ctr, fastnessEntities dbCtx)
      {
         this.ctr = ctr;
         this.dbCtx = dbCtx;
      }

      public void Run()
      {
         try
         {
            RunInternal();
         }
         catch (Exception e)
         {
            log.ErrorFormat("error while trying to add update existing contract: {0}", e);
            throw;
         }
      }

      private void RunInternal()
      {
         log.InfoFormat("updating existing contract: {0}. Service Level: {1}", ctr.LogDesc(), ctr.ServiceLevel);

         FastnessBusinessRules.ValidateContractConsistency(ctr);

         UpdateContractActionsIfServiceLevelChanged();

         dbCtx.Contract.Attach(ctr);
         dbCtx.SetAsModified(ctr);

         dbCtx.SaveChanges();
      }

      private void UpdateContractActionsIfServiceLevelChanged()
      {
         var contractActions =
            from ca in dbCtx.ContractAction
            where ca.ContractID == ctr.ContractID
            select ca;
         if (contractActions.Count() == 0)
         {
            throw new DataConstraintBrokenException(
               string.Format(
                  "there is no contract actions at all which indicates DB consistency error"));
         }
         if (ctr.ServiceLevel == ActionUtils.GetServiceLevelFromActonCode(contractActions.First().Code))
         {
            log.DebugFormat("Service level for contract {0} has not been changed. Updating contract actions will be skipped.", ctr.LogDesc());
            return;
         }

         var caIdMin = contractActions.Min(ca => ca.ContractActionID);
         var caIdMax = contractActions.Max(ca => ca.ContractActionID);

         var ctrHistory =
            from ah in dbCtx.ActionHistory
            where ah.ContractActionID >= caIdMin && ah.ContractActionID <= caIdMax
            select ah;
         if(ctrHistory.Count() > 0)
         {
            throw new BusinessRuleException(string.Format(
@"contract {0}, already has some actions in history. 
Servie level can be changed only for contracts which does not have any history.
The only option left now to change service level is to finish contract and add a new one", ctr.LogDesc()));
         }

         dbCtx.ContractAction.RemoveRange(contractActions);

         AddContract.AttachContractActions(dbCtx, ctr);
      }
   }
}
