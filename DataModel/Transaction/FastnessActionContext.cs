﻿using System;
using Fastness.DataModel.Utils;
using System.Linq;

namespace Fastness.DataModel.Transaction
{
   public class FastnessActionContext
   {
      public fastnessEntities DbCtx { get; private set; }

      public ItemDetail Item { get; private set; }
      public Contract Ctr { get; private set; }
      public ContractAction CtrAction { get; private set; }
      
      public DateTime Date { get; private set; }
      public double Charge { get; private set; }
      public string Author { get; private set; }

      public FastnessActionContext(ItemDetail item, Contract ctr, ContractAction ctrAction, DateTime date, double charge, string author)
      {
         Item = item;
         Ctr = ctr;
         CtrAction = ctrAction;
         Date = date;
         Charge = charge;
         Author = author;
      }
      public FastnessActionContext(Contract ctr, ContractAction ctrAction, DateTime date, double charge, string author)
         : this(null, ctr, ctrAction, date, charge, author)
      {
      }

      public void AttachDbContext(fastnessEntities dbctx)
      {
         DbCtx = dbctx;
      }
      public void AttachDbContextAndRefresh(fastnessEntities dbctx)
      {
         AttachDbContext(dbctx);

         // refresh entities
         // it can be context for consignment (then Item is not yet in db)
         // or for non-item action
         if (Item != null)
         {
            var query = DbCtx.ItemDetail.Where(i => i.ItemID == Item.ItemID);
            if (query.Count() > 0)
               Item = query.First();
         }

         Ctr = DbCtx.Contract.Where(c => c.ContractID == Ctr.ContractID).First();
      }
   }
}
