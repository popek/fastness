﻿using log4net;
using System.Linq;
using System;
using Fastness.Common;
using Fastness.DataModel.Utils;
using Fastness.DataModel.Exceptions;
using System.Data.Entity;


namespace Fastness.DataModel.Transaction
{
   public class GenericActionWithoutItem
   {
      static readonly ILog log = LogManager.GetLogger(typeof(RetrieveItem));

      FastnessActionContext ctx;
      string notes;
      
      public GenericActionWithoutItem(FastnessActionContext transactionCtx, string notes)
      {
         this.ctx = transactionCtx;
         this.notes = notes;
      }

      public void Run()
      {
         try
         {
            RunInternal();
         }
         catch (Exception e)
         {
            log.ErrorFormat("error while trying to '{0}': {1}", ctx.CtrAction.Description, e);
            throw;
         }
      }

      private void RunInternal()
      {
         log.InfoFormat("trying to '{0}'", ctx.CtrAction.Description);

         var historyEntry = TransactionUtils.CreateBasicActionHistoryEntry(ctx);
         historyEntry.Notes = notes;
         historyEntry.Location = "";
         historyEntry.Percentage = null;

         log.InfoFormat("adding '{0}' to actions history: {1}", ctx.CtrAction.Description, historyEntry.LogDesc());

         ctx.DbCtx.ActionHistory.Add(historyEntry);

         ctx.DbCtx.SaveChanges();

         log.InfoFormat("'{0}' actoion done succesfully", ctx.CtrAction.Description);
      }
   }
}
