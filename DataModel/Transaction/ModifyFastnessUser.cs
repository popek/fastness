﻿using log4net;
using System;
using System.Linq;
using Fastness.DataModel.Exceptions;

namespace Fastness.DataModel.Transaction
{
   public class ModifyFastnessUser
   {
      static readonly ILog log = LogManager.GetLogger(typeof(ModifyFastnessUser));

      public enum Context
      { 
         ADD_NEW_USER,
         ATTACH_EXISTING_USER,
         REMOVE_USER
      };

      Context context;
      Fastness.DataModel.User user;
      fastnessEntities dbCtx;

      public ModifyFastnessUser(Context context, Fastness.DataModel.User user, fastnessEntities dbCtx)
      {
         this.context = context;
         this.user = user;
         this.dbCtx = dbCtx;
      }

      public void Run()
      {
         try
         {
            RunInternal();
         }
         catch (Exception e)
         {
            log.ErrorFormat("error while trying to '{0}' fastness user: {1}", context.ToString(), e);
            throw;
         }
      }
      private void RunInternal()
      {
         switch (context)
         { 
            case Context.ADD_NEW_USER:
               log.DebugFormat("adding new fastness user {0}", user.Login);
               if(dbCtx.User.Count(u => u.Login == user.Login) > 0)
               {
                  throw new DataConstraintBrokenException(
                     string.Format("user '{0}' already exists! Adding the same user is forbidden",
                     user.Login));
               }
               dbCtx.User.Add(user);
               break;
            case Context.REMOVE_USER:
               log.DebugFormat("removing existing fastness user {0}", user.Login);
               if (dbCtx.User.Count(u => u.Login == user.Login) == 0)
               {
                  throw new DataConstraintBrokenException(
                     string.Format("user '{0}' does not exists! Removing not existing user not possible",
                     user.Login));
               }
               dbCtx.User.Remove(user);
               break;
            case Context.ATTACH_EXISTING_USER:
               log.DebugFormat("attaching existing user {0} after change", user.Login);
               dbCtx.User.Attach(user);
               dbCtx.SetAsModified(user);
               break;
         }
         dbCtx.SaveChanges();
      }
   }
}
