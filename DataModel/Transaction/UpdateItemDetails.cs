﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fastness.DataModel.Utils;
using Fastness.DataModel.Exceptions;

namespace Fastness.DataModel.Transaction
{
   public class UpdateItemDetails
   {
      static readonly ILog log = LogManager.GetLogger(typeof(UpdateItemDetails));

      ItemDetail itemToUpdate;
      int newBoxPercentage;
      string newDestruction; 
      string newDescription;
      string newCuid;
      fastnessEntities dbCtx;

      public UpdateItemDetails(
         ItemDetail itemToUpdate, 
         int newBoxPercentage, 
         string newDestruction, 
         string newDescription, 
         string newCuid, 
         fastnessEntities dbCtx)
      {
         this.itemToUpdate = itemToUpdate;
         this.newBoxPercentage = newBoxPercentage;
         this.newDestruction = newDestruction;
         this.newDescription = newDescription;
         this.newCuid = newCuid;
         this.dbCtx = dbCtx;
      }
      public void Run()
      {
         try
         {
            RunInternal();
         }
         catch (Exception e)
         {
            log.ErrorFormat("error while trying to update item ({0}) details : {1}", itemToUpdate.LogDesc(), e);
            throw;
         }
      }

      private void RunInternal()
      {
         CheckDataConstraintAndBusinessRules();

         if(itemToUpdate.Description != newDescription)
         {
            log.DebugFormat("updating item description ({0} -> {1})", itemToUpdate.Description, newDescription);
            itemToUpdate.Description = newDescription;
         }
         if(itemToUpdate.ClientItemRef != newCuid)
         {
            log.DebugFormat("updating item cuid ({0} -> {1})", itemToUpdate.ClientItemRef, newCuid);
            itemToUpdate.ClientItemRef = newCuid;
         }
         if(itemToUpdate.DestructionDate != newDestruction)
         {
            log.DebugFormat("updating item destruction date ({0} -> {1})", itemToUpdate.DestructionDate, newDestruction);
            itemToUpdate.DestructionDate = newDestruction;
         }
         if(itemToUpdate.Percentage != newBoxPercentage)
         {
            log.DebugFormat("changing box percentage: {0} -> {1}", itemToUpdate.Percentage, newBoxPercentage);
            itemToUpdate.Percentage = newBoxPercentage;

            var query = from ah in dbCtx.ActionHistory
                        where ah.ItemNumber == itemToUpdate.ItemNumber && (ah.Percentage??100) > 0
                        select ah;

            foreach (var action in query)
            {
               log.DebugFormat("box action '{0}' percentage changed: {1} -> {2}", action.LogDesc(), action.Percentage, newBoxPercentage);

               action.Percentage = newBoxPercentage;
               action.LastAmended = DateTime.Today;

               dbCtx.ActionHistory.Attach(action);
               dbCtx.SetAsModified(action);
            }
         }

         dbCtx.ItemDetail.Attach(itemToUpdate);
         dbCtx.SetAsModified(itemToUpdate);

         dbCtx.SaveChanges();
      }

      private void CheckDataConstraintAndBusinessRules()
      {
         int itemsCount = dbCtx.ItemDetail.Count(i => i.ItemNumber == itemToUpdate.ItemNumber);
         if (itemsCount == 0)
         {
            throw new DataDoesNotExistException(string.Format(
               "item was not found in db! item({0}) which is being updated MUST exist in db.", itemToUpdate.LogDesc()));
         }
         if (string.IsNullOrEmpty(itemToUpdate.ItemNumber))
         {
            throw new BusinessRuleException(string.Format(
               "item has no item number! item({0}) which is being consigned must have valid itemNumber.", itemToUpdate.LogDesc()));
         }
      }


   }
}
