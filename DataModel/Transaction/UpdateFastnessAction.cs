﻿using log4net;
using System;
using Fastness.DataModel.Utils;
using Fastness.DataModel.Exceptions;
using System.Linq;

namespace Fastness.DataModel.Transaction
{
   public class UpdateFastnessAction
   {
      public enum Context
      { 
         UPDATE_SINGLE_CONTRACT_ACTION,
         UPDATE_SERVICE_ACTION,
         UPDATE_SERVICE_ACTION_AND_ALL_CONTRACT_ACTIONS
      }

      static readonly ILog log = LogManager.GetLogger(typeof(UpdateFastnessAction));

      ContractAction ctrAction;
      double newCharge;
      double newPercentsFree;
      Context ctx;
      fastnessEntities dbCtx;

      public UpdateFastnessAction(ContractAction ctrAction, double newCharge, double newPercentsFree, Context ctx, fastnessEntities dbCtx)
      {
         this.ctrAction = ctrAction;
         this.newCharge = newCharge;
         this.newPercentsFree = newPercentsFree;
         this.ctx = ctx;
         this.dbCtx = dbCtx;
      }

      public void Run()
      {
         try
         {
            RunInternal();
         }
         catch (Exception e)
         {
            log.ErrorFormat("error while trying to add update contract action({0}): {1}", ctrAction.LogDesc(), e);
            throw;
         }
      }
      private void RunInternal()
      {
         CheckDataConstraintAndBusinessRules();

         log.InfoFormat("charge: {0}->{1}, percents free: {2}->{3}", ctrAction.Charge, newCharge, ctrAction.PerFreeAction, newPercentsFree);

         switch (ctx)
         { 
            case Context.UPDATE_SINGLE_CONTRACT_ACTION:
               UpdateSingleContractAction();
               break;
            case Context.UPDATE_SERVICE_ACTION:
               UpdateActionService();
               break;
            case Context.UPDATE_SERVICE_ACTION_AND_ALL_CONTRACT_ACTIONS:
               UpdateActionServiceAndAllCurrentContracts();
               break;
         }

         dbCtx.SaveChanges();
      }

      private void UpdateSingleContractAction()
      {
         log.InfoFormat("updating single contract action ({0})", ctrAction.LogDesc());

         ctrAction.Charge = newCharge;
         ctrAction.PerFreeAction = newPercentsFree;

         dbCtx.ContractAction.Attach(ctrAction);
         dbCtx.SetAsModified(ctrAction);
      }
      private void UpdateActionService()
      {
         log.InfoFormat("updating '{0}' action in service", ctrAction.Code);

         var query = from ta in dbCtx.ActionTemplate 
                     where ta.Code == ctrAction.Code 
                     select ta;

         var actionTemplate = query.ToList<ActionTemplate>();
         if (actionTemplate.Count > 1)
         {
            throw new DataNotUniqueException(string.Format("template action '{0}' is not unique ({1} entities), which indicates database consistency error!", ctrAction.Code, actionTemplate.Count));
         }
         else if (actionTemplate.Count == 0)
         {
            throw new DataDoesNotExistException(string.Format("template action '{0}' does not exist, which indicates database consistency error!", ctrAction.Code));
         }

         log.InfoFormat("template action which is being updated: {0}", actionTemplate[0].LogDesc());

         actionTemplate[0].Charge = newCharge;
         actionTemplate[0].PerFreeAction = newPercentsFree;

         dbCtx.ActionTemplate.Attach(actionTemplate[0]);
         dbCtx.SetAsModified(actionTemplate[0]);
      }
      private void UpdateActionServiceAndAllCurrentContracts()
      {
         UpdateActionService();

         log.InfoFormat("updating '{0}' action in current contracts", ctrAction.Code);

         var query = from ca in dbCtx.ContractAction
                     where ca.Code == ctrAction.Code
                     select ca;

         var allActions = query.ToList<ContractAction>();

         log.InfoFormat("ctr. actions to update count: {0}", allActions.Count);

         foreach (ContractAction ca in allActions)
         {
            log.InfoFormat("ctr. action being updated: {0}", ca.LogDesc());

            ca.Charge = newCharge;
            ca.PerFreeAction = newPercentsFree;

            dbCtx.ContractAction.Attach(ca);
            dbCtx.SetAsModified(ca);
         }
      }
      private void CheckDataConstraintAndBusinessRules()
      {
         if (newPercentsFree > 100)
         {
            throw new BusinessRuleException(string.Format("trying to set {0} (>100%) percents free for action {1}", newPercentsFree, ctrAction.LogDesc()));
         }
      }
   }
}
