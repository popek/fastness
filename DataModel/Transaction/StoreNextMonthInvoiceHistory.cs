﻿using log4net;
using System;
using Fastness.DataModel.Utils;
using System.Collections.Generic;
using System.Text;
using Fastness.DataModel.Exceptions;
using System.Linq;
using Fastness.Common;

namespace Fastness.DataModel.Transaction
{
   public class StoreNextMonthInvoiceHistory
   {
      static readonly ILog log = LogManager.GetLogger(typeof(StoreNextMonthInvoiceHistory));

      List<InvoiceHistory> entries;
      fastnessEntities dbCtx;

      public StoreNextMonthInvoiceHistory(List<InvoiceHistory> entries, fastnessEntities dbCtx)
      {
         this.entries = entries;
         this.dbCtx = dbCtx;
      }

      public void Run()
      {
         try
         {
            RunInternal();
         }
         catch (Exception e)
         {
            log.ErrorFormat("error while trying to add invoice history entry: {0}", e);
            throw;
         }
      }
      private void RunInternal()
      {
         CheckDataConstraintAndBusinessRules();

         AddInvoiceHistoryEntries();

         UpdateInvoiceNumberInInvoicedActions();

         UpdateStatsInContracts();

         UpdateNextMonthInvoiceNo();

         dbCtx.SaveChanges();
      }

      private void AddInvoiceHistoryEntries()
      {
         var sb = new StringBuilder();
         foreach (var entry in entries)
         {
            sb.AppendLine(entry.LogDesc());
         }
         log.DebugFormat("add new invoice history entries (amount {0}):\n {1}", entries.Count, sb.ToString());

         dbCtx.InvoiceHistory.AddRange(entries);
      }

      private void UpdateInvoiceNumberInInvoicedActions()
      {
         // turn off auto detect changes and call it explicitlly (it reduce processing time significantly)
         try
         {
            dbCtx.Configuration.AutoDetectChangesEnabled = false;

            foreach (var entry in entries)
            {
               var first = new DateTime(entry.Year, entry.Month, 1);
               var last = new DateTime(entry.Year, entry.Month, DateTime.DaysInMonth(entry.Year, entry.Month));

               log.DebugFormat("setting invoice no. for actions history on contract {0}", entry.ContractID);

               // actions are taken the same way as in invoice generator
               var ctrHistory = from ah in dbCtx.ActionHistory
                                join ca in dbCtx.ContractAction on ah.ContractActionID equals ca.ContractActionID
                                where ah.DateCompleted >= first && ah.DateCompleted <= last && ca.ContractID == entry.ContractID
                                select ah;

               foreach (var ah in ctrHistory)
               {
                  if ((ah.InvoiceNumber??0) != 0)
                  {
                     throw new DataConstraintBrokenException(string.Format(
                        "action {0} has already assign invoice no. ({1}) which means it was already invoiced!", 
                        ah.LogDesc(), ah.InvoiceNumber));
                  }

                  ah.InvoiceNumber = entry.InvoiceNo;

                  dbCtx.ActionHistory.Attach(ah);
                  dbCtx.SetAsModified(ah);
               }
               log.DebugFormat("invoice no. set in {0} actions", ctrHistory.Count());
            }
         }
         finally
         {
            dbCtx.ForceDetectChanges();
            dbCtx.Configuration.AutoDetectChangesEnabled = true;
         }
      }

      void UpdateStatsInContracts()
      {
         foreach (var ctr in new FetchFromDb(dbCtx).AvailableContracts())
         {
            ctr.ConLastMonth = ctr.Total;
            ctr.ConThisMonth = 0;
            ctr.ReturnedThisMonth = 0;
            ctr.ReConThisMonth = 0;
            ctr.DestroyedThisMonth = 0;

            dbCtx.SetAsModified(ctr);
         }
         log.Debug("all available contracts items statistics had been reseted");
      }

      void UpdateNextMonthInvoiceNo()
      {
         var miscParams = dbCtx.MiscParams.First();

         var newNextMonthInvoice = miscParams.NextInvoiceNo + entries.Count;

         log.DebugFormat("updating next invoice index ({0} -> {1})", miscParams.NextInvoiceNo, newNextMonthInvoice);

         miscParams.NextInvoiceNo = newNextMonthInvoice;

         dbCtx.MiscParams.Attach(miscParams);
         dbCtx.SetAsModified(miscParams);
      }

      void CheckDataConstraintAndBusinessRules()
      { 
         if(entries.Count == 0)
         {
            throw new BusinessRuleException("there are not invoice history entries which indicates some error!");
         }

         int month = entries[0].Month;
         int year = entries[0].Year;

         foreach (var entry in entries)
         {
            if (entry.Month != month || entry.Year != year)
            {
               throw new BusinessRuleException("invoice history entries for one month MSUT have the same month and year. Different month or year amont entries indicates an error");
            }
         }

         if (new AskDb(dbCtx).AreInvoicesAlreadyGenerated(year, month))
         {
            throw new BusinessRuleException(string.Format("invoice history for {0}/{1} already exist in DB!", year, month));
         }

         var existingContractsCount = new FetchFromDb(dbCtx).AvailableContracts().Count();
         if (entries.Count != existingContractsCount)
         {
            throw new BusinessRuleException(string.Format("invoice history entries count is different then existing contracts count! ({0} != {1})",
               entries.Count, existingContractsCount));
         }

         if (dbCtx.MiscParams.Count() != 1)
         {
            throw new BusinessRuleException("miscelaneous params table (in which VAT is) must contains only 1 record!");
         }
      }
   }
}
