﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fastness.DataModel.Exceptions
{
   public class BusinessRuleException : Exception
   {
      public BusinessRuleException(string msg)
         : base(msg)
      {}
   }
}
