﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fastness.Common;
using System.Diagnostics;

namespace CommonUT
{
   [TestClass]
   public class RetryUtilsUT
   {
      [TestMethod]
      public void test_retry_utils_when_all_attempts_fails()
      {
         int retriesCount = 0;
         var failing_task = new Action(() => 
         { 
            ++retriesCount; 
            throw new Exception(); 
         });

         var sleepBetweenTries = 40;
         var maxTries = 5;

         var sw = new Stopwatch();
         try
         {
            sw.Start();
            RetryUtils.RetryIfException(failing_task, sleepBetweenTries, maxTries);
            sw.Stop();

            Assert.Fail("RetryIfExcetpion should throw exception since all attempts failed");
         }
         catch (Exception)
         { 
         }

         var actualTime = (double)((maxTries-1) * sleepBetweenTries);
         var maxTimeError = .02*actualTime;

         Assert.AreEqual(retriesCount, maxTries, "Retries count is not max tries");

         Assert.AreEqual(sw.Elapsed.TotalMilliseconds, actualTime, maxTimeError, "Elapsed time is not near to exptected time");  
      }

      [TestMethod]
      public void test_retry_utils_when_attempt_succed()
      {
         int attemptWhichSucced = 8;
         int retriesCount = 0;
         var taskSuccededOnSomeAttempt = new Action(() => 
         { 
            ++retriesCount;

            if (retriesCount == attemptWhichSucced)
               return;

            throw new Exception(); 
         });

         RetryUtils.RetryIfException(taskSuccededOnSomeAttempt, 33, 2*attemptWhichSucced);

         Assert.AreEqual(retriesCount, attemptWhichSucced, "Expected attempt did not succed");
      }
   }
}
