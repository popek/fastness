﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fastness.Common;

namespace CommonUT
{
   class TestHelper
   {
      public string SomePropertyBlah { get; set; }
      public string SomeOtherPropertyBlah { get; set; }
   }

   [TestClass]
   public class ReflectionUtilsUT
   {
      [TestMethod]
      public void test_GetPropertyName_using_existing_property_owner_instance()
      {
         TestHelper propertyOwnerInstance = new TestHelper();

         var actual = ReflectionUtils.GetPropertyName(() => propertyOwnerInstance.SomePropertyBlah);

         Assert.AreEqual("SomePropertyBlah", actual);
      }
      [TestMethod]
      public void test_GetPropertyName_without_existing_instance()
      {
         var actual = ReflectionUtils.GetPropertyName((TestHelper nonExistingObject) => nonExistingObject.SomeOtherPropertyBlah);

         Assert.AreEqual("SomeOtherPropertyBlah", actual);
      }
   }
}
