﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fastness.Common;

namespace CommonUT
{
   [TestClass]
   public class PasswordUtilsUT
   {
      [TestMethod]
      public void test_input_match_password()
      {
         string password = "SomeDummyPassword2!4#";
         var passwordHash = PasswordUtils.CalculateHash(password);

         string inputMatchingPassword = password;

         Assert.IsTrue(PasswordUtils.IsPasswordOk(inputMatchingPassword, passwordHash));
      }

      [TestMethod]
      public void test_input_does_not_match_password()
      {
         string password = "SomeDummyPassword2!4#";
         var passwordHash = PasswordUtils.CalculateHash(password);

         string inputNotMatchingPassword = "SomeDumyPassword2!4#";

         Assert.IsFalse(PasswordUtils.IsPasswordOk(inputNotMatchingPassword, passwordHash));
      }
   }
}
