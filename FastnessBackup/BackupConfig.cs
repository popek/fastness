﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FastnessBackup
{
   [XmlRoot("BackupConfig")]
   public class BackupConfig
   {
      [XmlAttribute("DBSourceDir")]
      public string DBSourceDir;

      [XmlAttribute("MdfFileName")]
      public string MdfFileName;

      [XmlAttribute("LdfFileName")]
      public string LdfFileName;

      [XmlAttribute("TargetDir")]
      public string TargetDir { get; set; }

      [XmlAttribute("DaysToExpire")]
      public int DaysToExpire;

      [XmlArray("MailAddresses")]
      public string[] FilureNotificationMailAddresses;

      public static void ValidateAndInitTargetDir(BackupConfig config)
      {
         if(!Directory.Exists(config.DBSourceDir))
         {
            throw new InvalidOperationException(
               string.Format("DB source directory ({0}) does not exist", config.DBSourceDir));
         }
         if(!File.Exists(Path.Combine(config.DBSourceDir, config.MdfFileName)))
         {
            throw new InvalidOperationException(
               string.Format("DB data file ({0}) does not exist", config.MdfFileName));
         }
         if (!File.Exists(Path.Combine(config.DBSourceDir, config.LdfFileName)))
         {
            throw new InvalidOperationException(
               string.Format("DB log file ({0}) does not exist", config.LdfFileName));
         }
         if (!Directory.Exists(config.TargetDir))
         {
            Directory.CreateDirectory(config.TargetDir);
         }
      }
   }
}
