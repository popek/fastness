﻿using System;
using System.IO;
using System.IO.Compression;
using Fastness.Common;
using log4net;

namespace FastnessBackup
{
   class Program
   {
      static readonly ILog log = LogManager.GetLogger(typeof(Program));

      private static readonly string REPOSITORY_SUBDIR = "configs";

      static void Main(string[] args)
      {
         log4net.Config.XmlConfigurator.Configure();

         try
         {
            var repository = new XmlObjectsRepository(CurrentDirUtils.GetSubdirPath(REPOSITORY_SUBDIR));
            if (!repository.Exists<BackupConfig>())
            {
               throw new InvalidOperationException("Missing backup config object");
            }

            var config = repository.Get<BackupConfig>();
            BackupConfig.ValidateAndInitTargetDir(config);

            RunBackup(config);

            RemoveOldBackups(config);
         }
         catch (Exception e)
         {
            NotifyBackupFailure(e);
         }
      }

      public static void RunBackup(BackupConfig config)
      {
         log.Debug("starting backup procedure");

         var targetZipFilename = string.Format("fastness_backup_{0}_{1}.zip",
               DateTime.Now.ToShortDateString(),
               DateTime.Now.ToShortTimeString());
         foreach (var c in Path.GetInvalidFileNameChars())
         {
            targetZipFilename = targetZipFilename.Replace(c, '_');
         }
         Compress(config, Path.Combine(config.TargetDir, targetZipFilename));
      }
      public static void Compress(BackupConfig config, string targetZipFileName)
      {
         log.DebugFormat("preparing zip package: {0}", targetZipFileName);

         string[] files = 
         {
            Path.Combine(config.DBSourceDir, config.MdfFileName), 
            Path.Combine(config.DBSourceDir, config.LdfFileName) 
         };
         using (var zip = ZipFile.Open(targetZipFileName, ZipArchiveMode.Create))
         {
            foreach (var file in files)
            {
               log.DebugFormat("adding: {0} to zip package", file);

               zip.CreateEntryFromFile(
                  file, 
                  Path.GetFileName(file), 
                  CompressionLevel.Optimal);

               log.DebugFormat("file {0} added to zip package succesfully", file);
            }
         }
         log.DebugFormat("zip package created succesfully");
      }

      public static void NotifyBackupFailure(Exception reason)
      {
         var mailSubject = "Fastness DB backup failure";
         string[] failureMailRecepients = null;
         SmtpClientSettings smtpSettings = null;
         try
         {
            var repository = new XmlObjectsRepository(CurrentDirUtils.GetSubdirPath(REPOSITORY_SUBDIR));
            if (repository.Exists<SmtpClientSettings>())
            {
               smtpSettings = repository.Get<SmtpClientSettings>();
            }
            else
            {
               log.ErrorFormat(
                  "smtp client file does not exist. Mail notifying backup failure won't be send.\nFailure reason: {0}",
                  reason.Message);
               return;
            }

            failureMailRecepients = repository.Get<BackupConfig>().FilureNotificationMailAddresses;
         }
         catch (Exception)
         {
            mailSubject += " (send only to me)";
            failureMailRecepients = new string[] { "marwrobel@gmail.com" };
         }
         if (failureMailRecepients == null && failureMailRecepients.Length == 0)
         {
            mailSubject += " (send only to me)";
            failureMailRecepients = new string[] { "marwrobel@gmail.com" };
         }

         try
         {
            var mailBody = string.Format(
@"This is automatic notification that fastness DB backup has failed. Reason:

{0}

please do not reply to this e-mail.", reason.Message);

            log.DebugFormat("trying to send mail.\nSubject: {0}\nbody:{1}", mailSubject, mailBody);

            using (var sender = new MailSender(smtpSettings))
            {
               sender.SendMail(
                  "fastntester@gmail.com",
                  new string[] { "fastntester@gmail.com" },
                  failureMailRecepients,
                  mailSubject,
                  mailBody,
                  null);
            }
         }
         catch (Exception mailSendError)
         {
            log.ErrorFormat(
               "sending mail failed. Reason:\n {0}.\nBackup failure notification is not sent. Backup failure reason is:\n {1}",
               mailSendError.Message, reason.Message);
         }
      }

      public static void RemoveOldBackups(BackupConfig config)
      {
         log.Debug("removing old backups...");

         var expireDate = DateTime.Today - new TimeSpan(config.DaysToExpire, 0, 0, 0);
         foreach (var name in Directory.GetFiles(config.TargetDir, "fastness_backup_*.zip"))
         {
            if (File.GetCreationTime(name).Date <= expireDate)
            {
               log.DebugFormat("remove {0}", name);
               File.Delete(name);
            }
         }
         log.Debug("removing old backups done.");
      }
   }
}
