﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fastness.Invoicing;
using System.Collections.Generic;

namespace InvoicingUT
{
   [TestClass]
   public class InvoiceHistoryDetailsHandlerTest
   {
      [TestMethod]
      public void test_invoice_to_invoice_history_entry()
      {
         // GIVEN
         var source = new Invoice 
         { 
            Notes = "some notes line fdsa 1\nsome notes fdsafdsa line 2\n\nsome notes line 4",
            Entries = new List<Invoice.Entry>
            {
               new Invoice.Entry {Charge = 12.33, Quantity = 231, Description = "some description 1"},
               new Invoice.Entry {Charge = 1.323, Quantity = 100, Description = "some description 2"},
               new Invoice.Entry {Charge = 5.33, Quantity = 102, Description = "some description 3"},
               new Invoice.Entry {Charge = 867.432, Quantity = 104, Description = "some description 4"}            
            }
         };
         source.TotalNetto = 235.234;
         source.Tax = 32.543;
         
         var expected = new string[] 
         {
            "235.234;32.543",
            "12.330;231;some description 1",
            "1.323;100;some description 2",
            "5.330;102;some description 3",
            "867.432;104;some description 4",
            "",
            "some notes line fdsa 1",
            "some notes fdsafdsa line 2",
            "",
            "some notes line 4"
         };

         var target = new InvoiceHistoryDetailsHandler();

         // WHEN
         var output = target.ToDetails(source);

         // THEN
         Assert.AreEqual(expected : string.Join("\n", expected), actual : output);
      }

      [TestMethod]
      public void test_invoice_history_entry_to_invoice()
      {

         // GIVEN
         var source = new string[] 
         {
            "235.234;32.543",
            "12.330;231;some description 1",
            "1.323;100;some description 2",
            "5.330;102;some description 3",
            "867.432;104;some description 4",
            "",
            "some notes line fdsa 1",
            "some notes fdsafdsa line 2",
            "",
            "some notes line 4"
         };

         var expected = new Invoice
         {
            Notes = "some notes line fdsa 1\nsome notes fdsafdsa line 2\n\nsome notes line 4",
            Entries = new List<Invoice.Entry>
            {
               new Invoice.Entry {Charge = 12.33, Quantity = 231, Description = "some description 1"},
               new Invoice.Entry {Charge = 1.323, Quantity = 100, Description = "some description 2"},
               new Invoice.Entry {Charge = 5.33, Quantity = 102, Description = "some description 3"},
               new Invoice.Entry {Charge = 867.432, Quantity = 104, Description = "some description 4"}            
            }
         };
         expected.TotalNetto = 235.234;
         expected.Tax = 32.543;

         var target = new InvoiceHistoryDetailsHandler();

         // WHEN
         var output = target.FromDetails(string.Join("\n", source));

         // THEN
         Assert.AreEqual(expected.Notes, output.Notes);
         Assert.AreEqual(expected.TotalNetto, output.TotalNetto);
         Assert.AreEqual(expected.Tax, output.Tax);
         
         Assert.AreEqual(expected.Entries.Count, output.Entries.Count);
         
         Assert.AreEqual(expected.Entries[0].Quantity, output.Entries[0].Quantity);
         Assert.AreEqual(expected.Entries[0].Charge, output.Entries[0].Charge);
         Assert.AreEqual(expected.Entries[0].Description, output.Entries[0].Description);

         Assert.AreEqual(expected.Entries[1].Quantity, output.Entries[1].Quantity);
         Assert.AreEqual(expected.Entries[1].Charge, output.Entries[1].Charge);
         Assert.AreEqual(expected.Entries[1].Description, output.Entries[1].Description);

         Assert.AreEqual(expected.Entries[2].Quantity, output.Entries[2].Quantity);
         Assert.AreEqual(expected.Entries[2].Charge, output.Entries[2].Charge);
         Assert.AreEqual(expected.Entries[2].Description, output.Entries[2].Description);
      }
   }
}
