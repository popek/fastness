﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Data.Entity;
using Fastness.DataModel;
using System.Collections.Generic;
using Fastness.Common;
using Fastness.Invoicing;
using Fastness.DataModel.Utils;
using System.Linq;

namespace InvoicingUT
{
   [TestClass]
   public class InvoiceGeneratorUT
   {
      [TestMethod]
      public void test_non_consignment_and_reconsignment_actions_charge_calculation()
      {
         // GIVEN
         int month = 4;
         int year = 2012;
         string itemNo = "1110001";
         double dummyVat = 0.2;
         int dummyInvoiceNo = 34;
         int fixedUnitsAndItemsChargeEntriesCount = 2;

         var actionDate = new DateTime(year, month, 3);

         var ctr = new Contract
         {
            ContractID = 1,
            FixedFee = 10,
            FixedUnits = 5,
            Address = "al1\nal2\nal3\nal4\nal5\al6",
         };

         var contractsList = new List<Contract> { ctr };

         var actionsList = new List<ActionTemplate>
         {
            new ActionTemplate // consignment to be able to get item price
            {
               ActionTemplateID = 0,
               Code = "DP-STORE",
               Charge = 23,
               PerFreeAction = 8,
               Description = ActionUtils.ACTION_DESCRIPTION_CONSIGNMENT
            },
            new ActionTemplate
            {
               ActionTemplateID = 1,
               Code = "SH-DUMMYACTION1",
               Charge = 2.2f,
               PerFreeAction = 12,
               Description = "actionDescription1",
            },
            new ActionTemplate
            {
               ActionTemplateID = 2,
               Code = "SH-DUMMYACTION2",
               Charge = 3.534f,
               PerFreeAction = 5,
               Description = "actionDescription2",
            },
         };

         int ctrActionId = 0;
         var contractsActionsList = (from al in actionsList
                                     select new ContractAction
                                     {
                                        ContractActionID = ctrActionId++,
                                        Description = al.Description,
                                        Code = al.Code,
                                        Charge = al.Charge,
                                        ContractID = ctr.ContractID,
                                        PerFreeAction = al.PerFreeAction
                                     }).ToList<ContractAction>();


         var ctrConsignmentAction = contractsActionsList[0];

         var itemsList = new List<ItemDetail>
         {
            new ItemDetail 
            { 
               ItemNumber = itemNo, 
               ContractID = contractsList[0].ContractID,
               InStock = true,
               Percentage = 150
            }
         };

         var itemActionsDuringMonth = new List<ActionHistory>
         {
            new ActionHistory 
            {
               ItemNumber = itemsList[0].ItemNumber,
               DateCompleted = actionDate,
               Description = actionsList[1].Description,
                              
               Charge = 12.2,
               Percentage = 150, 
               ContractActionID = contractsActionsList[1].ContractActionID

            },
            new ActionHistory 
            {
               ItemNumber = itemsList[0].ItemNumber,
               DateCompleted = actionDate,
               Description = actionsList[2].Description,
               Charge = 44.2,
               ContractActionID = contractsActionsList[2].ContractActionID
            },
            new ActionHistory 
            {
               ItemNumber = itemsList[0].ItemNumber,
               DateCompleted = actionDate,
               Description = actionsList[2].Description,
               Charge = 31.45,
               ContractActionID = contractsActionsList[2].ContractActionID
            }
         };

         var dbCtxMock = DBTestHelper.GetDBContextMock(contractsList, actionsList, contractsActionsList, itemsList, itemActionsDuringMonth);

         var target = new InvoiceGeneratorForContract(dummyVat, contractsList[0], dbCtxMock.Object);

         // WHEN
         var result = target.Generate(new DateTime(year, month, 1), dummyInvoiceNo);

         // THEN
         Assert.AreEqual(result.Entries.Count, 2 + fixedUnitsAndItemsChargeEntriesCount);

         Assert.AreEqual(result.Entries[0].Quantity, 1);
         Assert.AreEqual(result.Entries[0].Charge, itemActionsDuringMonth[0].Charge * (100 - contractsActionsList[1].PerFreeAction)/100);
         Assert.AreEqual(result.Entries[0].Description, itemActionsDuringMonth[0].Description);

         Assert.AreEqual(result.Entries[1].Quantity, 2);
         Assert.AreEqual(result.Entries[1].Charge,
            (itemActionsDuringMonth[1].Charge + itemActionsDuringMonth[2].Charge) * (100 - contractsActionsList[2].PerFreeAction) / 100);
         Assert.AreEqual(result.Entries[1].Description, itemActionsDuringMonth[1].Description);

         Assert.AreEqual(result.Entries[2].Quantity, 0);
         Assert.AreEqual(result.Entries[2].Charge, 0);
         Assert.AreEqual(result.Entries[2].Description, InvoiceAdditionalData.ItemsChargeEntryCaption(contractsList[0].FixedFee));

         Assert.AreEqual(result.Entries[3].Quantity, ctr.FixedUnits);
         Assert.AreEqual(result.Entries[3].Charge, ctr.FixedFee);
         Assert.AreEqual(result.Entries[3].Description, InvoiceAdditionalData.FixedUnitsEntryCaption);

         Assert.AreEqual(result.ContractID, ctr.ContractID);
         Assert.AreEqual(result.InvoiceNo, dummyInvoiceNo);
         Assert.AreEqual(result.InvoiceAddress, ctr.Address);
         Assert.AreEqual(result.ClientAddress, ctr.AuxiliaryAddress);
      }

      [TestMethod]
      public void test_retrieval_actions_charge_calculation()
      {
         // GIVEN
         int month = 4;
         int year = 2012;


         var ctr = new Contract
         {
            ContractID = 1,
            FixedFee = 10,
            FixedUnits = 5,
         };

         var contractsList = new List<Contract> { ctr };

         var actionsList = new List<ActionTemplate>
         {
            new ActionTemplate // consignment to be able to get item price
            {
               ActionTemplateID = 0,
               Code = "DP-STORE",
               Description = ActionUtils.ACTION_DESCRIPTION_CONSIGNMENT,
               Charge = 3.1f
            },
            new ActionTemplate
            {
               ActionTemplateID = 1,
               Code = "DP-RETRIEVE",
               Description = ActionUtils.ACTION_DESCRIPTION_RETRIEVAL,
               PerFreeAction = 25,
               Charge = 23.1f
            },
         };

         int ctrActionId = 0;
         var contractsActionsList = (from al in actionsList
                                     select new ContractAction
                                       {
                                          ContractActionID = ctrActionId++,
                                          Description = al.Description,
                                          Code = al.Code,
                                          Charge = al.Charge,
                                          ContractID = ctr.ContractID,
                                          PerFreeAction = al.PerFreeAction
                                       }).ToList<ContractAction>();

         var countOfItems = 50;
         var itemsList = new List<ItemDetail>();
         for(int i = 0; i < countOfItems; ++i)
         {
            itemsList.Add(new ItemDetail
            {
               ItemNumber = "1110001" + string.Format("{0}", i),
               ContractID = ctr.ContractID,
               InStock = true,
               Percentage = 150
            });
         }

         int retrievalsCount = 20;
         var onlyRetrievalActionsDuringMonth = new List<ActionHistory>();
         for (int i = 0; i < retrievalsCount; ++i)
         {
            onlyRetrievalActionsDuringMonth.Add(new ActionHistory 
            {
               ItemNumber = itemsList[0].ItemNumber,
               DateCompleted = new DateTime(year, month, 4),
               Description = actionsList[1].Description,
               Charge = contractsActionsList[1].Charge,
               Percentage = 150, 
               ContractActionID = contractsActionsList[1].ContractActionID

            });
         };

         var dbCtxMock = DBTestHelper.GetDBContextMock(contractsList, actionsList, contractsActionsList, itemsList, onlyRetrievalActionsDuringMonth);

         var target = new InvoiceGeneratorForContract(0.2, contractsList[0], dbCtxMock.Object);

         var originalRetrievalsCharge = onlyRetrievalActionsDuringMonth.Sum(ia => ia.Charge);
         var noChargeRetrievsCount = (int)(contractsActionsList[1].PerFreeAction * countOfItems / 100);
         var afterDiscountRetrievalsCharge = originalRetrievalsCharge * (retrievalsCount - noChargeRetrievsCount) / retrievalsCount;

         // WHEN
         var result = target.Generate(new DateTime(year, month, 1), 22);

         // THEN
         Assert.AreEqual(result.Entries[0].Quantity, retrievalsCount);
         Assert.AreEqual(result.Entries[0].Charge, afterDiscountRetrievalsCharge);
         Assert.AreEqual(result.Entries[0].Description, ActionUtils.ACTION_DESCRIPTION_RETRIEVAL);
      }

      [TestMethod]
      public void test_con_recon_discount_when_items_count_less_then_fixed_units()
      {
         var month = 3;
         var year = 2012;

         var singleContractItemNumberMock = "11111111";

         var consignementsCount = 7;
         var reconsignmentsCount = 12;

         var dischargedReconsignmentsCount = reconsignmentsCount / 3;

         var fixedUnits = 40;
         var itemsAtBeginingOfMonth = fixedUnits - consignementsCount - dischargedReconsignmentsCount; // all cons and half of recons will be discharged


         var ctr = new Contract 
         {
            ContractID = 1,
            FixedUnits = fixedUnits,
            FixedFee = 200
         };

         var consAction = new ActionTemplate
         {
            ActionTemplateID = 0,
            Description = ActionUtils.ACTION_DESCRIPTION_CONSIGNMENT,
            Charge = 23.4f,
            Code = "DP-STORE",
            PerFreeAction = 10
         };
         var reconsAction = new ActionTemplate
         {
            ActionTemplateID = 1,
            Description = ActionUtils.ACTION_DESCRIPTION_RECONSIGNMENT,
            Charge = 13.4f,
            Code = "DP-COL",
            PerFreeAction = 6
         };

         var conCtrAction = new ContractAction
         {
            ContractActionID = 0,
            ContractID = ctr.ContractID,
            Description = consAction.Description,
            Code = consAction.Code,
            Charge = consAction.Charge,
            PerFreeAction = consAction.PerFreeAction,
         };
         var reconCtrAction = new ContractAction 
         {
            ContractActionID = 1,
            ContractID = ctr.ContractID,
            Description = reconsAction.Description,
            Code = reconsAction.Code,
            Charge = reconsAction.Charge,
            PerFreeAction = reconsAction.PerFreeAction,
         };

         var itemsList = new List<ItemDetail>();
         for (var i = 0; i < itemsAtBeginingOfMonth + consignementsCount + reconsignmentsCount; ++i)
         {
            itemsList.Add(new ItemDetail
            {
               ItemNumber = singleContractItemNumberMock + i.ToString(),
               ContractID = ctr.ContractID,
               InStock = true,
               Percentage = 100
            });
         }

         var rnd = new Random();

         var itemActionsList = new List<ActionHistory>();
         for (var i = 0; i < consignementsCount; ++i)
         {
            var itemIndex = rnd.Next(1, itemsList.Count());

            itemActionsList.Add(new ActionHistory
            {
               Percentage = itemsList[itemIndex].Percentage,
               ItemNumber = itemsList[itemIndex].ItemNumber,
               Charge = conCtrAction.Charge,
               Description = consAction.Description,
               DateCompleted = new DateTime(year, month, rnd.Next(1, 25)),
               ContractActionID = conCtrAction.ContractActionID
            });
         }
         for (var i = 0; i < reconsignmentsCount; ++i)
         {
            var itemIndex = rnd.Next(1, itemsList.Count());

            itemActionsList.Add(new ActionHistory
            {
               Percentage = itemsList[itemIndex].Percentage,
               ItemNumber = itemsList[itemIndex].ItemNumber,
               Charge = reconCtrAction.Charge,
               Description = reconsAction.Description,
               DateCompleted = new DateTime(year, month, rnd.Next(1, 25)),
               ContractActionID = reconCtrAction.ContractActionID
            });
         }

         var dbCtxMock = DBTestHelper.GetDBContextMock(
            new List<Contract> { ctr },
            new List<ActionTemplate> { consAction, reconsAction },
            new List<ContractAction> { conCtrAction, reconCtrAction },
            itemsList,
            itemActionsList);

         var target = new InvoiceGeneratorForContract(0.2, ctr, dbCtxMock.Object);

         var result = target.Generate(new DateTime(year, month, 1), 22);

         // THEN
         Assert.IsTrue(result.Entries.Exists(e => e.Description == ActionUtils.ACTION_DESCRIPTION_CONSIGNMENT));
         Assert.IsTrue(result.Entries.Exists(e => e.Description == ActionUtils.ACTION_DESCRIPTION_RECONSIGNMENT));

         var conInvEntry = result.Entries.Find(e => e.Description == ActionUtils.ACTION_DESCRIPTION_CONSIGNMENT);
         
         Assert.AreEqual(0, conInvEntry.Quantity, "all consignments MUST be discharged since items at the begining of month is lees then fixeUnits more then consignments count!");
         Assert.AreEqual(0, conInvEntry.Charge, "consignmets charge MUST be 0!");

         var reconInvEntry = result.Entries.Find(e => e.Description == ActionUtils.ACTION_DESCRIPTION_RECONSIGNMENT);
         
         Assert.AreEqual(reconsignmentsCount - dischargedReconsignmentsCount, reconInvEntry.Quantity, "half or reconsignments MUST be discharged since items at the begining of month is lees then fixeUnits more then consignments count + some of reconsignments!");
         Assert.AreEqual((reconsignmentsCount - dischargedReconsignmentsCount) * conCtrAction.Charge, reconInvEntry.Charge, "charge for reconsignments MUST be discharged!");
      }

      [TestMethod]
      public void test_3dots_charges_calculated_properly()
      {
         // GIVEN
         int month = 4;
         int year = 2012;

         double threeDotActionPrice = 0.155f;

         var ctr = new Contract
         {
            ContractID = 1,
            FixedFee = 0,
            FixedUnits = 0,
         };
         var contractsList = new List<Contract> { ctr };

         var actionsList = new List<ActionTemplate>
         {
            new ActionTemplate // consignment to be able to get item price
            {
               ActionTemplateID = 0,
               Code = "DP-STORE",
               Description = ActionUtils.ACTION_DESCRIPTION_CONSIGNMENT,
               Charge = 0.2f
            },
            new ActionTemplate
            {
               ActionTemplateID = 1,
               Code = "DP-RETRIEVE",
               Description = ActionUtils.ACTION_DESCRIPTION_RECONSIGNMENT,
               PerFreeAction = 0,
               Charge = threeDotActionPrice
            },
         };

         int ctrActionId = 0;
         var contractsActionsList = (from al in actionsList
                                     select new ContractAction
                                     {
                                        ContractActionID = ctrActionId++,
                                        Description = al.Description,
                                        Code = al.Code,
                                        Charge = al.Charge,
                                        ContractID = ctr.ContractID,
                                        PerFreeAction = al.PerFreeAction
                                     }).ToList<ContractAction>();

         var itemsList = new List<ItemDetail> { };

         int threeDotsActionsCount = 234;
         var actionsDuringMonth = new List<ActionHistory>();
         for (int i = 0; i < threeDotsActionsCount; ++i)
         {
            actionsDuringMonth.Add(new ActionHistory
            {
               ItemNumber = "dummyValue",
               DateCompleted = new DateTime(year, month, 4),
               Description = actionsList[1].Description,
               Charge = contractsActionsList[1].Charge,
               Percentage = 100,
               ContractActionID = contractsActionsList[1].ContractActionID
            });
         };

         var dbCtxMock = DBTestHelper.GetDBContextMock(contractsList, actionsList, contractsActionsList, itemsList, actionsDuringMonth);

         var target = new InvoiceGeneratorForContract(0.2, contractsList[0], dbCtxMock.Object);

         // WHEN
         var result = target.Generate(new DateTime(year, month, 1), 22);

         // THEN
         Assert.AreEqual(result.Entries[0].Charge, threeDotActionPrice * threeDotsActionsCount);
      }

      [TestMethod]
      public void test_items_at_the_begining_of_month_when_retrievals_during_month()
      { 
         //TODO: items at the begining = items at the end + retrievals count
      }
      [TestMethod]
      public void test_items_at_the_begining_of_month_when_shreading_during_month()
      {
         //TODO: items at the begining = items at the end + shreading count
      }
      [TestMethod]
      public void test_items_at_the_begining_of_month_when_reconsignments_during_month()
      {
         //TODO: items at the begining = items at the end - reconsignments count
      }
      [TestMethod]
      public void test_items_at_the_begining_of_month_when_consignments_during_month()
      {
         //TODO: items at the begining = items at the end - consignments count
      }
   }
}
