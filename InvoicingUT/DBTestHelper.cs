﻿using Fastness.DataModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoicingUT
{
   public class DBTestHelper
   {
      static public Mock<fastnessEntities> GetDBContextMock(
         List<Contract> contractsList, 
         List<ActionTemplate> actionsList,
         List<ContractAction> contractsActionsList,
         List<ItemDetail> itemsList,
         List<ActionHistory> itemActionsDuringMonth)
      {
         var contractsMock = new Mock<DbSet<Contract>>();
         contractsMock.SetupData(contractsList);

         var itemDetailMock = new Mock<DbSet<ItemDetail>>();
         itemDetailMock.SetupData(itemsList);

         var itemActionMock = new Mock<DbSet<ActionHistory>>();
         itemActionMock.SetupData(itemActionsDuringMonth);

         var actionMock = new Mock<DbSet<ActionTemplate>>();
         actionMock.SetupData(actionsList);

         var contractActionsMock = new Mock<DbSet<ContractAction>>();
         contractActionsMock.SetupData(contractsActionsList);

         var contextMock = new Mock<fastnessEntities>();
         contextMock.Setup(c => c.Contract).Returns(contractsMock.Object);
         contextMock.Setup(c => c.ItemDetail).Returns(itemDetailMock.Object);
         contextMock.Setup(c => c.ActionHistory).Returns(itemActionMock.Object);
         contextMock.Setup(c => c.ActionTemplate).Returns(actionMock.Object);
         contextMock.Setup(c => c.ContractAction).Returns(contractActionsMock.Object);

         return contextMock;
      }
   }
}
