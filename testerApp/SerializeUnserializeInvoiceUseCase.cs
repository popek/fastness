﻿using Fastness.Common;
using Fastness.Invoicing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testerApp
{
   public class SerializeUnserializeInvoiceUseCase
   {
      Invoice invoice;

      public SerializeUnserializeInvoiceUseCase()
      {
         invoice = new Invoice
         {
            InvoiceNo = 233,
            ContractID = 22,
            Date = DateTime.Today,
            ClientAddress = "addr line1\naddr line2\naddr line3\naddr line4\naddr line5",
            InvoiceAddress = "i addr line1\ni addr line2\ni addr line3\ni addr line4\ni addr line5",
            Notes = "some notest \n to and invoice\n ble \n blah",
            Entries = new List<Invoice.Entry> 
            {
               new Invoice.Entry { Charge = 21, Description = "entry 1", Quantity = 1},
               new Invoice.Entry { Charge = 22, Description = "entry 2", Quantity = 2},
               new Invoice.Entry { Charge = 23, Description = "entry 3", Quantity = 3},
               new Invoice.Entry { Charge = 24, Description = "entry 4", Quantity = 4},
            },
            TotalNetto = 675,
            Tax = 43
         };
      }
      public void Run()
      {
         string serializedInvoice = XmlUtils.Serialize<Invoice>(invoice);

         Invoice unserializedImage = XmlUtils.Unserialize<Invoice>(serializedInvoice);
      }
   }
}
