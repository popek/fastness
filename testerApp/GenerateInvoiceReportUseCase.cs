﻿using Fastness.Invoicing;
using Fastness.Invoicing.Report;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testerApp
{
   public class GenerateInvoiceReportUseCase
   {
      public void Run()
      {
         var invoice = GenerateSampleInvoice();
         File.WriteAllText(@"c:\temp\_test", new InvoiceHistoryDetailsHandler().ToDetails(invoice));

         using (var report = new PDFInvoiceReport(@"c:\temp\test_invoice.pdf"))
         {
            report.AddInvoice(invoice);
         }
      }

      Invoice GenerateSampleInvoice()
      {
         return new Invoice 
         { 
            InvoiceAddress = "Scott Duff & Co.\n40 king Street\nPenrith\nCumbria\nCA11 7AY",
            ClientAddress = "Scott Duff & Co.\n40 king Street\nPenrith\nCumbria\nCA11 7AY",

            ContractID = 11,
            InvoiceNo = 7399,
            Date = DateTime.Today,
            TotalNetto = 444.5,
            Tax = 88.9,
            Notes = "some notest to the invoice\ntelling that somehting and\nsometing \nhad happened",
            Entries = new List<Invoice.Entry>
            {
               new Invoice.Entry
               {
                  Description = "Fixed Units",
                  Quantity = 16,
                  Charge = 20
               },
               new Invoice.Entry
               {
                  Description = "AdditionalUnits at the begining of the month",
                  Quantity = 934,
                  Charge = 336.24
               },
               new Invoice.Entry
               {
                  Description = "Consignment",
                  Quantity = 49,
                  Charge = 17.64
               },
               new Invoice.Entry
               {
                  Description = "Box Purchase",
                  Quantity = 1,
                  Charge = 1.32
               },
               new Invoice.Entry
               {
                  Description = "Collection for Shreading",
                  Quantity = 2,
                  Charge = 8.4
               },
               new Invoice.Entry
               {
                  Description = "Routine File Consignment",
                  Quantity = 6,
                  Charge = 12.6
               },
               new Invoice.Entry
               {
                  Description = "Rotine File Retrieval",
                  Quantity = 6,
                  Charge = 31.5
               },
               new Invoice.Entry
               {
                  Description = "Sack Delivery",
                  Quantity = 1,
                  Charge = 0
               },
               new Invoice.Entry
               {
                  Description = "Shreading per Sack",
                  Quantity = 2,
                  Charge = 16.8
               },
               new Invoice.Entry
               {
                  Description = "Consignment",
                  Quantity = 49,
                  Charge = 17.64
               },
               new Invoice.Entry
               {
                  Description = "Box Purchase",
                  Quantity = 1,
                  Charge = 1.32
               },
               new Invoice.Entry
               {
                  Description = "Collection for Shreading",
                  Quantity = 2,
                  Charge = 8.4
               },
               new Invoice.Entry
               {
                  Description = "Routine File Consignment",
                  Quantity = 6,
                  Charge = 12.6
               },
               new Invoice.Entry
               {
                  Description = "Rotine File Retrieval",
                  Quantity = 6,
                  Charge = 31.5
               },
               new Invoice.Entry
               {
                  Description = "Sack Delivery",
                  Quantity = 1,
                  Charge = 0
               },
               new Invoice.Entry
               {
                  Description = "Shreading per Sack",
                  Quantity = 2,
                  Charge = 16.8
               }
               ,
                              new Invoice.Entry
               {
                  Description = "Consignment",
                  Quantity = 49,
                  Charge = 17.64
               },
               new Invoice.Entry
               {
                  Description = "Box Purchase",
                  Quantity = 1,
                  Charge = 1.32
               },
               new Invoice.Entry
               {
                  Description = "Collection for Shreading",
                  Quantity = 2,
                  Charge = 8.4
               },
               new Invoice.Entry
               {
                  Description = "Routine File Consignment",
                  Quantity = 6,
                  Charge = 12.6
               },
               new Invoice.Entry
               {
                  Description = "Rotine File Retrieval",
                  Quantity = 6,
                  Charge = 31.5
               },
               new Invoice.Entry
               {
                  Description = "Sack Delivery",
                  Quantity = 1,
                  Charge = 0
               },
               new Invoice.Entry
               {
                  Description = "Shreading per Sack",
                  Quantity = 2,
                  Charge = 16.8
               },
               new Invoice.Entry
               {
                  Description = "Consignment",
                  Quantity = 49,
                  Charge = 17.64
               },
               new Invoice.Entry
               {
                  Description = "Box Purchase",
                  Quantity = 1,
                  Charge = 1.32
               },
               new Invoice.Entry
               {
                  Description = "Collection for Shreading",
                  Quantity = 2,
                  Charge = 8.4
               },
               new Invoice.Entry
               {
                  Description = "Routine File Consignment",
                  Quantity = 6,
                  Charge = 12.6
               },
               new Invoice.Entry
               {
                  Description = "Rotine File Retrieval",
                  Quantity = 6,
                  Charge = 31.5
               },
               new Invoice.Entry
               {
                  Description = "Sack Delivery",
                  Quantity = 1,
                  Charge = 0
               },
               new Invoice.Entry
               {
                  Description = "Shreading per Sack",
                  Quantity = 2,
                  Charge = 16.8
               }
            }
         };
      }
   }
}
