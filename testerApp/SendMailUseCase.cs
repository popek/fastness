﻿using Fastness.Common;
using Fastness.DataModel.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testerApp
{
   public class SendMailUseCase
   {
      public void Run()
      {
         var settings = new SmtpClientSettings 
         { 
            User = "fastntester@gmail.com",
            ServerAddress = "smtp.gmail.com",
            ServerPort = 587,
            UseTls = true
         };

         settings.UpdatePassword("simple18");

         var attachementPath = Path.Combine(Directory.GetParent(SystemUtils.CurrentAssemblyPath).Parent.FullName, "Contract history.pdf");
         var body =
@"Dear Sair,

some text

sincerly,
Fastness team
";
         using (var sender = new MailSender(settings))
         {
            sender.SendMail(
               "fastntester@gmail.com",
               Misc.EmailAddressesListToArray(
@" 

  marwrobel@gmail.com 


  fastntester2@gmail.com 

     fastntester@gmail.com 

"),
               "fastness invoice",
               body,
               attachementPath);
         }
      }
   }
}
