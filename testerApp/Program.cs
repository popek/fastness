﻿using Fastness.DataModel.Utils;
using Fastness.Invoicing;
using Fastness.Invoicing.Report;
using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testerApp
{
   class Program
   {
      static readonly ILog log = LogManager.GetLogger(typeof(Program));

      static void Main(string[] args)
      {
         //XmlConfigurator.Configure();

         //new DBConstraintCorrectnessCheck().Run();

         //new GenerateLastMonthInvoicesUseCase().Run();

         //new GetInvoiceFromHistoryTableUseCase().Run();

         //new GenerateInvoiceReportUseCase().Run();

         //new SerializeUnserializeInvoiceUseCase().Run();
         
         new SendMailUseCase().Run();
         //new DateFormatUseCase().Run();

         return;
      }
   }
}
