﻿using Fastness.DataModel;
using Fastness.DataModel.Utils;
using Fastness.Invoicing;
using Fastness.Invoicing.Report;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testerApp
{
   public class GenerateLastMonthInvoicesUseCase
   {
      static readonly ILog log = LogManager.GetLogger(typeof(GenerateLastMonthInvoicesUseCase));

      public void Run()
      {
         log.Info("testing generation on all invoices for a months");

         using (var invoiceReport = new PDFInvoiceReport(@"c:\temp\fastnessInvoice.pdf"))
         {
            using (var dbCtx = new fastnessEntities())
            {
               var allInvoicesGenerator = new AllContractsInvoiceGenerator(dbCtx, new FetchFromDb(dbCtx).AvailableContracts());
               allInvoicesGenerator.NextInvoiceGenerated += invoice => invoiceReport.AddInvoice(invoice);

               allInvoicesGenerator.Generate(new DateTime(2003, 2, 1));
            }
         }
      }
   }
}
