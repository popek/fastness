﻿using Fastness.Common;
using Fastness.DataModel.Utils;
using Fastness.Invoicing;
using FastnessForms.Views;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MailSenderTester
{
   public partial class MainForm : Form
   {
      XmlObjectsRepository repository = new XmlObjectsRepository(CurrentDirUtils.GetSubdirPath("auxiliary_configs"));
      static readonly ILog log = LogManager.GetLogger(typeof(MainForm));

      public MainForm()
      {
         InitializeComponent();
      }

      private void btShowSenderSettings_Click(object sender, EventArgs e)
      {
         var settings = repository.Exists<SmtpClientSettings>() ?
           repository.Get<SmtpClientSettings>() : new SmtpClientSettings();
         var invoiceMailTemplate = repository.Exists<MailTemplate>() ?
            repository.Get<MailTemplate>() : Fastness.Invoicing.Utils.DefaultInvoiceMailTemplate();

         var mailSettingsForm = new InvoiceMailSettingsForm(settings, invoiceMailTemplate);
         mailSettingsForm.SmtpSettingsChanged += (updatedSettings) =>
         {
            repository.Update<SmtpClientSettings>(updatedSettings);
         };

         mailSettingsForm.MailTemplateChanged += (updatedInvoiceMailTemplate) =>
         {
            repository.Update<MailTemplate>(updatedInvoiceMailTemplate);
         };
         mailSettingsForm.ShowDialog();
      }

      private void SendInvoiceEmail(MailSender sender)
      {
         var invoiceMailTemplate = repository.Get<MailTemplate>();
         try
         {
            sender.SendMail(
               invoiceMailTemplate.Sender,
               Misc.EmailAddressesListToArray(txtDestinationEmail.Text),
               invoiceMailTemplate.SubjectTemplate,
               invoiceMailTemplate.BodyTemplate,
               "Contract history.pdf");

            log.InfoFormat("e-mail send succesfully");
         }
         catch (Exception e)
         {
            log.ErrorFormat("error while trying to send e-mail: {0}", e);
            UserInteraction.ShowError("failed to send e-mail: {0}", e);
            return;
         }
         UserInteraction.ShowInfo("e-mail send succesfully");
      }

      private void btSendEmail_Click(object sender, EventArgs e)
      {
         if (repository.Exists<SmtpClientSettings>() == false ||
             repository.Exists<MailTemplate>() == false)
         {
            UserInteraction.ShowError("setup smtp client and mail template first!");
            return;
         }

         using (var s = new MailSender(repository.Get<SmtpClientSettings>()))
         {
            SendInvoiceEmail(s);
         }
      }
   }
}
