﻿namespace MailSenderTester
{
   partial class MainForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.btShowSenderSettings = new System.Windows.Forms.Button();
         this.btSendEmail = new System.Windows.Forms.Button();
         this.txtDestinationEmail = new System.Windows.Forms.TextBox();
         this.label1 = new System.Windows.Forms.Label();
         this.SuspendLayout();
         // 
         // btShowSenderSettings
         // 
         this.btShowSenderSettings.Location = new System.Drawing.Point(12, 12);
         this.btShowSenderSettings.Name = "btShowSenderSettings";
         this.btShowSenderSettings.Size = new System.Drawing.Size(147, 42);
         this.btShowSenderSettings.TabIndex = 0;
         this.btShowSenderSettings.Text = "e-mail settings";
         this.btShowSenderSettings.UseVisualStyleBackColor = true;
         this.btShowSenderSettings.Click += new System.EventHandler(this.btShowSenderSettings_Click);
         // 
         // btSendEmail
         // 
         this.btSendEmail.Location = new System.Drawing.Point(12, 110);
         this.btSendEmail.Name = "btSendEmail";
         this.btSendEmail.Size = new System.Drawing.Size(147, 42);
         this.btSendEmail.TabIndex = 1;
         this.btSendEmail.Text = "send e-mail";
         this.btSendEmail.UseVisualStyleBackColor = true;
         this.btSendEmail.Click += new System.EventHandler(this.btSendEmail_Click);
         // 
         // txtDestinationEmail
         // 
         this.txtDestinationEmail.Location = new System.Drawing.Point(12, 84);
         this.txtDestinationEmail.Name = "txtDestinationEmail";
         this.txtDestinationEmail.Size = new System.Drawing.Size(288, 20);
         this.txtDestinationEmail.TabIndex = 2;
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(17, 66);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(128, 13);
         this.label1.TabIndex = 3;
         this.label1.Text = "destination e-mail address";
         // 
         // MainForm
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(312, 161);
         this.Controls.Add(this.label1);
         this.Controls.Add(this.txtDestinationEmail);
         this.Controls.Add(this.btSendEmail);
         this.Controls.Add(this.btShowSenderSettings);
         this.Name = "MainForm";
         this.Text = "mail sender tester";
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Button btShowSenderSettings;
      private System.Windows.Forms.Button btSendEmail;
      private System.Windows.Forms.TextBox txtDestinationEmail;
      private System.Windows.Forms.Label label1;
   }
}

