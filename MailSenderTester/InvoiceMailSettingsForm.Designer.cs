﻿namespace FastnessForms.Views
{
   partial class InvoiceMailSettingsForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InvoiceMailSettingsForm));
         this.groupBox1 = new System.Windows.Forms.GroupBox();
         this.txtPassword = new System.Windows.Forms.TextBox();
         this.txtUser = new System.Windows.Forms.TextBox();
         this.label5 = new System.Windows.Forms.Label();
         this.cbSecure = new System.Windows.Forms.CheckBox();
         this.label3 = new System.Windows.Forms.Label();
         this.label2 = new System.Windows.Forms.Label();
         this.label1 = new System.Windows.Forms.Label();
         this.txtPort = new System.Windows.Forms.TextBox();
         this.label6 = new System.Windows.Forms.Label();
         this.txtAddress = new System.Windows.Forms.TextBox();
         this.groupBox2 = new System.Windows.Forms.GroupBox();
         this.txtSender = new System.Windows.Forms.TextBox();
         this.label8 = new System.Windows.Forms.Label();
         this.label7 = new System.Windows.Forms.Label();
         this.txtBody = new System.Windows.Forms.TextBox();
         this.txtSubject = new System.Windows.Forms.TextBox();
         this.label4 = new System.Windows.Forms.Label();
         this.btSaveChanges = new System.Windows.Forms.Button();
         this.btCancel = new System.Windows.Forms.Button();
         this.groupBox1.SuspendLayout();
         this.groupBox2.SuspendLayout();
         this.SuspendLayout();
         // 
         // groupBox1
         // 
         this.groupBox1.Controls.Add(this.txtPassword);
         this.groupBox1.Controls.Add(this.txtUser);
         this.groupBox1.Controls.Add(this.label5);
         this.groupBox1.Controls.Add(this.cbSecure);
         this.groupBox1.Controls.Add(this.label3);
         this.groupBox1.Controls.Add(this.label2);
         this.groupBox1.Controls.Add(this.label1);
         this.groupBox1.Controls.Add(this.txtPort);
         this.groupBox1.Controls.Add(this.label6);
         this.groupBox1.Controls.Add(this.txtAddress);
         this.groupBox1.Location = new System.Drawing.Point(12, 12);
         this.groupBox1.Name = "groupBox1";
         this.groupBox1.Size = new System.Drawing.Size(354, 156);
         this.groupBox1.TabIndex = 0;
         this.groupBox1.TabStop = false;
         this.groupBox1.Text = "SMTP settings";
         // 
         // txtPassword
         // 
         this.txtPassword.Location = new System.Drawing.Point(88, 100);
         this.txtPassword.MaxLength = 200;
         this.txtPassword.Name = "txtPassword";
         this.txtPassword.Size = new System.Drawing.Size(219, 20);
         this.txtPassword.TabIndex = 3;
         this.txtPassword.UseSystemPasswordChar = true;
         // 
         // txtUser
         // 
         this.txtUser.Location = new System.Drawing.Point(88, 77);
         this.txtUser.MaxLength = 200;
         this.txtUser.Name = "txtUser";
         this.txtUser.Size = new System.Drawing.Size(219, 20);
         this.txtUser.TabIndex = 2;
         // 
         // label5
         // 
         this.label5.AutoSize = true;
         this.label5.Location = new System.Drawing.Point(30, 129);
         this.label5.Name = "label5";
         this.label5.Size = new System.Drawing.Size(52, 13);
         this.label5.TabIndex = 7;
         this.label5.Text = "TLS/SSL";
         // 
         // cbSecure
         // 
         this.cbSecure.AutoSize = true;
         this.cbSecure.Location = new System.Drawing.Point(88, 129);
         this.cbSecure.Name = "cbSecure";
         this.cbSecure.Size = new System.Drawing.Size(15, 14);
         this.cbSecure.TabIndex = 4;
         this.cbSecure.UseVisualStyleBackColor = true;
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Location = new System.Drawing.Point(30, 103);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(52, 13);
         this.label3.TabIndex = 5;
         this.label3.Text = "password";
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(55, 74);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(27, 13);
         this.label2.TabIndex = 3;
         this.label2.Text = "user";
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(25, 51);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(57, 13);
         this.label1.TabIndex = 1;
         this.label1.Text = "server port";
         // 
         // txtPort
         // 
         this.txtPort.Location = new System.Drawing.Point(88, 48);
         this.txtPort.MaxLength = 20;
         this.txtPort.Name = "txtPort";
         this.txtPort.Size = new System.Drawing.Size(62, 20);
         this.txtPort.TabIndex = 1;
         // 
         // label6
         // 
         this.label6.AutoSize = true;
         this.label6.Location = new System.Drawing.Point(6, 25);
         this.label6.Name = "label6";
         this.label6.Size = new System.Drawing.Size(76, 13);
         this.label6.TabIndex = 9;
         this.label6.Text = "server address";
         // 
         // txtAddress
         // 
         this.txtAddress.Location = new System.Drawing.Point(88, 22);
         this.txtAddress.MaxLength = 200;
         this.txtAddress.Name = "txtAddress";
         this.txtAddress.Size = new System.Drawing.Size(219, 20);
         this.txtAddress.TabIndex = 0;
         // 
         // groupBox2
         // 
         this.groupBox2.Controls.Add(this.txtSender);
         this.groupBox2.Controls.Add(this.label8);
         this.groupBox2.Controls.Add(this.label7);
         this.groupBox2.Controls.Add(this.txtBody);
         this.groupBox2.Controls.Add(this.txtSubject);
         this.groupBox2.Controls.Add(this.label4);
         this.groupBox2.Location = new System.Drawing.Point(12, 174);
         this.groupBox2.Name = "groupBox2";
         this.groupBox2.Size = new System.Drawing.Size(354, 220);
         this.groupBox2.TabIndex = 1;
         this.groupBox2.TabStop = false;
         this.groupBox2.Text = "e-mail details";
         // 
         // txtSender
         // 
         this.txtSender.Location = new System.Drawing.Point(58, 28);
         this.txtSender.MaxLength = 200;
         this.txtSender.Name = "txtSender";
         this.txtSender.Size = new System.Drawing.Size(287, 20);
         this.txtSender.TabIndex = 0;
         // 
         // label8
         // 
         this.label8.AutoSize = true;
         this.label8.Location = new System.Drawing.Point(11, 31);
         this.label8.Name = "label8";
         this.label8.Size = new System.Drawing.Size(39, 13);
         this.label8.TabIndex = 4;
         this.label8.Text = "sender";
         // 
         // label7
         // 
         this.label7.AutoSize = true;
         this.label7.Location = new System.Drawing.Point(22, 83);
         this.label7.Name = "label7";
         this.label7.Size = new System.Drawing.Size(30, 13);
         this.label7.TabIndex = 2;
         this.label7.Text = "body";
         // 
         // txtBody
         // 
         this.txtBody.Location = new System.Drawing.Point(58, 80);
         this.txtBody.MaxLength = 2048;
         this.txtBody.Multiline = true;
         this.txtBody.Name = "txtBody";
         this.txtBody.Size = new System.Drawing.Size(287, 126);
         this.txtBody.TabIndex = 2;
         // 
         // txtSubject
         // 
         this.txtSubject.Location = new System.Drawing.Point(58, 54);
         this.txtSubject.MaxLength = 200;
         this.txtSubject.Name = "txtSubject";
         this.txtSubject.Size = new System.Drawing.Size(287, 20);
         this.txtSubject.TabIndex = 1;
         // 
         // label4
         // 
         this.label4.AutoSize = true;
         this.label4.Location = new System.Drawing.Point(11, 57);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(41, 13);
         this.label4.TabIndex = 0;
         this.label4.Text = "subject";
         // 
         // btSaveChanges
         // 
         this.btSaveChanges.Location = new System.Drawing.Point(12, 409);
         this.btSaveChanges.Name = "btSaveChanges";
         this.btSaveChanges.Size = new System.Drawing.Size(77, 34);
         this.btSaveChanges.TabIndex = 2;
         this.btSaveChanges.Text = "Save changes";
         this.btSaveChanges.UseVisualStyleBackColor = true;
         this.btSaveChanges.Click += new System.EventHandler(this.btSaveChanges_Click);
         // 
         // btCancel
         // 
         this.btCancel.Location = new System.Drawing.Point(95, 409);
         this.btCancel.Name = "btCancel";
         this.btCancel.Size = new System.Drawing.Size(77, 34);
         this.btCancel.TabIndex = 3;
         this.btCancel.Text = "Cancel";
         this.btCancel.UseVisualStyleBackColor = true;
         this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
         // 
         // InvoiceMailSettingsForm
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(381, 455);
         this.ControlBox = false;
         this.Controls.Add(this.btCancel);
         this.Controls.Add(this.btSaveChanges);
         this.Controls.Add(this.groupBox2);
         this.Controls.Add(this.groupBox1);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
         this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
         this.MaximizeBox = false;
         this.MinimizeBox = false;
         this.Name = "InvoiceMailSettingsForm";
         this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
         this.Text = "e-mail for sending invoice settings";
         this.groupBox1.ResumeLayout(false);
         this.groupBox1.PerformLayout();
         this.groupBox2.ResumeLayout(false);
         this.groupBox2.PerformLayout();
         this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.TextBox txtPort;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.TextBox txtAddress;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.GroupBox groupBox2;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.TextBox txtSubject;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.TextBox txtBody;
      private System.Windows.Forms.Button btSaveChanges;
      private System.Windows.Forms.Button btCancel;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.CheckBox cbSecure;
      private System.Windows.Forms.TextBox txtUser;
      private System.Windows.Forms.TextBox txtSender;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.TextBox txtPassword;
   }
}