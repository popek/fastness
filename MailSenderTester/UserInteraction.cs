﻿using System;
using System.Windows.Forms;

namespace FastnessForms.Views
{
   public static class UserInteraction
   {
      public static void ShowError(string format, params object[] args)
      {
         MessageBox.Show(string.Format(format, args), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }

      public static void ShowInfo(string format, params object[] args)
      {
         MessageBox.Show(string.Format(format, args), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
      }

      public static bool CanContinue(string format, params object[] args)
      {
         return MessageBox.Show(string.Format(format, args), Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes;
      }

      public static bool AskUser(string format, params object[] args)
      {
         return MessageBox.Show(string.Format(format, args), Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes;
      }

   }
}
