﻿using Fastness.Common;
using Fastness.Common.Events;
using Fastness.DataModel;
using Fastness.DataModel.Transaction;
using Fastness.Invoicing;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using Fastness.DataModel.Utils;

namespace Fastness.BusinessOperations
{
   public class GenerateLastMonthInvoices
   {
      static readonly ILog log = LogManager.GetLogger(typeof(GenerateLastMonthInvoices));

      public event DbContentChangedEventHandler DBChanged;
      public event ProgressChangedEventHandler ProgressChanged;

      Func<int, string> invoiceNoteProvider;
      IEnumerable<Contract> contracts;

      public Exception Error { get; private set; }

      public GenerateLastMonthInvoices(Func<int, string> invoiceNoteProvider, IEnumerable<Contract> contracts)
      {
         this.invoiceNoteProvider = invoiceNoteProvider;
         this.contracts = contracts;
      }
      public void Run()
      {
         try
         {
            RunInternal();
         }
         catch (Exception e)
         {
            log.ErrorFormat("error while generating last month invoices: {0}", e);
            Error = e;
         }
      }
      private void RunInternal()
      {
         var invoicesCollector = new InvoiceHistoryCollector();
         int invoiceCounter = 1;

         int progressMax = 0;

         using(var dbCtx = new fastnessEntities())
         {
            progressMax = contracts.Count() + 1;
            ProgressChanged(0, progressMax);

            var generator = new AllContractsInvoiceGenerator(dbCtx, contracts);
            generator.NextInvoiceGenerated += invoice =>
            {
               invoice.Notes = invoiceNoteProvider(invoice.ContractID);

               invoicesCollector.Collect(invoice);
               ProgressChanged(invoiceCounter++, null);
            };
            generator.Generate(DateUtils.LastMonthDate);
         }

         // take second db handle for storing
         ModifyDb.RunTransaction(dbCtx => new StoreNextMonthInvoiceHistory(
            invoicesCollector.Entries,
            dbCtx).Run());

         ProgressChanged(progressMax, null);

         DBChanged(null);
      }
   }
}
