﻿using Fastness.Common;
using Fastness.Common.Events;
using Fastness.DataModel;
using Fastness.DataModel.Transaction;
using Fastness.DataModel.Utils;
using Fastness.Invoicing;
using Fastness.Invoicing.Report;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Fastness.BusinessOperations
{
   public class EmailLastMonthInvoices
   {
      public class Status
      {
         public int ContractID { get; set; }
         public string ContractName { get; set; }
         public bool Success { get; set; }
         public string ErrorInfo { get; set; }
         public bool DbUpdated { get; set; }

         public void Update(Status newStatus)
         {
            Success = newStatus.Success;
            ErrorInfo = newStatus.ErrorInfo;
         }
      }

      static readonly ILog log = LogManager.GetLogger(typeof(EmailLastMonthInvoices));

      SmtpClientSettings clientSettings;
      MailTemplate invoiceMailTemplate;

      List<Status> statuses = new List<Status>();
      public List<Status> Statuses { get { return statuses; } }

      public event DbContentChangedEventHandler DBChanged;
      public event ProgressChangedEventHandler ProgressChanged;

      public Exception Error { get; private set; }

      public EmailLastMonthInvoices(SmtpClientSettings clientSettings, MailTemplate invoiceMailTemplate)
      {
         this.clientSettings = clientSettings;
         this.invoiceMailTemplate = invoiceMailTemplate;
      }

      private void UpdateDbIfNecessary()
      {
         var successfullStatusesNoYetInDb = statuses.Where(s => s.Success && !s.DbUpdated);
         if (successfullStatusesNoYetInDb.Count() > 0)
         {
            ModifyDb.RunTransaction(dbCtx => new UpdateEmailedInvoiceDetails(
               successfullStatusesNoYetInDb.Select(s => s.ContractID).ToList<int>(), 
               DateUtils.LastMonthDate, 
               dbCtx).Run());
            
            foreach (var status in successfullStatusesNoYetInDb)
            {
               status.DbUpdated = true;
            }
            DBChanged(null);
         }
      }
      private Status SendInvoiceEmail(MailSender sender, Contract ctr, Invoice invoice)
      {
         var attachementFilePath = PdfFilesHelper.PrepareReportFilePath(string.Format("Invoice for {0}", DateUtils.LastMonthDate.ToString("MMMM yyyy")));
         using (var attachement = new PDFInvoiceReport(attachementFilePath))
         {
            attachement.AddInvoice(invoice);
         }

         var status = new Status { ContractID = ctr.ContractID, ContractName = ctr.Name, Success = true, DbUpdated = false };
         try
         {
            sender.SendMail(
               invoiceMailTemplate.Sender,
               Misc.EmailAddressesListToArray(ctr.EmailAddress),
               Utils.StringFromTemplate(invoiceMailTemplate.SubjectTemplate, ctr.Name, DateUtils.LastMonthDate),
               Utils.StringFromTemplate(invoiceMailTemplate.BodyTemplate, ctr.Name, DateUtils.LastMonthDate),
               attachementFilePath);

            log.InfoFormat("{0} invoice e-mail for {1} send succesfully", DateUtils.ToString(DateUtils.LastMonthDate), ctr.LogDesc());
         }
         catch (Exception e)
         {
            log.ErrorFormat("error while trying to send {0} invoice e-mail for {1}: {2}", DateUtils.ToString(DateUtils.LastMonthDate), ctr.LogDesc(), e);
            status.Success = false;
            status.ErrorInfo = e.Message;
         }

         // Pam reported error that sometimes deletion throws exception (IOException: file used by other process). 
         // This is defensive code which makes few attempts with deletion
         RetryUtils.RetryIfException(() => File.Delete(attachementFilePath), 400, 10);

         return status;
      }

      private void InitialiseStatuses(IEnumerable<InvoiceHistory> histories, fastnessEntities dbCtx)
      { 
         foreach(var history in histories)
         {
            var ctrs = dbCtx.Contract.Where(c => c.ContractID == history.ContractID && !c.ContractFinished);
            if (ctrs.Count() == 0)
            {
               log.DebugFormat("contract with id {0} is finished or had been removed from DB. E-mail sent will not take plce for not existing or finished contract", 
                  history.ContractID);
               continue;
            }

            var ctr = ctrs.First();
            bool lmiInvoiceAlreadySend = false;
            var lastInvoiceSendByEmail = ctr.LastSendInvoice;
            lmiInvoiceAlreadySend = lastInvoiceSendByEmail.HasValue && lastInvoiceSendByEmail.Value.Equals(DateUtils.LastMonthDate);

            var status = new Status
            {
               ContractID = ctr.ContractID,
               ContractName = ctr.Name,
               DbUpdated = lmiInvoiceAlreadySend,
               Success = lmiInvoiceAlreadySend
            };
            log.DebugFormat("invoice e-mail for {0} has {1}", ctr.LogDesc(), status.Success ? "been sent already" : "not been sent yet");

            statuses.Add(status);
         }
      }

      public void Run()
      {
         try
         { 
            RunInternal();
         }
         catch(Exception e)
         {
            log.ErrorFormat("error during e-mailing LMI: {0}", e);
            Error = e;
         }
      }
      private void RunInternal()
      {
         statuses.Clear();

         try
         {
            SendAllInvoices();
         }
         catch (Exception)
         {
            log.Warn("error during sending invoices mails. Updating DB and rethrowing");
            throw;
         }
         finally
         {
            log.Debug("updating send invoices data in DB");
            UpdateDbIfNecessary();
         }
      }

      private void SendAllInvoices()
      {
         statuses.Clear();

         using(var dbCtx = new fastnessEntities())
         {
            var fetchDb = new FetchFromDb(dbCtx);
            var invoicesCollection = fetchDb.AllStoredInvoices(DateUtils.LastMonthDate);

            InitialiseStatuses(invoicesCollection, dbCtx);

            ProgressChanged(0, invoicesCollection.Count());

            var deserializer = new InvoiceHistoryEntryDeserializer(dbCtx);

            using (var sender = new MailSender(clientSettings))
            {
               int invoiceCounter = 0;
               foreach (var ih in invoicesCollection)
               {
                  if (statuses.Exists(s => s.ContractID == ih.ContractID && s.Success == false))
                  {
                     var status = SendInvoiceEmail(sender, fetchDb.ContractBasedOnContractId(ih.ContractID), deserializer.Deserialize(ih));

                     if (statuses.Exists(s => s.ContractID == ih.ContractID))
                     {
                        statuses.Find(s => s.ContractID == ih.ContractID).Update(status);
                     }
                     else
                     {
                        statuses.Add(status);
                     }
                  }
                  ProgressChanged(++invoiceCounter, null);
               }
            }
         }
      }
   }
}
