﻿using System;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace Fastness.Common
{
   public class MailSender : IDisposable
   {
      private SmtpClient client;
      public MailSender(SmtpClientSettings settings)
      {
         var basicCredential = new NetworkCredential(settings.User, settings.GetPassword());
         client = new SmtpClient(settings.ServerAddress, settings.ServerPort)
         {
            UseDefaultCredentials = false,
            Credentials = basicCredential,
            EnableSsl = settings.UseTls,
            Timeout = 20000,
            DeliveryMethod = SmtpDeliveryMethod.Network,
         };
      }
      public void Dispose()
      {
         client.Dispose();
      }

      public void SendMail(string from, string[] to, string subject, string body, string attachementPath)
      {
         using (var attachement = new Attachment(attachementPath))
         {
            SendMail(from, to, null, subject, body, attachement);
         }
      }

      public void SendMail(string from, string[] to, string[] cc, string subject, string body, Attachment attachement)
      {
         using (var msg = new MailMessage()
         {
            From = new MailAddress(from),
            Subject = subject,
            Body = body,
            BodyEncoding = Encoding.UTF8,
            IsBodyHtml = false,
            DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure,
         })
         {
            foreach(var dest in to)
            {
               msg.To.Add(new MailAddress(dest));
            }
            if(attachement != null)
            {
               msg.Attachments.Add(attachement);
            }
            if (cc != null)
            {
               foreach (var ccAddress in cc)
               {
                  msg.CC.Add(new MailAddress(ccAddress));
               }
            }
            client.Send(msg);
         }
      }
   }
}
