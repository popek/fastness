﻿using Fastness.Common;

namespace Fastness.Common.Events
{
   public delegate void DbContentChangedEventHandler(object changeContext);

   public delegate void SmtpClientSettingsChangedEventHandler(SmtpClientSettings newSettings);
   public delegate void MailTemplatedChangedEventHandler(MailTemplate newTemplate);

   public delegate void ProgressChangedEventHandler(int percentsFinished, object status);
}
