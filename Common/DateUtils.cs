﻿using System;
using System.Globalization;

namespace Fastness.Common
{
   public class DateUtils
   {
      public static DateTime LastMonthDate
      {
         get 
         {
            int prevMonth = DateTime.Today.Month == 1 ? 12 : DateTime.Today.Month - 1;
            int year = DateTime.Today.Month == 1 ? DateTime.Today.Year - 1 : DateTime.Today.Year;
            return new DateTime(year, prevMonth, DateTime.DaysInMonth(year, prevMonth));
         }
      }

      public static string ToString(DateTime date)
      {
         return date.ToString("d", CultureInfo.CurrentCulture);
      }
      public static string ToString(Nullable<DateTime> date)
      {
         if (date.HasValue)
            return ToString(date.Value);

         return string.Empty;
      }
      public static string ToMonthYearString(DateTime date)
      {
         return date.ToString("MM\\/yy");
      }

   }
}
