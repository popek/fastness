﻿using System;
using System.Globalization;

namespace Fastness.Common
{
   public interface ICurrencyFormatter
   {
      string Symbol { get; }
      string Format(double charge);
      double RoundUp(double charge);
   }

   //
   public class CurrencyFormatter : ICurrencyFormatter
   {
      NumberFormatInfo nfi;

      public CurrencyFormatter()
      {
         nfi = new CultureInfo("en-GB").NumberFormat;
         nfi.CurrencyDecimalDigits = 2;
         nfi.CurrencyPositivePattern = 0; //$n
      }

      public string Symbol { get { return nfi.CurrencySymbol; } }
      
      public string Format(double charge)
      {
         return charge.ToString("C", nfi).Substring(Symbol.Length);
      }

      public double RoundUp(double charge)
      {
         return Double.Parse(Format(charge), NumberStyles.Currency, nfi);
      }
   }
}
