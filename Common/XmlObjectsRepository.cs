﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fastness.Common
{
   public class XmlObjectsRepository
   {
      string directory;

      public XmlObjectsRepository(string directory)
      {
         this.directory = directory;
         if (!Directory.Exists(directory))
         {
            Directory.CreateDirectory(directory);
         }
      }

      public void Update<T>(T obj)
      {
         var filePath = ObjectFilePath<T>();
         if (File.Exists(filePath))
         {
            File.Delete(filePath);
         }
         File.WriteAllText(filePath, XmlUtils.Serialize<T>(obj));
      }

      public T Get<T>()
      {
         var content = File.ReadAllText(ObjectFilePath<T>());
         return XmlUtils.Unserialize<T>(content);
      }

      public bool Exists<T>()
      {
         return File.Exists(ObjectFilePath<T>());
      }

      private string ObjectFilePath<T>()
      {
         var fileName = typeof(T).Name;
         return Path.Combine(directory, fileName);
      }
   }
}
