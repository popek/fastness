﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Fastness.Common
{
   public static class XmlUtils
   {
      //public static string Serialize<T>(T value)
      //{
      //   if (value == null)
      //   {
      //      throw new InvalidOperationException(string.Format("null object of type {0} can't be serialised!", typeof(T).Name));
      //   }
      //   var serializer = new XmlSerializer(typeof(T));
      //   using (var output = new StringWriter())
      //   {
      //      serializer.Serialize(XmlWriter.Create(output), value);
      //      return output.ToString();
      //   }
      //}

      public static string Serialize<T>(T value)
      {
         var xmlSerializer = new XmlSerializer(value.GetType());

         using (var ms = new MemoryStream())
         {
            using (var xw = XmlWriter.Create(ms,
                new XmlWriterSettings()
                {
                   Encoding = new UTF8Encoding(false),
                   Indent = true,
                   NewLineOnAttributes = true,
                }))
            {
               xmlSerializer.Serialize(xw, value);
               return Encoding.UTF8.GetString(ms.ToArray());
            }
         }
      }

      public static T Unserialize<T>(string image)
      {
         if (string.IsNullOrEmpty(image))
         {
            throw new InvalidOperationException("not able to deserialize object from null or empty image!");
         }

         var deserializer = new XmlSerializer(typeof(T));
         using (var reader = XmlReader.Create(new StringReader(image)))
         {
            if (!deserializer.CanDeserialize(reader))
            {
               throw new InvalidOperationException(string.Format("not able to deserialize object from corruptem image: {0}", image));
            }

            return (T)deserializer.Deserialize(reader);
         }
      }

   }
}
