﻿using System.Configuration;
using System.Security;
using System.Xml;
using System.Xml.Serialization;

namespace Fastness.Common
{
   [XmlRoot("SmtpClientSettings")]
   public class SmtpClientSettings
   {
      [XmlAttribute("address")]
      public string ServerAddress { get; set; }

      [XmlAttribute("port")]
      public int ServerPort { get; set; }

      [XmlAttribute("useTls")]
      public bool UseTls { get; set; }

      [XmlAttribute("user")]
      public string User { get; set; }
      
      [XmlAttribute("password")]
      public string EncryptedPassword { get; set; }

      public void UpdatePassword(string plain)
      {
         EncryptedPassword = CryptoUtils.Encrypt(plain);
      }
      public SecureString GetPassword()
      {
         return CryptoUtils.Decrypt(EncryptedPassword);
      }
   }
}
