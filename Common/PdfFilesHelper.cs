﻿using System;
using System.Diagnostics;
using System.IO;

namespace Fastness.Common
{
   public class PdfFilesHelper
   {
      static public string PrepareReportFilePath(string reportAlias)
      {
         var dir = Path.Combine(SystemUtils.CurrentAssemblyPath, "reports");
         Directory.CreateDirectory(dir);
         return Path.Combine(dir, string.Format("{0}.pdf", reportAlias));
      }

      static public bool IsReportOpened(string reportAlias)
      { 
         FileStream fs = null;
         try
         {
            fs = File.OpenWrite(PrepareReportFilePath(reportAlias));
            return false;
         }
         catch (Exception)
         {
            return true;
         }
         finally
         {
            if (fs != null) 
               fs.Close();
         }
      }

      static public void Show(string pdfFilePath)
      {
         Process.Start(pdfFilePath);
      }
   }
}
