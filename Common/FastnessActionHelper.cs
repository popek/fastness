﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fastness.Common
{
   //
   public class FastnessActionHelper
   {
      public string ShortName { get; private set; }
      public string LongName { get; private set; }
      public int IndexOnInvoice {get; private set;}

      public FastnessActionHelper(string code, string description, int indexOnInvoice)
      {
         ShortName = code;
         LongName = description;
         IndexOnInvoice = indexOnInvoice;
      }
   }

   //
   public class FastnessActionsList
   {
      private static object sync = new object();
      private static List<FastnessActionHelper> availableActionFixedInfo;
      public static List<FastnessActionHelper> FixedList 
      { 
         get 
         {
            lock (sync)
            {
               if (availableActionFixedInfo == null)
               {
                  availableActionFixedInfo = new List<FastnessActionHelper>();

                  int cnt = 0;
                  availableActionFixedInfo.Add(new FastnessActionHelper("STORE", "Consignment", cnt++));
                  availableActionFixedInfo.Add(new FastnessActionHelper("COL", "Re-consignment", cnt++));
                  availableActionFixedInfo.Add(new FastnessActionHelper("RETRIEVE", "Routine Retrieval", cnt++));
                  availableActionFixedInfo.Add(new FastnessActionHelper("ONSITE", "On-site Retrieval", cnt++));
                  availableActionFixedInfo.Add(new FastnessActionHelper("RETRIEVE-OUT", "Retrieval out of Routine Times", cnt++));
                  availableActionFixedInfo.Add(new FastnessActionHelper("EME-RET", "2Hr Emergency Retrieval during Office Hrs", cnt++));
                  availableActionFixedInfo.Add(new FastnessActionHelper("SHRED", "Shredding out of Fastness Store", cnt++));
                  availableActionFixedInfo.Add(new FastnessActionHelper("FILE", "Routine File Consignment", cnt++));
                  availableActionFixedInfo.Add(new FastnessActionHelper("FILERET", "Routine File Retrieval", cnt++));
                  availableActionFixedInfo.Add(new FastnessActionHelper("FILEONSITE", "On-site File Retrieval", cnt++));
                  availableActionFixedInfo.Add(new FastnessActionHelper("FILERET-OUT", "File Retrieval out of Routine Times", cnt++));
                  availableActionFixedInfo.Add(new FastnessActionHelper("EME-FILERET", "2 Hr Emergency File Retrieval", cnt++));
                  availableActionFixedInfo.Add(new FastnessActionHelper("BOX-PURCH", "Box Purchase", cnt++));
                  availableActionFixedInfo.Add(new FastnessActionHelper("SACK-DEL", "Sack Delivery", cnt++));
                  availableActionFixedInfo.Add(new FastnessActionHelper("COLLECT-SHRED", "Collection for Shredding", cnt++));
                  availableActionFixedInfo.Add(new FastnessActionHelper("SHRED-COLLECTED", "Collected Shredding", cnt++));
                  availableActionFixedInfo.Add(new FastnessActionHelper("SHRED-SACK", "Shredding per Sack", cnt++));
                  availableActionFixedInfo.Add(new FastnessActionHelper("DOC-FAX", "Document Faxed", cnt++));
                  availableActionFixedInfo.Add(new FastnessActionHelper("SCAN", "Scan & Despatch Document in PDF Format via E-mail", cnt++));
                  availableActionFixedInfo.Add(new FastnessActionHelper("EME-CHG", "Emergency Charge", cnt++));
               }
               return availableActionFixedInfo;
            }
         } 
      }
   }
}
