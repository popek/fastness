﻿using System.Xml.Serialization;

namespace Fastness.Common
{
   [XmlRoot("MailTemplate")]
   public class MailTemplate
   {
      [XmlAttribute("sender")]
      public string Sender { get; set; }

      [XmlAttribute("subject")]
      public string SubjectTemplate { get; set; }

      [XmlAttribute("body")]
      public string BodyTemplate { get; set; }
   }
}
