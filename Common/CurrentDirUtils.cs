﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fastness.Common
{
   public class CurrentDirUtils
   {
      public static void ResetSubdir(string subdir)
      {
         var dir = Path.Combine(SystemUtils.CurrentAssemblyPath, subdir);
         
         if(Directory.Exists(dir))
            Directory.Delete(dir, true);

         Directory.CreateDirectory(dir);
      }

      public static string GenerateFilePath(string subdir, string filename)
      {
         return Path.Combine(SystemUtils.CurrentAssemblyPath, subdir, filename);
      }

      public static string GetSubdirPath(string subdir)
      {
         return Path.Combine(SystemUtils.CurrentAssemblyPath, subdir);
      }
   }
}
