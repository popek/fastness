﻿using System;
using System.IO;
using System.Reflection;

namespace Fastness.Common
{
   public static class SystemUtils
   {
      public static string CurrentAssemblyPath
      {
         get 
         {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            return Path.GetDirectoryName(path);
         }
      }
   }
}
