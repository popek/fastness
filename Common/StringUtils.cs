﻿
using System;
using System.Globalization;
namespace Fastness.Common
{
   public static class StringUtils
   {
      public static bool HasDigitsOnly(string value)
      { 
         for(var i = 0; i < value.Length; ++i)
         {
            if(!char.IsDigit(value[i]))
            {
               return false;
            }
         }
         return true;
      }

      public static bool ValidNumericValue(string text)
      { 
         double value = 0;
         return Double.TryParse(text, NumberStyles.Number, CultureInfo.CurrentCulture.NumberFormat, out value);
      }

      public static bool ValidNumericIntegerValue(string text)
      { 
         int value = 0;
         return Int32.TryParse(text, NumberStyles.Number, CultureInfo.CurrentCulture.NumberFormat, out value);
      }

      public static string Date(DateTime date)
      {
         return date.ToShortDateString();
      }
      public static string Date(Nullable<DateTime> date)
      {
         return Date(date.Value);
      }
   }
}
