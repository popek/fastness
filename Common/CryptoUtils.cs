﻿using System;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace Fastness.Common
{
   // based on http://weblogs.asp.net/jongalloway/encrypting-passwords-in-a-net-app-config-file
   public class CryptoUtils
   {
      static byte[] entropy = Encoding.Unicode.GetBytes("fdsakOfdsa98r3jnl dsa9803 2 -=+!");

      public static string Encrypt(string plainData)
      {
         byte[] encryptedData = ProtectedData.Protect(
             Encoding.Unicode.GetBytes(plainData),
             entropy,
             DataProtectionScope.CurrentUser);
         return Convert.ToBase64String(encryptedData);
      }

      public static SecureString Decrypt(string encryptedData)
      {
         try
         {
            byte[] decryptedData = ProtectedData.Unprotect(
                Convert.FromBase64String(encryptedData),
                entropy,
                DataProtectionScope.CurrentUser);
            return ToSecureString(Encoding.Unicode.GetString(decryptedData));
         }
         catch
         {
            return new SecureString();
         }
      }

      private static SecureString ToSecureString(string input)
      {
         SecureString secure = new SecureString();
         foreach (char c in input)
         {
            secure.AppendChar(c);
         }
         secure.MakeReadOnly();
         return secure;
      }
   }
}
