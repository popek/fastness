﻿
namespace Fastness.Common
{
   public class ActionUtils
   {
      public static readonly string ITEM_NUMBER_FOR_NO_ITEM_ACTION_HITORY = "0";

      public static readonly string ACTION_DESCRIPTION_CONSIGNMENT = "Consignment";
      public static readonly string ACTION_DESCRIPTION_RECONSIGNMENT = "Re-consignment";

      public static readonly string ACTION_DESCRIPTION_SHREADING = "Shredding out of Fastness Store";
      public static readonly string ACTION_DESCRIPTION_RETRIEVAL = "Routine Retrieval";
      public static readonly string ACTION_DESCRIPTION_ONSITERETRIEVAL = "On-site Retrieval";
      public static readonly string ACTION_DESCRIPTION_EMERETRIEVAL = "2Hr Emergency Retrieval during Office Hrs";

      public static bool IsAddingItemAction(string actionCode)
      {
         var subCode = GetActionCodeUniquePart(actionCode);
         return
            (subCode == "COL" ||
             subCode == "STORE");
      }

      public static bool IsReleasingItemAction(string actionCode)
      {
         var subCode = GetActionCodeUniquePart(actionCode);
         return
            (subCode == "SHRED" ||
             subCode == "EME-RET" ||
             subCode == "ONSITE" ||
             subCode == "RETRIEVE");
      }

      public static bool IsConsignment(string actionCode)
      {
         return GetActionCodeUniquePart(actionCode) == "STORE";
      }

      public static bool IsReconsignment(string actionCode)
      {
         return GetActionCodeUniquePart(actionCode) == "COL";
      }

      public static bool IsRetrieval(string actionCode)
      {
         return GetActionCodeUniquePart(actionCode) == "RETRIEVE";
      }

      public static bool IsShreading(string actionCode)
      {
         return GetActionCodeUniquePart(actionCode) == "SHRED";
      }

      public static bool IsAnyKindOfRetrieval(string actionCode)
      {
         return IsRetrieval(actionCode) ||  
                GetActionCodeUniquePart(actionCode) == "EME-RET" ||
                GetActionCodeUniquePart(actionCode) == "ONSITE" ||
                GetActionCodeUniquePart(actionCode) == "RETRIEVE-OUT";
      }

      public static bool IsGenericWithoutItem(string actionCode)
      {
         var uniquePart = GetActionCodeUniquePart(actionCode);
         return uniquePart == "COLLECT-SHRED" ||
                uniquePart == "SHRED-SACK" ||
                uniquePart == "BOX-PURCH" ||
                uniquePart == "SACK-DEL" ||
                uniquePart == "EME-CHG";
      }

      private static string GetActionCodeUniquePart(string actionCode)
      {
         return actionCode.Substring(3);
      }

      public static string GetServiceLevelFromActonCode(string actionCode)
      {
         var actionCodePrefix = actionCode.Substring(0, 2);
         if (actionCodePrefix == "SH")
         {
            return FastnesConsts.SERVICE_LEVEL_SHALLOW;
         }
         if (actionCodePrefix == "DP")
         {
            return FastnesConsts.SERVICE_LEVEL_DEEP;
         }
         return FastnesConsts.SERVICE_LEVEL_STANDARD;
      }
   }
}
