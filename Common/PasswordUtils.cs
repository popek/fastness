﻿using System;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace Fastness.Common
{
   public class PasswordUtils
   {
      public static string CalculateHash(string password)
      {
         using (var hasher = MD5.Create())
         {
            return GetMD5Hash(hasher, password);
         }
      }

      public static bool IsPasswordOk(string input, string passwordHash)
      {
         using (var hasher = MD5.Create())
         {
            string hashOfInput = GetMD5Hash(hasher, input);
            return 0 == StringComparer.OrdinalIgnoreCase.Compare(hashOfInput, passwordHash);
         }
      }

      private static string GetMD5Hash(MD5 hasher, string plainData)
      {
         var sb = new StringBuilder();
         foreach (var b in hasher.ComputeHash(Encoding.UTF8.GetBytes(plainData)))
         {
            sb.Append(b.ToString("x2"));
         }
         return sb.ToString();
      }

      private static SecureString ToSecureString(string input)
      {
         SecureString secure = new SecureString();
         foreach (char c in input)
         {
            secure.AppendChar(c);
         }
         secure.MakeReadOnly();
         return secure;
      }
   }
}
