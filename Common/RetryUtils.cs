﻿using System;
using System.Threading;

namespace Fastness.Common
{
   public class RetryUtils
   {
      public static void RetryIfException(Action task, int millisecondsBetweenAttempt, int maxRetriesCount)
      {
         if (maxRetriesCount < 1)
         {
            throw new InvalidOperationException("max retries count must be grater then 0!");
         }

         while (true)
         {
            try
            {
               task();
               return;
            }
            catch (Exception)
            {
               // rethrow in case max retires had been done
               --maxRetriesCount;
               if (maxRetriesCount == 0)
               {
                  throw;
               }
               Thread.Sleep(millisecondsBetweenAttempt);
            }            
         }
      }
   }
}
