﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fastness.Common
{
   public static class FastnesConsts
   {
      public static readonly string SERVICE_LEVEL_DEEP = "deep";
      public static readonly string SERVICE_LEVEL_SHALLOW = "shallow";
      public static readonly string SERVICE_LEVEL_STANDARD = "standard";
   }
}
