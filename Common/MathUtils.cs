﻿using System;

namespace Fastness.Common
{
   public static class MathUtils
   {
      public static bool AreEqual(double l, double r, double marginInPercents)
      {
         var eps = Math.Abs(l * marginInPercents / 100);
         
         return (l <= r + eps) && (l >= r - eps);         
      }

      public static bool AreEqual(double l, double r)
      {
         return AreEqual(l, r, .1);
      }
   }
}
