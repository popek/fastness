﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fastness.DataModel;

namespace correctDb
{
   public class CorrectActionsCharges : TableCorrector
   {
      protected override void CorrectInternal(fastnessEntities ctx)
      {
         var firstDate = new DateTime(2015, 08, 01);
         var actions = from ah in ctx.ActionHistory
                       where (
                             (
                             ah.ContractActionID == 1171 ||  // CP1(67) consignment
                             ah.ContractActionID == 1172 ||  // CP1(67) reconsignment
                             ah.ContractActionID == 1834 ||  // CP-FIN(105) consignment
                             ah.ContractActionID == 1835 ||  // CP-FIN(105) reconsignment
                             ah.ContractActionID == 1906 ||  // CP-HR(109) consignment
                             ah.ContractActionID == 1907     // CP-HR(109) reconsignment
                             )
                             &&
                             ah.DateCompleted >= firstDate
                             )
                       select ah;

         foreach (var ah in actions)
         {
            ah.Charge = 0.155;
            ctx.SetAsModified(ah);
         }
      }
   }
}
