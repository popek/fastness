﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fastness.DataModel;

namespace correctDb
{
   public abstract class TableCorrector
   {
      public void Correct()
      {
         using (var ctx = new fastnessEntities())
         {
            ctx.Configuration.AutoDetectChangesEnabled = false;

            CorrectInternal(ctx);

            ctx.Configuration.AutoDetectChangesEnabled = true;

            ctx.ForceDetectChanges();
            ctx.SaveChanges();
         }
      }
      protected abstract void CorrectInternal(fastnessEntities ctx);
   }
}
