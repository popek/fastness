﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fastness.DataModel;
using Fastness.Invoicing;

namespace correctDb
{
   public class CorrectCP1Inoice201605 : TableCorrector
   {
      protected override void CorrectInternal(fastnessEntities ctx)
      {
         var invHistory = (from ih in ctx.InvoiceHistory
                           where (ih.Year == 2016 && ih.Month == 5 && ih.ContractID == 67)
                           select ih).First();

         var handler = new InvoiceHistoryDetailsHandler();
         var invoice = handler.FromDetails(invHistory.Details);
         invoice.Entries[1].Charge = 58.745; // change consignment charge
         invoice.Entries[2].Charge = 61.070; // change reconsignment charge

         invHistory.Details = handler.ToDetails(invoice);

         ctx.SetAsModified(invHistory);

//7194.535; 1438.907
//4968.370; 32054; Units at the beginning of the month
//58.745; 379; Consignment
//61.070; 394; Re - consignment
//2013.650; 393; Routine Retrieval
//92.700; 3; 2Hr Emergency Retrieval during Office Hrs




      }
   }
}
