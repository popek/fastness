﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace correctDb
{
   class Program
   {
      static void Main(string[] args)
      {
         foreach(var correction in new List<TableCorrector> {
            new CorrectActionsCharges(),
            new CorrectCP1Inoice201605()
         })
         {
            correction.Correct();
         }
      }
   }
}
