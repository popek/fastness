﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FastnessForms.ThirdParty;
using FastnessForms.Views;

// this file is a modification of project: http://www.codeproject.com/Articles/18440/DataGridView-Multi-column-Sort

namespace FastnessForms.ThirdParty
{
   public class MultiColumnSortDataGridViewManager<T> : DataGridViewManager<T> where T : class
   {
      DataGridComparer columnSorter;

      public override T CurrentValue
      {
         get
         {
            return dgv.SelectedRows.Count > 0 ? dgv.SelectedRows[0].Tag as T : null;
         }
      }

      public MultiColumnSortDataGridViewManager(DataGridView dgv, int[] paramsIndexesToHide)
         : base(dgv, paramsIndexesToHide)
      {
         columnSorter = new DataGridComparer(dgv);
         dgv.ColumnHeaderMouseClick += (s, e) => OnColumnHeaderMouseClick(e);
      }

      public string SortOrderDescription
      {
         get { return columnSorter.SortOrderDescription; }
      }

      private void OnColumnHeaderMouseClick(DataGridViewCellMouseEventArgs e)
      {
         dgv.Columns[e.ColumnIndex].SortMode = DataGridViewColumnSortMode.Programmatic;
         columnSorter.SetSortColumn(e.ColumnIndex, Control.ModifierKeys);
         dgv.Sort(columnSorter);
      }
      protected override void Reload()
      {
         dgv.Rows.Clear();
         dgv.Columns.Clear();

         var index = 0;
         var columns = 
            from prop in Props
            select new DataGridViewColumn
            {
               CellTemplate = new DataGridViewTextBoxCell(),
               DataPropertyName = prop.Name,
               HeaderText = prop.Name,
               Name = prop.Name,
               ReadOnly = dgv.ReadOnly,
               Resizable = DataGridViewTriState.True,
               ValueType = prop.GetType(),

               DisplayIndex = index++
            };

         foreach(var col in columns)
         {
            dgv.Columns.Add(col);
         }

         foreach (T item in currentContent)
         {
            dgv.Rows.Add((
               from prop in Props
               select prop.GetValue(item)
               ).ToArray());

            dgv.Rows[dgv.Rows.Count - 1].Tag = item;
         }
      }
   }
}
