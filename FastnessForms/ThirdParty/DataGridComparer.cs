//
// Mobius.Utility.SortedDataGridView
//
// Authors:
//    Paul T Anderson (paul.t.anderson at gmail)
//
// (C) 2007 Paul T Anderson
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

// this file is a modification of project: http://www.codeproject.com/Articles/18440/DataGridView-Multi-column-Sort
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using System.ComponentModel;
using System.Diagnostics;

namespace FastnessForms.ThirdParty
{
	#region Data Grid Column Comparer

	public class DataGridComparer : System.Collections.IComparer
	{
		DataGridView _grid;
		List<SortColDefn> _sortedColumns;
		int _maxSortColumns;

		private class SortColDefn
		{
			internal Int16 colNum;
			internal bool ascending;

			internal SortColDefn(int columnNum, SortOrder sortOrder)
			{
				colNum = Convert.ToInt16(columnNum);
				ascending = (sortOrder != SortOrder.Descending);
			}
		}

		public DataGridComparer(DataGridView datagrid)
		{
			_grid = datagrid;

			// No limit by default to the number of columns sorted.
			_maxSortColumns = 0;

			_sortedColumns = new List<SortColDefn>(_maxSortColumns);
		}

		public int MaxSortColumns
		{
			get { return _sortedColumns.Capacity; }
			set
			{
				if (_sortedColumns.Count > value)
					_sortedColumns.RemoveRange(value - 1, _sortedColumns.Count);

				_sortedColumns.Capacity = value;
			}
		}
      public SortOrder SetSortColumn(int columnIndex, Keys ModifierKeys)
		{
         var updateGlyph = new Func<SortOrder, SortOrder>((SortOrder order) =>
         {
            _grid.Columns[columnIndex].HeaderCell.SortGlyphDirection = order;
            return order;
         });

			bool removeSortedColumn = ((ModifierKeys & Keys.Control) == Keys.Control);
         if(removeSortedColumn)
         {
            _sortedColumns.RemoveAll(scd => scd.colNum == columnIndex);
            return updateGlyph(SortOrder.None);
         }

         var sortedColIndex = _sortedColumns.FindIndex(scd => scd.colNum == columnIndex);
         if(sortedColIndex == -1)
         {
            // If got to limit of num sorted columns, remove the current last one.
            if (_maxSortColumns > 0 && _sortedColumns.Count == _sortedColumns.Capacity)
               _sortedColumns.RemoveAt(_sortedColumns.Count - 1);

            // adding new column to sorted columns
            _sortedColumns.Add(new SortColDefn(columnIndex, SortOrder.Ascending));
            return updateGlyph(SortOrder.Ascending);
         }
         // if column is exists then toggle sorting order
         var sortedCol = _sortedColumns[sortedColIndex];
         if(sortedCol.ascending)
         {
            sortedCol.ascending = false;
            return updateGlyph(SortOrder.Descending);
         }
         else
         {
            sortedCol.ascending = true;
            return updateGlyph(SortOrder.Ascending);
         }
		}

		public string SortOrderDescription
		{
			get
			{
				StringBuilder sb = new StringBuilder("Sorted by ");

				foreach (SortColDefn colDefn in _sortedColumns)
				{
					sb.Append(_grid.Columns[colDefn.colNum].HeaderText);
					sb.Append(colDefn.ascending ? " ASC, " : " DESC, ");
				}

				sb.Length -= 2;

				return sb.ToString();
			}
		}

		public int Compare(DataGridViewCellCollection lhs, DataGridViewCellCollection rhs)
		{
			foreach (SortColDefn colDefn in _sortedColumns)
			{
				int retval = Comparer<object>.Default.Compare(
					lhs[colDefn.colNum].Value,
					rhs[colDefn.colNum].Value);

				if (retval != 0)
					return (colDefn.ascending ? retval : -retval);
			}

			// These two rows are indistinguishable.
			return 0;
		}

		#region IComparer Members

		public int Compare(object x, object y)
		{
			DataGridViewRow lhs = x as DataGridViewRow;
			DataGridViewRow rhs = y as DataGridViewRow;

			return Compare(lhs.Cells, rhs.Cells);
		}

		#endregion
	}

	#endregion
}
