﻿namespace FastnessForms.Views
{
   partial class ActionReconsignForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ActionReconsignForm));
      this.txtCharge = new System.Windows.Forms.TextBox();
      this.txtConfirmItemNumber = new System.Windows.Forms.TextBox();
      this.txtAuthoriziedBy = new System.Windows.Forms.TextBox();
      this.txtLocation = new System.Windows.Forms.TextBox();
      this.txtPercentage = new System.Windows.Forms.TextBox();
      this.txtType = new System.Windows.Forms.TextBox();
      this.txtDescription = new System.Windows.Forms.TextBox();
      this.txtClientItemReference = new System.Windows.Forms.TextBox();
      this.txtItemNumber = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.label8 = new System.Windows.Forms.Label();
      this.label9 = new System.Windows.Forms.Label();
      this.label10 = new System.Windows.Forms.Label();
      this.label11 = new System.Windows.Forms.Label();
      this.btReconsign = new System.Windows.Forms.Button();
      this.dtpReconsignment = new System.Windows.Forms.DateTimePicker();
      this.btClose = new System.Windows.Forms.Button();
      this.btShow = new System.Windows.Forms.Button();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.btAllowChargeChange = new System.Windows.Forms.Button();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.groupBox1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.SuspendLayout();
      // 
      // txtCharge
      // 
      this.txtCharge.AcceptsReturn = true;
      this.txtCharge.BackColor = System.Drawing.SystemColors.Window;
      this.txtCharge.Cursor = System.Windows.Forms.Cursors.IBeam;
      this.txtCharge.ForeColor = System.Drawing.SystemColors.WindowText;
      this.txtCharge.Location = new System.Drawing.Point(89, 19);
      this.txtCharge.MaxLength = 0;
      this.txtCharge.Name = "txtCharge";
      this.txtCharge.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtCharge.Size = new System.Drawing.Size(49, 20);
      this.txtCharge.TabIndex = 0;
      // 
      // txtConfirmItemNumber
      // 
      this.txtConfirmItemNumber.AcceptsReturn = true;
      this.txtConfirmItemNumber.BackColor = System.Drawing.SystemColors.Window;
      this.txtConfirmItemNumber.Cursor = System.Windows.Forms.Cursors.IBeam;
      this.txtConfirmItemNumber.ForeColor = System.Drawing.SystemColors.WindowText;
      this.txtConfirmItemNumber.ImeMode = System.Windows.Forms.ImeMode.Disable;
      this.txtConfirmItemNumber.Location = new System.Drawing.Point(86, 48);
      this.txtConfirmItemNumber.MaxLength = 0;
      this.txtConfirmItemNumber.Name = "txtConfirmItemNumber";
      this.txtConfirmItemNumber.PasswordChar = '*';
      this.txtConfirmItemNumber.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtConfirmItemNumber.Size = new System.Drawing.Size(89, 20);
      this.txtConfirmItemNumber.TabIndex = 1;
      // 
      // txtAuthoriziedBy
      // 
      this.txtAuthoriziedBy.AcceptsReturn = true;
      this.txtAuthoriziedBy.BackColor = System.Drawing.SystemColors.Window;
      this.txtAuthoriziedBy.Cursor = System.Windows.Forms.Cursors.IBeam;
      this.txtAuthoriziedBy.ForeColor = System.Drawing.SystemColors.WindowText;
      this.txtAuthoriziedBy.Location = new System.Drawing.Point(89, 74);
      this.txtAuthoriziedBy.MaxLength = 0;
      this.txtAuthoriziedBy.Name = "txtAuthoriziedBy";
      this.txtAuthoriziedBy.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtAuthoriziedBy.Size = new System.Drawing.Size(135, 20);
      this.txtAuthoriziedBy.TabIndex = 2;
      this.txtAuthoriziedBy.Tag = "0";
      // 
      // txtLocation
      // 
      this.txtLocation.AcceptsReturn = true;
      this.txtLocation.BackColor = System.Drawing.SystemColors.Window;
      this.txtLocation.Cursor = System.Windows.Forms.Cursors.IBeam;
      this.txtLocation.ForeColor = System.Drawing.SystemColors.WindowText;
      this.txtLocation.Location = new System.Drawing.Point(86, 219);
      this.txtLocation.MaxLength = 0;
      this.txtLocation.Name = "txtLocation";
      this.txtLocation.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtLocation.Size = new System.Drawing.Size(89, 20);
      this.txtLocation.TabIndex = 6;
      // 
      // txtPercentage
      // 
      this.txtPercentage.AcceptsReturn = true;
      this.txtPercentage.BackColor = System.Drawing.SystemColors.Window;
      this.txtPercentage.Cursor = System.Windows.Forms.Cursors.IBeam;
      this.txtPercentage.Enabled = false;
      this.txtPercentage.ForeColor = System.Drawing.SystemColors.WindowText;
      this.txtPercentage.Location = new System.Drawing.Point(86, 193);
      this.txtPercentage.MaxLength = 0;
      this.txtPercentage.Name = "txtPercentage";
      this.txtPercentage.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtPercentage.Size = new System.Drawing.Size(41, 20);
      this.txtPercentage.TabIndex = 5;
      this.txtPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // txtType
      // 
      this.txtType.AcceptsReturn = true;
      this.txtType.BackColor = System.Drawing.SystemColors.Window;
      this.txtType.Cursor = System.Windows.Forms.Cursors.IBeam;
      this.txtType.Enabled = false;
      this.txtType.ForeColor = System.Drawing.SystemColors.WindowText;
      this.txtType.Location = new System.Drawing.Point(86, 167);
      this.txtType.MaxLength = 0;
      this.txtType.Name = "txtType";
      this.txtType.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtType.Size = new System.Drawing.Size(89, 20);
      this.txtType.TabIndex = 4;
      // 
      // txtDescription
      // 
      this.txtDescription.AcceptsReturn = true;
      this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
      this.txtDescription.Cursor = System.Windows.Forms.Cursors.IBeam;
      this.txtDescription.Enabled = false;
      this.txtDescription.ForeColor = System.Drawing.SystemColors.WindowText;
      this.txtDescription.Location = new System.Drawing.Point(86, 112);
      this.txtDescription.MaxLength = 0;
      this.txtDescription.Multiline = true;
      this.txtDescription.Name = "txtDescription";
      this.txtDescription.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtDescription.Size = new System.Drawing.Size(234, 49);
      this.txtDescription.TabIndex = 3;
      // 
      // txtClientItemReference
      // 
      this.txtClientItemReference.AcceptsReturn = true;
      this.txtClientItemReference.BackColor = System.Drawing.SystemColors.Window;
      this.txtClientItemReference.Cursor = System.Windows.Forms.Cursors.IBeam;
      this.txtClientItemReference.Enabled = false;
      this.txtClientItemReference.ForeColor = System.Drawing.SystemColors.WindowText;
      this.txtClientItemReference.Location = new System.Drawing.Point(86, 86);
      this.txtClientItemReference.MaxLength = 0;
      this.txtClientItemReference.Name = "txtClientItemReference";
      this.txtClientItemReference.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtClientItemReference.Size = new System.Drawing.Size(89, 20);
      this.txtClientItemReference.TabIndex = 2;
      // 
      // txtItemNumber
      // 
      this.txtItemNumber.AcceptsReturn = true;
      this.txtItemNumber.BackColor = System.Drawing.SystemColors.Window;
      this.txtItemNumber.Cursor = System.Windows.Forms.Cursors.IBeam;
      this.txtItemNumber.ForeColor = System.Drawing.SystemColors.WindowText;
      this.txtItemNumber.Location = new System.Drawing.Point(86, 22);
      this.txtItemNumber.MaxLength = 0;
      this.txtItemNumber.Name = "txtItemNumber";
      this.txtItemNumber.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtItemNumber.Size = new System.Drawing.Size(89, 20);
      this.txtItemNumber.TabIndex = 0;
      this.txtItemNumber.Text = "100";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(6, 25);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(65, 13);
      this.label1.TabIndex = 48;
      this.label1.Text = "Box Number";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(6, 51);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(63, 13);
      this.label2.TabIndex = 49;
      this.label2.Text = "Confirm Box";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(6, 89);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(80, 13);
      this.label3.TabIndex = 50;
      this.label3.Text = "Item Reference";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(6, 115);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(60, 13);
      this.label4.TabIndex = 51;
      this.label4.Text = "Description";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(6, 170);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(31, 13);
      this.label6.TabIndex = 53;
      this.label6.Text = "Type";
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(6, 196);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(62, 13);
      this.label7.TabIndex = 54;
      this.label7.Text = "Percentage";
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(6, 222);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(48, 13);
      this.label8.TabIndex = 55;
      this.label8.Text = "Location";
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(9, 51);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(30, 13);
      this.label9.TabIndex = 56;
      this.label9.Text = "Date";
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(9, 77);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(71, 13);
      this.label10.TabIndex = 57;
      this.label10.Text = "Authorized by";
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(9, 22);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(41, 13);
      this.label11.TabIndex = 58;
      this.label11.Text = "Charge";
      // 
      // btReconsign
      // 
      this.btReconsign.Location = new System.Drawing.Point(103, 389);
      this.btReconsign.Name = "btReconsign";
      this.btReconsign.Size = new System.Drawing.Size(104, 35);
      this.btReconsign.TabIndex = 3;
      this.btReconsign.Text = "Re-Consign Box";
      this.btReconsign.UseVisualStyleBackColor = true;
      this.btReconsign.Click += new System.EventHandler(this.btReconsign_Click);
      // 
      // dtpReconsignment
      // 
      this.dtpReconsignment.Location = new System.Drawing.Point(89, 45);
      this.dtpReconsignment.Name = "dtpReconsignment";
      this.dtpReconsignment.Size = new System.Drawing.Size(135, 20);
      this.dtpReconsignment.TabIndex = 1;
      // 
      // btClose
      // 
      this.btClose.Location = new System.Drawing.Point(258, 389);
      this.btClose.Name = "btClose";
      this.btClose.Size = new System.Drawing.Size(85, 35);
      this.btClose.TabIndex = 4;
      this.btClose.Text = "Close";
      this.btClose.UseVisualStyleBackColor = true;
      this.btClose.Click += new System.EventHandler(this.btClose_Click);
      // 
      // btShow
      // 
      this.btShow.Location = new System.Drawing.Point(12, 389);
      this.btShow.Name = "btShow";
      this.btShow.Size = new System.Drawing.Size(85, 35);
      this.btShow.TabIndex = 2;
      this.btShow.Text = "Show";
      this.btShow.UseVisualStyleBackColor = true;
      this.btShow.Click += new System.EventHandler(this.btShow_Click);
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.btAllowChargeChange);
      this.groupBox1.Controls.Add(this.txtCharge);
      this.groupBox1.Controls.Add(this.txtAuthoriziedBy);
      this.groupBox1.Controls.Add(this.label11);
      this.groupBox1.Controls.Add(this.label10);
      this.groupBox1.Controls.Add(this.label9);
      this.groupBox1.Controls.Add(this.dtpReconsignment);
      this.groupBox1.Location = new System.Drawing.Point(12, 267);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(331, 105);
      this.groupBox1.TabIndex = 1;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Action details";
      // 
      // btAllowChargeChange
      // 
      this.btAllowChargeChange.Location = new System.Drawing.Point(144, 15);
      this.btAllowChargeChange.Name = "btAllowChargeChange";
      this.btAllowChargeChange.Size = new System.Drawing.Size(118, 27);
      this.btAllowChargeChange.TabIndex = 14;
      this.btAllowChargeChange.TabStop = false;
      this.btAllowChargeChange.Text = "Allow charge change";
      this.btAllowChargeChange.UseVisualStyleBackColor = true;
      this.btAllowChargeChange.Click += new System.EventHandler(this.btAllowChargeChange_Click);
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.label1);
      this.groupBox2.Controls.Add(this.txtItemNumber);
      this.groupBox2.Controls.Add(this.txtClientItemReference);
      this.groupBox2.Controls.Add(this.txtDescription);
      this.groupBox2.Controls.Add(this.txtType);
      this.groupBox2.Controls.Add(this.label8);
      this.groupBox2.Controls.Add(this.txtPercentage);
      this.groupBox2.Controls.Add(this.label7);
      this.groupBox2.Controls.Add(this.txtLocation);
      this.groupBox2.Controls.Add(this.label6);
      this.groupBox2.Controls.Add(this.txtConfirmItemNumber);
      this.groupBox2.Controls.Add(this.label4);
      this.groupBox2.Controls.Add(this.label2);
      this.groupBox2.Controls.Add(this.label3);
      this.groupBox2.Location = new System.Drawing.Point(12, 12);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(331, 249);
      this.groupBox2.TabIndex = 0;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Box details";
      // 
      // ActionReconsignForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(353, 435);
      this.Controls.Add(this.groupBox2);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.btShow);
      this.Controls.Add(this.btClose);
      this.Controls.Add(this.btReconsign);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "ActionReconsignForm";
      this.Text = "Re-Consignment";
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      this.ResumeLayout(false);

      }

      #endregion

      public System.Windows.Forms.TextBox txtCharge;
      public System.Windows.Forms.TextBox txtConfirmItemNumber;
      public System.Windows.Forms.TextBox txtAuthoriziedBy;
      public System.Windows.Forms.TextBox txtLocation;
      public System.Windows.Forms.TextBox txtPercentage;
      public System.Windows.Forms.TextBox txtType;
      public System.Windows.Forms.TextBox txtDescription;
      public System.Windows.Forms.TextBox txtClientItemReference;
      public System.Windows.Forms.TextBox txtItemNumber;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.Label label10;
      private System.Windows.Forms.Label label11;
      private System.Windows.Forms.Button btReconsign;
      private System.Windows.Forms.DateTimePicker dtpReconsignment;
      private System.Windows.Forms.Button btClose;
      private System.Windows.Forms.Button btShow;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.GroupBox groupBox2;
      private System.Windows.Forms.Button btAllowChargeChange;
   }
}