﻿using System;
using System.Linq;
using System.Windows.Forms;
using Fastness.DataModel.Utils;
using Fastness.DataModel;
using Fastness.Common;
using Fastness.DataModel.Transaction;
using Fastness.Common.Events;

namespace FastnessForms.Views
{
   public partial class UpdateActionHisotryEntryForm : Form
   {
      fastnessEntities dbCtx = new fastnessEntities();
      ActionHistory targetAction;

      public event DbContentChangedEventHandler DBChanged;

      string actionCode;

      bool locationChanged = false;
      bool chargeChanged = false;
      bool authorizationChanged = false;
      bool notesChanged = false;
      bool dateChanged = false;
      bool UnsavedChangesOnBoard
      {
         get 
         {
            return 
               locationChanged ||
               chargeChanged ||
               authorizationChanged ||
               notesChanged ||
               dateChanged;
         }
      }
      ItemDetail itemLinkedWithHistoryEntry;
      ItemDetail LinkedItem
      {
         get
         {
            if (itemLinkedWithHistoryEntry == null)
            {
               itemLinkedWithHistoryEntry = new FetchFromDb(dbCtx).ItemBasedOnItemNo(targetAction.ItemNumber);
            }
            return itemLinkedWithHistoryEntry;
         }
      }

      bool LocationChangeAllowed
      {
         get
         {
            if (!ActionUtils.IsAddingItemAction(actionCode))
               return false;

            if(ActionUtils.IsReconsignment(actionCode))
            {
               if (LinkedItem.DateDestroyed != null || LinkedItem.DateRetrieved != null)
                  return false;
               // only latest reconsignment allows to change location
               return new AskDb(dbCtx).IsItLatestReconsignment(targetAction);
            }

            if (ActionUtils.IsConsignment(actionCode))
               return LinkedItem.DateDestroyed == null && LinkedItem.DateRetrieved == null && LinkedItem.DateReturned == null;

            return true;
         }
      }

      public UpdateActionHisotryEntryForm(ActionHistory targetAction)
      {
         InitializeComponent();

         this.targetAction = targetAction;

         actionCode = (from ca in dbCtx.ContractAction
                       where ca.ContractActionID == targetAction.ContractActionID
                       select ca.Code).First();

         txtLocation.Enabled = LocationChangeAllowed;
         txtLocation.Text = targetAction.Location;
         if (txtLocation.Enabled)
         {
            txtLocation.AddKeyPressWithTextLengthLimitAndIntegerValue(DBFieldsConstraint.LOCATION_NUMBER_LENGTH);
            txtLocation.AddTextChangeHandler(() => targetAction.Location, (textChanged) =>
            {
               locationChanged = textChanged;
               HandleChange();
            });
         }

         txtCharge.AddKeyPressWithNumericValue();
         txtCharge.AddTextChangeHandler(() => targetAction.Charge.ToString(), (textChanged) =>
         {
            chargeChanged = textChanged;
            HandleChange();
         });
         txtCharge.Text = targetAction.Charge.ToString();

         txtAuthoriziedBy.AddKeyPressWithTextLengthLimit(DBFieldsConstraint.ACTION_HISTORY_AUTHORIZED_BY_MAX_LENGHT);
         txtAuthoriziedBy.AddTextChangeHandler(() => targetAction.AuthorisedBy??"", (textChanged) =>
         {
            authorizationChanged = textChanged;
            HandleChange();
         });
         txtAuthoriziedBy.Text = targetAction.AuthorisedBy;

         txtNotes.AddKeyPressWithTextLengthLimit(DBFieldsConstraint.ACTION_HISTORY_NOTES_MAX_LENGHT);
         txtNotes.AddTextChangeHandler(() => targetAction.Notes??"", (textChanged) =>
         {
            notesChanged = textChanged;
            HandleChange();
         });
         txtNotes.Text = targetAction.Notes;

         dtpDate.ValueChanged += (s, e) =>
         {
            dateChanged = targetAction.DateCompleted.Value != dtpDate.Value;
            HandleChange();
         };
         dtpDate.Value = targetAction.DateCompleted.Value;

         lbAction.Text = targetAction.Description;

         HandleChange();
      }
      private void HandleChange()
      {
         btSaveChanges.Enabled = !LoggedUserUtils.CanEditDBContent() ? false : UnsavedChangesOnBoard;
      }

      private bool CheckFormData()
      {
         if (locationChanged)
         {
            if (!txtLocation.IsTextLenthExactlyTheSame(DBFieldsConstraint.LOCATION_NUMBER_LENGTH, "Location"))
            {
               return false;
            }
            if (!txtLocation.IsDigitsOnlyText("Location"))
            {
               return false;
            }
            if (new AskDb(dbCtx).LocationOccupied(txtLocation.Text))
            {
               txtLocation.NotifyTextError("Location is already ocupied!");
               return false;
            }
         }

         if (!txtCharge.IsValidNumericValueFormat("Charge"))
         {
            return false;
         }
         if (txtNotes.IsTextToLong(DBFieldsConstraint.ACTION_HISTORY_NOTES_MAX_LENGHT, "notes"))
         {
            return false;
         }
         if (txtAuthoriziedBy.IsTextToLong(DBFieldsConstraint.ACTION_HISTORY_AUTHORIZED_BY_MAX_LENGHT, "Authorization name"))
         {
            return false;
         }
         return true;
      }
      private void btSaveChanges_Click(object sender, EventArgs e)
      {
         if (!CheckFormData())
         {
            return;
         }

         ModifyDb.RunTransaction(freshDbCtx => new UpdateActionHistoryEntry(
            targetAction, 
            txtLocation.Text, 
            Double.Parse(txtCharge.Text), 
            dtpDate.Value, 
            txtAuthoriziedBy.Text, 
            txtNotes.Text, 
            actionCode,
            freshDbCtx).Run());

         DBChanged(null);

         UserInteraction.ShowInfo("changes in history entry finished succesfully");
         Close();
      }

      private void btClose_Click(object sender, EventArgs e)
      {
         Close();
      }
   }
}
