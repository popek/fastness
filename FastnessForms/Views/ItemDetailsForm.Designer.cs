﻿namespace FastnessForms.Views
{
   partial class ItemDetailsForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemDetailsForm));
         this.txtBoxPercentage = new System.Windows.Forms.TextBox();
         this.txtDescription = new System.Windows.Forms.TextBox();
         this.groupBox1 = new System.Windows.Forms.GroupBox();
         this.btShowAll = new System.Windows.Forms.Button();
         this.btShowRange = new System.Windows.Forms.Button();
         this.label2 = new System.Windows.Forms.Label();
         this.label1 = new System.Windows.Forms.Label();
         this.dtpTo = new System.Windows.Forms.DateTimePicker();
         this.dtpFrom = new System.Windows.Forms.DateTimePicker();
         this.dgvItemsHistory = new System.Windows.Forms.DataGridView();
         this.btOK = new System.Windows.Forms.Button();
         this.btAllowChange = new System.Windows.Forms.Button();
         this.groupBox2 = new System.Windows.Forms.GroupBox();
         this.label4 = new System.Windows.Forms.Label();
         this.destructionDate = new System.Windows.Forms.TextBox();
         this.label3 = new System.Windows.Forms.Label();
         this.lbDescription = new System.Windows.Forms.Label();
         this.btShowReport = new System.Windows.Forms.Button();
         this.label5 = new System.Windows.Forms.Label();
         this.txtCuid = new System.Windows.Forms.TextBox();
         this.groupBox1.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.dgvItemsHistory)).BeginInit();
         this.groupBox2.SuspendLayout();
         this.SuspendLayout();
         // 
         // txtBoxPercentage
         // 
         this.txtBoxPercentage.AcceptsReturn = true;
         this.txtBoxPercentage.BackColor = System.Drawing.SystemColors.Window;
         this.txtBoxPercentage.Cursor = System.Windows.Forms.Cursors.IBeam;
         this.txtBoxPercentage.Enabled = false;
         this.txtBoxPercentage.ForeColor = System.Drawing.SystemColors.WindowText;
         this.txtBoxPercentage.Location = new System.Drawing.Point(77, 84);
         this.txtBoxPercentage.MaxLength = 0;
         this.txtBoxPercentage.Name = "txtBoxPercentage";
         this.txtBoxPercentage.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.txtBoxPercentage.Size = new System.Drawing.Size(130, 20);
         this.txtBoxPercentage.TabIndex = 1;
         // 
         // txtDescription
         // 
         this.txtDescription.AcceptsReturn = true;
         this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
         this.txtDescription.Cursor = System.Windows.Forms.Cursors.IBeam;
         this.txtDescription.Enabled = false;
         this.txtDescription.ForeColor = System.Drawing.SystemColors.WindowText;
         this.txtDescription.Location = new System.Drawing.Point(77, 19);
         this.txtDescription.MaxLength = 0;
         this.txtDescription.Multiline = true;
         this.txtDescription.Name = "txtDescription";
         this.txtDescription.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.txtDescription.Size = new System.Drawing.Size(130, 46);
         this.txtDescription.TabIndex = 0;
         // 
         // groupBox1
         // 
         this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.groupBox1.Controls.Add(this.btShowAll);
         this.groupBox1.Controls.Add(this.btShowRange);
         this.groupBox1.Controls.Add(this.label2);
         this.groupBox1.Controls.Add(this.label1);
         this.groupBox1.Controls.Add(this.dtpTo);
         this.groupBox1.Controls.Add(this.dtpFrom);
         this.groupBox1.Location = new System.Drawing.Point(12, 289);
         this.groupBox1.Name = "groupBox1";
         this.groupBox1.Size = new System.Drawing.Size(353, 80);
         this.groupBox1.TabIndex = 33;
         this.groupBox1.TabStop = false;
         this.groupBox1.Text = "Show actions";
         // 
         // btShowAll
         // 
         this.btShowAll.Location = new System.Drawing.Point(268, 25);
         this.btShowAll.Name = "btShowAll";
         this.btShowAll.Size = new System.Drawing.Size(77, 34);
         this.btShowAll.TabIndex = 7;
         this.btShowAll.Text = "Show all";
         this.btShowAll.UseVisualStyleBackColor = true;
         this.btShowAll.Click += new System.EventHandler(this.btShowAll_Click);
         // 
         // btShowRange
         // 
         this.btShowRange.Location = new System.Drawing.Point(185, 25);
         this.btShowRange.Name = "btShowRange";
         this.btShowRange.Size = new System.Drawing.Size(77, 34);
         this.btShowRange.TabIndex = 6;
         this.btShowRange.Text = "Show range";
         this.btShowRange.UseVisualStyleBackColor = true;
         this.btShowRange.Click += new System.EventHandler(this.btShowRange_Click);
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(8, 51);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(19, 13);
         this.label2.TabIndex = 5;
         this.label2.Text = "to:";
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(8, 25);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(30, 13);
         this.label1.TabIndex = 4;
         this.label1.Text = "from:";
         // 
         // dtpTo
         // 
         this.dtpTo.Location = new System.Drawing.Point(44, 45);
         this.dtpTo.Name = "dtpTo";
         this.dtpTo.Size = new System.Drawing.Size(135, 20);
         this.dtpTo.TabIndex = 3;
         // 
         // dtpFrom
         // 
         this.dtpFrom.Location = new System.Drawing.Point(44, 19);
         this.dtpFrom.Name = "dtpFrom";
         this.dtpFrom.Size = new System.Drawing.Size(135, 20);
         this.dtpFrom.TabIndex = 2;
         // 
         // dgvItemsHistory
         // 
         this.dgvItemsHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.dgvItemsHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
         this.dgvItemsHistory.Location = new System.Drawing.Point(12, 12);
         this.dgvItemsHistory.Name = "dgvItemsHistory";
         this.dgvItemsHistory.ReadOnly = true;
         this.dgvItemsHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
         this.dgvItemsHistory.Size = new System.Drawing.Size(803, 259);
         this.dgvItemsHistory.TabIndex = 34;
         this.dgvItemsHistory.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvItemsHistory_CellDoubleClick);
         // 
         // btOK
         // 
         this.btOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.btOK.Location = new System.Drawing.Point(12, 390);
         this.btOK.Name = "btOK";
         this.btOK.Size = new System.Drawing.Size(77, 34);
         this.btOK.TabIndex = 38;
         this.btOK.Text = "Save changes";
         this.btOK.UseVisualStyleBackColor = true;
         this.btOK.Click += new System.EventHandler(this.btOK_Click);
         // 
         // btAllowChange
         // 
         this.btAllowChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.btAllowChange.Location = new System.Drawing.Point(95, 390);
         this.btAllowChange.Name = "btAllowChange";
         this.btAllowChange.Size = new System.Drawing.Size(77, 34);
         this.btAllowChange.TabIndex = 39;
         this.btAllowChange.Text = "Allow changes";
         this.btAllowChange.UseVisualStyleBackColor = true;
         this.btAllowChange.Click += new System.EventHandler(this.btAllowChange_Click);
         // 
         // groupBox2
         // 
         this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.groupBox2.Controls.Add(this.label5);
         this.groupBox2.Controls.Add(this.txtCuid);
         this.groupBox2.Controls.Add(this.label4);
         this.groupBox2.Controls.Add(this.destructionDate);
         this.groupBox2.Controls.Add(this.label3);
         this.groupBox2.Controls.Add(this.lbDescription);
         this.groupBox2.Controls.Add(this.txtDescription);
         this.groupBox2.Controls.Add(this.txtBoxPercentage);
         this.groupBox2.Location = new System.Drawing.Point(371, 289);
         this.groupBox2.Name = "groupBox2";
         this.groupBox2.Size = new System.Drawing.Size(444, 135);
         this.groupBox2.TabIndex = 40;
         this.groupBox2.TabStop = false;
         this.groupBox2.Text = "Item details";
         // 
         // label4
         // 
         this.label4.BackColor = System.Drawing.SystemColors.Control;
         this.label4.Cursor = System.Windows.Forms.Cursors.Default;
         this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
         this.label4.Location = new System.Drawing.Point(249, 16);
         this.label4.Name = "label4";
         this.label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.label4.Size = new System.Drawing.Size(65, 28);
         this.label4.TabIndex = 59;
         this.label4.Text = "Destruction date:";
         this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
         // 
         // destructionDate
         // 
         this.destructionDate.AcceptsReturn = true;
         this.destructionDate.BackColor = System.Drawing.SystemColors.Window;
         this.destructionDate.Cursor = System.Windows.Forms.Cursors.IBeam;
         this.destructionDate.Enabled = false;
         this.destructionDate.ForeColor = System.Drawing.SystemColors.WindowText;
         this.destructionDate.Location = new System.Drawing.Point(320, 21);
         this.destructionDate.MaxLength = 0;
         this.destructionDate.Name = "destructionDate";
         this.destructionDate.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.destructionDate.Size = new System.Drawing.Size(109, 20);
         this.destructionDate.TabIndex = 2;
         // 
         // label3
         // 
         this.label3.BackColor = System.Drawing.SystemColors.Control;
         this.label3.Cursor = System.Windows.Forms.Cursors.Default;
         this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
         this.label3.Location = new System.Drawing.Point(6, 76);
         this.label3.Name = "label3";
         this.label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.label3.Size = new System.Drawing.Size(65, 28);
         this.label3.TabIndex = 42;
         this.label3.Text = "Box percentage:";
         this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
         // 
         // lbDescription
         // 
         this.lbDescription.BackColor = System.Drawing.SystemColors.Control;
         this.lbDescription.Cursor = System.Windows.Forms.Cursors.Default;
         this.lbDescription.ForeColor = System.Drawing.SystemColors.ControlText;
         this.lbDescription.Location = new System.Drawing.Point(6, 21);
         this.lbDescription.Name = "lbDescription";
         this.lbDescription.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.lbDescription.Size = new System.Drawing.Size(65, 28);
         this.lbDescription.TabIndex = 41;
         this.lbDescription.Text = "Description of a box :";
         this.lbDescription.TextAlign = System.Drawing.ContentAlignment.TopCenter;
         // 
         // btShowReport
         // 
         this.btShowReport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.btShowReport.Location = new System.Drawing.Point(280, 390);
         this.btShowReport.Name = "btShowReport";
         this.btShowReport.Size = new System.Drawing.Size(77, 34);
         this.btShowReport.TabIndex = 8;
         this.btShowReport.Text = "Report";
         this.btShowReport.UseVisualStyleBackColor = true;
         this.btShowReport.Click += new System.EventHandler(this.btShowReport_Click);
         // 
         // label5
         // 
         this.label5.BackColor = System.Drawing.SystemColors.Control;
         this.label5.Cursor = System.Windows.Forms.Cursors.Default;
         this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
         this.label5.Location = new System.Drawing.Point(249, 87);
         this.label5.Name = "label5";
         this.label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.label5.Size = new System.Drawing.Size(65, 25);
         this.label5.TabIndex = 61;
         this.label5.Text = "CUID:";
         this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
         // 
         // txtCuid
         // 
         this.txtCuid.AcceptsReturn = true;
         this.txtCuid.BackColor = System.Drawing.SystemColors.Window;
         this.txtCuid.Cursor = System.Windows.Forms.Cursors.IBeam;
         this.txtCuid.Enabled = false;
         this.txtCuid.ForeColor = System.Drawing.SystemColors.WindowText;
         this.txtCuid.Location = new System.Drawing.Point(320, 84);
         this.txtCuid.MaxLength = 0;
         this.txtCuid.Name = "txtCuid";
         this.txtCuid.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.txtCuid.Size = new System.Drawing.Size(109, 20);
         this.txtCuid.TabIndex = 60;
         // 
         // ItemDetailsForm
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(830, 436);
         this.Controls.Add(this.btShowReport);
         this.Controls.Add(this.groupBox2);
         this.Controls.Add(this.btAllowChange);
         this.Controls.Add(this.btOK);
         this.Controls.Add(this.dgvItemsHistory);
         this.Controls.Add(this.groupBox1);
         this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
         this.MinimumSize = new System.Drawing.Size(846, 474);
         this.Name = "ItemDetailsForm";
         this.Text = "ItemDetailsForm";
         this.groupBox1.ResumeLayout(false);
         this.groupBox1.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.dgvItemsHistory)).EndInit();
         this.groupBox2.ResumeLayout(false);
         this.groupBox2.PerformLayout();
         this.ResumeLayout(false);

      }

      #endregion

      public System.Windows.Forms.TextBox txtBoxPercentage;
      public System.Windows.Forms.TextBox txtDescription;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.Button btShowAll;
      private System.Windows.Forms.Button btShowRange;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.DateTimePicker dtpTo;
      private System.Windows.Forms.DateTimePicker dtpFrom;
      private System.Windows.Forms.DataGridView dgvItemsHistory;
      private System.Windows.Forms.Button btOK;
      private System.Windows.Forms.Button btAllowChange;
      private System.Windows.Forms.GroupBox groupBox2;
      public System.Windows.Forms.Label lbDescription;
      private System.Windows.Forms.Button btShowReport;
      public System.Windows.Forms.Label label3;
      public System.Windows.Forms.Label label4;
      public System.Windows.Forms.TextBox destructionDate;
      public System.Windows.Forms.Label label5;
      public System.Windows.Forms.TextBox txtCuid;
   }
}