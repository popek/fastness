﻿namespace FastnessForms.Views
{
   partial class ManageUsersForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManageUsersForm));
         this.rbEditCurrentUser = new System.Windows.Forms.RadioButton();
         this.groupBox1 = new System.Windows.Forms.GroupBox();
         this.rbRemoveOtherUser = new System.Windows.Forms.RadioButton();
         this.rbAddNewUser = new System.Windows.Forms.RadioButton();
         this.txtUserName = new System.Windows.Forms.TextBox();
         this.txtPassword = new System.Windows.Forms.MaskedTextBox();
         this.txtPasswordConfirm = new System.Windows.Forms.MaskedTextBox();
         this.label2 = new System.Windows.Forms.Label();
         this.label1 = new System.Windows.Forms.Label();
         this.label3 = new System.Windows.Forms.Label();
         this.cbRole = new System.Windows.Forms.ComboBox();
         this.label4 = new System.Windows.Forms.Label();
         this.groupBox2 = new System.Windows.Forms.GroupBox();
         this.btSave = new System.Windows.Forms.Button();
         this.tbCancel = new System.Windows.Forms.Button();
         this.btRemoveUser = new System.Windows.Forms.Button();
         this.dgvUsersList = new System.Windows.Forms.DataGridView();
         this.groupBox3 = new System.Windows.Forms.GroupBox();
         this.groupBox1.SuspendLayout();
         this.groupBox2.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.dgvUsersList)).BeginInit();
         this.groupBox3.SuspendLayout();
         this.SuspendLayout();
         // 
         // rbEditCurrentUser
         // 
         this.rbEditCurrentUser.AutoSize = true;
         this.rbEditCurrentUser.Location = new System.Drawing.Point(6, 19);
         this.rbEditCurrentUser.Name = "rbEditCurrentUser";
         this.rbEditCurrentUser.Size = new System.Drawing.Size(101, 17);
         this.rbEditCurrentUser.TabIndex = 0;
         this.rbEditCurrentUser.TabStop = true;
         this.rbEditCurrentUser.Text = "edit current user";
         this.rbEditCurrentUser.UseVisualStyleBackColor = true;
         this.rbEditCurrentUser.CheckedChanged += new System.EventHandler(this.rbEditCurrentUser_CheckedChanged);
         // 
         // groupBox1
         // 
         this.groupBox1.Controls.Add(this.rbRemoveOtherUser);
         this.groupBox1.Controls.Add(this.rbAddNewUser);
         this.groupBox1.Controls.Add(this.rbEditCurrentUser);
         this.groupBox1.Location = new System.Drawing.Point(12, 17);
         this.groupBox1.Name = "groupBox1";
         this.groupBox1.Size = new System.Drawing.Size(335, 91);
         this.groupBox1.TabIndex = 0;
         this.groupBox1.TabStop = false;
         this.groupBox1.Text = "What to do?";
         // 
         // rbRemoveOtherUser
         // 
         this.rbRemoveOtherUser.AutoSize = true;
         this.rbRemoveOtherUser.Location = new System.Drawing.Point(6, 65);
         this.rbRemoveOtherUser.Name = "rbRemoveOtherUser";
         this.rbRemoveOtherUser.Size = new System.Drawing.Size(83, 17);
         this.rbRemoveOtherUser.TabIndex = 2;
         this.rbRemoveOtherUser.TabStop = true;
         this.rbRemoveOtherUser.Text = "remove user";
         this.rbRemoveOtherUser.UseVisualStyleBackColor = true;
         this.rbRemoveOtherUser.CheckedChanged += new System.EventHandler(this.rbRemoveOtherUser_CheckedChanged);
         // 
         // rbAddNewUser
         // 
         this.rbAddNewUser.AutoSize = true;
         this.rbAddNewUser.Location = new System.Drawing.Point(6, 42);
         this.rbAddNewUser.Name = "rbAddNewUser";
         this.rbAddNewUser.Size = new System.Drawing.Size(89, 17);
         this.rbAddNewUser.TabIndex = 1;
         this.rbAddNewUser.TabStop = true;
         this.rbAddNewUser.Text = "add new user";
         this.rbAddNewUser.UseVisualStyleBackColor = true;
         this.rbAddNewUser.CheckedChanged += new System.EventHandler(this.rbAddNewUser_CheckedChanged);
         // 
         // txtUserName
         // 
         this.txtUserName.Location = new System.Drawing.Point(104, 19);
         this.txtUserName.Name = "txtUserName";
         this.txtUserName.Size = new System.Drawing.Size(225, 20);
         this.txtUserName.TabIndex = 0;
         // 
         // txtPassword
         // 
         this.txtPassword.Location = new System.Drawing.Point(104, 77);
         this.txtPassword.Name = "txtPassword";
         this.txtPassword.PasswordChar = '*';
         this.txtPassword.Size = new System.Drawing.Size(225, 20);
         this.txtPassword.TabIndex = 2;
         // 
         // txtPasswordConfirm
         // 
         this.txtPasswordConfirm.Location = new System.Drawing.Point(104, 103);
         this.txtPasswordConfirm.Name = "txtPasswordConfirm";
         this.txtPasswordConfirm.PasswordChar = '*';
         this.txtPasswordConfirm.Size = new System.Drawing.Size(225, 20);
         this.txtPasswordConfirm.TabIndex = 3;
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(6, 80);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(56, 13);
         this.label2.TabIndex = 6;
         this.label2.Text = "Password:";
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(6, 22);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(38, 13);
         this.label1.TabIndex = 4;
         this.label1.Text = "Name:";
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Location = new System.Drawing.Point(6, 106);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(93, 13);
         this.label3.TabIndex = 7;
         this.label3.Text = "Confirm password:";
         // 
         // cbRole
         // 
         this.cbRole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.cbRole.FormattingEnabled = true;
         this.cbRole.Items.AddRange(new object[] {
            "admin",
            "read-write",
            "read-only"});
         this.cbRole.Location = new System.Drawing.Point(104, 45);
         this.cbRole.Name = "cbRole";
         this.cbRole.Size = new System.Drawing.Size(225, 21);
         this.cbRole.TabIndex = 1;
         // 
         // label4
         // 
         this.label4.AutoSize = true;
         this.label4.Location = new System.Drawing.Point(6, 48);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(32, 13);
         this.label4.TabIndex = 5;
         this.label4.Text = "Role:";
         // 
         // groupBox2
         // 
         this.groupBox2.Controls.Add(this.txtUserName);
         this.groupBox2.Controls.Add(this.label4);
         this.groupBox2.Controls.Add(this.txtPassword);
         this.groupBox2.Controls.Add(this.cbRole);
         this.groupBox2.Controls.Add(this.txtPasswordConfirm);
         this.groupBox2.Controls.Add(this.label3);
         this.groupBox2.Controls.Add(this.label1);
         this.groupBox2.Controls.Add(this.label2);
         this.groupBox2.Location = new System.Drawing.Point(12, 114);
         this.groupBox2.Name = "groupBox2";
         this.groupBox2.Size = new System.Drawing.Size(335, 132);
         this.groupBox2.TabIndex = 1;
         this.groupBox2.TabStop = false;
         this.groupBox2.Text = "User details";
         // 
         // btSave
         // 
         this.btSave.Location = new System.Drawing.Point(179, 459);
         this.btSave.Name = "btSave";
         this.btSave.Size = new System.Drawing.Size(78, 35);
         this.btSave.TabIndex = 1;
         this.btSave.Text = "Save user";
         this.btSave.UseVisualStyleBackColor = true;
         this.btSave.Click += new System.EventHandler(this.btSave_Click);
         // 
         // tbCancel
         // 
         this.tbCancel.Location = new System.Drawing.Point(269, 459);
         this.tbCancel.Name = "tbCancel";
         this.tbCancel.Size = new System.Drawing.Size(78, 35);
         this.tbCancel.TabIndex = 2;
         this.tbCancel.Text = "Close";
         this.tbCancel.UseVisualStyleBackColor = true;
         this.tbCancel.Click += new System.EventHandler(this.tbCancel_Click);
         // 
         // btRemoveUser
         // 
         this.btRemoveUser.Location = new System.Drawing.Point(95, 459);
         this.btRemoveUser.Name = "btRemoveUser";
         this.btRemoveUser.Size = new System.Drawing.Size(78, 35);
         this.btRemoveUser.TabIndex = 0;
         this.btRemoveUser.Text = "Remove user";
         this.btRemoveUser.UseVisualStyleBackColor = true;
         this.btRemoveUser.Click += new System.EventHandler(this.btRemoveUser_Click);
         // 
         // dgvUsersList
         // 
         this.dgvUsersList.AllowUserToAddRows = false;
         this.dgvUsersList.AllowUserToDeleteRows = false;
         this.dgvUsersList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
         this.dgvUsersList.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
         this.dgvUsersList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
         this.dgvUsersList.Location = new System.Drawing.Point(6, 19);
         this.dgvUsersList.MultiSelect = false;
         this.dgvUsersList.Name = "dgvUsersList";
         this.dgvUsersList.ReadOnly = true;
         this.dgvUsersList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
         this.dgvUsersList.Size = new System.Drawing.Size(319, 176);
         this.dgvUsersList.TabIndex = 0;
         this.dgvUsersList.SelectionChanged += new System.EventHandler(this.dgvUsersList_SelectionChanged);
         // 
         // groupBox3
         // 
         this.groupBox3.Controls.Add(this.dgvUsersList);
         this.groupBox3.Location = new System.Drawing.Point(15, 252);
         this.groupBox3.Name = "groupBox3";
         this.groupBox3.Size = new System.Drawing.Size(331, 201);
         this.groupBox3.TabIndex = 6;
         this.groupBox3.TabStop = false;
         this.groupBox3.Text = "All Fastness users list";
         // 
         // ManageUsersForm
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(357, 505);
         this.Controls.Add(this.groupBox3);
         this.Controls.Add(this.btRemoveUser);
         this.Controls.Add(this.tbCancel);
         this.Controls.Add(this.btSave);
         this.Controls.Add(this.groupBox2);
         this.Controls.Add(this.groupBox1);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
         this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
         this.MaximizeBox = false;
         this.MinimizeBox = false;
         this.Name = "ManageUsersForm";
         this.Text = "Manage Fastness system users";
         this.groupBox1.ResumeLayout(false);
         this.groupBox1.PerformLayout();
         this.groupBox2.ResumeLayout(false);
         this.groupBox2.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.dgvUsersList)).EndInit();
         this.groupBox3.ResumeLayout(false);
         this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.RadioButton rbEditCurrentUser;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.RadioButton rbAddNewUser;
      private System.Windows.Forms.TextBox txtUserName;
      private System.Windows.Forms.MaskedTextBox txtPassword;
      private System.Windows.Forms.MaskedTextBox txtPasswordConfirm;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.ComboBox cbRole;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.GroupBox groupBox2;
      private System.Windows.Forms.Button btSave;
      private System.Windows.Forms.Button tbCancel;
      private System.Windows.Forms.Button btRemoveUser;
      private System.Windows.Forms.RadioButton rbRemoveOtherUser;
      private System.Windows.Forms.DataGridView dgvUsersList;
      private System.Windows.Forms.GroupBox groupBox3;

   }
}