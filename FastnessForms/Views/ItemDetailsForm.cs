﻿using Fastness.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Fastness.DataModel.Utils;
using Fastness.Common;
using Fastness.DataModel.Transaction;
using System.Globalization;
using Fastness.DataModel.Exceptions;
using Fastness.Common.Events;
using FastnessReports;

namespace FastnessForms.Views
{
   public partial class ItemDetailsForm : Form
   {
      DBHandle hDB = new DBHandle();
      ItemDetail item;

      DataGridViewManager<ActionHistory> itemsHistoryGridMan;

      bool allBoxActionsLoaded;

      bool descriptionChanged = false;
      bool percentageChanged = false;
      bool destructionDateChanged = false;
      bool cuidChanged = false;

      public event DbContentChangedEventHandler DBChanged;

      ItemDestructionDateHandler destructionDateHandler;

      bool AllowChanges 
      {
         set 
         {
            txtDescription.Enabled = value;
            txtBoxPercentage.Enabled = value;
            destructionDate.Enabled = value;
            txtCuid.Enabled = value;
         }
      }
      bool UnsavedChangesOnBoard
      {
         get 
         {
            return destructionDateChanged || percentageChanged || descriptionChanged || cuidChanged;
         }
      }

      void UpdateSaveButton()
      {
         btOK.Enabled = !LoggedUserUtils.CanEditDBContent() ? false : UnsavedChangesOnBoard;
      }

      public ItemDetailsForm(ItemDetail item)
      {
         InitializeComponent();

         destructionDateHandler = new ItemDestructionDateHandler(
            destructionDate);

         InitItemsHistoryGrid();

         this.item = item;

         Text = string.Format("Details of '{0}' item", item.ItemNumber);
         dtpFrom.Value = DateTime.Today;
         dtpTo.Value = DateTime.Today;

         txtBoxPercentage.AddKeyPressWithTextLengthLimitAndIntegerValue(DBFieldsConstraint.ITEM_PERCENTAGE_MAX_LENGTH);
         txtDescription.AddKeyPressWithTextLengthLimit(DBFieldsConstraint.ITEM_DESCRIPTION_MAX_LENGTH);
         txtCuid.AddKeyPressWithTextLengthLimit(DBFieldsConstraint.CLIENT_ITEM_REF_MAX_LENGTH);

         txtBoxPercentage.Text = item.Percentage.ToString();
         txtDescription.Text = item.Description;
         txtCuid.Text = item.ClientItemRef;

         btOK.Enabled = false;

         AllowChanges = false;

         txtDescription.AddTextChangeHandler(() => item.Description??"", (textChanged) =>
         {
            descriptionChanged = textChanged;
            UpdateSaveButton();
         });
         txtBoxPercentage.AddTextChangeHandler(() => (item.Percentage??100).ToString(), (textChanged) =>
         {
            percentageChanged = textChanged;
            UpdateSaveButton();
         });
         txtCuid.AddTextChangeHandler(() => item.ClientItemRef, (textChanged) =>
         {
            cuidChanged = textChanged;
            UpdateSaveButton();
         });

         destructionDate.Text = item.DestructionDate;

         destructionDate.TextChanged += (s, e) =>
         {
            destructionDateChanged = 
               (item.DestructionDate ?? "") != destructionDateHandler.GetDestructionDateString();
            UpdateSaveButton();
         };

         btAllowChange.Enabled = LoggedUserUtils.CanEditDBContent();
         UpdateSaveButton();

         LoadAllBoxActions();
      }

      private void InitItemsHistoryGrid()
      {
         itemsHistoryGridMan = FastnessUI.CreateActionHistoryGridManager(dgvItemsHistory);
      }

      private void LoadActionsRange()
      {
         var query = from ah in hDB.DBCtx.ActionHistory
                     where ah.ItemNumber == item.ItemNumber && ah.DateCompleted >= dtpFrom.Value && ah.DateCompleted <= dtpTo.Value
                     orderby ah.DateCompleted
                     select ah;

         ReloadGridData(query);

         allBoxActionsLoaded = false;
      }

      private void LoadAllBoxActions()
      {
         var query = from ah in hDB.DBCtx.ActionHistory
                     where ah.ItemNumber == item.ItemNumber
                     orderby ah.DateCompleted
                     select ah;

         ReloadGridData(query);

         allBoxActionsLoaded = true;
      }

      private void ReloadGridData(IEnumerable<ActionHistory> query)
      {
         itemsHistoryGridMan.Refresh(query.ToList<ActionHistory>());
      }

      private void btAllowChange_Click(object sender, EventArgs e)
      {
         AllowChanges = true;
      }

      private void btShowReport_Click(object sender, EventArgs e)
      {
         string headerText;
         if (allBoxActionsLoaded)
         {
            headerText = string.Format("'{0}' item actions history", item.ItemNumber);
         }
         else
         {
            headerText = string.Format("'{2}' item actions history range ({0} - {1})",
               DateUtils.ToString(dtpFrom.Value), DateUtils.ToString(dtpTo.Value), item.ItemNumber);
         }

         var filePath = PdfFilesHelper.PrepareReportFilePath("Contract history");

         if (PdfFilesHelper.IsReportOpened("Item history"))
         {
            UserInteraction.ShowInfo("Report file for item history is already opened.\nYou must close it before regenerating it one more time.");
            return;
         }

         using (var report = new PDFItemActionsHistoryReport(itemsHistoryGridMan.Content, filePath, headerText))
         {
            report.Print();
         }
         PdfFilesHelper.Show(filePath);
      }

      private void btOK_Click(object sender, EventArgs e)
      {
         if (!UserInteraction.AskUser("Do you want to save changes in item '{0}'?", 
                                       item.ItemNumber))
         {
            return;
         }
         if (!destructionDateHandler.IsDestructionDateValid())
         {
            return;
         }

         ModifyDb.RunTransaction(dbCtx => new UpdateItemDetails(
            item,
            Int32.Parse(txtBoxPercentage.Text),
            destructionDateHandler.GetDestructionDateString(),
            txtDescription.Text,
            txtCuid.Text,
            dbCtx).Run());

         CallDbChangeOnFormClose();

         Close();
      }

      private void btShowRange_Click(object sender, EventArgs e)
      {
         if (dtpFrom.Value > dtpTo.Value)
         {
            UserInteraction.ShowError(
               "Date range is invalid. 'From' date ({0}) is after 'to' ({1}) date", dtpFrom.Value, dtpTo.Value);
            dtpFrom.Focus();
            return;
         }
         
         LoadActionsRange();
      }

      private void btShowAll_Click(object sender, EventArgs e)
      {
         LoadAllBoxActions();
      }

      private void dgvItemsHistory_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
      {
         var itemHistoryEntry = itemsHistoryGridMan.CurrentValue;
         if (itemHistoryEntry != null)
         {
            var updateHistoryEntryForm = new UpdateActionHisotryEntryForm(itemHistoryEntry);
            updateHistoryEntryForm.DBChanged += (cc) =>
            {
               var posBeforeReload = itemsHistoryGridMan.FirstDisplayedScrollingRowIndex;

               hDB.NeedReload = true;
               if (allBoxActionsLoaded)
                  LoadAllBoxActions();
               else
                  LoadActionsRange();
               
               CallDbChangeOnFormClose();

               itemsHistoryGridMan.SelectFirst(
                  ah => ah.ActionHistoryID == itemHistoryEntry.ActionHistoryID);
               itemsHistoryGridMan.FirstDisplayedScrollingRowIndex = posBeforeReload;
            };
            updateHistoryEntryForm.ShowDialog();
         }
      }
      private void CallDbChangeOnFormClose()
      {
         // - and + to avoid sending dbchange multiple times (in case it was added multiple times)
         this.FormClosed -= SendDbChangeOnClose;
         this.FormClosed += SendDbChangeOnClose;
      }
      private void SendDbChangeOnClose(object sender, FormClosedEventArgs e)
      {
         this.DBChanged(null);
      }
   }
}
