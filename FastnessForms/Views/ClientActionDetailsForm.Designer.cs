﻿namespace FastnessForms.Views
{
   partial class ClientActionDetailsForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClientActionDetailsForm));
         this.optSpecificClientOnly = new System.Windows.Forms.RadioButton();
         this.optServiceOnly = new System.Windows.Forms.RadioButton();
         this.optClientsAndService = new System.Windows.Forms.RadioButton();
         this.frChargeOptions = new System.Windows.Forms.GroupBox();
         this.txtPerFree = new System.Windows.Forms.TextBox();
         this.txtCharge = new System.Windows.Forms.TextBox();
         this.lbPerFree = new System.Windows.Forms.Label();
         this.lbAuxiliaryInfo = new System.Windows.Forms.Label();
         this.lbCharge = new System.Windows.Forms.Label();
         this.btSaveChanges = new System.Windows.Forms.Button();
         this.btClose = new System.Windows.Forms.Button();
         this.frChargeOptions.SuspendLayout();
         this.SuspendLayout();
         // 
         // optSpecificClientOnly
         // 
         this.optSpecificClientOnly.BackColor = System.Drawing.SystemColors.Control;
         this.optSpecificClientOnly.Cursor = System.Windows.Forms.Cursors.Default;
         this.optSpecificClientOnly.ForeColor = System.Drawing.SystemColors.ControlText;
         this.optSpecificClientOnly.Location = new System.Drawing.Point(6, 19);
         this.optSpecificClientOnly.Name = "optSpecificClientOnly";
         this.optSpecificClientOnly.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.optSpecificClientOnly.Size = new System.Drawing.Size(112, 25);
         this.optSpecificClientOnly.TabIndex = 0;
         this.optSpecificClientOnly.TabStop = true;
         this.optSpecificClientOnly.Text = "specific client";
         this.optSpecificClientOnly.UseVisualStyleBackColor = false;
         // 
         // optServiceOnly
         // 
         this.optServiceOnly.BackColor = System.Drawing.SystemColors.Control;
         this.optServiceOnly.Cursor = System.Windows.Forms.Cursors.Default;
         this.optServiceOnly.ForeColor = System.Drawing.SystemColors.ControlText;
         this.optServiceOnly.Location = new System.Drawing.Point(6, 81);
         this.optServiceOnly.Name = "optServiceOnly";
         this.optServiceOnly.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.optServiceOnly.Size = new System.Drawing.Size(129, 25);
         this.optServiceOnly.TabIndex = 2;
         this.optServiceOnly.TabStop = true;
         this.optServiceOnly.Text = "service only";
         this.optServiceOnly.UseVisualStyleBackColor = false;
         // 
         // optClientsAndService
         // 
         this.optClientsAndService.BackColor = System.Drawing.SystemColors.Control;
         this.optClientsAndService.Cursor = System.Windows.Forms.Cursors.Default;
         this.optClientsAndService.ForeColor = System.Drawing.SystemColors.ControlText;
         this.optClientsAndService.Location = new System.Drawing.Point(6, 50);
         this.optClientsAndService.Name = "optClientsAndService";
         this.optClientsAndService.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.optClientsAndService.Size = new System.Drawing.Size(129, 25);
         this.optClientsAndService.TabIndex = 1;
         this.optClientsAndService.TabStop = true;
         this.optClientsAndService.Text = "all clients and service";
         this.optClientsAndService.UseVisualStyleBackColor = false;
         // 
         // frChargeOptions
         // 
         this.frChargeOptions.BackColor = System.Drawing.SystemColors.Control;
         this.frChargeOptions.Controls.Add(this.optSpecificClientOnly);
         this.frChargeOptions.Controls.Add(this.optClientsAndService);
         this.frChargeOptions.Controls.Add(this.optServiceOnly);
         this.frChargeOptions.ForeColor = System.Drawing.SystemColors.ControlText;
         this.frChargeOptions.Location = new System.Drawing.Point(12, 135);
         this.frChargeOptions.Name = "frChargeOptions";
         this.frChargeOptions.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.frChargeOptions.Size = new System.Drawing.Size(136, 113);
         this.frChargeOptions.TabIndex = 5;
         this.frChargeOptions.TabStop = false;
         this.frChargeOptions.Text = "Apply changes to :";
         // 
         // txtPerFree
         // 
         this.txtPerFree.AcceptsReturn = true;
         this.txtPerFree.BackColor = System.Drawing.SystemColors.Window;
         this.txtPerFree.Cursor = System.Windows.Forms.Cursors.IBeam;
         this.txtPerFree.ForeColor = System.Drawing.SystemColors.WindowText;
         this.txtPerFree.Location = new System.Drawing.Point(83, 90);
         this.txtPerFree.MaxLength = 0;
         this.txtPerFree.Name = "txtPerFree";
         this.txtPerFree.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.txtPerFree.Size = new System.Drawing.Size(65, 20);
         this.txtPerFree.TabIndex = 4;
         // 
         // txtCharge
         // 
         this.txtCharge.AcceptsReturn = true;
         this.txtCharge.BackColor = System.Drawing.SystemColors.Window;
         this.txtCharge.Cursor = System.Windows.Forms.Cursors.IBeam;
         this.txtCharge.ForeColor = System.Drawing.SystemColors.WindowText;
         this.txtCharge.Location = new System.Drawing.Point(83, 65);
         this.txtCharge.MaxLength = 0;
         this.txtCharge.Name = "txtCharge";
         this.txtCharge.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.txtCharge.Size = new System.Drawing.Size(65, 20);
         this.txtCharge.TabIndex = 3;
         // 
         // lbPerFree
         // 
         this.lbPerFree.BackColor = System.Drawing.SystemColors.Control;
         this.lbPerFree.Cursor = System.Windows.Forms.Cursors.Default;
         this.lbPerFree.ForeColor = System.Drawing.SystemColors.ControlText;
         this.lbPerFree.Location = new System.Drawing.Point(12, 93);
         this.lbPerFree.Name = "lbPerFree";
         this.lbPerFree.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.lbPerFree.Size = new System.Drawing.Size(65, 25);
         this.lbPerFree.TabIndex = 2;
         this.lbPerFree.Text = "Percent free:";
         this.lbPerFree.TextAlign = System.Drawing.ContentAlignment.TopCenter;
         // 
         // lbAuxiliaryInfo
         // 
         this.lbAuxiliaryInfo.BackColor = System.Drawing.SystemColors.Control;
         this.lbAuxiliaryInfo.Cursor = System.Windows.Forms.Cursors.Default;
         this.lbAuxiliaryInfo.ForeColor = System.Drawing.SystemColors.ControlText;
         this.lbAuxiliaryInfo.Location = new System.Drawing.Point(12, 9);
         this.lbAuxiliaryInfo.Name = "lbAuxiliaryInfo";
         this.lbAuxiliaryInfo.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.lbAuxiliaryInfo.Size = new System.Drawing.Size(214, 41);
         this.lbAuxiliaryInfo.TabIndex = 0;
         this.lbAuxiliaryInfo.Text = "Label1";
         // 
         // lbCharge
         // 
         this.lbCharge.BackColor = System.Drawing.SystemColors.Control;
         this.lbCharge.Cursor = System.Windows.Forms.Cursors.Default;
         this.lbCharge.ForeColor = System.Drawing.SystemColors.ControlText;
         this.lbCharge.Location = new System.Drawing.Point(12, 68);
         this.lbCharge.Name = "lbCharge";
         this.lbCharge.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.lbCharge.Size = new System.Drawing.Size(65, 25);
         this.lbCharge.TabIndex = 1;
         this.lbCharge.Text = "Charge:";
         this.lbCharge.TextAlign = System.Drawing.ContentAlignment.TopCenter;
         // 
         // btSaveChanges
         // 
         this.btSaveChanges.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.btSaveChanges.Enabled = false;
         this.btSaveChanges.Location = new System.Drawing.Point(12, 270);
         this.btSaveChanges.Name = "btSaveChanges";
         this.btSaveChanges.Size = new System.Drawing.Size(91, 30);
         this.btSaveChanges.TabIndex = 6;
         this.btSaveChanges.Text = "Save changes";
         this.btSaveChanges.UseVisualStyleBackColor = true;
         this.btSaveChanges.Click += new System.EventHandler(this.btSaveChanges_Click);
         // 
         // btClose
         // 
         this.btClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.btClose.Location = new System.Drawing.Point(135, 270);
         this.btClose.Name = "btClose";
         this.btClose.Size = new System.Drawing.Size(91, 30);
         this.btClose.TabIndex = 7;
         this.btClose.Text = "Close";
         this.btClose.UseVisualStyleBackColor = true;
         this.btClose.Click += new System.EventHandler(this.btClose_Click);
         // 
         // ClientActionDetailsForm
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(238, 312);
         this.Controls.Add(this.btClose);
         this.Controls.Add(this.btSaveChanges);
         this.Controls.Add(this.frChargeOptions);
         this.Controls.Add(this.txtPerFree);
         this.Controls.Add(this.txtCharge);
         this.Controls.Add(this.lbPerFree);
         this.Controls.Add(this.lbAuxiliaryInfo);
         this.Controls.Add(this.lbCharge);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
         this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
         this.MaximizeBox = false;
         this.MinimizeBox = false;
         this.Name = "ClientActionDetailsForm";
         this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
         this.Text = "Action Details";
         this.Load += new System.EventHandler(this.ClientActionDetailsForm_Load);
         this.frChargeOptions.ResumeLayout(false);
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      public System.Windows.Forms.RadioButton optSpecificClientOnly;
      public System.Windows.Forms.RadioButton optServiceOnly;
      public System.Windows.Forms.RadioButton optClientsAndService;
      public System.Windows.Forms.GroupBox frChargeOptions;
      public System.Windows.Forms.TextBox txtPerFree;
      public System.Windows.Forms.TextBox txtCharge;
      public System.Windows.Forms.Label lbPerFree;
      public System.Windows.Forms.Label lbAuxiliaryInfo;
      public System.Windows.Forms.Label lbCharge;
      private System.Windows.Forms.Button btSaveChanges;
      private System.Windows.Forms.Button btClose;
   }
}