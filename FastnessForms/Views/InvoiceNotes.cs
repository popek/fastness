﻿using Fastness.DataModel;

namespace FastnessForms.Views
{
   public class InvoiceNotes
   {
      public int ContractID { get; set; }
      public string AccountRef { get; set; }
      public string Name { get; set; }
      public string Notes { get; set; }

      public InvoiceNotes(Contract ctr)
      {
         ContractID = ctr.ContractID;
         AccountRef = ctr.AccountRef;
         Name = ctr.Name;
         Notes = "";
      }
   }
}
