﻿namespace FastnessForms.Views
{
   partial class ContractDetailsForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContractDetailsForm));
         this.accountRefText = new System.Windows.Forms.TextBox();
         this.nameText = new System.Windows.Forms.TextBox();
         this.auxiliaryAddressText = new System.Windows.Forms.TextBox();
         this.clientInvoiceRefText = new System.Windows.Forms.TextBox();
         this.descriptionText = new System.Windows.Forms.TextBox();
         this.fixedFeeText = new System.Windows.Forms.TextBox();
         this.label1 = new System.Windows.Forms.Label();
         this.label2 = new System.Windows.Forms.Label();
         this.label3 = new System.Windows.Forms.Label();
         this.label4 = new System.Windows.Forms.Label();
         this.label5 = new System.Windows.Forms.Label();
         this.label6 = new System.Windows.Forms.Label();
         this.addressText = new System.Windows.Forms.TextBox();
         this.label7 = new System.Windows.Forms.Label();
         this.saveChangesButton = new System.Windows.Forms.Button();
         this.closeButton = new System.Windows.Forms.Button();
         this.serviceLevelCombo = new System.Windows.Forms.ComboBox();
         this.label8 = new System.Windows.Forms.Label();
         this.label9 = new System.Windows.Forms.Label();
         this.fixedUnitsText = new System.Windows.Forms.TextBox();
         this.groupBox1 = new System.Windows.Forms.GroupBox();
         this.label10 = new System.Windows.Forms.Label();
         this.contractFinishedCheckbox = new System.Windows.Forms.CheckBox();
         this.emailText = new System.Windows.Forms.TextBox();
         this.label11 = new System.Windows.Forms.Label();
         this.groupBox1.SuspendLayout();
         this.SuspendLayout();
         // 
         // accountRefText
         // 
         this.accountRefText.Location = new System.Drawing.Point(85, 12);
         this.accountRefText.MaxLength = 8;
         this.accountRefText.Name = "accountRefText";
         this.accountRefText.Size = new System.Drawing.Size(115, 20);
         this.accountRefText.TabIndex = 1;
         // 
         // nameText
         // 
         this.nameText.Location = new System.Drawing.Point(85, 40);
         this.nameText.MaxLength = 60;
         this.nameText.Name = "nameText";
         this.nameText.Size = new System.Drawing.Size(115, 20);
         this.nameText.TabIndex = 3;
         // 
         // auxiliaryAddressText
         // 
         this.auxiliaryAddressText.Location = new System.Drawing.Point(256, 42);
         this.auxiliaryAddressText.MaxLength = 200;
         this.auxiliaryAddressText.Multiline = true;
         this.auxiliaryAddressText.Name = "auxiliaryAddressText";
         this.auxiliaryAddressText.Size = new System.Drawing.Size(237, 84);
         this.auxiliaryAddressText.TabIndex = 1;
         // 
         // clientInvoiceRefText
         // 
         this.clientInvoiceRefText.Location = new System.Drawing.Point(85, 72);
         this.clientInvoiceRefText.MaxLength = 30;
         this.clientInvoiceRefText.Name = "clientInvoiceRefText";
         this.clientInvoiceRefText.Size = new System.Drawing.Size(115, 20);
         this.clientInvoiceRefText.TabIndex = 5;
         // 
         // descriptionText
         // 
         this.descriptionText.Location = new System.Drawing.Point(85, 106);
         this.descriptionText.MaxLength = 60;
         this.descriptionText.Name = "descriptionText";
         this.descriptionText.Size = new System.Drawing.Size(115, 20);
         this.descriptionText.TabIndex = 9;
         // 
         // fixedFeeText
         // 
         this.fixedFeeText.Location = new System.Drawing.Point(85, 252);
         this.fixedFeeText.MaxLength = 20;
         this.fixedFeeText.Name = "fixedFeeText";
         this.fixedFeeText.Size = new System.Drawing.Size(64, 20);
         this.fixedFeeText.TabIndex = 15;
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(13, 12);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(67, 13);
         this.label1.TabIndex = 0;
         this.label1.Text = "Account Ref";
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(13, 40);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(35, 13);
         this.label2.TabIndex = 2;
         this.label2.Text = "Name";
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Location = new System.Drawing.Point(246, 26);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(137, 13);
         this.label3.TabIndex = 4;
         this.label3.Text = "Invoice address (if different)";
         // 
         // label4
         // 
         this.label4.AutoSize = true;
         this.label4.Location = new System.Drawing.Point(13, 66);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(52, 26);
         this.label4.TabIndex = 4;
         this.label4.Text = "Invoice\r\nreference";
         // 
         // label5
         // 
         this.label5.AutoSize = true;
         this.label5.Location = new System.Drawing.Point(11, 106);
         this.label5.Name = "label5";
         this.label5.Size = new System.Drawing.Size(60, 13);
         this.label5.TabIndex = 8;
         this.label5.Text = "Description";
         // 
         // label6
         // 
         this.label6.AutoSize = true;
         this.label6.Location = new System.Drawing.Point(13, 252);
         this.label6.Name = "label6";
         this.label6.Size = new System.Drawing.Size(50, 13);
         this.label6.TabIndex = 14;
         this.label6.Text = "Fixed fee";
         // 
         // addressText
         // 
         this.addressText.Location = new System.Drawing.Point(6, 42);
         this.addressText.MaxLength = 200;
         this.addressText.Multiline = true;
         this.addressText.Name = "addressText";
         this.addressText.Size = new System.Drawing.Size(237, 84);
         this.addressText.TabIndex = 0;
         // 
         // label7
         // 
         this.label7.AutoSize = true;
         this.label7.Location = new System.Drawing.Point(3, 26);
         this.label7.Name = "label7";
         this.label7.Size = new System.Drawing.Size(73, 13);
         this.label7.TabIndex = 12;
         this.label7.Text = "Client address";
         // 
         // saveChangesButton
         // 
         this.saveChangesButton.Location = new System.Drawing.Point(12, 475);
         this.saveChangesButton.Name = "saveChangesButton";
         this.saveChangesButton.Size = new System.Drawing.Size(91, 30);
         this.saveChangesButton.TabIndex = 19;
         this.saveChangesButton.Text = "Save changes";
         this.saveChangesButton.UseVisualStyleBackColor = true;
         this.saveChangesButton.Click += new System.EventHandler(this.saveChangesButton_Click);
         // 
         // closeButton
         // 
         this.closeButton.Location = new System.Drawing.Point(109, 475);
         this.closeButton.Name = "closeButton";
         this.closeButton.Size = new System.Drawing.Size(91, 30);
         this.closeButton.TabIndex = 20;
         this.closeButton.Text = "Close";
         this.closeButton.UseVisualStyleBackColor = true;
         this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
         // 
         // serviceLevelCombo
         // 
         this.serviceLevelCombo.FormattingEnabled = true;
         this.serviceLevelCombo.Location = new System.Drawing.Point(85, 225);
         this.serviceLevelCombo.Name = "serviceLevelCombo";
         this.serviceLevelCombo.Size = new System.Drawing.Size(115, 21);
         this.serviceLevelCombo.TabIndex = 13;
         // 
         // label8
         // 
         this.label8.AutoSize = true;
         this.label8.Location = new System.Drawing.Point(11, 225);
         this.label8.Name = "label8";
         this.label8.Size = new System.Drawing.Size(68, 13);
         this.label8.TabIndex = 12;
         this.label8.Text = "Service level";
         // 
         // label9
         // 
         this.label9.AutoSize = true;
         this.label9.Location = new System.Drawing.Point(9, 280);
         this.label9.Name = "label9";
         this.label9.Size = new System.Drawing.Size(59, 13);
         this.label9.TabIndex = 16;
         this.label9.Text = "Fixed Units";
         // 
         // fixedUnitsText
         // 
         this.fixedUnitsText.Location = new System.Drawing.Point(85, 278);
         this.fixedUnitsText.MaxLength = 20;
         this.fixedUnitsText.Name = "fixedUnitsText";
         this.fixedUnitsText.Size = new System.Drawing.Size(64, 20);
         this.fixedUnitsText.TabIndex = 17;
         // 
         // groupBox1
         // 
         this.groupBox1.Controls.Add(this.label3);
         this.groupBox1.Controls.Add(this.auxiliaryAddressText);
         this.groupBox1.Controls.Add(this.addressText);
         this.groupBox1.Controls.Add(this.label7);
         this.groupBox1.Location = new System.Drawing.Point(12, 313);
         this.groupBox1.Name = "groupBox1";
         this.groupBox1.Size = new System.Drawing.Size(499, 138);
         this.groupBox1.TabIndex = 18;
         this.groupBox1.TabStop = false;
         this.groupBox1.Text = "Address";
         // 
         // label10
         // 
         this.label10.AutoSize = true;
         this.label10.Location = new System.Drawing.Point(232, 59);
         this.label10.Name = "label10";
         this.label10.Size = new System.Drawing.Size(47, 26);
         this.label10.TabIndex = 6;
         this.label10.Text = "Contract\r\nFinished";
         // 
         // contractFinishedCheckbox
         // 
         this.contractFinishedCheckbox.AutoSize = true;
         this.contractFinishedCheckbox.Location = new System.Drawing.Point(285, 65);
         this.contractFinishedCheckbox.Name = "contractFinishedCheckbox";
         this.contractFinishedCheckbox.Size = new System.Drawing.Size(15, 14);
         this.contractFinishedCheckbox.TabIndex = 7;
         this.contractFinishedCheckbox.UseVisualStyleBackColor = true;
         // 
         // emailText
         // 
         this.emailText.Location = new System.Drawing.Point(85, 135);
         this.emailText.MaxLength = 200;
         this.emailText.Multiline = true;
         this.emailText.Name = "emailText";
         this.emailText.Size = new System.Drawing.Size(237, 84);
         this.emailText.TabIndex = 11;
         // 
         // label11
         // 
         this.label11.AutoSize = true;
         this.label11.Location = new System.Drawing.Point(13, 138);
         this.label11.Name = "label11";
         this.label11.Size = new System.Drawing.Size(55, 26);
         this.label11.TabIndex = 10;
         this.label11.Text = "e-mail\r\naddresses";
         // 
         // ContractDetailsForm
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(525, 517);
         this.Controls.Add(this.label11);
         this.Controls.Add(this.emailText);
         this.Controls.Add(this.contractFinishedCheckbox);
         this.Controls.Add(this.label10);
         this.Controls.Add(this.groupBox1);
         this.Controls.Add(this.label9);
         this.Controls.Add(this.fixedUnitsText);
         this.Controls.Add(this.label8);
         this.Controls.Add(this.serviceLevelCombo);
         this.Controls.Add(this.closeButton);
         this.Controls.Add(this.saveChangesButton);
         this.Controls.Add(this.label6);
         this.Controls.Add(this.label5);
         this.Controls.Add(this.label4);
         this.Controls.Add(this.label2);
         this.Controls.Add(this.label1);
         this.Controls.Add(this.fixedFeeText);
         this.Controls.Add(this.descriptionText);
         this.Controls.Add(this.clientInvoiceRefText);
         this.Controls.Add(this.nameText);
         this.Controls.Add(this.accountRefText);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
         this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
         this.MaximizeBox = false;
         this.MinimizeBox = false;
         this.Name = "ContractDetailsForm";
         this.Text = "Contract details";
         this.Load += new System.EventHandler(this.ContractDetailsForm_Load);
         this.groupBox1.ResumeLayout(false);
         this.groupBox1.PerformLayout();
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.TextBox accountRefText;
      private System.Windows.Forms.TextBox nameText;
      private System.Windows.Forms.TextBox auxiliaryAddressText;
      private System.Windows.Forms.TextBox clientInvoiceRefText;
      private System.Windows.Forms.TextBox descriptionText;
      private System.Windows.Forms.TextBox fixedFeeText;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.TextBox addressText;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.Button saveChangesButton;
      private System.Windows.Forms.Button closeButton;
      private System.Windows.Forms.ComboBox serviceLevelCombo;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.TextBox fixedUnitsText;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.Label label10;
      private System.Windows.Forms.CheckBox contractFinishedCheckbox;
      private System.Windows.Forms.TextBox emailText;
      private System.Windows.Forms.Label label11;
   }
}