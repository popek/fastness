﻿namespace FastnessForms.Views
{
   partial class PerformHeavyTaskForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.components = new System.ComponentModel.Container();
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PerformHeavyTaskForm));
         this.async = new System.ComponentModel.BackgroundWorker();
         this.lbInfo = new System.Windows.Forms.Label();
         this.timer1 = new System.Windows.Forms.Timer(this.components);
         this.SuspendLayout();
         // 
         // async
         // 
         this.async.WorkerReportsProgress = true;
         this.async.DoWork += new System.ComponentModel.DoWorkEventHandler(this.async_DoWork);
         this.async.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.async_RunWorkerCompleted);
         // 
         // lbInfo
         // 
         this.lbInfo.BackColor = System.Drawing.SystemColors.Control;
         this.lbInfo.Cursor = System.Windows.Forms.Cursors.Default;
         this.lbInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
         this.lbInfo.ForeColor = System.Drawing.SystemColors.ControlText;
         this.lbInfo.Location = new System.Drawing.Point(12, 9);
         this.lbInfo.Name = "lbInfo";
         this.lbInfo.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.lbInfo.Size = new System.Drawing.Size(521, 35);
         this.lbInfo.TabIndex = 12;
         this.lbInfo.Text = "lbInfo";
         this.lbInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // timer1
         // 
         this.timer1.Interval = 1000;
         this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
         // 
         // PerformHeavyTaskForm
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(545, 54);
         this.ControlBox = false;
         this.Controls.Add(this.lbInfo);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
         this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
         this.Name = "PerformHeavyTaskForm";
         this.Text = "Running task...";
         this.Load += new System.EventHandler(this.PerformHeavyTaskForm_Load);
         this.ResumeLayout(false);

      }

      #endregion

      private System.ComponentModel.BackgroundWorker async;
      public System.Windows.Forms.Label lbInfo;
      private System.Windows.Forms.Timer timer1;
   }
}