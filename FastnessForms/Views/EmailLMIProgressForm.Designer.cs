﻿namespace FastnessForms.Views
{
   partial class EmailLMIProgressForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmailLMIProgressForm));
         this.asyncLMISender = new System.ComponentModel.BackgroundWorker();
         this.pbProgressInfo = new System.Windows.Forms.ProgressBar();
         this.lbInfo = new System.Windows.Forms.Label();
         this.dgvStatuses = new System.Windows.Forms.DataGridView();
         this.btClose = new System.Windows.Forms.Button();
         this.btResendFailures = new System.Windows.Forms.Button();
         ((System.ComponentModel.ISupportInitialize)(this.dgvStatuses)).BeginInit();
         this.SuspendLayout();
         // 
         // asyncLMISender
         // 
         this.asyncLMISender.WorkerReportsProgress = true;
         this.asyncLMISender.DoWork += new System.ComponentModel.DoWorkEventHandler(this.asyncLMISender_DoWork);
         this.asyncLMISender.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.asyncLMISender_ProgressChanged);
         this.asyncLMISender.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.asyncLMISender_RunWorkerCompleted);
         // 
         // pbProgressInfo
         // 
         this.pbProgressInfo.Location = new System.Drawing.Point(12, 38);
         this.pbProgressInfo.Name = "pbProgressInfo";
         this.pbProgressInfo.Size = new System.Drawing.Size(521, 23);
         this.pbProgressInfo.TabIndex = 0;
         // 
         // lbInfo
         // 
         this.lbInfo.BackColor = System.Drawing.SystemColors.Control;
         this.lbInfo.Cursor = System.Windows.Forms.Cursors.Default;
         this.lbInfo.ForeColor = System.Drawing.SystemColors.ControlText;
         this.lbInfo.Location = new System.Drawing.Point(12, 9);
         this.lbInfo.Name = "lbInfo";
         this.lbInfo.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.lbInfo.Size = new System.Drawing.Size(521, 26);
         this.lbInfo.TabIndex = 12;
         this.lbInfo.Text = "lbInfo";
         // 
         // dgvStatuses
         // 
         this.dgvStatuses.AllowUserToAddRows = false;
         this.dgvStatuses.AllowUserToDeleteRows = false;
         this.dgvStatuses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
         this.dgvStatuses.Location = new System.Drawing.Point(12, 67);
         this.dgvStatuses.MultiSelect = false;
         this.dgvStatuses.Name = "dgvStatuses";
         this.dgvStatuses.ReadOnly = true;
         this.dgvStatuses.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
         this.dgvStatuses.Size = new System.Drawing.Size(521, 276);
         this.dgvStatuses.TabIndex = 13;
         this.dgvStatuses.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvStatuses_CellFormatting);
         // 
         // btClose
         // 
         this.btClose.Location = new System.Drawing.Point(94, 359);
         this.btClose.Name = "btClose";
         this.btClose.Size = new System.Drawing.Size(76, 36);
         this.btClose.TabIndex = 26;
         this.btClose.Text = "Close";
         this.btClose.UseVisualStyleBackColor = true;
         this.btClose.Click += new System.EventHandler(this.btClose_Click);
         // 
         // btResendFailures
         // 
         this.btResendFailures.Location = new System.Drawing.Point(12, 359);
         this.btResendFailures.Name = "btResendFailures";
         this.btResendFailures.Size = new System.Drawing.Size(76, 36);
         this.btResendFailures.TabIndex = 27;
         this.btResendFailures.Text = "Resend failures";
         this.btResendFailures.UseVisualStyleBackColor = true;
         this.btResendFailures.Click += new System.EventHandler(this.btResendFailures_Click);
         // 
         // EmailLMIProgressForm
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(545, 406);
         this.ControlBox = false;
         this.Controls.Add(this.btResendFailures);
         this.Controls.Add(this.btClose);
         this.Controls.Add(this.dgvStatuses);
         this.Controls.Add(this.lbInfo);
         this.Controls.Add(this.pbProgressInfo);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
         this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
         this.Name = "EmailLMIProgressForm";
         this.Text = "Printing last month invoices";
         this.Load += new System.EventHandler(this.EmailLMIProgressForm_Load);
         ((System.ComponentModel.ISupportInitialize)(this.dgvStatuses)).EndInit();
         this.ResumeLayout(false);

      }

      #endregion

      private System.ComponentModel.BackgroundWorker asyncLMISender;
      private System.Windows.Forms.ProgressBar pbProgressInfo;
      public System.Windows.Forms.Label lbInfo;
      private System.Windows.Forms.DataGridView dgvStatuses;
      private System.Windows.Forms.Button btClose;
      private System.Windows.Forms.Button btResendFailures;
   }
}