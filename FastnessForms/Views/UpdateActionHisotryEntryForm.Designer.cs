﻿namespace FastnessForms.Views
{
   partial class UpdateActionHisotryEntryForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateActionHisotryEntryForm));
         this.txtLocation = new System.Windows.Forms.TextBox();
         this.label8 = new System.Windows.Forms.Label();
         this.label11 = new System.Windows.Forms.Label();
         this.txtCharge = new System.Windows.Forms.TextBox();
         this.dtpDate = new System.Windows.Forms.DateTimePicker();
         this.label9 = new System.Windows.Forms.Label();
         this.txtAuthoriziedBy = new System.Windows.Forms.TextBox();
         this.label10 = new System.Windows.Forms.Label();
         this.txtNotes = new System.Windows.Forms.TextBox();
         this.label4 = new System.Windows.Forms.Label();
         this.lbAction = new System.Windows.Forms.Label();
         this.btSaveChanges = new System.Windows.Forms.Button();
         this.btClose = new System.Windows.Forms.Button();
         this.SuspendLayout();
         // 
         // txtLocation
         // 
         this.txtLocation.AcceptsReturn = true;
         this.txtLocation.BackColor = System.Drawing.SystemColors.Window;
         this.txtLocation.Cursor = System.Windows.Forms.Cursors.IBeam;
         this.txtLocation.ForeColor = System.Drawing.SystemColors.WindowText;
         this.txtLocation.Location = new System.Drawing.Point(92, 45);
         this.txtLocation.MaxLength = 0;
         this.txtLocation.Name = "txtLocation";
         this.txtLocation.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.txtLocation.Size = new System.Drawing.Size(89, 20);
         this.txtLocation.TabIndex = 56;
         // 
         // label8
         // 
         this.label8.AutoSize = true;
         this.label8.Location = new System.Drawing.Point(36, 48);
         this.label8.Name = "label8";
         this.label8.Size = new System.Drawing.Size(48, 13);
         this.label8.TabIndex = 57;
         this.label8.Text = "Location";
         // 
         // label11
         // 
         this.label11.AutoSize = true;
         this.label11.Location = new System.Drawing.Point(43, 74);
         this.label11.Name = "label11";
         this.label11.Size = new System.Drawing.Size(41, 13);
         this.label11.TabIndex = 61;
         this.label11.Text = "Charge";
         // 
         // txtCharge
         // 
         this.txtCharge.AcceptsReturn = true;
         this.txtCharge.BackColor = System.Drawing.SystemColors.Window;
         this.txtCharge.Cursor = System.Windows.Forms.Cursors.IBeam;
         this.txtCharge.ForeColor = System.Drawing.SystemColors.WindowText;
         this.txtCharge.Location = new System.Drawing.Point(92, 71);
         this.txtCharge.MaxLength = 0;
         this.txtCharge.Name = "txtCharge";
         this.txtCharge.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.txtCharge.Size = new System.Drawing.Size(89, 20);
         this.txtCharge.TabIndex = 60;
         // 
         // dtpDate
         // 
         this.dtpDate.Location = new System.Drawing.Point(92, 97);
         this.dtpDate.Name = "dtpDate";
         this.dtpDate.Size = new System.Drawing.Size(135, 20);
         this.dtpDate.TabIndex = 62;
         // 
         // label9
         // 
         this.label9.AutoSize = true;
         this.label9.Location = new System.Drawing.Point(10, 103);
         this.label9.Name = "label9";
         this.label9.Size = new System.Drawing.Size(74, 13);
         this.label9.TabIndex = 63;
         this.label9.Text = "Date of action";
         // 
         // txtAuthoriziedBy
         // 
         this.txtAuthoriziedBy.AcceptsReturn = true;
         this.txtAuthoriziedBy.BackColor = System.Drawing.SystemColors.Window;
         this.txtAuthoriziedBy.Cursor = System.Windows.Forms.Cursors.IBeam;
         this.txtAuthoriziedBy.ForeColor = System.Drawing.SystemColors.WindowText;
         this.txtAuthoriziedBy.Location = new System.Drawing.Point(92, 123);
         this.txtAuthoriziedBy.MaxLength = 0;
         this.txtAuthoriziedBy.Name = "txtAuthoriziedBy";
         this.txtAuthoriziedBy.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.txtAuthoriziedBy.Size = new System.Drawing.Size(135, 20);
         this.txtAuthoriziedBy.TabIndex = 64;
         this.txtAuthoriziedBy.Tag = "0";
         // 
         // label10
         // 
         this.label10.AutoSize = true;
         this.label10.Location = new System.Drawing.Point(13, 126);
         this.label10.Name = "label10";
         this.label10.Size = new System.Drawing.Size(71, 13);
         this.label10.TabIndex = 65;
         this.label10.Text = "Authorized by";
         // 
         // txtNotes
         // 
         this.txtNotes.AcceptsReturn = true;
         this.txtNotes.BackColor = System.Drawing.SystemColors.Window;
         this.txtNotes.Cursor = System.Windows.Forms.Cursors.IBeam;
         this.txtNotes.ForeColor = System.Drawing.SystemColors.WindowText;
         this.txtNotes.Location = new System.Drawing.Point(92, 149);
         this.txtNotes.MaxLength = 0;
         this.txtNotes.Multiline = true;
         this.txtNotes.Name = "txtNotes";
         this.txtNotes.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.txtNotes.Size = new System.Drawing.Size(135, 49);
         this.txtNotes.TabIndex = 66;
         // 
         // label4
         // 
         this.label4.AutoSize = true;
         this.label4.Location = new System.Drawing.Point(49, 152);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(35, 13);
         this.label4.TabIndex = 67;
         this.label4.Text = "Notes";
         // 
         // lbAction
         // 
         this.lbAction.BackColor = System.Drawing.SystemColors.Control;
         this.lbAction.Cursor = System.Windows.Forms.Cursors.Default;
         this.lbAction.ForeColor = System.Drawing.SystemColors.ControlText;
         this.lbAction.Location = new System.Drawing.Point(12, 9);
         this.lbAction.Name = "lbAction";
         this.lbAction.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.lbAction.Size = new System.Drawing.Size(217, 33);
         this.lbAction.TabIndex = 68;
         this.lbAction.Text = "Label8";
         this.lbAction.TextAlign = System.Drawing.ContentAlignment.TopCenter;
         // 
         // btSaveChanges
         // 
         this.btSaveChanges.Location = new System.Drawing.Point(12, 216);
         this.btSaveChanges.Name = "btSaveChanges";
         this.btSaveChanges.Size = new System.Drawing.Size(85, 35);
         this.btSaveChanges.TabIndex = 69;
         this.btSaveChanges.Text = "Save changes";
         this.btSaveChanges.UseVisualStyleBackColor = true;
         this.btSaveChanges.Click += new System.EventHandler(this.btSaveChanges_Click);
         // 
         // btClose
         // 
         this.btClose.Location = new System.Drawing.Point(142, 216);
         this.btClose.Name = "btClose";
         this.btClose.Size = new System.Drawing.Size(85, 35);
         this.btClose.TabIndex = 70;
         this.btClose.Text = "Cancel";
         this.btClose.UseVisualStyleBackColor = true;
         this.btClose.Click += new System.EventHandler(this.btClose_Click);
         // 
         // UpdateActionHisotryEntryForm
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(238, 260);
         this.Controls.Add(this.btClose);
         this.Controls.Add(this.btSaveChanges);
         this.Controls.Add(this.lbAction);
         this.Controls.Add(this.txtNotes);
         this.Controls.Add(this.label4);
         this.Controls.Add(this.txtAuthoriziedBy);
         this.Controls.Add(this.label10);
         this.Controls.Add(this.dtpDate);
         this.Controls.Add(this.label9);
         this.Controls.Add(this.label11);
         this.Controls.Add(this.txtCharge);
         this.Controls.Add(this.txtLocation);
         this.Controls.Add(this.label8);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
         this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
         this.MaximizeBox = false;
         this.MinimizeBox = false;
         this.Name = "UpdateActionHisotryEntryForm";
         this.Text = "Amend history";
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      public System.Windows.Forms.TextBox txtLocation;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.Label label11;
      public System.Windows.Forms.TextBox txtCharge;
      private System.Windows.Forms.DateTimePicker dtpDate;
      private System.Windows.Forms.Label label9;
      public System.Windows.Forms.TextBox txtAuthoriziedBy;
      private System.Windows.Forms.Label label10;
      public System.Windows.Forms.TextBox txtNotes;
      private System.Windows.Forms.Label label4;
      public System.Windows.Forms.Label lbAction;
      private System.Windows.Forms.Button btSaveChanges;
      private System.Windows.Forms.Button btClose;
   }
}