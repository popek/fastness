﻿using Fastness.DataModel;
using Fastness.DataModel.Transaction;
using Fastness.DataModel.Utils;
using System;
using System.Windows.Forms;

namespace FastnessForms.Views
{
   public partial class ActionGenericForm : Form
   {
      ActionContext actionCtx;
      ActionFormContext formCtx;
      Action<FastnessActionContext, string> itemSaver;

      fastnessEntities dbCtx = new fastnessEntities();
      ItemDetail fetchedItem = null;
      TextBoxChangesTracker itemNumberTrack = null;

      private bool ReadyToMakeAction
      { 
         set 
         {
            this.txtCharge.Enabled = false; // charge enabled explicitly only by pressing button
            this.dtpGeneric.Enabled = value;
            this.txtAuthoriziedBy.Enabled = value;
            this.txtNotes.Enabled = value;
            this.btMake.Enabled = value;
            this.btShow.Enabled = !value;
            this.btAllowChargeChange.Enabled = value;
         } 
      }

      public ActionGenericForm(ActionContext actionCtx, ActionFormContext formCtx, Action<FastnessActionContext, string> itemSaver)
      {
         this.actionCtx = actionCtx;
         this.formCtx = formCtx;
         this.itemSaver = itemSaver;

         InitializeComponent();

         this.Text = this.actionCtx.FormCaption;

         this.lbActionTypeInfo.Text = actionCtx.CtrAction.Description;
         this.lbContractIDInfo.Text = actionCtx.Ctr.AccountRef;
         this.lbContractNameInfo.Text = actionCtx.Ctr.Name;

         this.txtItemNumber.AddKeyPressWithTextLengthLimitAndIntegerValue(DBFieldsConstraint.ITEM_NUMBER_LENGTH);
         this.txtConfirmItemNumber.AddKeyPressWithTextLengthLimitAndIntegerValue(DBFieldsConstraint.ITEM_NUMBER_LENGTH);
         
         this.txtCharge.AddKeyPressWithNumericValue();

         this.txtNotes.AddKeyPressWithTextLengthLimit(DBFieldsConstraint.ACTION_HISTORY_NOTES_MAX_LENGHT);
         this.txtAuthoriziedBy.AddKeyPressWithTextLengthLimit(DBFieldsConstraint.ACTION_HISTORY_AUTHORIZED_BY_MAX_LENGHT);

         this.txtCharge.Text = actionCtx.CtrAction.Charge.ToString();
         this.dtpGeneric.Value = this.formCtx.LastActionDate;
         this.txtAuthoriziedBy.Text = this.formCtx.LastActionAuthorizer;

         itemNumberTrack = new TextBoxChangesTracker(this.txtItemNumber, (itemNumberChanged) =>
         {
            ReadyToMakeAction = !itemNumberChanged;
         });

         ReadyToMakeAction = false;
      }

      private bool FormDataValid()
      {
         if (!txtItemNumber.IsTextLenthExactlyTheSame(DBFieldsConstraint.ITEM_NUMBER_LENGTH, "Box number"))
         {
            return false;
         }

         if (!txtItemNumber.IsDigitsOnlyText("Box number"))
         {
            return false;               

         }
         if (!new AskDb(dbCtx).ItemAlreadyExists(this.txtItemNumber.Text))
         {
            this.txtItemNumber.NotifyTextError("Box with this number does not exists!");
            return false;
         }
         if (this.txtItemNumber.Text != this.txtConfirmItemNumber.Text)
         {
            this.txtConfirmItemNumber.NotifyTextError("Box Number and confirmed box number are different!");
            return false;
         }

         if (!this.txtCharge.IsValidNumericValueFormat("Charge"))
         {
            return false;
         }

         if (!this.dtpGeneric.IsActionDateConstraintsValid(formCtx.LastActionDate, actionCtx.CtrAction.Description))
         {
            return false;
         }

         if (this.txtNotes.IsTextToLong(DBFieldsConstraint.ACTION_HISTORY_NOTES_MAX_LENGHT, "notes"))
         {
            return false;
         }
         if (this.txtAuthoriziedBy.IsTextToLong(DBFieldsConstraint.ACTION_HISTORY_AUTHORIZED_BY_MAX_LENGHT, "Authorization name"))
         {
            return false;
         }

         return true;
      }

      private void btGeneric_Click(object sender, EventArgs e)
      {
         if (!FormDataValid())
         {
            return;
         }

         itemSaver(
            new FastnessActionContext(
               fetchedItem,
               actionCtx.Ctr,
               actionCtx.CtrAction,
               dtpGeneric.Value,
               Double.Parse(this.txtCharge.Text),
               txtAuthoriziedBy.Text),
            txtNotes.Text);

         formCtx.Update(txtAuthoriziedBy.Text, dtpGeneric.Value);

         Close();
      }

      private void btClose_Click(object sender, EventArgs e)
      {
         Close();
      }

      private void btShow_Click(object sender, EventArgs e)
      {
         var validateFormBeforeShow = new Func<bool>(() =>
         {
            if (!txtItemNumber.IsTextLenthExactlyTheSame(DBFieldsConstraint.ITEM_NUMBER_LENGTH, "Box number"))
            {
               return false;
            }
            if (!txtItemNumber.IsDigitsOnlyText("Box number"))
            {
               return false;
            }
            if (!new AskDb(dbCtx).ItemAlreadyExists(txtItemNumber.Text))
            {
               txtItemNumber.NotifyTextError("Box with this number does not exists!");
               return false;
            }
            if (txtItemNumber.Text != txtConfirmItemNumber.Text)
            {
               txtConfirmItemNumber.NotifyTextError("Box Number and confirmed box number are different!");
               return false;
            }
            return true;
         });

         if (!validateFormBeforeShow())
         {
            return;
         }

         fetchedItem = new FetchFromDb(dbCtx).ItemBasedOnItemNo(txtItemNumber.Text);

         if (fetchedItem.ContractID != actionCtx.Ctr.ContractID)
         {
            UserInteraction.ShowError(
               "Box '{0}' does not belong to '{1}'! You must choose box owned by '{1}'", 
               fetchedItem.ItemNumber, actionCtx.Ctr.Name);
            fetchedItem = null;
            return;
         }
         if (fetchedItem.Destroyed ?? false)
         {
            UserInteraction.ShowError(
               "Box '{0}' has been destroyed! You must choose box which still exists.", 
               fetchedItem.ItemNumber);
            fetchedItem = null;
            return;
         }
         if (!(fetchedItem.InStock ?? true))
         {
            UserInteraction.ShowError(
               "Box '{0}' is out of stock! You must choose box which is still in stock.", 
               fetchedItem.ItemNumber);
            fetchedItem = null;
            return;
         }

         txtClientItemReference.Text = fetchedItem.ClientItemRef;
         txtDescription.Text = fetchedItem.Description;
         txtType.Text = fetchedItem.Type;
         txtPercentage.Text = fetchedItem.Percentage.ToString();
         txtLocation.Text = fetchedItem.Location;

         itemNumberTrack.TrackChanges = true;
         ReadyToMakeAction = true;

         txtNotes.Focus();
      }

      private void btAllowChargeChange_Click(object sender, EventArgs e)
      {
         txtCharge.Enabled = true;
      }
   }
}
