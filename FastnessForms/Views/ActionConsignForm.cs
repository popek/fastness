﻿using Fastness.Common;
using Fastness.DataModel;
using Fastness.DataModel.Transaction;
using Fastness.DataModel.Utils;
using System;
using System.Windows.Forms;
using System.Globalization;

namespace FastnessForms.Views
{
   public partial class ActionConsignForm : Form
   {
      ItemDestructionDateHandler destructionDateHandler;

      ActionContext actionCtx;
      ActionFormContext formCtx;
      Action<FastnessActionContext> itemSaver;

      fastnessEntities dbCtx = new fastnessEntities();

      public ActionConsignForm(ActionContext actionCtx, ActionFormContext formCtx, Action<FastnessActionContext> itemSaver)
      {
         this.actionCtx = actionCtx;
         this.formCtx = formCtx;
         this.itemSaver = itemSaver;

         InitializeComponent();

         this.Text = this.actionCtx.FormCaption;

         this.txtItemNumber.AddKeyPressWithTextLengthLimitAndIntegerValue(DBFieldsConstraint.ITEM_NUMBER_LENGTH);
         this.txtConfirmItemNumber.AddKeyPressWithTextLengthLimitAndIntegerValue(DBFieldsConstraint.ITEM_NUMBER_LENGTH);
         this.txtClientItemReference.AddKeyPressWithTextLengthLimit(DBFieldsConstraint.CLIENT_ITEM_REF_MAX_LENGTH);
         this.txtDescription.AddKeyPressWithTextLengthLimit(DBFieldsConstraint.ITEM_DESCRIPTION_MAX_LENGTH);
         this.txtAuthoriziedBy.AddKeyPressWithTextLengthLimit(DBFieldsConstraint.ACTION_HISTORY_AUTHORIZED_BY_MAX_LENGHT);

         this.txtPercentage.AddKeyPressWithTextLengthLimitAndIntegerValue(DBFieldsConstraint.ITEM_PERCENTAGE_MAX_LENGTH);
         this.txtCharge.AddKeyPressWithNumericValue();
         this.txtLocation.AddKeyPressWithTextLengthLimitAndIntegerValue(DBFieldsConstraint.LOCATION_NUMBER_LENGTH);

         this.txtItemNumber.Text = "100";
         this.txtType.Text = "standard";
         this.txtCharge.Text = actionCtx.CtrAction.Charge.ToString();
         this.dtpConsignment.Value = this.formCtx.LastActionDate;
         this.txtAuthoriziedBy.Text = this.formCtx.LastActionAuthorizer;

         destructionDateHandler = new ItemDestructionDateHandler(
            destructionDate);

         txtCharge.Enabled = false;
      }

      private bool FormDataValid()
      {
         if (!txtItemNumber.IsTextLenthExactlyTheSame(DBFieldsConstraint.ITEM_NUMBER_LENGTH, "Box number"))
         {
            return false;
         }

         if (!txtItemNumber.IsDigitsOnlyText("Box number"))
         {
            return false;               

         }
         if (new AskDb(dbCtx).ItemAlreadyExists(this.txtItemNumber.Text))
         {
            this.txtItemNumber.NotifyTextError("Box with this number already exists!");
            return false;
         }
         if (this.txtItemNumber.Text != this.txtConfirmItemNumber.Text)
         {
            this.txtConfirmItemNumber.NotifyTextError("Box Number and confirmed box number are different!");
            return false;
         }

         if (this.txtClientItemReference.IsTextToLong(DBFieldsConstraint.CLIENT_ITEM_REF_MAX_LENGTH, "Item reference"))
         {
            return false;
         }

         if (this.txtDescription.IsTextToLong(DBFieldsConstraint.ITEM_DESCRIPTION_MAX_LENGTH, "Description"))
         {
            return false;
         }

         if (this.txtPercentage.IsTextToLong(DBFieldsConstraint.ITEM_PERCENTAGE_MAX_LENGTH, "Percentage"))
         {
            return false;
         }
         if (!this.txtPercentage.IsDigitsOnlyText("Percentage"))
         {
            return false;
         }

         if (!this.txtLocation.IsTextLenthExactlyTheSame(DBFieldsConstraint.LOCATION_NUMBER_LENGTH, "Location"))
         {
            return false;
         }
         if (!this.txtLocation.IsDigitsOnlyText("Location"))
         {
            return false;
         }
         if (new AskDb(dbCtx).LocationOccupied(this.txtLocation.Text))
         {
            this.txtLocation.NotifyTextError("Location is already ocupied!");
            return false;
         }
         if(!destructionDateHandler.IsDestructionDateValid())
         {
            return false;
         }
         if (!this.txtCharge.IsValidNumericValueFormat("Charge"))
         {
            return false;
         }
         if (this.txtAuthoriziedBy.IsTextToLong(DBFieldsConstraint.ACTION_HISTORY_AUTHORIZED_BY_MAX_LENGHT, "Authorization name"))
         {
            return false;
         }

         if (!this.dtpConsignment.IsActionDateConstraintsValid(formCtx.LastActionDate, actionCtx.CtrAction.Description))
         {
            return false;
         }

         return true;
      }

      private void btConsign_Click(object sender, EventArgs e)
      {
         if (!FormDataValid())
         {
            return;
         }

         itemSaver(
            new FastnessActionContext(
               new ItemDetail
               {
                  ItemNumber = txtItemNumber.Text,
                  ContractID = actionCtx.Ctr.ContractID,
                  ClientItemRef  = txtClientItemReference.Text,
                  Description = txtDescription.Text,
                  DestructionDate = destructionDateHandler.GetDestructionDateString(),
                  Type = txtType.Text,
                  Percentage = Int32.Parse(txtPercentage.Text),
                  Location = txtLocation.Text
               },
               actionCtx.Ctr,
               actionCtx.CtrAction,
               dtpConsignment.Value,
               Double.Parse(txtCharge.Text),
               txtAuthoriziedBy.Text));

         formCtx.Update(txtAuthoriziedBy.Text, dtpConsignment.Value);

         Close();
      }

      private void btClose_Click(object sender, EventArgs e)
      {
         Close();
      }

      private void btAllowChargeChange_Click(object sender, EventArgs e)
      {
         txtCharge.Enabled = true;
      }
   }
}
