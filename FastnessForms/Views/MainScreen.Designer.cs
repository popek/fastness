﻿namespace FastnessForms.Views
{
   partial class MainScreen
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainScreen));
         this.contractsGrid = new System.Windows.Forms.DataGridView();
         this.itemsGrid = new System.Windows.Forms.DataGridView();
         this.addNewContractButton = new System.Windows.Forms.Button();
         this.showContractActionsButton = new System.Windows.Forms.Button();
         this.showContractsButton = new System.Windows.Forms.Button();
         this.contractActionsGrid = new System.Windows.Forms.DataGridView();
         this.doActionButton = new System.Windows.Forms.Button();
         this.showContractHistoryButton = new System.Windows.Forms.Button();
         this.lbItemAuxiliaryInfo = new System.Windows.Forms.Label();
         this.lbContractAuxiliaryInfo = new System.Windows.Forms.Label();
         this.btSearchDescription = new System.Windows.Forms.Button();
         this.btSearchIuid = new System.Windows.Forms.Button();
         this.btSearchCuid = new System.Windows.Forms.Button();
         this.btSearchLocation = new System.Windows.Forms.Button();
         this.txtSearchDescription = new System.Windows.Forms.TextBox();
         this.txtSearchCuid = new System.Windows.Forms.TextBox();
         this.txtSearchLocation = new System.Windows.Forms.TextBox();
         this.txtSearchIuid = new System.Windows.Forms.TextBox();
         this.tbShowAllBoxes = new System.Windows.Forms.Button();
         this.btStockListReport = new System.Windows.Forms.Button();
         this.btSetVat = new System.Windows.Forms.Button();
         this.btGenerateLMI = new System.Windows.Forms.Button();
         this.btPrintLMI = new System.Windows.Forms.Button();
         this.btSaveStocks = new System.Windows.Forms.Button();
         this.groupBox1 = new System.Windows.Forms.GroupBox();
         this.btMailLMI = new System.Windows.Forms.Button();
         this.groupBox2 = new System.Windows.Forms.GroupBox();
         this.btSetInvoiceMailSettings = new System.Windows.Forms.Button();
         this.modifyFastnessUserButton = new System.Windows.Forms.Button();
         this.groupBox4 = new System.Windows.Forms.GroupBox();
         this.optFilterSearch = new System.Windows.Forms.RadioButton();
         this.optJumpSearch = new System.Windows.Forms.RadioButton();
         ((System.ComponentModel.ISupportInitialize)(this.contractsGrid)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.itemsGrid)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.contractActionsGrid)).BeginInit();
         this.groupBox1.SuspendLayout();
         this.groupBox2.SuspendLayout();
         this.groupBox4.SuspendLayout();
         this.SuspendLayout();
         // 
         // contractsGrid
         // 
         this.contractsGrid.AllowUserToAddRows = false;
         this.contractsGrid.AllowUserToDeleteRows = false;
         this.contractsGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
         this.contractsGrid.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
         this.contractsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
         this.contractsGrid.Location = new System.Drawing.Point(12, 12);
         this.contractsGrid.MultiSelect = false;
         this.contractsGrid.Name = "contractsGrid";
         this.contractsGrid.ReadOnly = true;
         this.contractsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
         this.contractsGrid.Size = new System.Drawing.Size(424, 336);
         this.contractsGrid.TabIndex = 0;
         this.contractsGrid.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.contractsGrid_CellFormatting);
         this.contractsGrid.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.contractsView_CellMouseDoubleClick);
         this.contractsGrid.SelectionChanged += new System.EventHandler(this.contractsView_SelectionChanged);
         // 
         // itemsGrid
         // 
         this.itemsGrid.AllowUserToAddRows = false;
         this.itemsGrid.AllowUserToDeleteRows = false;
         this.itemsGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.itemsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
         this.itemsGrid.Location = new System.Drawing.Point(442, 12);
         this.itemsGrid.MultiSelect = false;
         this.itemsGrid.Name = "itemsGrid";
         this.itemsGrid.ReadOnly = true;
         this.itemsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
         this.itemsGrid.Size = new System.Drawing.Size(490, 336);
         this.itemsGrid.TabIndex = 1;
         this.itemsGrid.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.itemsGrid_CellFormatting);
         this.itemsGrid.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.itemsGrid_CellMouseDoubleClick);
         this.itemsGrid.SelectionChanged += new System.EventHandler(this.itemsGrid_SelectionChanged);
         // 
         // addNewContractButton
         // 
         this.addNewContractButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.addNewContractButton.Location = new System.Drawing.Point(198, 602);
         this.addNewContractButton.Name = "addNewContractButton";
         this.addNewContractButton.Size = new System.Drawing.Size(87, 36);
         this.addNewContractButton.TabIndex = 7;
         this.addNewContractButton.Text = "Add contract";
         this.addNewContractButton.UseVisualStyleBackColor = true;
         this.addNewContractButton.Click += new System.EventHandler(this.addNewContractButton_Click);
         // 
         // showContractActionsButton
         // 
         this.showContractActionsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.showContractActionsButton.Location = new System.Drawing.Point(12, 560);
         this.showContractActionsButton.Name = "showContractActionsButton";
         this.showContractActionsButton.Size = new System.Drawing.Size(87, 36);
         this.showContractActionsButton.TabIndex = 2;
         this.showContractActionsButton.Text = "Show contract actions";
         this.showContractActionsButton.UseVisualStyleBackColor = true;
         this.showContractActionsButton.Click += new System.EventHandler(this.showContractActionsButton_Click);
         // 
         // showContractsButton
         // 
         this.showContractsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.showContractsButton.Enabled = false;
         this.showContractsButton.Location = new System.Drawing.Point(105, 560);
         this.showContractsButton.Name = "showContractsButton";
         this.showContractsButton.Size = new System.Drawing.Size(87, 36);
         this.showContractsButton.TabIndex = 3;
         this.showContractsButton.Text = "Show contracts";
         this.showContractsButton.UseVisualStyleBackColor = true;
         this.showContractsButton.Click += new System.EventHandler(this.showContractsButton_Click);
         // 
         // contractActionsGrid
         // 
         this.contractActionsGrid.AllowUserToAddRows = false;
         this.contractActionsGrid.AllowUserToDeleteRows = false;
         this.contractActionsGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
         this.contractActionsGrid.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
         this.contractActionsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
         this.contractActionsGrid.Enabled = false;
         this.contractActionsGrid.Location = new System.Drawing.Point(305, 572);
         this.contractActionsGrid.MultiSelect = false;
         this.contractActionsGrid.Name = "contractActionsGrid";
         this.contractActionsGrid.ReadOnly = true;
         this.contractActionsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
         this.contractActionsGrid.Size = new System.Drawing.Size(44, 67);
         this.contractActionsGrid.TabIndex = 9;
         this.contractActionsGrid.Visible = false;
         this.contractActionsGrid.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.contractActoinsGrid_CellMouseDoubleClick);
         // 
         // doActionButton
         // 
         this.doActionButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.doActionButton.Enabled = false;
         this.doActionButton.Location = new System.Drawing.Point(12, 602);
         this.doActionButton.Name = "doActionButton";
         this.doActionButton.Size = new System.Drawing.Size(87, 36);
         this.doActionButton.TabIndex = 5;
         this.doActionButton.Text = "Do action";
         this.doActionButton.UseVisualStyleBackColor = true;
         this.doActionButton.Click += new System.EventHandler(this.doActionButton_Click);
         // 
         // showContractHistoryButton
         // 
         this.showContractHistoryButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.showContractHistoryButton.Location = new System.Drawing.Point(105, 602);
         this.showContractHistoryButton.Name = "showContractHistoryButton";
         this.showContractHistoryButton.Size = new System.Drawing.Size(87, 36);
         this.showContractHistoryButton.TabIndex = 6;
         this.showContractHistoryButton.Text = "Show contract history";
         this.showContractHistoryButton.UseVisualStyleBackColor = true;
         this.showContractHistoryButton.Click += new System.EventHandler(this.showContractHistoryButton_Click);
         // 
         // lbItemAuxiliaryInfo
         // 
         this.lbItemAuxiliaryInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.lbItemAuxiliaryInfo.AutoEllipsis = true;
         this.lbItemAuxiliaryInfo.Font = new System.Drawing.Font("Courier New", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
         this.lbItemAuxiliaryInfo.Location = new System.Drawing.Point(559, 369);
         this.lbItemAuxiliaryInfo.Name = "lbItemAuxiliaryInfo";
         this.lbItemAuxiliaryInfo.Size = new System.Drawing.Size(361, 173);
         this.lbItemAuxiliaryInfo.TabIndex = 12;
         this.lbItemAuxiliaryInfo.Text = "label1";
         // 
         // lbContractAuxiliaryInfo
         // 
         this.lbContractAuxiliaryInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.lbContractAuxiliaryInfo.Font = new System.Drawing.Font("Courier New", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
         this.lbContractAuxiliaryInfo.Location = new System.Drawing.Point(9, 369);
         this.lbContractAuxiliaryInfo.Name = "lbContractAuxiliaryInfo";
         this.lbContractAuxiliaryInfo.Size = new System.Drawing.Size(269, 173);
         this.lbContractAuxiliaryInfo.TabIndex = 13;
         this.lbContractAuxiliaryInfo.Text = "label1";
         // 
         // btSearchDescription
         // 
         this.btSearchDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.btSearchDescription.Location = new System.Drawing.Point(620, 559);
         this.btSearchDescription.Name = "btSearchDescription";
         this.btSearchDescription.Size = new System.Drawing.Size(76, 36);
         this.btSearchDescription.TabIndex = 10;
         this.btSearchDescription.Text = "Search (Description)";
         this.btSearchDescription.UseVisualStyleBackColor = true;
         this.btSearchDescription.Click += new System.EventHandler(this.btSearchDescription_Click);
         // 
         // btSearchIuid
         // 
         this.btSearchIuid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.btSearchIuid.Location = new System.Drawing.Point(783, 560);
         this.btSearchIuid.Name = "btSearchIuid";
         this.btSearchIuid.Size = new System.Drawing.Size(70, 36);
         this.btSearchIuid.TabIndex = 12;
         this.btSearchIuid.Text = "Search (IUID)";
         this.btSearchIuid.UseVisualStyleBackColor = true;
         this.btSearchIuid.Click += new System.EventHandler(this.btSearchIuid_Click);
         // 
         // btSearchCuid
         // 
         this.btSearchCuid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.btSearchCuid.Location = new System.Drawing.Point(619, 601);
         this.btSearchCuid.Name = "btSearchCuid";
         this.btSearchCuid.Size = new System.Drawing.Size(77, 36);
         this.btSearchCuid.TabIndex = 15;
         this.btSearchCuid.Text = "Search (CUID)";
         this.btSearchCuid.UseVisualStyleBackColor = true;
         this.btSearchCuid.Click += new System.EventHandler(this.btSearchCuid_Click);
         // 
         // btSearchLocation
         // 
         this.btSearchLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.btSearchLocation.Location = new System.Drawing.Point(783, 601);
         this.btSearchLocation.Name = "btSearchLocation";
         this.btSearchLocation.Size = new System.Drawing.Size(70, 36);
         this.btSearchLocation.TabIndex = 17;
         this.btSearchLocation.Text = "Search (Location)";
         this.btSearchLocation.UseVisualStyleBackColor = true;
         this.btSearchLocation.Click += new System.EventHandler(this.btSearchLocation_Click);
         // 
         // txtSearchDescription
         // 
         this.txtSearchDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.txtSearchDescription.Location = new System.Drawing.Point(424, 560);
         this.txtSearchDescription.Multiline = true;
         this.txtSearchDescription.Name = "txtSearchDescription";
         this.txtSearchDescription.Size = new System.Drawing.Size(189, 51);
         this.txtSearchDescription.TabIndex = 9;
         this.txtSearchDescription.Enter += new System.EventHandler(this.txtSearchDescription_Enter);
         // 
         // txtSearchCuid
         // 
         this.txtSearchCuid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.txtSearchCuid.Location = new System.Drawing.Point(424, 617);
         this.txtSearchCuid.Name = "txtSearchCuid";
         this.txtSearchCuid.Size = new System.Drawing.Size(189, 20);
         this.txtSearchCuid.TabIndex = 14;
         this.txtSearchCuid.Enter += new System.EventHandler(this.txtSearchCuid_Enter);
         // 
         // txtSearchLocation
         // 
         this.txtSearchLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.txtSearchLocation.Location = new System.Drawing.Point(702, 610);
         this.txtSearchLocation.Name = "txtSearchLocation";
         this.txtSearchLocation.Size = new System.Drawing.Size(75, 20);
         this.txtSearchLocation.TabIndex = 16;
         this.txtSearchLocation.Enter += new System.EventHandler(this.txtSearchLocation_Enter);
         // 
         // txtSearchIuid
         // 
         this.txtSearchIuid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.txtSearchIuid.Location = new System.Drawing.Point(702, 568);
         this.txtSearchIuid.Name = "txtSearchIuid";
         this.txtSearchIuid.Size = new System.Drawing.Size(75, 20);
         this.txtSearchIuid.TabIndex = 11;
         this.txtSearchIuid.Enter += new System.EventHandler(this.txtSearchIuid_Enter);
         // 
         // tbShowAllBoxes
         // 
         this.tbShowAllBoxes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.tbShowAllBoxes.Location = new System.Drawing.Point(859, 559);
         this.tbShowAllBoxes.Name = "tbShowAllBoxes";
         this.tbShowAllBoxes.Size = new System.Drawing.Size(70, 36);
         this.tbShowAllBoxes.TabIndex = 13;
         this.tbShowAllBoxes.Text = "Show All Boxes";
         this.tbShowAllBoxes.UseVisualStyleBackColor = true;
         this.tbShowAllBoxes.Click += new System.EventHandler(this.tbShowAllBoxes_Click);
         // 
         // btStockListReport
         // 
         this.btStockListReport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.btStockListReport.Location = new System.Drawing.Point(859, 601);
         this.btStockListReport.Name = "btStockListReport";
         this.btStockListReport.Size = new System.Drawing.Size(70, 36);
         this.btStockListReport.TabIndex = 18;
         this.btStockListReport.Text = "Stock list";
         this.btStockListReport.UseVisualStyleBackColor = true;
         this.btStockListReport.Click += new System.EventHandler(this.btStockListReport_Click);
         // 
         // btSetVat
         // 
         this.btSetVat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.btSetVat.Location = new System.Drawing.Point(88, 19);
         this.btSetVat.Name = "btSetVat";
         this.btSetVat.Size = new System.Drawing.Size(76, 36);
         this.btSetVat.TabIndex = 1;
         this.btSetVat.Text = "Set VAT";
         this.btSetVat.UseVisualStyleBackColor = true;
         this.btSetVat.Click += new System.EventHandler(this.btSetVat_Click);
         // 
         // btGenerateLMI
         // 
         this.btGenerateLMI.Location = new System.Drawing.Point(6, 19);
         this.btGenerateLMI.Name = "btGenerateLMI";
         this.btGenerateLMI.Size = new System.Drawing.Size(76, 36);
         this.btGenerateLMI.TabIndex = 0;
         this.btGenerateLMI.Text = "Generate L.M.I";
         this.btGenerateLMI.UseVisualStyleBackColor = true;
         this.btGenerateLMI.Click += new System.EventHandler(this.btGenerateLMI_Click);
         // 
         // btPrintLMI
         // 
         this.btPrintLMI.Location = new System.Drawing.Point(88, 19);
         this.btPrintLMI.Name = "btPrintLMI";
         this.btPrintLMI.Size = new System.Drawing.Size(76, 36);
         this.btPrintLMI.TabIndex = 1;
         this.btPrintLMI.Text = "Print L.M.I";
         this.btPrintLMI.UseVisualStyleBackColor = true;
         this.btPrintLMI.Click += new System.EventHandler(this.btPrintLMI_Click);
         // 
         // btSaveStocks
         // 
         this.btSaveStocks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.btSaveStocks.Location = new System.Drawing.Point(6, 19);
         this.btSaveStocks.Name = "btSaveStocks";
         this.btSaveStocks.Size = new System.Drawing.Size(76, 36);
         this.btSaveStocks.TabIndex = 0;
         this.btSaveStocks.Text = "Save stock list to the file";
         this.btSaveStocks.UseVisualStyleBackColor = true;
         this.btSaveStocks.Click += new System.EventHandler(this.btSaveStocks_Click);
         // 
         // groupBox1
         // 
         this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.groupBox1.Controls.Add(this.btMailLMI);
         this.groupBox1.Controls.Add(this.btGenerateLMI);
         this.groupBox1.Controls.Add(this.btPrintLMI);
         this.groupBox1.Location = new System.Drawing.Point(284, 369);
         this.groupBox1.Name = "groupBox1";
         this.groupBox1.Size = new System.Drawing.Size(256, 70);
         this.groupBox1.TabIndex = 28;
         this.groupBox1.TabStop = false;
         this.groupBox1.Text = "Invoicing";
         // 
         // btMailLMI
         // 
         this.btMailLMI.Location = new System.Drawing.Point(170, 19);
         this.btMailLMI.Name = "btMailLMI";
         this.btMailLMI.Size = new System.Drawing.Size(76, 36);
         this.btMailLMI.TabIndex = 2;
         this.btMailLMI.Text = "e-mail L.M.I";
         this.btMailLMI.UseVisualStyleBackColor = true;
         this.btMailLMI.Click += new System.EventHandler(this.btMailLMI_Click);
         // 
         // groupBox2
         // 
         this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.groupBox2.Controls.Add(this.btSetInvoiceMailSettings);
         this.groupBox2.Controls.Add(this.btSaveStocks);
         this.groupBox2.Controls.Add(this.btSetVat);
         this.groupBox2.Location = new System.Drawing.Point(284, 445);
         this.groupBox2.Name = "groupBox2";
         this.groupBox2.Size = new System.Drawing.Size(256, 70);
         this.groupBox2.TabIndex = 29;
         this.groupBox2.TabStop = false;
         this.groupBox2.Text = "Miscelaneous";
         // 
         // btSetInvoiceMailSettings
         // 
         this.btSetInvoiceMailSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.btSetInvoiceMailSettings.Location = new System.Drawing.Point(170, 19);
         this.btSetInvoiceMailSettings.Name = "btSetInvoiceMailSettings";
         this.btSetInvoiceMailSettings.Size = new System.Drawing.Size(76, 36);
         this.btSetInvoiceMailSettings.TabIndex = 2;
         this.btSetInvoiceMailSettings.Text = "e-mail settings";
         this.btSetInvoiceMailSettings.UseVisualStyleBackColor = true;
         this.btSetInvoiceMailSettings.Click += new System.EventHandler(this.btSetInvoiceMailSettings_Click);
         // 
         // modifyFastnessUserButton
         // 
         this.modifyFastnessUserButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.modifyFastnessUserButton.Location = new System.Drawing.Point(198, 560);
         this.modifyFastnessUserButton.Name = "modifyFastnessUserButton";
         this.modifyFastnessUserButton.Size = new System.Drawing.Size(87, 36);
         this.modifyFastnessUserButton.TabIndex = 4;
         this.modifyFastnessUserButton.Text = "Manage Fastness users";
         this.modifyFastnessUserButton.UseVisualStyleBackColor = true;
         this.modifyFastnessUserButton.Click += new System.EventHandler(this.modifyFastnessUserButton_Click);
         // 
         // groupBox4
         // 
         this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.groupBox4.Controls.Add(this.optFilterSearch);
         this.groupBox4.Controls.Add(this.optJumpSearch);
         this.groupBox4.Location = new System.Drawing.Point(355, 559);
         this.groupBox4.Name = "groupBox4";
         this.groupBox4.Size = new System.Drawing.Size(63, 77);
         this.groupBox4.TabIndex = 8;
         this.groupBox4.TabStop = false;
         this.groupBox4.Text = "Search";
         // 
         // optFilterSearch
         // 
         this.optFilterSearch.AutoSize = true;
         this.optFilterSearch.Location = new System.Drawing.Point(8, 43);
         this.optFilterSearch.Name = "optFilterSearch";
         this.optFilterSearch.Size = new System.Drawing.Size(44, 17);
         this.optFilterSearch.TabIndex = 0;
         this.optFilterSearch.Text = "filter";
         this.optFilterSearch.UseVisualStyleBackColor = true;
         // 
         // optJumpSearch
         // 
         this.optJumpSearch.AutoSize = true;
         this.optJumpSearch.Checked = true;
         this.optJumpSearch.Location = new System.Drawing.Point(8, 19);
         this.optJumpSearch.Name = "optJumpSearch";
         this.optJumpSearch.Size = new System.Drawing.Size(47, 17);
         this.optJumpSearch.TabIndex = 1;
         this.optJumpSearch.TabStop = true;
         this.optJumpSearch.Text = "jump";
         this.optJumpSearch.UseVisualStyleBackColor = true;
         // 
         // MainScreen
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(944, 647);
         this.Controls.Add(this.groupBox4);
         this.Controls.Add(this.modifyFastnessUserButton);
         this.Controls.Add(this.groupBox2);
         this.Controls.Add(this.groupBox1);
         this.Controls.Add(this.btStockListReport);
         this.Controls.Add(this.tbShowAllBoxes);
         this.Controls.Add(this.txtSearchIuid);
         this.Controls.Add(this.txtSearchLocation);
         this.Controls.Add(this.txtSearchCuid);
         this.Controls.Add(this.txtSearchDescription);
         this.Controls.Add(this.btSearchLocation);
         this.Controls.Add(this.btSearchCuid);
         this.Controls.Add(this.btSearchIuid);
         this.Controls.Add(this.btSearchDescription);
         this.Controls.Add(this.lbContractAuxiliaryInfo);
         this.Controls.Add(this.lbItemAuxiliaryInfo);
         this.Controls.Add(this.showContractHistoryButton);
         this.Controls.Add(this.doActionButton);
         this.Controls.Add(this.contractActionsGrid);
         this.Controls.Add(this.showContractsButton);
         this.Controls.Add(this.showContractActionsButton);
         this.Controls.Add(this.addNewContractButton);
         this.Controls.Add(this.itemsGrid);
         this.Controls.Add(this.contractsGrid);
         this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
         this.MinimumSize = new System.Drawing.Size(960, 685);
         this.Name = "MainScreen";
         this.Text = "Fastness";
         this.Load += new System.EventHandler(this.MainScreen_Load);
         ((System.ComponentModel.ISupportInitialize)(this.contractsGrid)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.itemsGrid)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.contractActionsGrid)).EndInit();
         this.groupBox1.ResumeLayout(false);
         this.groupBox2.ResumeLayout(false);
         this.groupBox4.ResumeLayout(false);
         this.groupBox4.PerformLayout();
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.DataGridView contractsGrid;
      private System.Windows.Forms.DataGridView itemsGrid;
      private System.Windows.Forms.Button addNewContractButton;
      private System.Windows.Forms.Button showContractActionsButton;
      private System.Windows.Forms.Button showContractsButton;
      private System.Windows.Forms.DataGridView contractActionsGrid;
      private System.Windows.Forms.Button doActionButton;
      private System.Windows.Forms.Button showContractHistoryButton;
      private System.Windows.Forms.Label lbItemAuxiliaryInfo;
      private System.Windows.Forms.Label lbContractAuxiliaryInfo;
      private System.Windows.Forms.Button btSearchDescription;
      private System.Windows.Forms.Button btSearchIuid;
      private System.Windows.Forms.Button btSearchCuid;
      private System.Windows.Forms.Button btSearchLocation;
      private System.Windows.Forms.TextBox txtSearchDescription;
      private System.Windows.Forms.TextBox txtSearchCuid;
      private System.Windows.Forms.TextBox txtSearchLocation;
      private System.Windows.Forms.TextBox txtSearchIuid;
      private System.Windows.Forms.Button tbShowAllBoxes;
      private System.Windows.Forms.Button btStockListReport;
      private System.Windows.Forms.Button btSetVat;
      private System.Windows.Forms.Button btGenerateLMI;
      private System.Windows.Forms.Button btPrintLMI;
      private System.Windows.Forms.Button btSaveStocks;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.GroupBox groupBox2;
      private System.Windows.Forms.Button btMailLMI;
      private System.Windows.Forms.Button btSetInvoiceMailSettings;
      private System.Windows.Forms.Button modifyFastnessUserButton;
      private System.Windows.Forms.GroupBox groupBox4;
      private System.Windows.Forms.RadioButton optFilterSearch;
      private System.Windows.Forms.RadioButton optJumpSearch;
   }
}

