﻿namespace FastnessForms.Views
{
   partial class ActionGenericForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ActionGenericForm));
      this.txtCharge = new System.Windows.Forms.TextBox();
      this.txtConfirmItemNumber = new System.Windows.Forms.TextBox();
      this.txtAuthoriziedBy = new System.Windows.Forms.TextBox();
      this.txtLocation = new System.Windows.Forms.TextBox();
      this.txtPercentage = new System.Windows.Forms.TextBox();
      this.txtType = new System.Windows.Forms.TextBox();
      this.txtDescription = new System.Windows.Forms.TextBox();
      this.txtClientItemReference = new System.Windows.Forms.TextBox();
      this.txtItemNumber = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.label8 = new System.Windows.Forms.Label();
      this.label9 = new System.Windows.Forms.Label();
      this.label10 = new System.Windows.Forms.Label();
      this.label11 = new System.Windows.Forms.Label();
      this.btMake = new System.Windows.Forms.Button();
      this.dtpGeneric = new System.Windows.Forms.DateTimePicker();
      this.btClose = new System.Windows.Forms.Button();
      this.btShow = new System.Windows.Forms.Button();
      this.txtNotes = new System.Windows.Forms.TextBox();
      this.label5 = new System.Windows.Forms.Label();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.btAllowChargeChange = new System.Windows.Forms.Button();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.label12 = new System.Windows.Forms.Label();
      this.label13 = new System.Windows.Forms.Label();
      this.label14 = new System.Windows.Forms.Label();
      this.lbActionTypeInfo = new System.Windows.Forms.Label();
      this.lbContractNameInfo = new System.Windows.Forms.Label();
      this.lbContractIDInfo = new System.Windows.Forms.Label();
      this.groupBox3 = new System.Windows.Forms.GroupBox();
      this.groupBox1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.groupBox3.SuspendLayout();
      this.SuspendLayout();
      // 
      // txtCharge
      // 
      this.txtCharge.AcceptsReturn = true;
      this.txtCharge.BackColor = System.Drawing.SystemColors.Window;
      this.txtCharge.Cursor = System.Windows.Forms.Cursors.IBeam;
      this.txtCharge.ForeColor = System.Drawing.SystemColors.WindowText;
      this.txtCharge.Location = new System.Drawing.Point(87, 77);
      this.txtCharge.MaxLength = 0;
      this.txtCharge.Name = "txtCharge";
      this.txtCharge.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtCharge.Size = new System.Drawing.Size(49, 20);
      this.txtCharge.TabIndex = 1;
      // 
      // txtConfirmItemNumber
      // 
      this.txtConfirmItemNumber.AcceptsReturn = true;
      this.txtConfirmItemNumber.BackColor = System.Drawing.SystemColors.Window;
      this.txtConfirmItemNumber.Cursor = System.Windows.Forms.Cursors.IBeam;
      this.txtConfirmItemNumber.ForeColor = System.Drawing.SystemColors.WindowText;
      this.txtConfirmItemNumber.ImeMode = System.Windows.Forms.ImeMode.Disable;
      this.txtConfirmItemNumber.Location = new System.Drawing.Point(87, 48);
      this.txtConfirmItemNumber.MaxLength = 0;
      this.txtConfirmItemNumber.Name = "txtConfirmItemNumber";
      this.txtConfirmItemNumber.PasswordChar = '*';
      this.txtConfirmItemNumber.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtConfirmItemNumber.Size = new System.Drawing.Size(89, 20);
      this.txtConfirmItemNumber.TabIndex = 1;
      // 
      // txtAuthoriziedBy
      // 
      this.txtAuthoriziedBy.AcceptsReturn = true;
      this.txtAuthoriziedBy.BackColor = System.Drawing.SystemColors.Window;
      this.txtAuthoriziedBy.Cursor = System.Windows.Forms.Cursors.IBeam;
      this.txtAuthoriziedBy.ForeColor = System.Drawing.SystemColors.WindowText;
      this.txtAuthoriziedBy.Location = new System.Drawing.Point(87, 132);
      this.txtAuthoriziedBy.MaxLength = 0;
      this.txtAuthoriziedBy.Name = "txtAuthoriziedBy";
      this.txtAuthoriziedBy.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtAuthoriziedBy.Size = new System.Drawing.Size(135, 20);
      this.txtAuthoriziedBy.TabIndex = 3;
      this.txtAuthoriziedBy.Tag = "0";
      // 
      // txtLocation
      // 
      this.txtLocation.AcceptsReturn = true;
      this.txtLocation.BackColor = System.Drawing.SystemColors.Window;
      this.txtLocation.Cursor = System.Windows.Forms.Cursors.IBeam;
      this.txtLocation.Enabled = false;
      this.txtLocation.ForeColor = System.Drawing.SystemColors.WindowText;
      this.txtLocation.Location = new System.Drawing.Point(87, 219);
      this.txtLocation.MaxLength = 0;
      this.txtLocation.Name = "txtLocation";
      this.txtLocation.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtLocation.Size = new System.Drawing.Size(89, 20);
      this.txtLocation.TabIndex = 6;
      // 
      // txtPercentage
      // 
      this.txtPercentage.AcceptsReturn = true;
      this.txtPercentage.BackColor = System.Drawing.SystemColors.Window;
      this.txtPercentage.Cursor = System.Windows.Forms.Cursors.IBeam;
      this.txtPercentage.Enabled = false;
      this.txtPercentage.ForeColor = System.Drawing.SystemColors.WindowText;
      this.txtPercentage.Location = new System.Drawing.Point(87, 193);
      this.txtPercentage.MaxLength = 0;
      this.txtPercentage.Name = "txtPercentage";
      this.txtPercentage.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtPercentage.Size = new System.Drawing.Size(41, 20);
      this.txtPercentage.TabIndex = 5;
      this.txtPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // txtType
      // 
      this.txtType.AcceptsReturn = true;
      this.txtType.BackColor = System.Drawing.SystemColors.Window;
      this.txtType.Cursor = System.Windows.Forms.Cursors.IBeam;
      this.txtType.Enabled = false;
      this.txtType.ForeColor = System.Drawing.SystemColors.WindowText;
      this.txtType.Location = new System.Drawing.Point(87, 167);
      this.txtType.MaxLength = 0;
      this.txtType.Name = "txtType";
      this.txtType.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtType.Size = new System.Drawing.Size(89, 20);
      this.txtType.TabIndex = 4;
      // 
      // txtDescription
      // 
      this.txtDescription.AcceptsReturn = true;
      this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
      this.txtDescription.Cursor = System.Windows.Forms.Cursors.IBeam;
      this.txtDescription.Enabled = false;
      this.txtDescription.ForeColor = System.Drawing.SystemColors.WindowText;
      this.txtDescription.Location = new System.Drawing.Point(87, 112);
      this.txtDescription.MaxLength = 0;
      this.txtDescription.Multiline = true;
      this.txtDescription.Name = "txtDescription";
      this.txtDescription.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtDescription.Size = new System.Drawing.Size(234, 49);
      this.txtDescription.TabIndex = 3;
      // 
      // txtClientItemReference
      // 
      this.txtClientItemReference.AcceptsReturn = true;
      this.txtClientItemReference.BackColor = System.Drawing.SystemColors.Window;
      this.txtClientItemReference.Cursor = System.Windows.Forms.Cursors.IBeam;
      this.txtClientItemReference.Enabled = false;
      this.txtClientItemReference.ForeColor = System.Drawing.SystemColors.WindowText;
      this.txtClientItemReference.Location = new System.Drawing.Point(87, 86);
      this.txtClientItemReference.MaxLength = 0;
      this.txtClientItemReference.Name = "txtClientItemReference";
      this.txtClientItemReference.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtClientItemReference.Size = new System.Drawing.Size(89, 20);
      this.txtClientItemReference.TabIndex = 2;
      // 
      // txtItemNumber
      // 
      this.txtItemNumber.AcceptsReturn = true;
      this.txtItemNumber.BackColor = System.Drawing.SystemColors.Window;
      this.txtItemNumber.Cursor = System.Windows.Forms.Cursors.IBeam;
      this.txtItemNumber.ForeColor = System.Drawing.SystemColors.WindowText;
      this.txtItemNumber.Location = new System.Drawing.Point(87, 22);
      this.txtItemNumber.MaxLength = 0;
      this.txtItemNumber.Name = "txtItemNumber";
      this.txtItemNumber.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtItemNumber.Size = new System.Drawing.Size(89, 20);
      this.txtItemNumber.TabIndex = 0;
      this.txtItemNumber.Text = "100";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(6, 25);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(65, 13);
      this.label1.TabIndex = 48;
      this.label1.Text = "Box Number";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(6, 51);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(63, 13);
      this.label2.TabIndex = 49;
      this.label2.Text = "Confirm Box";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(6, 89);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(80, 13);
      this.label3.TabIndex = 50;
      this.label3.Text = "Item Reference";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(6, 115);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(60, 13);
      this.label4.TabIndex = 51;
      this.label4.Text = "Description";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(6, 170);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(31, 13);
      this.label6.TabIndex = 53;
      this.label6.Text = "Type";
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(6, 196);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(62, 13);
      this.label7.TabIndex = 54;
      this.label7.Text = "Percentage";
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(6, 222);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(48, 13);
      this.label8.TabIndex = 55;
      this.label8.Text = "Location";
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(6, 109);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(30, 13);
      this.label9.TabIndex = 56;
      this.label9.Text = "Date";
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(6, 135);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(71, 13);
      this.label10.TabIndex = 57;
      this.label10.Text = "Authorized by";
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(6, 80);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(41, 13);
      this.label11.TabIndex = 58;
      this.label11.Text = "Charge";
      // 
      // btMake
      // 
      this.btMake.Location = new System.Drawing.Point(103, 564);
      this.btMake.Name = "btMake";
      this.btMake.Size = new System.Drawing.Size(104, 35);
      this.btMake.TabIndex = 3;
      this.btMake.Text = "Make action";
      this.btMake.UseVisualStyleBackColor = true;
      this.btMake.Click += new System.EventHandler(this.btGeneric_Click);
      // 
      // dtpGeneric
      // 
      this.dtpGeneric.Location = new System.Drawing.Point(87, 103);
      this.dtpGeneric.Name = "dtpGeneric";
      this.dtpGeneric.Size = new System.Drawing.Size(135, 20);
      this.dtpGeneric.TabIndex = 2;
      // 
      // btClose
      // 
      this.btClose.Location = new System.Drawing.Point(262, 564);
      this.btClose.Name = "btClose";
      this.btClose.Size = new System.Drawing.Size(85, 35);
      this.btClose.TabIndex = 4;
      this.btClose.Text = "Close";
      this.btClose.UseVisualStyleBackColor = true;
      this.btClose.Click += new System.EventHandler(this.btClose_Click);
      // 
      // btShow
      // 
      this.btShow.Location = new System.Drawing.Point(12, 564);
      this.btShow.Name = "btShow";
      this.btShow.Size = new System.Drawing.Size(85, 35);
      this.btShow.TabIndex = 2;
      this.btShow.Text = "Show";
      this.btShow.UseVisualStyleBackColor = true;
      this.btShow.Click += new System.EventHandler(this.btShow_Click);
      // 
      // txtNotes
      // 
      this.txtNotes.AcceptsReturn = true;
      this.txtNotes.BackColor = System.Drawing.SystemColors.Window;
      this.txtNotes.Cursor = System.Windows.Forms.Cursors.IBeam;
      this.txtNotes.ForeColor = System.Drawing.SystemColors.WindowText;
      this.txtNotes.Location = new System.Drawing.Point(87, 18);
      this.txtNotes.MaxLength = 0;
      this.txtNotes.Multiline = true;
      this.txtNotes.Name = "txtNotes";
      this.txtNotes.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.txtNotes.Size = new System.Drawing.Size(234, 49);
      this.txtNotes.TabIndex = 0;
      this.txtNotes.Tag = "0";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(6, 25);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(35, 13);
      this.label5.TabIndex = 62;
      this.label5.Text = "Notes";
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.btAllowChargeChange);
      this.groupBox1.Controls.Add(this.label11);
      this.groupBox1.Controls.Add(this.label5);
      this.groupBox1.Controls.Add(this.txtAuthoriziedBy);
      this.groupBox1.Controls.Add(this.txtCharge);
      this.groupBox1.Controls.Add(this.txtNotes);
      this.groupBox1.Controls.Add(this.dtpGeneric);
      this.groupBox1.Controls.Add(this.label9);
      this.groupBox1.Controls.Add(this.label10);
      this.groupBox1.Location = new System.Drawing.Point(12, 387);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(335, 161);
      this.groupBox1.TabIndex = 1;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Action details";
      // 
      // btAllowChargeChange
      // 
      this.btAllowChargeChange.Location = new System.Drawing.Point(142, 73);
      this.btAllowChargeChange.Name = "btAllowChargeChange";
      this.btAllowChargeChange.Size = new System.Drawing.Size(118, 27);
      this.btAllowChargeChange.TabIndex = 12;
      this.btAllowChargeChange.TabStop = false;
      this.btAllowChargeChange.Text = "Allow charge change";
      this.btAllowChargeChange.UseVisualStyleBackColor = true;
      this.btAllowChargeChange.Click += new System.EventHandler(this.btAllowChargeChange_Click);
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.label1);
      this.groupBox2.Controls.Add(this.txtItemNumber);
      this.groupBox2.Controls.Add(this.txtClientItemReference);
      this.groupBox2.Controls.Add(this.txtPercentage);
      this.groupBox2.Controls.Add(this.txtDescription);
      this.groupBox2.Controls.Add(this.txtLocation);
      this.groupBox2.Controls.Add(this.txtType);
      this.groupBox2.Controls.Add(this.txtConfirmItemNumber);
      this.groupBox2.Controls.Add(this.label7);
      this.groupBox2.Controls.Add(this.label6);
      this.groupBox2.Controls.Add(this.label2);
      this.groupBox2.Controls.Add(this.label8);
      this.groupBox2.Controls.Add(this.label3);
      this.groupBox2.Controls.Add(this.label4);
      this.groupBox2.Location = new System.Drawing.Point(12, 127);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(335, 254);
      this.groupBox2.TabIndex = 0;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Box details";
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Location = new System.Drawing.Point(6, 25);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(56, 13);
      this.label12.TabIndex = 5;
      this.label12.Text = "acion type";
      // 
      // label13
      // 
      this.label13.AutoSize = true;
      this.label13.Location = new System.Drawing.Point(6, 55);
      this.label13.Name = "label13";
      this.label13.Size = new System.Drawing.Size(75, 13);
      this.label13.TabIndex = 6;
      this.label13.Text = "contract name";
      // 
      // label14
      // 
      this.label14.AutoSize = true;
      this.label14.Location = new System.Drawing.Point(6, 85);
      this.label14.Name = "label14";
      this.label14.Size = new System.Drawing.Size(60, 13);
      this.label14.TabIndex = 7;
      this.label14.Text = "contract ID";
      // 
      // lbActionTypeInfo
      // 
      this.lbActionTypeInfo.AutoEllipsis = true;
      this.lbActionTypeInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.lbActionTypeInfo.Location = new System.Drawing.Point(88, 25);
      this.lbActionTypeInfo.Name = "lbActionTypeInfo";
      this.lbActionTypeInfo.Size = new System.Drawing.Size(233, 13);
      this.lbActionTypeInfo.TabIndex = 8;
      this.lbActionTypeInfo.Text = "acion type";
      // 
      // lbContractNameInfo
      // 
      this.lbContractNameInfo.AutoEllipsis = true;
      this.lbContractNameInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.lbContractNameInfo.Location = new System.Drawing.Point(88, 55);
      this.lbContractNameInfo.Name = "lbContractNameInfo";
      this.lbContractNameInfo.Size = new System.Drawing.Size(233, 13);
      this.lbContractNameInfo.TabIndex = 9;
      this.lbContractNameInfo.Text = "contract name";
      // 
      // lbContractIDInfo
      // 
      this.lbContractIDInfo.AutoEllipsis = true;
      this.lbContractIDInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.lbContractIDInfo.Location = new System.Drawing.Point(88, 85);
      this.lbContractIDInfo.Name = "lbContractIDInfo";
      this.lbContractIDInfo.Size = new System.Drawing.Size(233, 13);
      this.lbContractIDInfo.TabIndex = 10;
      this.lbContractIDInfo.Text = "contract ID";
      // 
      // groupBox3
      // 
      this.groupBox3.Controls.Add(this.lbActionTypeInfo);
      this.groupBox3.Controls.Add(this.lbContractIDInfo);
      this.groupBox3.Controls.Add(this.label12);
      this.groupBox3.Controls.Add(this.lbContractNameInfo);
      this.groupBox3.Controls.Add(this.label13);
      this.groupBox3.Controls.Add(this.label14);
      this.groupBox3.Location = new System.Drawing.Point(12, 12);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new System.Drawing.Size(335, 109);
      this.groupBox3.TabIndex = 11;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "Action info";
      // 
      // ActionGenericForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(360, 612);
      this.Controls.Add(this.groupBox3);
      this.Controls.Add(this.groupBox2);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.btShow);
      this.Controls.Add(this.btClose);
      this.Controls.Add(this.btMake);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "ActionGenericForm";
      this.Text = "Generic";
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      this.ResumeLayout(false);

      }

      #endregion

      public System.Windows.Forms.TextBox txtCharge;
      public System.Windows.Forms.TextBox txtConfirmItemNumber;
      public System.Windows.Forms.TextBox txtAuthoriziedBy;
      public System.Windows.Forms.TextBox txtLocation;
      public System.Windows.Forms.TextBox txtPercentage;
      public System.Windows.Forms.TextBox txtType;
      public System.Windows.Forms.TextBox txtDescription;
      public System.Windows.Forms.TextBox txtClientItemReference;
      public System.Windows.Forms.TextBox txtItemNumber;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.Label label10;
      private System.Windows.Forms.Label label11;
      private System.Windows.Forms.Button btMake;
      private System.Windows.Forms.DateTimePicker dtpGeneric;
      private System.Windows.Forms.Button btClose;
      private System.Windows.Forms.Button btShow;
      public System.Windows.Forms.TextBox txtNotes;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.GroupBox groupBox2;
      private System.Windows.Forms.Label label12;
      private System.Windows.Forms.Label label13;
      private System.Windows.Forms.Label label14;
      private System.Windows.Forms.Label lbActionTypeInfo;
      private System.Windows.Forms.Label lbContractNameInfo;
      private System.Windows.Forms.Label lbContractIDInfo;
      private System.Windows.Forms.GroupBox groupBox3;
      private System.Windows.Forms.Button btAllowChargeChange;
   }
}