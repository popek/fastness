﻿using Fastness.Common;
using Fastness.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace FastnessForms.Views
{
   public partial class InvoicesNotesListForm : Form
   {
      DataGridViewManager<InvoiceNotes> allNotestGridMan;

      public List<InvoiceNotes> AllNotes { get; private set; }

      public InvoicesNotesListForm(IEnumerable<Contract> contracts)
      {
         InitializeComponent();

         AllNotes = (from ctr in contracts select new InvoiceNotes(ctr)).ToList<InvoiceNotes>();

         Text = string.Format("Notes for {0}/{1} invoices", DateUtils.LastMonthDate.Year, DateUtils.LastMonthDate.Month);
      }

      private void btContinue_Click(object sender, EventArgs e)
      {
         Close();
      }

      private void InvoicesNotesListForm_Load(object sender, EventArgs e)
      {
         allNotestGridMan = new DataGridViewManager<InvoiceNotes>(dgvNotes, new int[]{0});
         allNotestGridMan.Refresh(AllNotes);
      }

      private void dgvNotes_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
      {
         var notesToEdit = allNotestGridMan.CurrentValue;
         new InvoiceNotesForm(notesToEdit).ShowDialog();
         dgvNotes.Refresh();
      }
   }
}
