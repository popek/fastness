﻿using Fastness.Common;
using Fastness.DataModel.Utils;
using System;
using System.Globalization;
using System.Windows.Forms;

namespace FastnessForms.Views
{
   internal class Helper
   {

      public static void KeyPressCheckMaxLengthCondition(TextBox textbox, KeyPressEventArgs e, int maxLength)
      {
         HandleKeyPress(e, () => textbox.Text.Length + 1 <= maxLength);
      }

      public static void KeyPressCheckNumericIntCondition(TextBox textbox, KeyPressEventArgs e)
      {
         HandleKeyPress(e, () => char.IsDigit(e.KeyChar));
      }

      public static void KeyPressCheckNumericDoubleCondition(TextBox textbox, KeyPressEventArgs e)
      {
         HandleKeyPress(e, () => StringUtils.ValidNumericValue(textbox.Text + e.KeyChar));
      }

      public static void KeyPressCheckNumericIntAndLengthConditions(TextBox textbox, KeyPressEventArgs e, int maxLength)
      {
         HandleKeyPress(e, () => textbox.Text.Length + 1 <= maxLength && char.IsDigit(e.KeyChar));
      }
      
      private static void HandleKeyPress(KeyPressEventArgs e, Func<bool> evalCondition)
      {
         if (evalCondition() || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Delete)
         {
            e.Handled = e.KeyChar == 0;
         }
         else
         {
            e.Handled = true;
            e.KeyChar = Convert.ToChar(0);
         }
      }
   }

   static class TextBoxExtensions
   {
      public static void AddKeyPressWithTextLengthLimit(this TextBox textbox, int maxLength)
      {
         textbox.KeyPress += new KeyPressEventHandler((s, e) => Helper.KeyPressCheckMaxLengthCondition(textbox, e, maxLength));
      }

      public static void AddKeyPressWithIntegerValue(this TextBox textbox)
      {
         textbox.KeyPress += new KeyPressEventHandler((s, e) => Helper.KeyPressCheckNumericIntCondition(textbox, e));
      }

      public static void AddKeyPressWithNumericValue(this TextBox textbox)
      {
         textbox.KeyPress += new KeyPressEventHandler((s, e) => Helper.KeyPressCheckNumericDoubleCondition(textbox, e));
      }

      public static void AddKeyPressWithTextLengthLimitAndIntegerValue(this TextBox textbox, int maxLength)
      {
         textbox.KeyPress += new KeyPressEventHandler((s, e) => Helper.KeyPressCheckNumericIntAndLengthConditions(textbox, e, maxLength));
      }
      
      public static void AddTextChangeHandler(this TextBoxBase textbox, Func<string> baseTextProvider, Action<bool> textChanged)
      {
         textbox.TextChanged += (s, e) =>
         {
            textChanged(textbox.Text != baseTextProvider());
         };
      }

      public static bool IsTextToLong(this TextBox textbox, int maxLength, string tag)
      {
         if (!string.IsNullOrEmpty(textbox.Text) && textbox.Text.Length > maxLength)
         {
            UserInteraction.ShowError("{0} is to long.\nMax length is {0}.", tag, maxLength);
            textbox.Focus();
            return true;
         }
         return false;
      }

      public static bool IsValidNumericValueFormat(this TextBox textbox, string tag)
      {
         if (string.IsNullOrEmpty(textbox.Text))
         {
            UserInteraction.ShowError("{0} is empty!", tag);
            textbox.Focus();
            return false;
         }

         if (!StringUtils.ValidNumericValue(textbox.Text))
         {
            UserInteraction.ShowError("{0} text is not a proper numeric value!", tag);
            textbox.Focus();
            return false;
         }
         return true;
      }

      public static bool IsValidNumericValueInRange(this TextBox textbox, string tag, double min, double max)
      {
         if (!textbox.IsValidNumericValueFormat(tag))
         {
            return false;
         }
         var value = Double.Parse(textbox.Text);
         if (value > max || value < min)
         {
            UserInteraction.ShowError("{0} value must be in range {1}-{2}.", tag, min, max);
            textbox.Focus();
            return false;
         }
         return true;
      }

      public static bool IsValidIntegerValueFormat(this TextBox textbox, string tag)
      {
         if (string.IsNullOrEmpty(textbox.Text))
         {
            UserInteraction.ShowError("{0} is empty", tag);
            textbox.Focus();
            return false;
         }
         if (!StringUtils.ValidNumericIntegerValue(textbox.Text))
         {
            UserInteraction.ShowError("{0} text is not a proper integer value", tag);
            textbox.Focus();
            return false;
         }
         return true;
      }

      public static bool IsTextEmpty(this TextBoxBase textbox, string tag) 
      {
         if (string.IsNullOrEmpty(textbox.Text))
         {
            UserInteraction.ShowError("{0} can't be empty! Correct it.", tag);
            textbox.Focus();
            return true;
         }
         return false;
      }

      public static bool IsTextLenthExactlyTheSame(this TextBox textbox, int exactLength, string tag)
      {
         if (textbox.Text.Length == exactLength)
         {
            return true;
         }
         UserInteraction.ShowError("{0} length is wrong! Proper lenght should be {1}.", tag, exactLength);
         textbox.Focus();
         return false;
      }

      public static bool IsDigitsOnlyText(this TextBox textbox, string tag)
      {
         if (StringUtils.HasDigitsOnly(textbox.Text))
         {
            return true;
         }
         UserInteraction.ShowError("{0} contains forbidden character(s)! Only digits are allowed.", tag);
         textbox.Focus();
         return false;
      }

      public static bool IsValidAddress(this TextBox textbox, bool allowEmpty, string tag)
      {
         switch (Misc.ValidateAddress(textbox.Text, 200, 6))
         { 
            case Misc.AddressStatus.ADDRESS_IS_EMPTY:
               if (allowEmpty)
               {
                  return true;
               }
               else 
               {
                  UserInteraction.ShowError("{0} is empty. Empty addres not allowed.", tag);
                  textbox.Focus();
                  return false;
               }
            case Misc.AddressStatus.ADDRESS_TO_LONG:
               UserInteraction.ShowError("{0} is to long. 200 characters total allowed.", tag);
               textbox.Focus();
               return false;
            case Misc.AddressStatus.ADDRESS_TO_MUCH_LINES:
               UserInteraction.ShowError("{0} has to much lines. 6 lines total allowed.", tag);
               textbox.Focus();
               return false;
            default:
               return true;
         }
      }

      public static void NotifyTextError(this TextBoxBase textbox, string errormMessage)
      {
         UserInteraction.ShowError(errormMessage);
         textbox.Focus();
      }

      public static double GetValue(this TextBox textbox)
      {
         double value = 0;
         Double.TryParse(textbox.Text, NumberStyles.Number, CultureInfo.CurrentCulture.NumberFormat, out value);
         return value;
      }
   }

   // 
   public static class DateTimePickerExtensions
   {
      public static bool IsActionDateConstraintsValid(this DateTimePicker dtp, DateTime lastActionDate, string actionTag)
      {
         // reasonable (so action can be invoiced) dates for action is: this month or month before (e.g. if previous month hasn't been done yet)
         // Any date before previous month should be reported as non-invoce action
         var reasonableMonth = DateTime.Today.Month > 1 ? DateTime.Today.Month - 1 : 12;
         var reasonableYear = DateTime.Today.Month > 1 ? DateTime.Today.Year : DateTime.Today.Year - 1;
         if (dtp.Value < new DateTime(reasonableYear, reasonableMonth, 1))
         {
            if (!UserInteraction.CanContinue(
               "This '{0}' won't be invoiced (date is not current, nor previus month).\n" +
               "Are you sure you want to do '{0}' with this date?", actionTag))
            {
               dtp.Focus();
               return false;
            }
         }
         else if (dtp.Value.Year < lastActionDate.Year)
         {
            if (!UserInteraction.CanContinue(
               "Last action you made during session, or current date year is grater then this action year.\n" +
               "Are you sure you want to do '{0}' with this date?", actionTag))
            {
               dtp.Focus();
               return false;
            }
         }
         else if ((dtp.Value.Month < lastActionDate.Month) &&
                  (lastActionDate.Year == dtp.Value.Year))
         {
            if (!UserInteraction.CanContinue(
               "Last action you made during session, or current date month is grater then this action month.\n" +
               "Are you sure you want to do '{0}' with this date?", actionTag))
            {
               dtp.Focus();
               return false;
            }
         }
         else  if (dtp.Value.Month > lastActionDate.Month)
         {
            if (!UserInteraction.CanContinue(
                  "Last action you made during session, or current date month is smaller then this action month.\n" +
                  "Are you sure you want to do '{0}' with this date?", actionTag))
            {
               dtp.Focus();
               return false;
            }
         }
         else if (dtp.Value.Year > lastActionDate.Year)
         {
            if (!UserInteraction.CanContinue(
                  "Last action you made during session, or current date year is smaller then this action year.\n" +
                  "Are you sure you want to do '{0}' with this date?", actionTag))
            {
               dtp.Focus();
               return false;
            }
         }
         return true;
      }
   }

   //
   public static class DataGridViewExtensions
   {
      public static void HideColumns(this DataGridView dgv, int[] columnsToHideIndexes)
      {
         foreach (var colIndex in columnsToHideIndexes)
         {
            dgv.Columns[colIndex].Visible = false;
         }
      }
   }

   //
   public class TextBoxChangesTracker
   {
      string textBaseValue;
      bool trackingOn;

      TextBox textbox;
      Action<bool> changeForwarder;

      public bool TrackChanges 
      { 
         set 
         {
            if (trackingOn)
            {
               textbox.TextChanged -= this.TextChangedHandler;
               textBaseValue = string.Empty;
            }

            trackingOn = value;
            if(trackingOn)
            {
               textBaseValue = textbox.Text;
               if (trackingOn)
               {
                  textBaseValue = textbox.Text;
                  textbox.TextChanged += this.TextChangedHandler;
               }
            }
         } 
      }

      public TextBoxChangesTracker(TextBox textbox, Action<bool> changeForwarder)
      {
         this.textbox = textbox;
         this.changeForwarder = changeForwarder;
         trackingOn = false;
      }

      private void TextChangedHandler(object s, EventArgs e)
      {
         if (trackingOn)
         {
            changeForwarder(textbox.Text != textBaseValue);
         }
      }
   }

}
