﻿using Fastness.DataModel;
using Fastness.DataModel.Transaction;
using Fastness.DataModel.Utils;
using System;
using System.Windows.Forms;

namespace FastnessForms.Views
{
   public partial class ActionReconsignForm : Form
   {
      fastnessEntities dbCtx = new fastnessEntities();

      ActionContext actionCtx;
      ActionFormContext formCtx;
      Action<FastnessActionContext, string> itemSaver;

      ItemDetail fetchedItem = null;

      TextBoxChangesTracker itemNumberTrack = null;

      private bool ReadyToReconsign
      { 
         set 
         {
            txtCharge.Enabled = false; // enabled explicitly by button
            txtLocation.Enabled = value;
            dtpReconsignment.Enabled = value;
            btReconsign.Enabled = value;
            btShow.Enabled = !value;
            btAllowChargeChange.Enabled = value;
            txtAuthoriziedBy.Enabled = value;
         } 
      }

      public ActionReconsignForm(ActionContext actionCtx, ActionFormContext formCtx, Action<FastnessActionContext, string> itemSaver)
      {
         this.actionCtx = actionCtx;
         this.formCtx = formCtx;

         this.itemSaver = itemSaver;

         InitializeComponent();

         this.Text = this.actionCtx.FormCaption;

         this.txtItemNumber.AddKeyPressWithTextLengthLimitAndIntegerValue(DBFieldsConstraint.ITEM_NUMBER_LENGTH);
         this.txtConfirmItemNumber.AddKeyPressWithTextLengthLimitAndIntegerValue(DBFieldsConstraint.ITEM_NUMBER_LENGTH);

         this.txtCharge.AddKeyPressWithNumericValue();
         this.txtLocation.AddKeyPressWithTextLengthLimitAndIntegerValue(DBFieldsConstraint.LOCATION_NUMBER_LENGTH);
         this.txtAuthoriziedBy.AddKeyPressWithTextLengthLimit(DBFieldsConstraint.ACTION_HISTORY_AUTHORIZED_BY_MAX_LENGHT);

         this.txtCharge.Text = actionCtx.CtrAction.Charge.ToString();
         this.dtpReconsignment.Value = this.formCtx.LastActionDate;
         this.txtAuthoriziedBy.Text = this.formCtx.LastActionAuthorizer;

         itemNumberTrack = new TextBoxChangesTracker(this.txtItemNumber, (itemNumberChanged) =>
         {
            ReadyToReconsign = !itemNumberChanged;
         });

         ReadyToReconsign = false;
      }

      private bool FormDataValid()
      {
         if (!txtItemNumber.IsTextLenthExactlyTheSame(DBFieldsConstraint.ITEM_NUMBER_LENGTH, "Box number"))
         {
            return false;
         }

         if (!txtItemNumber.IsDigitsOnlyText("Box number"))
         {
            return false;               
         }

         if (!new AskDb(dbCtx).ItemAlreadyExists(txtItemNumber.Text))
         {
            txtItemNumber.NotifyTextError("Box with this number does not exists!");
            return false;
         }
         if (txtItemNumber.Text != txtConfirmItemNumber.Text)
         {
            txtConfirmItemNumber.NotifyTextError("Box Number and confirmed box number are different!");
            return false;
         }

         if (!txtLocation.IsTextLenthExactlyTheSame(DBFieldsConstraint.LOCATION_NUMBER_LENGTH, "Location"))
         {
            return false;
         }
         if (!txtLocation.IsDigitsOnlyText("Location"))
         {
            return false;
         }
         if (new AskDb(dbCtx).LocationOccupied(txtLocation.Text))
         {
            txtLocation.NotifyTextError("Location is already ocupied!");
            return false;
         }

         if (!txtCharge.IsValidNumericValueFormat("Charge"))
         {
            return false;
         }
         if (txtAuthoriziedBy.IsTextToLong(DBFieldsConstraint.ACTION_HISTORY_AUTHORIZED_BY_MAX_LENGHT, "Authorization name"))
         {
            return false;
         }

         if (!dtpReconsignment.IsActionDateConstraintsValid(formCtx.LastActionDate, actionCtx.CtrAction.Description))
         {
            return false;
         }

         var retrievedAt = fetchedItem.DateRetrieved ?? this.dtpReconsignment.Value;
         if(dtpReconsignment.Value < retrievedAt)
         {
            UserInteraction.ShowError(
               string.Format("Reconsign date {0} is before the box had be returned to the client ({1})! Reconsign can take place after the box had been returned to the client.", 
               dtpReconsignment.Value.ToShortDateString(), retrievedAt.ToShortDateString()));
            dtpReconsignment.Focus();

            return false;
         }
         
         return true;
      }

      private void btReconsign_Click(object sender, EventArgs e)
      {
         if (!FormDataValid())
         {
            return;
         }

         itemSaver(
            new FastnessActionContext(
               fetchedItem,
               actionCtx.Ctr,
               actionCtx.CtrAction,
               dtpReconsignment.Value,
               Double.Parse(this.txtCharge.Text),
               txtAuthoriziedBy.Text),
            txtLocation.Text);

         formCtx.Update(txtAuthoriziedBy.Text, dtpReconsignment.Value);

         Close();
      }

      private void btClose_Click(object sender, EventArgs e)
      {
         Close();
      }

      private void btShow_Click(object sender, EventArgs e)
      {
         var validateFormBeforeShow = new Func<bool>(() =>
         {
            if (!txtItemNumber.IsTextLenthExactlyTheSame(DBFieldsConstraint.ITEM_NUMBER_LENGTH, "Box number"))
            {
               return false;
            }
            if (!txtItemNumber.IsDigitsOnlyText("Box number"))
            {
               return false;
            }
            if (!new AskDb(dbCtx).ItemAlreadyExists(txtItemNumber.Text))
            {
               txtItemNumber.NotifyTextError("Box with this number does not exists!");
               return false;
            }
            if (txtItemNumber.Text != txtConfirmItemNumber.Text)
            {
               txtConfirmItemNumber.NotifyTextError("Box Number and confirmed box number are different!");
               return false;
            }
            return true;
         });

         if (!validateFormBeforeShow())
         {
            return;
         }

         fetchedItem = new FetchFromDb(dbCtx).ItemBasedOnItemNo(txtItemNumber.Text);

         if (fetchedItem.ContractID != actionCtx.Ctr.ContractID)
         {
            UserInteraction.ShowError(
               "Box '{0}' does not belong to '{1}'.\nYou must choose box owned by '{1}'", 
               fetchedItem.ItemNumber, actionCtx.Ctr.Name);
            fetchedItem = null;
            return;
         }
         if(fetchedItem.Destroyed ?? false)
         {
            UserInteraction.ShowError(
               "Box '{0}' has been destroyed! You must choose box which still exists.", 
               fetchedItem.ItemNumber);
            fetchedItem = null;
            return;
         }
         if ((fetchedItem.InStock ?? true))
         {
            UserInteraction.ShowError(
               "Box '{0}' is already in stock.\nYou must choose box which had been returned to the client (is not in stock).", 
               fetchedItem.ItemNumber);
            fetchedItem = null;
            return;
         }
         
         txtClientItemReference.Text = fetchedItem.ClientItemRef;
         txtDescription.Text = fetchedItem.Description;
         txtType.Text = fetchedItem.Type;
         txtPercentage.Text = fetchedItem.Percentage.ToString();
         txtLocation.Text = fetchedItem.Location;

         itemNumberTrack.TrackChanges = true;
         ReadyToReconsign = true;
         txtLocation.Focus();
      }

      private void btAllowChargeChange_Click(object sender, EventArgs e)
      {
         this.txtCharge.Enabled = true;
      }
   }
}
