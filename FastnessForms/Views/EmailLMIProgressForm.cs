﻿using Fastness.BusinessOperations;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace FastnessForms.Views
{
   public partial class EmailLMIProgressForm : Form
   {
      EmailLastMonthInvoices operation;

      public EmailLMIProgressForm(EmailLastMonthInvoices operation)
      {
         InitializeComponent();

         btClose.Enabled = false;
         btResendFailures.Enabled = false;

         this.operation = operation;
         this.operation.ProgressChanged += (percents, status) =>
         {
            asyncLMISender.ReportProgress(percents, status);
         };
      }

      private void asyncLMISender_DoWork(object sender, DoWorkEventArgs e)
      {
         operation.Run();
      }

      private void asyncLMISender_ProgressChanged(object sender, ProgressChangedEventArgs e)
      {
         pbProgressInfo.Value = e.ProgressPercentage;

         if (e.UserState != null)
         {
            pbProgressInfo.Value = 0;
            pbProgressInfo.Minimum = 0;
            pbProgressInfo.Maximum = (int)e.UserState;
            return;
         }

         lbInfo.Text = string.Format("Sending last month invoices in progress... ({0}/{1})",
            pbProgressInfo.Value, pbProgressInfo.Maximum);

         RefreshGrid();
      }
      private void RefreshGrid()
      {
         dgvStatuses.DataSource = operation.Statuses;
         dgvStatuses.HideColumns(new int[] { 0, 2, 4 });
         dgvStatuses.Refresh();
      }
      private void asyncLMISender_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
      {
         RefreshGrid();

         lbInfo.Text = "Sending last month invoices completed.";
         
         btClose.Enabled = true;
         btResendFailures.Enabled = operation.Statuses.Count(s => !s.Success) > 0;

         if (operation.Error != null)
         {
            UserInteraction.ShowError("Error occured during e-mailing L.M.I:\n\n{0}", operation.Error.Message);
         }
      }

      private void SendUnsentEmails()
      {
         lbInfo.Text = "Sending last month invoices in progress...";
         asyncLMISender.RunWorkerAsync();
      }
      private void EmailLMIProgressForm_Load(object sender, EventArgs e)
      {
         SendUnsentEmails();
      }

      private void dgvStatuses_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
      {
         if (dgvStatuses.DataSource == null ||
             operation.Statuses.Count == 0)
         {
            return;
         }

         var status = operation.Statuses[e.RowIndex];
         e.CellStyle.BackColor = status.Success ? Color.LightGreen : Color.Red;
      }

      private void btResendFailures_Click(object sender, EventArgs e)
      {
         SendUnsentEmails();
      }

      private void btClose_Click(object sender, EventArgs e)
      {
         Close();
      }
   }
}
