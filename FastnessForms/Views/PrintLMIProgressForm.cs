﻿using Fastness.Common;
using Fastness.DataModel;
using Fastness.DataModel.Utils;
using Fastness.Invoicing;
using Fastness.Invoicing.Report;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace FastnessForms.Views
{
   public partial class PrintLMIProgressForm : Form
   {
      public static string LmiInvoicesReportAlias { get { return "L.M.I report"; } }

      public PrintLMIProgressForm()
      {
         InitializeComponent();
      }

      private void asyncLMIPrinter_DoWork(object sender, DoWorkEventArgs e)
      {
         int invoiceCounter = 1;

         var reportFilePath = PdfFilesHelper.PrepareReportFilePath(LmiInvoicesReportAlias);
         using (var report = new PDFInvoiceReport(reportFilePath))
         {
            using (var dbCtx = new fastnessEntities())
            {
               var invoicesCollection = new FetchFromDb(dbCtx).AllStoredInvoices(DateUtils.LastMonthDate);

               asyncLMIPrinter.ReportProgress(0, invoicesCollection.Count());

               var deserializer = new InvoiceHistoryEntryDeserializer(dbCtx);
               foreach (var ih in invoicesCollection)
               {
                  report.AddInvoice(deserializer.Deserialize(ih));

                  asyncLMIPrinter.ReportProgress(invoiceCounter++);
               }
            }
         }
         PdfFilesHelper.Show(reportFilePath);
      }

      private void asyncLMIPrinter_ProgressChanged(object sender, ProgressChangedEventArgs e)
      {
         pbProgressInfo.Value = e.ProgressPercentage;

         if (e.UserState != null)
         {
            pbProgressInfo.Value = 0;
            pbProgressInfo.Minimum = 0;
            pbProgressInfo.Maximum = (int)e.UserState;
            return;
         }

         lbInfo.Text = string.Format("Last month invoices printing in progress... ({0}/{1})",
            pbProgressInfo.Value, pbProgressInfo.Maximum);
      }

      private void asyncLMIPrinter_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
      {
         lbInfo.Text = "Last month invoices printing completed.";
         Close();
      }

      private void PrintLMIProgressForm_Load(object sender, EventArgs e)
      {
         lbInfo.Text = "Last month invoices printing in progress...";

         asyncLMIPrinter.RunWorkerAsync();
      }
   }
}
