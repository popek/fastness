﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastnessForms.Views
{
   public class ActionFormContext
   {
      public string LastActionAuthorizer { get; private set; }
      public DateTime LastActionDate { get; private set; }

      public ActionFormContext()
      {
         LastActionDate = DateTime.Today;
      }
      public void Update(string newAuthorizer, DateTime newDate)
      {
         LastActionAuthorizer = newAuthorizer;
         LastActionDate = newDate;
      }
      public void Reset()
      {
         LastActionAuthorizer = "";
         LastActionDate = DateTime.Today;
      }
   }
}
