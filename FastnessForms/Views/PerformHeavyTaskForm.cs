﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace FastnessForms.Views
{
   public partial class PerformHeavyTaskForm : Form
   {
      Action taskToPerform;
      string taskDescription;
      int processingTimeInSeconds = 0;

      public static void Run(string formLabel, string taskDescription, Action taskToPerform)
      {
         new PerformHeavyTaskForm(formLabel, taskDescription, taskToPerform).ShowDialog();
      }
      public PerformHeavyTaskForm(string formLabel, string taskDescription, Action taskToPerform)
      {
         this.taskToPerform = taskToPerform;
         this.taskDescription = taskDescription;

         InitializeComponent();

         Text = formLabel;
         UpdateInfo();
      }

      private void PerformHeavyTaskForm_Load(object sender, EventArgs e)
      {
         async.RunWorkerAsync();
         timer1.Start();
      }

      private void async_DoWork(object sender, DoWorkEventArgs e)
      {
         taskToPerform();
      }

      private void async_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
      {
         timer1.Stop();
         Close();
      }

      private void timer1_Tick(object sender, EventArgs e)
      {
         ++processingTimeInSeconds;
         UpdateInfo();
      }
      private void UpdateInfo()
      {
         lbInfo.Text = string.Format("{0} (processing time: {1} s)", taskDescription, processingTimeInSeconds);
      }
   }
}
