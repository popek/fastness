﻿using Fastness.DataModel.Utils;
using Fastness.Common.Events;
using System;
using System.Windows.Forms;

namespace FastnessForms.Views
{
   public partial class SetupVATForm : Form
   {
      double currentVat;

      public SetupVATForm()
      {
         InitializeComponent();

         currentVat = Misc.CurrentVAT;

         btSaveChanges.Enabled = false;

         txtVAT.AddKeyPressWithNumericValue();
         txtVAT.AddTextChangeHandler(() => currentVat.ToString(), (textChanged) =>
         {
            btSaveChanges.Enabled = textChanged;
         });

         txtVAT.Text = currentVat.ToString();
      }

      private void btSaveChanges_Click(object sender, EventArgs e)
      {
         if (!txtVAT.IsValidNumericValueFormat("VAT"))
         {
            return;
         }

         double newVat = Double.Parse(txtVAT.Text);

         if (newVat > 100)
         {
            UserInteraction.ShowError("VAT can't be grater then 100% !!!");
            txtVAT.Focus();
            return;
         }
         else if (newVat > 40)
         {
            if (!UserInteraction.AskUser("Are you sure you want to set VAT to {0}% ?", txtVAT.Text))
            {
               return;
            }
         }

         if (newVat != currentVat)
         {
            Misc.CurrentVAT = newVat;
            UserInteraction.ShowInfo("New VAT succesfully set to {0}%.", txtVAT.Text);
         }
         Close();
      }

      private void btClose_Click(object sender, EventArgs e)
      {
         Close();
      }
   }
}
