﻿using Fastness.Common;
using Fastness.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FastnessForms.Views
{
   public partial class ConfirmPasswordBeforeFragileActionForm : Form
   {
      bool passwordOk = false;
      Action<bool> passwordOkHandler;

      public ConfirmPasswordBeforeFragileActionForm(string caption, Action<bool> passwordOkHandler)
      {
         InitializeComponent();

         this.passwordOkHandler = passwordOkHandler;
         txtPassword.Focus();
         Text = caption;
      }

      private void btCancel_Click(object sender, EventArgs e)
      {
         Close();
      }

      private void btContinue_Click(object sender, EventArgs e)
      {
         if (!ValidateFormData())
         {
            return;
         }
         passwordOk = LoggedUserUtils.IsValidPassword(txtPassword.Text);
         if(!passwordOk)
         {
            txtPassword.NotifyTextError("Invalid password. Provided password does not match logged user password!");
            return;
         }
         Close();
      }

      private bool ValidateFormData()
      {
         if (txtPassword.IsTextEmpty("password"))
         {
            return false;
         }
         return true;
      }

      private void UserLoginForm_FormClosed(object sender, FormClosedEventArgs e)
      {
         passwordOkHandler(passwordOk);
      }
   }
}
