﻿using Fastness.Common;
using Fastness.Common.Events;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FastnessForms.Views
{
   public partial class InvoiceMailSettingsForm : Form
   {
      static readonly ILog log = LogManager.GetLogger(typeof(InvoiceMailSettingsForm));
      private static readonly int PORT_MAX_LENGTH = 4;

      SmtpClientSettings smtpClientSettings;
      MailTemplate invoiceMailTemplate;

      bool serverAddressChanged = false;
      bool serverPortChanged = false;
      bool smtpUserChanged = false;
      bool smtpPasswordChanged = false;
      bool useTlsChanged = false;

      bool senderChanged = false;
      bool subjectChanged = false;
      bool bodyChanged = false;

      private bool UnsavedSmtpSettingsOnBoard
      {
         get
         {
            return serverPortChanged ||
               serverAddressChanged ||
               smtpUserChanged ||
               smtpPasswordChanged ||
               useTlsChanged;
         }
      }
      private bool UnsavedMailTemplatedOnBoard
      { 
         get
         {
            return senderChanged ||
               subjectChanged ||
               bodyChanged;
         }
         
      }
      private bool UnsavedChangesOnBoard
      {
         get { return UnsavedSmtpSettingsOnBoard || UnsavedMailTemplatedOnBoard; }
      }

      public event SmtpClientSettingsChangedEventHandler SmtpSettingsChanged;
      public event MailTemplatedChangedEventHandler MailTemplateChanged;

      public InvoiceMailSettingsForm(SmtpClientSettings smtpClientSettings, MailTemplate invoiceMailTemplate)
      {
         InitializeComponent();

         this.smtpClientSettings = smtpClientSettings;
         this.invoiceMailTemplate = invoiceMailTemplate;

         // setup smtp details controls
         txtAddress.Text = smtpClientSettings.ServerAddress;
         txtAddress.AddTextChangeHandler(() => smtpClientSettings.ServerAddress, (textChanged) =>
         {
            serverAddressChanged = textChanged;
            btSaveChanges.Enabled = UnsavedChangesOnBoard;
         });
         txtPort.Text = smtpClientSettings.ServerPort.ToString();
         txtPort.AddKeyPressWithTextLengthLimitAndIntegerValue(PORT_MAX_LENGTH);
         txtPort.AddTextChangeHandler(() => smtpClientSettings.ServerPort.ToString(), (textChanged) =>
         {
            serverPortChanged = textChanged;
            btSaveChanges.Enabled = UnsavedChangesOnBoard;
         });
         txtUser.Text = smtpClientSettings.User;
         txtUser.AddTextChangeHandler(() => smtpClientSettings.User, (textChanged) =>
         {
            smtpUserChanged = textChanged;
            btSaveChanges.Enabled = UnsavedChangesOnBoard;
         });
         txtPassword.Text = "";
         txtPassword.AddTextChangeHandler(() => "", (textChanged) =>
         {
            smtpPasswordChanged = textChanged;
            btSaveChanges.Enabled = UnsavedChangesOnBoard;
         });
         cbSecure.Checked = smtpClientSettings.UseTls;
         cbSecure.CheckedChanged += (s, e) =>
         {
            useTlsChanged = cbSecure.Checked != smtpClientSettings.UseTls;
            btSaveChanges.Enabled = UnsavedChangesOnBoard;
         };

         // setup mail details controls
         txtSender.Text = invoiceMailTemplate.Sender;
         txtSender.AddTextChangeHandler(() => invoiceMailTemplate.Sender, (textChanged) =>
         {
            senderChanged = textChanged;
            btSaveChanges.Enabled = UnsavedChangesOnBoard;
         });
         txtSubject.Text = invoiceMailTemplate.SubjectTemplate;
         txtSubject.AddTextChangeHandler(() => invoiceMailTemplate.SubjectTemplate, (textChanged) =>
         {
            subjectChanged = textChanged;
            btSaveChanges.Enabled = UnsavedChangesOnBoard;
         });
         txtBody.Text = invoiceMailTemplate.BodyTemplate;
         txtBody.AddTextChangeHandler(() => invoiceMailTemplate.BodyTemplate, (textChanged) =>
         {
            bodyChanged = textChanged;
            btSaveChanges.Enabled = UnsavedChangesOnBoard;
         });

         btSaveChanges.Enabled = UnsavedChangesOnBoard;
      }

      private bool FormDataValid()
      {
         if(txtAddress.IsTextEmpty("SMTP server address") ||
            txtUser.IsTextEmpty("SMTP user") ||
            txtPassword.IsTextEmpty("SMTP password") ||
            txtPort.IsTextToLong(PORT_MAX_LENGTH, "SMTP server port") ||
            !txtPort.IsValidIntegerValueFormat("SMTP server port"))
         {
            return false;
         }
         if (txtSender.IsTextEmpty("Invoice e-mail sender"))
         {
            return false;
         }
         return true;
      }
      private void btSaveChanges_Click(object sender, EventArgs e)
      {
         if (!FormDataValid())
         {
            return;
         }
         if (!UserInteraction.AskUser("Are you sure you want to chagend invoice mail settings?"))
         {
            return;
         }
         if(serverAddressChanged)
         {
            log.InfoFormat("changing smtp server addres ({0} -> {1})", smtpClientSettings.ServerAddress, txtAddress.Text);
            smtpClientSettings.ServerAddress = txtAddress.Text;
         }
         if(serverPortChanged)
         {
            log.InfoFormat("changing smtp server port ({0} -> {1})", smtpClientSettings.ServerPort, txtPort.Text);
            smtpClientSettings.ServerPort = Int32.Parse(txtPort.Text);
         }
         if (smtpUserChanged)
         {
            log.InfoFormat("changing smtp user ({0} -> {1})", smtpClientSettings.User, txtUser.Text);
            smtpClientSettings.User = txtUser.Text;
         }
         if (smtpPasswordChanged)
         {
            log.InfoFormat("changing smtp user password.");
            smtpClientSettings.UpdatePassword(txtPassword.Text);
         }
         if (useTlsChanged)
         {
            log.InfoFormat("changing tls/ssl usage ({0} -> {1})", smtpClientSettings.UseTls, cbSecure.Checked);
            smtpClientSettings.UseTls = cbSecure.Checked;
         }

         if (senderChanged)
         {
            log.InfoFormat("ivnoice mail sender changed ({0} -> {1})", invoiceMailTemplate.Sender, txtSender.Text);
            invoiceMailTemplate.Sender = txtSender.Text;
         }
         if (subjectChanged)
         {
            log.InfoFormat("ivnoice mail subject changed ({0} -> {1})", invoiceMailTemplate.SubjectTemplate, txtSubject.Text);
            invoiceMailTemplate.SubjectTemplate = txtSubject.Text;
         }
         if (bodyChanged)
         {
            log.InfoFormat("ivnoice mail body changed ({0} -> {1})", invoiceMailTemplate.BodyTemplate, txtBody.Text);
            invoiceMailTemplate.BodyTemplate = txtBody.Text;
         }

         if (UnsavedSmtpSettingsOnBoard)
         {
            SmtpSettingsChanged(smtpClientSettings);
         }
         if (UnsavedMailTemplatedOnBoard)
         {
            MailTemplateChanged(invoiceMailTemplate);
         }         

         Close();
      }

      private void btCancel_Click(object sender, EventArgs e)
      {
         if (UnsavedChangesOnBoard)
         {
            if (!UserInteraction.AskUser("Are you sure you want to cancel? (there are unsaved changes)"))
            {
               return;
            }
         }
         Close();
      }
   }
}
