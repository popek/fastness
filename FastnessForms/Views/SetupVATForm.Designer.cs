﻿namespace FastnessForms.Views
{
   partial class SetupVATForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetupVATForm));
         this.label6 = new System.Windows.Forms.Label();
         this.txtVAT = new System.Windows.Forms.TextBox();
         this.btClose = new System.Windows.Forms.Button();
         this.btSaveChanges = new System.Windows.Forms.Button();
         this.SuspendLayout();
         // 
         // label6
         // 
         this.label6.AutoSize = true;
         this.label6.Location = new System.Drawing.Point(12, 12);
         this.label6.Name = "label6";
         this.label6.Size = new System.Drawing.Size(89, 13);
         this.label6.TabIndex = 12;
         this.label6.Text = "VAT (in percents)";
         // 
         // txtVAT
         // 
         this.txtVAT.Location = new System.Drawing.Point(107, 9);
         this.txtVAT.MaxLength = 20;
         this.txtVAT.Name = "txtVAT";
         this.txtVAT.Size = new System.Drawing.Size(91, 20);
         this.txtVAT.TabIndex = 11;
         // 
         // btClose
         // 
         this.btClose.Location = new System.Drawing.Point(107, 49);
         this.btClose.Name = "btClose";
         this.btClose.Size = new System.Drawing.Size(91, 30);
         this.btClose.TabIndex = 14;
         this.btClose.Text = "Cancel";
         this.btClose.UseVisualStyleBackColor = true;
         this.btClose.Click += new System.EventHandler(this.btClose_Click);
         // 
         // btSaveChanges
         // 
         this.btSaveChanges.Location = new System.Drawing.Point(10, 49);
         this.btSaveChanges.Name = "btSaveChanges";
         this.btSaveChanges.Size = new System.Drawing.Size(91, 30);
         this.btSaveChanges.TabIndex = 13;
         this.btSaveChanges.Text = "OK";
         this.btSaveChanges.UseVisualStyleBackColor = true;
         this.btSaveChanges.Click += new System.EventHandler(this.btSaveChanges_Click);
         // 
         // SetupVATForm
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(213, 90);
         this.ControlBox = false;
         this.Controls.Add(this.btClose);
         this.Controls.Add(this.btSaveChanges);
         this.Controls.Add(this.label6);
         this.Controls.Add(this.txtVAT);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
         this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
         this.Name = "SetupVATForm";
         this.Text = "Set VAT value";
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.TextBox txtVAT;
      private System.Windows.Forms.Button btClose;
      private System.Windows.Forms.Button btSaveChanges;
   }
}