﻿using Fastness.DataModel;
using Fastness.DataModel.Transaction;
using Fastness.DataModel.Utils;
using System;
using System.Windows.Forms;

namespace FastnessForms.Views
{
   public partial class ActionRetrieveForm : Form
   {
      fastnessEntities dbCtx = new fastnessEntities();

      ActionContext actionCtx;
      ActionFormContext formCtx;
      Action<FastnessActionContext, string> itemSaver;

      ItemDetail fetchedItem = null;

      TextBoxChangesTracker itemNumberTrack = null;

      private bool ReadyToRetrieve
      { 
         set 
         {
            this.txtCharge.Enabled = false; // enable explicitly by pressing button
            this.dtpRetrieve.Enabled = value;
            this.txtAuthoriziedBy.Enabled = value;
            this.txtNotes.Enabled = value;
            this.btRetrieve.Enabled = value;
            this.btShow.Enabled = !value;
            this.btAllowChargeChange.Enabled = value;
         } 
      }

      public ActionRetrieveForm(ActionContext actionCtx, ActionFormContext formCtx, Action<FastnessActionContext, string> itemSaver)
      {
         this.actionCtx = actionCtx;
         this.formCtx = formCtx;

         this.itemSaver = itemSaver;

         InitializeComponent();

         this.Text = this.actionCtx.FormCaption;

         this.txtItemNumber.AddKeyPressWithTextLengthLimitAndIntegerValue(DBFieldsConstraint.ITEM_NUMBER_LENGTH);
         this.txtConfirmItemNumber.AddKeyPressWithTextLengthLimitAndIntegerValue(DBFieldsConstraint.ITEM_NUMBER_LENGTH);
         
         this.txtCharge.AddKeyPressWithNumericValue();

         this.txtNotes.AddKeyPressWithTextLengthLimit(DBFieldsConstraint.ACTION_HISTORY_NOTES_MAX_LENGHT);
         this.txtAuthoriziedBy.AddKeyPressWithTextLengthLimit(DBFieldsConstraint.ACTION_HISTORY_AUTHORIZED_BY_MAX_LENGHT);

         this.txtCharge.Text = actionCtx.CtrAction.Charge.ToString();
         this.dtpRetrieve.Value = this.formCtx.LastActionDate;
         this.txtAuthoriziedBy.Text = this.formCtx.LastActionAuthorizer;

         this.txtClientItemReference.Enabled = false;
         this.txtDescription.Enabled = false;
         this.txtType.Enabled = false;
         this.txtPercentage.Enabled = false;
         this.txtLocation.Enabled = false;

         itemNumberTrack = new TextBoxChangesTracker(this.txtItemNumber, (itemNumberChanged) =>
         {
            ReadyToRetrieve = !itemNumberChanged;
         });

         ReadyToRetrieve = false;
      }

      private bool FormDataValid()
      {
         if (!txtItemNumber.IsTextLenthExactlyTheSame(DBFieldsConstraint.ITEM_NUMBER_LENGTH, "Box number"))
         {
            return false;
         }

         if (!txtItemNumber.IsDigitsOnlyText("Box number"))
         {
            return false;               

         }
         if (!new AskDb(dbCtx).ItemAlreadyExists(txtItemNumber.Text))
         {
            txtItemNumber.NotifyTextError("Box with this number does not exists!");
            return false;
         }
         if (txtItemNumber.Text != txtConfirmItemNumber.Text)
         {
            txtConfirmItemNumber.NotifyTextError("Box Number and confirmed box number are different!");
            return false;
         }

         if (!txtLocation.IsTextLenthExactlyTheSame(DBFieldsConstraint.LOCATION_NUMBER_LENGTH, "Location"))
         {
            return false;
         }
         if (!txtLocation.IsDigitsOnlyText("Location"))
         {
            return false;
         }

         if (!txtCharge.IsValidNumericValueFormat("Charge"))
         {
            return false;
         }

         if (!dtpRetrieve.IsActionDateConstraintsValid(formCtx.LastActionDate, actionCtx.CtrAction.Description))
         {
            return false;
         }
         var takenFromClientAt = fetchedItem.DateReturned ?? fetchedItem.DateRecieved;
         if (dtpRetrieve.Value < takenFromClientAt)
         {
            UserInteraction.ShowError(
               "Retrieve date '{0}' is before the box had been put in stock ({1}).\nRetrieve can take place after the date box had been put to in stock", 
               dtpRetrieve.Value.ToShortDateString(), takenFromClientAt.Value.ToShortDateString());
            dtpRetrieve.Focus();
            return false;
         }

         if (txtNotes.IsTextToLong(DBFieldsConstraint.ACTION_HISTORY_NOTES_MAX_LENGHT, "notes"))
         {
            return false;
         }
         if (txtAuthoriziedBy.IsTextToLong(DBFieldsConstraint.ACTION_HISTORY_AUTHORIZED_BY_MAX_LENGHT, "Authorization name"))
         {
            return false;
         }
        
         return true;
      }

      private void btRetrieve_Click(object sender, EventArgs e)
      {
         if (!FormDataValid())
         {
            return;
         }

         itemSaver(
            new FastnessActionContext(
               fetchedItem,
               actionCtx.Ctr,
               actionCtx.CtrAction,
               dtpRetrieve.Value,
               Double.Parse(this.txtCharge.Text),
               txtAuthoriziedBy.Text),
            txtNotes.Text);

         formCtx.Update(txtAuthoriziedBy.Text, dtpRetrieve.Value);

         Close();
      }

      private void btClose_Click(object sender, EventArgs e)
      {
         Close();
      }

      private void btShow_Click(object sender, EventArgs e)
      {
         var validateFormBeforeShow = new Func<bool>(() =>
         {
            if (!txtItemNumber.IsTextLenthExactlyTheSame(DBFieldsConstraint.ITEM_NUMBER_LENGTH, "Box number"))
            {
               return false;
            }
            if (!txtItemNumber.IsDigitsOnlyText("Box number"))
            {
               return false;
            }
            if (!new AskDb(dbCtx).ItemAlreadyExists(txtItemNumber.Text))
            {
               txtItemNumber.NotifyTextError("Box with this number does not exists!");
               return false;
            }
            if (txtItemNumber.Text != txtConfirmItemNumber.Text)
            {
               txtConfirmItemNumber.NotifyTextError("Box Number and confirmed box number are different!");
               return false;
            }
            return true;
         });

         if (!validateFormBeforeShow())
         {
            return;
         }

         fetchedItem = new FetchFromDb(dbCtx).ItemBasedOnItemNo(txtItemNumber.Text);

         if (fetchedItem.ContractID != actionCtx.Ctr.ContractID)
         {
            UserInteraction.ShowError("Box '{0}' does not belong to '{1}'.\nYou must choose box owned by '{1}'", 
               fetchedItem.ItemNumber, actionCtx.Ctr.Name);
            fetchedItem = null;
            return;
         }
         if(fetchedItem.Destroyed ?? false)
         {
            UserInteraction.ShowError("Box '{0}' has been destroyed.\nYou must choose box which still exists.", 
               fetchedItem.ItemNumber);
            fetchedItem = null;
            return;
         }
         if (!(fetchedItem.InStock ?? true))
         {
            UserInteraction.ShowError("Box '{0}' is already out of stock.\nYou must choose box which is still in stock.", 
               fetchedItem.ItemNumber);
            fetchedItem = null;
            return;
         }

         txtClientItemReference.Text = fetchedItem.ClientItemRef;
         txtDescription.Text = fetchedItem.Description;
         txtType.Text = fetchedItem.Type;
         txtPercentage.Text = fetchedItem.Percentage.ToString();
         txtLocation.Text = fetchedItem.Location;

         itemNumberTrack.TrackChanges = true;
         ReadyToRetrieve = true;

         dtpRetrieve.Focus();
      }

      private void btAllowChargeChange_Click(object sender, EventArgs e)
      {
         this.txtCharge.Enabled = true;
      }
   }
}
