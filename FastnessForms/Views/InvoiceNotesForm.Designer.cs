﻿namespace FastnessForms.Views
{
   partial class InvoiceNotesForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.txtNotes = new System.Windows.Forms.TextBox();
         this.btOk = new System.Windows.Forms.Button();
         this.SuspendLayout();
         // 
         // txtNotes
         // 
         this.txtNotes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.txtNotes.Location = new System.Drawing.Point(12, 12);
         this.txtNotes.Multiline = true;
         this.txtNotes.Name = "txtNotes";
         this.txtNotes.Size = new System.Drawing.Size(260, 83);
         this.txtNotes.TabIndex = 0;
         // 
         // btOk
         // 
         this.btOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.btOk.Location = new System.Drawing.Point(12, 101);
         this.btOk.Name = "btOk";
         this.btOk.Size = new System.Drawing.Size(73, 25);
         this.btOk.TabIndex = 2;
         this.btOk.Text = "OK";
         this.btOk.UseVisualStyleBackColor = true;
         this.btOk.Click += new System.EventHandler(this.btOk_Click);
         // 
         // InvoiceNotesForm
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(284, 139);
         this.ControlBox = false;
         this.Controls.Add(this.btOk);
         this.Controls.Add(this.txtNotes);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
         this.Name = "InvoiceNotesForm";
         this.Text = "InvoiceNotesForm";
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.TextBox txtNotes;
      private System.Windows.Forms.Button btOk;
   }
}