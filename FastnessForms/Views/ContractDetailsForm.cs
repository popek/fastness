﻿using Fastness.Common;
using Fastness.DataModel;
using Fastness.DataModel.Utils;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FastnessForms.Views;

namespace FastnessForms.Views
{
   public partial class ContractDetailsForm : Form
   {
      static readonly ILog log = LogManager.GetLogger(typeof(ContractDetailsForm));

      static readonly int NAME_MAX_LENGHT = 60;
      static readonly int ACCOUNT_REF_MAX_LENGHT = 8;
      static readonly int CLIENT_INVOICE_REF_MAX_LENGTH = 30;
      static readonly int DESCRIPTION_MAX_LENGTH = 60;

      private Contract contract;

      private string CtrDisplayName { get { return !string.IsNullOrEmpty(contract.Name) ? contract.Name : nameText.Text; } }

      private bool UnsavedChangesOnBoard
      {
         get
         {
            return accountRefChanged ||
               nameChanged ||
               clientInvoiceRefChanged ||
               contractFinishedChanged ||
               descriptionChanged ||
               emailChanged ||
               serviceLevelChanged ||
               fixedFeeChanged ||
               fixedUnitsChanged ||
               addressChanged ||
               auxiliaryAddressChanged;
         }
      }
      
      private bool accountRefChanged = false;
      private bool nameChanged = false;
      private bool clientInvoiceRefChanged = false;
      private bool contractFinishedChanged = false;
      private bool descriptionChanged = false;
      private bool emailChanged = false;
      private bool serviceLevelChanged = false;
      private bool fixedFeeChanged = false;
      private bool fixedUnitsChanged = false;
      private bool addressChanged = false;
      private bool auxiliaryAddressChanged = false;
      
      private Action<Contract> contractUpdateHandler;

      public ContractDetailsForm(Contract contract, Action<Contract> contractUpdateHandler)
      {
         this.contract = contract;

         this.contractUpdateHandler = contractUpdateHandler;

         InitializeComponent();

         this.nameText.AddKeyPressWithTextLengthLimit(NAME_MAX_LENGHT);
         this.accountRefText.AddKeyPressWithTextLengthLimit(ACCOUNT_REF_MAX_LENGHT);
         this.clientInvoiceRefText.AddKeyPressWithTextLengthLimit(CLIENT_INVOICE_REF_MAX_LENGTH);
         this.descriptionText.AddKeyPressWithTextLengthLimit(DESCRIPTION_MAX_LENGTH);

         this.fixedFeeText.AddKeyPressWithNumericValue();
         this.fixedUnitsText.AddKeyPressWithIntegerValue();

         this.saveChangesButton.Enabled = LoggedUserUtils.CanEditDBContent();
      }

      private void HandleChange()
      {
         if (!LoggedUserUtils.CanEditDBContent())
         {
            this.saveChangesButton.Enabled = false;
            return;
         }
         this.saveChangesButton.Enabled = UnsavedChangesOnBoard;
      }

      private void ContractDetailsForm_Load(object sender, EventArgs e)
      {
         this.saveChangesButton.Enabled = false;
         this.closeButton.Enabled = true;

         this.accountRefText.Text = contract.AccountRef;
         this.nameText.Text = contract.Name;
         this.clientInvoiceRefText.Text = contract.ClientInvoiceRef;
         this.contractFinishedCheckbox.Checked = contract.ContractFinished;
         this.descriptionText.Text = contract.Description;
         this.emailText.Text = contract.EmailAddress;

         this.serviceLevelCombo.DataSource = new List<string> { FastnesConsts.SERVICE_LEVEL_STANDARD, FastnesConsts.SERVICE_LEVEL_SHALLOW, FastnesConsts.SERVICE_LEVEL_DEEP };
         this.serviceLevelCombo.DropDownStyle = ComboBoxStyle.DropDownList;
         this.serviceLevelCombo.Text = contract.ServiceLevel;
         this.fixedFeeText.Text = contract.FixedFee.ToString();
         this.fixedUnitsText.Text = contract.FixedUnits.ToString();

         this.addressText.Text = contract.Address;
         this.auxiliaryAddressText.Text = contract.AuxiliaryAddress;

         this.accountRefText.AddTextChangeHandler(() => contract.AccountRef, (textChanged) =>
         {
            accountRefChanged = textChanged;
            this.HandleChange();
         });
         this.nameText.AddTextChangeHandler(() => contract.Name, (textChanged) =>
         {
            nameChanged = textChanged;
            this.HandleChange();
         });
         this.clientInvoiceRefText.AddTextChangeHandler(() => contract.ClientInvoiceRef??"", (textChanged) =>
         {
            clientInvoiceRefChanged = textChanged;
            this.HandleChange();
         });
         this.contractFinishedCheckbox.CheckedChanged += (sen, ev) =>
         {
            contractFinishedChanged = this.contractFinishedCheckbox.Checked != contract.ContractFinished;
            this.HandleChange();
         };
         this.descriptionText.AddTextChangeHandler(() => contract.Description, (textChanged) =>
         {
            descriptionChanged = textChanged;
            this.HandleChange();
         });
         this.emailText.AddTextChangeHandler(() => contract.EmailAddress, (textChanged) =>
         {
            emailChanged = textChanged;
            this.HandleChange();
         });

         this.serviceLevelCombo.SelectedValueChanged += (sen, ev) =>
         {
            serviceLevelChanged = this.serviceLevelCombo.Text != contract.ServiceLevel;
            this.HandleChange();
         };
         this.fixedFeeText.AddTextChangeHandler(() => contract.FixedFee.ToString(), (textChanged) => 
         { 
            fixedFeeChanged = textChanged;
            this.HandleChange();
         });
         this.fixedUnitsText.AddTextChangeHandler(() => contract.FixedUnits.ToString(), (textChanged) =>
         {
            fixedUnitsChanged = textChanged;
            this.HandleChange();
         });
         this.addressText.AddTextChangeHandler(() => contract.Address, (textChanged) =>
         {
            addressChanged = textChanged;
            this.HandleChange();
         });
         this.auxiliaryAddressText.AddTextChangeHandler(() => contract.AuxiliaryAddress??"", (textChanged) =>
         {
            auxiliaryAddressChanged = textChanged;
            this.HandleChange();
         });
      }

      private bool CheckFormData() 
      {
         if (accountRefText.IsTextToLong(ACCOUNT_REF_MAX_LENGHT, "Contract account ref."))
         {
            return false;         
         }

         if (nameText.IsTextEmpty("Contract Name"))
         {
            return false;
         }
         if (nameText.IsTextToLong(NAME_MAX_LENGHT, "Contract name"))
         {
            return false;
         }

         if (clientInvoiceRefText.IsTextToLong(CLIENT_INVOICE_REF_MAX_LENGTH, "Invoice ref."))
         {
            return false;
         }
         if (descriptionText.IsTextToLong(DESCRIPTION_MAX_LENGTH, "Description"))
         {
            return false;
         }

         if (!fixedFeeText.IsValidNumericValueFormat("Fixed Fee"))
         {
            return false;
         }

         if (!fixedUnitsText.IsValidIntegerValueFormat("Fixed Units"))
         {
            return false;
         }

         if (!addressText.IsValidAddress(false, "Address"))
         {
            return false;
         }

         if (!auxiliaryAddressText.IsValidAddress(true, "Auxiliary address"))
         {
            return false;
         }

         if (this.contractFinishedCheckbox.Checked)
         {
            if (!UserInteraction.CanContinue("Finish contract is checked. Are you sure you want to finish contract?"))
            {
               return false;
            }
         }

         return true;
      }

      private void saveChangesButton_Click(object sender, EventArgs e)
      {
         if (!CheckFormData())
         {
            return;
         }

         if (!UserInteraction.AskUser("Do you want to save changes in '{0}' contract?", 
                                      CtrDisplayName))
         {
            return;
         }

         contract.AccountRef = this.accountRefText.Text;
         contract.Name = this.nameText.Text;
         contract.ClientInvoiceRef = this.clientInvoiceRefText.Text;
         contract.ContractFinished = this.contractFinishedCheckbox.Checked;
         contract.Description = this.descriptionText.Text;
         contract.EmailAddress = this.emailText.Text;

         contract.ServiceLevel = this.serviceLevelCombo.Text;
         contract.AuxiliaryAddress = this.auxiliaryAddressText.Text;
         contract.FixedFee = Double.Parse(this.fixedFeeText.Text);
         contract.FixedUnits = Int32.Parse(this.fixedUnitsText.Text);

         contract.Address = Misc.FixAddress(this.addressText.Text);
         contract.AuxiliaryAddress = Misc.FixAddress(this.auxiliaryAddressText.Text);

         this.contractUpdateHandler(contract);

         accountRefChanged = false;
         nameChanged = false;
         clientInvoiceRefChanged = false;
         contractFinishedChanged = false;
         descriptionChanged = false;
         emailChanged = false;
         serviceLevelChanged = false;
         fixedFeeChanged = false;
         fixedUnitsChanged = false;
         addressChanged = false;
         auxiliaryAddressChanged = false;
        
         this.saveChangesButton.Enabled = false;
      }

      private void closeButton_Click(object sender, EventArgs e)
      {
         if (LoggedUserUtils.CanEditDBContent())
         {
            if (UnsavedChangesOnBoard &&
               !UserInteraction.CanContinue(
                  "Unsaved changes will be lost.\nDo you want to close form and loose all changes in '{0}' contract?", 
                  CtrDisplayName))
            {
               return;
            }
         }
         Close();
      }

   }
}
