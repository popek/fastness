﻿using Fastness.Common;
using Fastness.DataModel;
using Fastness.DataModel.Utils;
using Fastness.Invoicing;
using Fastness.Invoicing.Report;
using Fastness.Common.Events;
using FastnessReports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace FastnessForms.Views
{
   public partial class ContractActionsHistoryForm : Form
   {
      DBHandle hDB = new DBHandle();
      Contract ctr;

      DataGridViewManager<ActionHistory> actionsHistoryGridMan;

      bool allActionsLoaded;

      public event DbContentChangedEventHandler DBChanged;

      public ContractActionsHistoryForm(Contract ctr)
      {
         InitializeComponent();

         InitActionsHistoryGrid();

         this.ctr = ctr;
         Text = string.Format("History for {0}", ctr.Name);
         dtpFrom.Value = DateTime.Today;
         dtpTo.Value = DateTime.Today;

         LoadAllActions();
      }

      private void InitActionsHistoryGrid()
      {
         actionsHistoryGridMan = FastnessUI.CreateActionHistoryGridManager(dgvActionsHistory);
      }

      private void LoadAllActions()
      {
         var query = from ah in hDB.DBCtx.ActionHistory
                     join ca in hDB.DBCtx.ContractAction on ah.ContractActionID equals ca.ContractActionID
                     where ca.ContractID == ctr.ContractID
                     orderby ah.DateCompleted
                     select ah;

         ReloadGridData(query);

         allActionsLoaded = true;
      }
      private void LoadActionsFromRange()
      {
         if (dtpFrom.Value > dtpTo.Value)
         {
            UserInteraction.ShowError(
               "Date range is invalid. 'From' date ({0}) is after 'to' ({1}) date", 
               dtpFrom.Value, dtpTo.Value);
            dtpFrom.Focus();
            return;
         }

         var query = from ah in hDB.DBCtx.ActionHistory
                     join ca in hDB.DBCtx.ContractAction on ah.ContractActionID equals ca.ContractActionID
                     where ca.ContractID == ctr.ContractID && ah.DateCompleted >= dtpFrom.Value && ah.DateCompleted <= dtpTo.Value
                     orderby ah.DateCompleted
                     select ah;

         ReloadGridData(query);

         allActionsLoaded = false;
      }

      private void ReloadGridData(IEnumerable<ActionHistory> query)
      {
         List<ActionHistory> historyEntriesList = null;
         PerformHeavyTaskForm.Run(
            "Preparing history entries list ...",
            "History entries list preparation in progress",
            () => historyEntriesList = query.ToList<ActionHistory>());

         actionsHistoryGridMan.Refresh(historyEntriesList);
      }

      private void btShowRange_Click(object sender, EventArgs e)
      {
         LoadActionsFromRange();
      }

      private void btShowAll_Click(object sender, EventArgs e)
      {
         LoadAllActions();
      }

      private void btClose_Click(object sender, EventArgs e)
      {
         Close();
      }

      private void btShowInvoice_Click(object sender, EventArgs e)
      {
         var invoiceHistory = new FetchFromDb(hDB.DBCtx).ContractStoredInvoice(ctr, dtpInvoice.Value);
         if (invoiceHistory == null)
         {
            UserInteraction.ShowInfo("Invoice '{0}/{1}' for contract '{2}' wasn't found", 
               dtpInvoice.Value.Year, 
               dtpInvoice.Value.Month, 
               ctr.Name);
            
            return;
         }

         var reportFilename = "Contract single invoice";
         if(PdfFilesHelper.IsReportOpened(reportFilename))
         {
            UserInteraction.ShowInfo(
               "Historical invoice PDF file ({0}) is already opened.\nIt must be closed before showing", 
               reportFilename);
            return;
         }

         var invoice = new InvoiceHistoryEntryDeserializer(hDB.DBCtx).Deserialize(invoiceHistory);
         var reportFilePath = PdfFilesHelper.PrepareReportFilePath(reportFilename);
         using (var report = new PDFInvoiceReport(reportFilePath))
         {
            report.AddInvoice(invoice);
         }
         PdfFilesHelper.Show(reportFilePath);
      }

      private void dgvActionsHistory_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
      {
         var historyEntry = actionsHistoryGridMan.CurrentValue;
         if (historyEntry != null)
         {
            var updateHistoryEntryForm = new UpdateActionHisotryEntryForm(historyEntry);
            updateHistoryEntryForm.DBChanged += (cc) =>
            {
               var posBeforeReload = actionsHistoryGridMan.FirstDisplayedScrollingRowIndex;

               hDB.NeedReload = true;
               if (allActionsLoaded)
                  LoadAllActions();
               else
                  LoadActionsFromRange();

               // - + to avoid multiple invocations
               this.FormClosed -= RunDBChangedOnClose;
               this.FormClosed += RunDBChangedOnClose;

               actionsHistoryGridMan.SelectFirst(
                  ah => ah.ActionHistoryID == historyEntry.ActionHistoryID);
               actionsHistoryGridMan.FirstDisplayedScrollingRowIndex = posBeforeReload;
            };
            updateHistoryEntryForm.ShowDialog();
         }
      }

      private void RunDBChangedOnClose(object sender, FormClosedEventArgs e)
      {
         DBChanged(null);
      }

      private void btReport_Click(object sender, EventArgs e)
      {
         string headerText;
         if(allActionsLoaded)
         {
            headerText = string.Format("'{0}' actions history", ctr.Name);
         }
         else
         {
            headerText = string.Format("'{2}' actions history range ({0} - {1})", 
               DateUtils.ToString(dtpFrom.Value), DateUtils.ToString(dtpTo.Value), ctr.Name);
         }
         
         var filePath = PdfFilesHelper.PrepareReportFilePath("Contract history");

         if (PdfFilesHelper.IsReportOpened("Contract history"))
         {
            UserInteraction.ShowInfo("Report file for client history is already opened.\nYou must close it before regenerating it one more time.");
            return;
         }

         PerformHeavyTaskForm.Run(
            "Preparing history entries report ...",
            "History entries PDF report preparation in progress",
            () =>
            {
               var ctrItems = new FetchFromDb(hDB.DBCtx).AllContractItems(ctr).ToList();
               var iuid2cuid = new Func<string, string>(
                  iuid => ctrItems.First(i => i.ItemNumber == iuid).ClientItemRef);

               using (var report = new PDFContractActionsHistoryReport(
                  actionsHistoryGridMan.Content,
                  iuid2cuid, 
                  filePath, 
                  headerText))
               {
                  report.Print();
               }
               PdfFilesHelper.Show(filePath);
            });
      }
   }
}
