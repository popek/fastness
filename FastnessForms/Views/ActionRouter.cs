﻿using Fastness.Common;
using Fastness.DataModel;
using Fastness.DataModel.Transaction;
using Fastness.DataModel.Utils;
using Fastness.Common.Events;
using System;
using System.Windows.Forms;

namespace FastnessForms.Views
{
   public delegate void FastnessActionDone();

   public class ActionRouter
   {
      ActionContextFactory actionCtxFactory;
      ActionFormContext formCtx;

      public event DbContentChangedEventHandler ActionDone;

      public ActionRouter(ActionContextFactory actionCtxFactory, ActionFormContext formCtx)
      {
         this.actionCtxFactory = actionCtxFactory;
         this.formCtx = formCtx;
      }

      public void Invoke(ContractAction ctrAction)
      {
         var actionCtx = actionCtxFactory.Create(ctrAction);

         var actionForm = CreateProperForm(actionCtx);
         actionForm.ShowDialog();
      }

      // returns true if transactin had been executed succesfully
      private bool AttachDbCtxAndRun(string actionAlias, FastnessActionContext transactionContext, Action callback)
      {
         return UserInteraction.RunAndNotifyIfTransactionError(dbCtx =>
         {
            transactionContext.AttachDbContextAndRefresh(dbCtx);
            callback();
         }, 
         actionAlias);
      }

      private Form CreateProperForm(ActionContext actionCtx)
      {
         var successTag = new Func<FastnessActionContext, string>( transactionCtx =>
        {
           return string.Format("with item '{0}'", transactionCtx.Item.ItemNumber);

        });

         var actionCode = actionCtx.CtrAction.Code;

         if (ActionUtils.IsConsignment(actionCode))
         {
            return new ActionConsignForm(
               actionCtx,
               formCtx,
               (transactionCtx) => 
               {
                  if(AttachDbCtxAndRun(
                     "Consignment", transactionCtx, () => new ConsignItem(transactionCtx).Run()))
                  {
                     ActionDone(successTag(transactionCtx));
                  }
               });
         }
         else if (ActionUtils.IsReconsignment(actionCode))
         {
            return new ActionReconsignForm(
               actionCtx,
               formCtx,
               (transactionCtx, location) =>
               {
                  if(AttachDbCtxAndRun(
                     "Reconsignment", transactionCtx, () => new ReconsignItem(transactionCtx, location).Run()))
                  {
                     ActionDone(successTag(transactionCtx));
                  }
               });
         }
         else if (ActionUtils.IsAnyKindOfRetrieval(actionCode))
         {
            return new ActionRetrieveForm(
               actionCtx,
               formCtx,
               (transactionCtx, notes) =>
               {
                  if(AttachDbCtxAndRun(
                     "Retrieval", transactionCtx, () => new RetrieveItem(transactionCtx, notes).Run()))
                  {
                     ActionDone(successTag(transactionCtx));
                  }
               });
         }
         else if (ActionUtils.IsShreading(actionCode))
         {
            return new ActionDestroyForm(
               actionCtx,
               formCtx,
               (transactionCtx, notes) =>
               {
                  if(AttachDbCtxAndRun(
                     "Shreading", transactionCtx, () => new DestroyItem(transactionCtx, notes).Run()))
                  {
                     ActionDone(successTag(transactionCtx));
                  }
               });
         }
         else if (ActionUtils.IsGenericWithoutItem(actionCode))
         {
            return new ActionGenericNoItemForm(
               actionCtx,
               formCtx,
               (transactionCtx, notes) =>
               {
                  if(AttachDbCtxAndRun(
                     actionCode, transactionCtx, () => new GenericActionWithoutItem(transactionCtx, notes).Run()))
                  {
                     ActionDone(null);
                  }
               });
         }
         else
         {
            return new ActionGenericForm(
               actionCtx,
               formCtx,
               (transactionCtx, notes) =>
               {
                  if(AttachDbCtxAndRun(
                     actionCode, transactionCtx, () => new GenericActionOnItem(transactionCtx, notes).Run()))
                  {
                     ActionDone(successTag(transactionCtx));
                  }
               });
         }
      }
   }
}
