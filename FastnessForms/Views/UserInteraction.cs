﻿using System;
using System.Windows.Forms;
using Fastness.DataModel;
using Fastness.DataModel.Exceptions;
using Fastness.DataModel.Utils;

namespace FastnessForms.Views
{
   public static class UserInteraction
   {
      public static void ShowError(string format, params object[] args)
      {
         MessageBox.Show(string.Format(format, args), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }

      public static void ShowInfo(string format, params object[] args)
      {
         MessageBox.Show(string.Format(format, args), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
      }

      public static bool CanContinue(string format, params object[] args)
      {
         return MessageBox.Show(string.Format(format, args), Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes;
      }

      public static bool AskUser(string format, params object[] args)
      {
         return MessageBox.Show(string.Format(format, args), Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes;
      }

      public static bool RunAndNotifyIfTransactionError(Action<fastnessEntities> transaction, 
                                                        string transactionAlias)
      {
         try
         {
            ModifyDb.RunTransaction(transaction);
            return true;
         }
         catch(DataConstraintBrokenException e)
         {
            ShowError(
@"{0} failed with error:

{1}

Error occured most probably because multiple users tried to do clashing actions
(e.g. both  users made attempt to consign the same items).
In case of any doubts please contact fastness application provider (Marcin ;))", 
               transactionAlias, e.Message);

            return false;
         }
      }
   }
}
