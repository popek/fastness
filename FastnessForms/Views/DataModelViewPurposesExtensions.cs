﻿using Fastness.Common;
using Fastness.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastnessForms.Views
{
   public static class DataModelViewPurposesExtensions
   {
      public static string ShortInfo(this ItemDetail item, Contract itemOwner)
      {
         var sb = new StringBuilder();

         // contract id and iuid must have 3 spaces less. Why? don't know
         sb.AppendFormat("Contract id:       {0}\n", item.ContractID);
         sb.AppendFormat("Client name:       {0}\n", itemOwner.Name);
         
         sb.AppendFormat("IUID:              {0}\n", item.ItemNumber);
         
         if (!string.IsNullOrEmpty(item.Location))
         {
            sb.AppendFormat("Location:          {0}\n", item.Location);
         }
         else { sb.Append("\n"); }
         
         if(!string.IsNullOrEmpty(item.ClientItemRef))
         {
            sb.AppendFormat("CUID:              {0}\n", item.ClientItemRef);
         }
         else { sb.AppendLine(""); }
         
         if(!string.IsNullOrEmpty(item.Description))
         {
            sb.AppendFormat("Description:       {0}\n", item.Description);
         }
         else { sb.Append("\n"); }

            sb.AppendFormat("Date consigned:    {0}\n", StringUtils.Date(item.DateRecieved));
         
         if(item.DateDestroyed != null)
         {
            sb.AppendFormat("Date destroyed:    {0}\n", StringUtils.Date(item.DateDestroyed));
         }
         else { sb.Append("\n"); } 
         
         if (item.DateReturned != null)
         {
            sb.AppendFormat("Date re-consigned: {0}\n", StringUtils.Date(item.DateReturned));
         }
         else { sb.Append("\n"); }

         if(item.DateRetrieved != null)
         {
            sb.AppendFormat("Date retrieved:    {0}\n", StringUtils.Date(item.DateRetrieved));
         }
         else { sb.Append("\n"); }

         return sb.ToString();
      }

      public static string ShortInfo(this Contract ctr)
      {
         var sb = new StringBuilder();
         sb.AppendFormat("Amount of boxes last month : {0}\n", ctr.ConLastMonth);
         sb.AppendFormat("Consigned  this  month     : {0}\n", ctr.ConThisMonth);
         sb.AppendFormat("Retrieved  this  month     : {0}\n", ctr.ReturnedThisMonth);
         sb.AppendFormat("Re-consigned this month    : {0}\n", ctr.ReConThisMonth);
         sb.AppendFormat("Destroyed  this  month     : {0}\n", ctr.DestroyedThisMonth);
         sb.AppendFormat("Total                      : {0}", ctr.Total);
         return sb.ToString();
      }
   }
}
