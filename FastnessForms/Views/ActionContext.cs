﻿using Fastness.DataModel;
using System;

namespace FastnessForms.Views
{
   public class ActionContext
   {
      public Contract Ctr {get; private set;}
      public ContractAction CtrAction { get; private set; }

      public string FormCaption 
      {
         get 
         {
            return string.Format("{0} ({1})", CtrAction.Description, Ctr.Name);
         }
      }

      public ActionContext(Contract ctr, ContractAction ctrAction)
      {
         Ctr = ctr;
         CtrAction = ctrAction;

         if (ctr.ContractID != ctrAction.ContractID)
         {
            throw new Exception(string.Format("Inconsistent data! action and contract MUST have the same ContractID. ctr:({0}) !=  ctrAction:({1})", Ctr.ContractID, CtrAction.ContractID));
         }
      }
   }

   //
   public class ActionContextFactory
   {
      private Contract currentCtr;
      public Contract CurrentContract { get { return currentCtr; } set { currentCtr = value; } }

      public ActionContext Create(ContractAction ctrAction)
      {
         return new ActionContext(currentCtr, ctrAction);
      }
   }
}
