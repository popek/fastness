﻿namespace FastnessForms.Views
{
   partial class ConfirmPasswordBeforeFragileActionForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfirmPasswordBeforeFragileActionForm));
         this.txtPassword = new System.Windows.Forms.MaskedTextBox();
         this.label2 = new System.Windows.Forms.Label();
         this.btContinue = new System.Windows.Forms.Button();
         this.btCancel = new System.Windows.Forms.Button();
         this.SuspendLayout();
         // 
         // txtPassword
         // 
         this.txtPassword.Location = new System.Drawing.Point(74, 12);
         this.txtPassword.Name = "txtPassword";
         this.txtPassword.PasswordChar = '*';
         this.txtPassword.Size = new System.Drawing.Size(311, 20);
         this.txtPassword.TabIndex = 1;
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(12, 15);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(56, 13);
         this.label2.TabIndex = 3;
         this.label2.Text = "Password:";
         // 
         // btContinue
         // 
         this.btContinue.Location = new System.Drawing.Point(229, 38);
         this.btContinue.Name = "btContinue";
         this.btContinue.Size = new System.Drawing.Size(75, 23);
         this.btContinue.TabIndex = 3;
         this.btContinue.Text = "Continue";
         this.btContinue.UseVisualStyleBackColor = true;
         this.btContinue.Click += new System.EventHandler(this.btContinue_Click);
         // 
         // btCancel
         // 
         this.btCancel.Location = new System.Drawing.Point(310, 38);
         this.btCancel.Name = "btCancel";
         this.btCancel.Size = new System.Drawing.Size(75, 23);
         this.btCancel.TabIndex = 4;
         this.btCancel.Text = "Cancel";
         this.btCancel.UseVisualStyleBackColor = true;
         this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
         // 
         // ConfirmPasswordBeforeFragileActionForm
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(397, 69);
         this.Controls.Add(this.btCancel);
         this.Controls.Add(this.btContinue);
         this.Controls.Add(this.label2);
         this.Controls.Add(this.txtPassword);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
         this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
         this.MaximizeBox = false;
         this.MinimizeBox = false;
         this.Name = "ConfirmPasswordBeforeFragileActionForm";
         this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.UserLoginForm_FormClosed);
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.MaskedTextBox txtPassword;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Button btContinue;
      private System.Windows.Forms.Button btCancel;
   }
}