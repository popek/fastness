﻿using Fastness.DataModel;
using Fastness.DataModel.Transaction;
using Fastness.DataModel.Utils;
using System;
using System.Windows.Forms;

namespace FastnessForms.Views
{
   public partial class ActionGenericNoItemForm : Form
   {
      ActionContext actionCtx;
      ActionFormContext formCtx;
      Action<FastnessActionContext, string> actionSaver;

      public ActionGenericNoItemForm(ActionContext actionCtx, ActionFormContext formCtx, Action<FastnessActionContext, string> actionSaver)
      {
         this.actionCtx = actionCtx;
         this.formCtx = formCtx;

         this.actionSaver = actionSaver;

         InitializeComponent();

         this.Text = this.actionCtx.FormCaption;

         this.lbActionTypeInfo.Text = actionCtx.CtrAction.Description;
         this.lbContractIDInfo.Text = actionCtx.Ctr.AccountRef;
         this.lbContractNameInfo.Text = actionCtx.Ctr.Name;

         this.txtCharge.AddKeyPressWithNumericValue();

         this.txtNotes.AddKeyPressWithTextLengthLimit(DBFieldsConstraint.ACTION_HISTORY_NOTES_MAX_LENGHT);
         this.txtAuthoriziedBy.AddKeyPressWithTextLengthLimit(DBFieldsConstraint.ACTION_HISTORY_AUTHORIZED_BY_MAX_LENGHT);

         this.txtCharge.Text = actionCtx.CtrAction.Charge.ToString();
         this.dtpGenericNoItem.Value = formCtx.LastActionDate;
         this.txtAuthoriziedBy.Text = formCtx.LastActionAuthorizer;
         this.txtCharge.Enabled = false;
      }

      private bool FormDataValid()
      {
         if (!this.txtCharge.IsValidNumericValueFormat("Charge"))
         {
            return false;
         }

         if (!this.dtpGenericNoItem.IsActionDateConstraintsValid(formCtx.LastActionDate, actionCtx.CtrAction.Description))
         {
            return false;
         }

         if (this.txtNotes.IsTextToLong(DBFieldsConstraint.ACTION_HISTORY_NOTES_MAX_LENGHT, "notes"))
         {
            return false;
         }
         if (this.txtAuthoriziedBy.IsTextToLong(DBFieldsConstraint.ACTION_HISTORY_AUTHORIZED_BY_MAX_LENGHT, "Authorization name"))
         {
            return false;
         }

         return true;
      }

      private void btGenericNoItem_Click(object sender, EventArgs e)
      {
         if (!FormDataValid())
         {
            return;
         }

         actionSaver(
            new FastnessActionContext(
               actionCtx.Ctr,
               actionCtx.CtrAction,
               dtpGenericNoItem.Value,
               Double.Parse(txtCharge.Text),
               txtAuthoriziedBy.Text),
            txtNotes.Text);

         formCtx.Update(txtAuthoriziedBy.Text, dtpGenericNoItem.Value);

         Close();
      }

      private void btClose_Click(object sender, EventArgs e)
      {
         Close();
      }

      private void btAllowChargeChange_Click(object sender, EventArgs e)
      {
         this.txtCharge.Enabled = true;
      }
   }
}
