﻿using Fastness.DataModel;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using log4net;
using FastnessForms.FuncTests;
using System.Threading;
using Fastness.Common;
using Fastness.DataModel.Utils;
using Fastness.DataModel.Transaction;
using FastnessReports;
using Fastness.BusinessOperations;
using System.Collections.Generic;
using FastnessForms.ThirdParty;
using System.Text.RegularExpressions;

namespace FastnessForms.Views
{
   public partial class MainScreen : Form
   {
      static readonly ILog log = LogManager.GetLogger(typeof(MainScreen));

      private DBHandle hDB = new DBHandle();

      ActionContextFactory actionCtxFactory = new ActionContextFactory();
      ActionFormContext formCtx = new ActionFormContext();

      DataGridViewManager<Contract> ctrsGridMan;
      DataGridViewManager<ItemDetail> itemsGridMan;

      DataGridViewManager<ContractAction> ctrActionsGridMan;

      XmlObjectsRepository repository = new XmlObjectsRepository(CurrentDirUtils.GetSubdirPath("auxiliary_configs"));

      public MainScreen()
      {
         InitializeComponent();

         UpdateGUIBasedOnUserRole();

         this.FormClosed += FormClosedHandler;
      }

      private void UpdateGUIBasedOnUserRole()
      {
         if (!LoggedUserUtils.CanEditDBContent())
         {
            btSetVat.Enabled = false;
            btSetInvoiceMailSettings.Enabled = false;
            doActionButton.Enabled = false;
            addNewContractButton.Enabled = false;
         }
         btMailLMI.Enabled = LoggedUserUtils.IsAdmin();
         btGenerateLMI.Enabled = LoggedUserUtils.IsAdmin();
      }

      private void MainScreen_Load(object sender, EventArgs e)
      {
         /*
            0 ContractID
            1 AccountRef
            2 Name
            3 ClientInvoiceRef
            4 ServiceLevel
            5 ConLastMonth
            6 ConThisMonth
            7 ReturnedThisMonth
            8 ReConThisMonth
            9 DestroyedThisMonth
            10 Total
            11 FixedFee
            12 FixedUnits
            13 FixedUnitsToDate
            14 ContractFinished
            15 Description 
            16 Address 
            17 AuxiliaryAddress 
          */
         ctrsGridMan = new DataGridViewManager<Contract>(contractsGrid, new int[] {13, 14});
         ctrsGridMan.ColumnName2Caption = new Dictionary<string, string> 
         {
            {"ConLastMonth", "Amount of boxes last month"},
            {"ReturnedThisMonth", "Retrieved this month"},
            {"Address", "Client address"},
            {"AuxiliaryAddress", "Invoice address (if different)"}
         };
         /*
           0 ItemID
           1 ItemNumber 
           2 ContractID 
           3 ClientItemRef 
           4 Description 
           5 DestructionDate 
           6 Type 
           7 Percentage 
           8 DateRecieved 
           9 DateDestroyed 
           10 DateRetrieved 
           11 DateReturned 
           12 Destroyed 
           13 Location 
           14 InStock 
           15 InStockEOM 
          */
         itemsGridMan = new DataGridViewManager<ItemDetail>(itemsGrid, new int[] { 0, 2, 6, 7, 12, 14 });

         itemsGridMan.SortableOnColumnHeaderClick = true;
         itemsGridMan.ColumnName2DisplayIndex = new Dictionary<string, int> { {"Location", 4} };
         itemsGridMan.ColumnName2Caption = new Dictionary<string, string> 
         { 
            {"ItemNumber", "IUID"},
            {"ClientItemRef", "CUID"},
            {"DateRecieved", "Date consigned"},
            {"DateReturned", "Date re-consigned"}
         };
         itemsGridMan.AllowCopyCellContentContextMenu = true;

         /*
           0 ContractActionID
           1 Description
           2 Code
           3 Charge
           4 PerFreeAction
           5 ContractID
          */
         ctrActionsGridMan = new DataGridViewManager<ContractAction>(
            contractActionsGrid, 
            new int[] { 0, 5});
         ctrActionsGridMan.ColumnName2Caption = new Dictionary<string, string> 
         {
            {"Description", "Action"}
         };

         ReloadContracts();

         UpdateCurrentContract();

         try
         {
            new SessionStateLoader().WithContractsGrid(this.contractsGrid);
         }
         catch (Exception exc)
         {
            log.WarnFormat("exception during session load: {0}", exc.Message);
         }

         txtSearchIuid.AddKeyPressWithTextLengthLimitAndIntegerValue(DBFieldsConstraint.ITEM_NUMBER_LENGTH);
         txtSearchLocation.AddKeyPressWithTextLengthLimitAndIntegerValue(DBFieldsConstraint.LOCATION_NUMBER_LENGTH);
      }

      private void ReloadContracts()
      {
         var ctrs = (from ctr in hDB.DBCtx.Contract select ctr).ToList<Contract>();
         ctrs.Sort((x, y) => string.Compare(x.AccountRef, y.AccountRef));

         ctrsGridMan.Refresh(ctrs);
      }
      
      private void ReloadContractActions(int ctrId)
      {
         var ctrActions = (from ca in hDB.DBCtx.ContractAction
                           where ca.ContractID == ctrId
                           select ca).ToList<ContractAction>();
         ctrActionsGridMan.Refresh(ctrActions);
      }

      private void ReloadItemsGrid(int contractId)
      {
         var ctr = hDB.DBCtx.Contract.Where(c => c.ContractID == contractId).FirstOrDefault();

         var contractItems = (from item in hDB.DBCtx.ItemDetail
                              where item.ContractID == contractId
                              select item).ToList<ItemDetail>();
         itemsGridMan.Refresh(contractItems, ctr.Name);
      }

      private void FormClosedHandler(object sender, FormClosedEventArgs e)
      {
         var sessionSaver = new SessionStateSaver();

         sessionSaver.WithContractsGrid(this.contractsGrid);
         sessionSaver.Save();
      }

      private void EditContract(Contract ctr, Action<Contract> ctrUpdater)
      {
         bool contractChanged = false;
         new ContractDetailsForm(
            ctr,
            (Contract c) =>
            {
               ctrUpdater(c);
               contractChanged = true;
            })
            .ShowDialog();

         if (contractChanged)
         {
            ReloadContracts();
         }
      }

      private void SwitchContractViewContext(DataGridView target, DataGridView source, Button targetB, Button sourceB)
      {
         targetB.Enabled = false;
         sourceB.Enabled = true;

         source.Enabled = false;
         source.Visible = false;

         target.Location = source.Location;
         target.Width = source.Width;
         target.Height = source.Height;

         target.Visible = true;
         target.Enabled = true;
      }

      private void SwitchGridToContracts()
      {
         SwitchContractViewContext(this.contractsGrid, this.contractActionsGrid, this.showContractsButton, this.showContractActionsButton);

         ReloadContracts();
         ctrsGridMan.SelectFirst(ctr => actionCtxFactory.CurrentContract.ContractID == ctr.ContractID);

         this.addNewContractButton.Enabled = true;
         this.showContractHistoryButton.Enabled = true;
         this.doActionButton.Enabled = false;

         UpdateGUIBasedOnUserRole();
      }
      private void SwitchGridToContractActions()
      {
         var currentCtr = ctrsGridMan.CurrentValue;

         actionCtxFactory.CurrentContract = currentCtr;

         SwitchContractViewContext(this.contractActionsGrid, this.contractsGrid, this.showContractActionsButton, this.showContractsButton);

         ReloadContractActions(currentCtr.ContractID);

         this.addNewContractButton.Enabled = false;
         this.showContractHistoryButton.Enabled = false;
         this.doActionButton.Enabled = true;

         UpdateGUIBasedOnUserRole();
      }
      private void HandleDoAction()
      {
         var router = new ActionRouter(actionCtxFactory, formCtx);
         var ctrAction = ctrActionsGridMan.CurrentValue;

         router.ActionDone += (cc) => 
         {
            if(cc != null)
            {
               UserInteraction.ShowInfo("{0} ({1}) finished succesfully", ctrAction.Description, cc.ToString());
            }
            else
            {
               UserInteraction.ShowInfo("{0} finished succesfully", ctrAction.Description);
            }

            hDB.NeedReload = true;
            ReloadItemsGrid(ctrAction.ContractID); 
         };

         router.Invoke(ctrAction);
      }
      //TODO: test data load is obsolete and can be removed
      private void setupTestData_Click(object sender, EventArgs e)
      {
         var tdi = new TestDataInfo();

         new TestInvoiceDataSetupForm(tdi).ShowDialog();

         var t = new Thread(() => 
         {
            new InvoiceTestDataReloader(tdi, DateUtils.LastMonthDate).Reload();
         });

         t.Start();
         t.Join();

         hDB.NeedReload = true;

         ReloadContracts();
         var currentCtr = ctrsGridMan.CurrentValue;
         if (itemsGrid.Enabled)
         {
            ReloadItemsGrid(ctrsGridMan.CurrentValue.ContractID);
         }
         else
         {
            ReloadContractActions(ctrsGridMan.CurrentValue.ContractID);
         }
      }

      private void UpdateCurrentContract()
      {
         var currentCtr = ctrsGridMan.CurrentValue;
         if (currentCtr != null)
         {
            ReloadItemsGrid(currentCtr.ContractID);
            lbContractAuxiliaryInfo.Text = currentCtr.ShortInfo();
            Text = string.Format("Fastness [{0}]", currentCtr.Name);
            
            // on contract change reset action form context
            formCtx.Reset();
         }
      }

      private void contractsView_SelectionChanged(object sender, EventArgs e)
      {
         UpdateCurrentContract();
      }

      private void contractsView_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
      {
         if (e.ColumnIndex < 0)
         {
            return;
         }

         EditContract(
            ctrsGridMan.CurrentValue,
            (Contract ctr) =>
            {
               ModifyDb.RunTransaction(dbCtx => new UpdateContract(ctr, dbCtx).Run());
               hDB.NeedReload = true;
            });
      }

      private void addNewContractButton_Click(object sender, EventArgs e)
      {
         bool alreadyAdded = false;
         
         Action<Contract> updater = ctr => 
         {
            ModifyDb.RunTransaction(
               dbCtx =>
               {
                  if (alreadyAdded)
                  {
                     new UpdateContract(ctr, dbCtx).Run();
                  }
                  else
                  {
                     new AddContract(ctr, dbCtx).Run();
                     alreadyAdded = true;
                  }
               });
            hDB.NeedReload = true;
         };

         var newCtr = new Contract { ServiceLevel = FastnesConsts.SERVICE_LEVEL_STANDARD};

         EditContract(newCtr, updater);
      }

      private void showContractsButton_Click(object sender, EventArgs e)
      {
         SwitchGridToContracts();
      }

      private void showContractActionsButton_Click(object sender, EventArgs e)
      {
         SwitchGridToContractActions();
      }

      private void doActionButton_Click(object sender, EventArgs e)
      {
         HandleDoAction();
      }

      private void itemsGrid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
      {
         if (!itemsGridMan.InRange(e.RowIndex))
         {
            return;
         }

         var item = itemsGridMan.GetValue(e.RowIndex);
         if (!(item.InStock ?? true))
         {
            e.CellStyle.BackColor = Color.LightGray;
         }
         if (item.Destroyed ?? false)
         {
            e.CellStyle.BackColor = Color.Gray;
         }
      }

      private void showContractHistoryButton_Click(object sender, EventArgs e)
      {
         var currentCtr = ctrsGridMan.CurrentValue;
         var actionsHistoryForm = new ContractActionsHistoryForm(currentCtr);
         actionsHistoryForm.DBChanged += (cc) => hDB.NeedReload = true;

         actionsHistoryForm.ShowDialog();
      }

      private void contractActoinsGrid_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
      {
         if (e.ColumnIndex < 0)
         {
            return;
         }

         var ctrAction = ctrActionsGridMan.CurrentValue;
         var editActionForm = new ClientActionDetailsForm(ctrAction, () => 
            {
               hDB.NeedReload = true;
               ReloadContractActions(ctrAction.ContractID);
            });
         editActionForm.ShowDialog();
      }

      private void itemsGrid_SelectionChanged(object sender, EventArgs e)
      {
         var currentItem = itemsGridMan.CurrentValue;
         if(currentItem == null)
         {
            lbItemAuxiliaryInfo.Text = "";
            return;
         }
         var ctr = hDB.DBCtx.Contract.Where(c => c.ContractID == currentItem.ContractID).FirstOrDefault();
         lbItemAuxiliaryInfo.Text = currentItem.ShortInfo(ctr);
      }

      private void btSearchDescription_Click(object sender, EventArgs e)
      {
         SearchItemsGrid(txtSearchDescription, "description", i => i.Description);
      }
      private void btSearchCuid_Click(object sender, EventArgs e)
      {
         SearchItemsGrid(txtSearchCuid, "CUID", i => i.ClientItemRef);
      }
      private void btSearchIuid_Click(object sender, EventArgs e)
      {
         SearchItemsGrid(txtSearchIuid, "IUID", i => i.ItemNumber);
      }
      private void btSearchLocation_Click(object sender, EventArgs e)
      {
         SearchItemsGrid(txtSearchLocation, "location", i => i.Location);
      }
      private void SearchItemsGrid(TextBox filteredFieldText, string fieldTag, Func<ItemDetail, string> getItemString)
      {
         bool itemFound = false;
         var textToSearch = filteredFieldText.Text.Trim();
         if (optJumpSearch.Checked)
         {
            itemFound = JumpToNextItem(textToSearch, getItemString);
         }
         else
         {
            itemFound = FilterItemsGrid(textToSearch, getItemString);
         }
         if(!itemFound)
         {
            if (optJumpSearch.Checked)
            {
               UserInteraction.ShowInfo("There is no next item with '{0}' {1}", filteredFieldText.Text, fieldTag);
            }
            else
            {
               UserInteraction.ShowInfo("There is no item with '{0}' {1}", filteredFieldText.Text, fieldTag);
            }
         }
      }
      private bool JumpToNextItem(string textToSearch, Func<ItemDetail, string> getItemString)
      {
         return itemsGridMan.SelectNext(i =>
         {
            return ItemTextMatches(i, textToSearch, getItemString);
         });
      }
      private bool FilterItemsGrid(string textToSearch, Func<ItemDetail, string> getItemString)
      {
         if (textToSearch.Length == 0)
         {
            itemsGridMan.LoadWholeContent();
            return true;
         }
         return itemsGridMan.FilterOut(i =>
         {
            return ItemTextMatches(i, textToSearch, getItemString);
         });
      }
      private bool ItemTextMatches(ItemDetail item, 
                                   string textToSearch, 
                                   Func<ItemDetail, string> getItemString)
      {
         var itemFieldText = getItemString(item);
         if (string.IsNullOrEmpty(itemFieldText))
            return false;

         return Regex.IsMatch(itemFieldText, textToSearch, RegexOptions.IgnoreCase);
      }

      private void tbShowAllBoxes_Click(object sender, EventArgs e)
      {
         List<ItemDetail> itemsList = null;

         PerformHeavyTaskForm.Run(
            "Preparing all boxes list ...",
            "All boxes list preparation in progress",
            () => itemsList = hDB.DBCtx.ItemDetail.ToList<ItemDetail>());

         itemsGridMan.Refresh(itemsList, "All");
      }

      private void btStockListReport_Click(object sender, EventArgs e)
      {
         string filePath = PdfFilesHelper.PrepareReportFilePath("inStockItems");
         if (PdfFilesHelper.IsReportOpened(filePath))
         {
            UserInteraction.ShowInfo(
               "Items in stock PDF file ({0}) is already opened.\nIt must be closed before showing", 
               filePath);
            return;
         }

         PerformHeavyTaskForm.Run(
            "Preparing items list ...",
            "Items list PDF report preparation in progress",
            () => 
            {
               var inStockItems = itemsGridMan.Content.Where(i => i.InStock ?? true).ToList<ItemDetail>();
               using (var inStockItemsReport = new PDFItemsReport(filePath, inStockItems, string.Format("{0} Items in stock", itemsGridMan.LastRefreshContext)))
               {
                  inStockItemsReport.Print();
               }
               PdfFilesHelper.Show(filePath);
            });
      }

      private void itemsGrid_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
      {
         if(e.ColumnIndex < 0)
         {
            return;
         }

         var item = itemsGridMan.CurrentValue;
         var editItemDetailsForm = new ItemDetailsForm(item);
         editItemDetailsForm.DBChanged += (cc) =>
         {
            hDB.NeedReload = true;
            ReloadItemsGrid(item.ContractID);
         };

         editItemDetailsForm.ShowDialog();
      }

      private void btSetVat_Click(object sender, EventArgs e)
      {
         var form = new SetupVATForm();
         form.ShowDialog();
      }

      private void btGenerateLMI_Click(object sender, EventArgs e)
      {
         bool generateLMIAllowed = false;
         new ConfirmPasswordBeforeFragileActionForm(
            "Confirm your password to continue invoices generation",
            passwordOk => generateLMIAllowed = passwordOk).ShowDialog();

         if (!generateLMIAllowed)
         {
            return;
         }
         if (new AskDb(hDB.DBCtx).AreInvoicesAlreadyGenerated(DateUtils.LastMonthDate.Year, DateUtils.LastMonthDate.Month))
         {
            UserInteraction.ShowInfo("Invoices for previous month had been already generated. There is no possibility to generate them second time");
            return;
         }
         if (!UserInteraction.AskUser("Are you sure you want to generate invoices for the last month?\n(once they are generated, they can't be regenerated)"))
         {
            return;
         }

         var ctrsByAccountRef = new FetchFromDb(hDB.DBCtx).AvailableContracts().ToList();
         ctrsByAccountRef.Sort((x, y) => string.Compare(x.AccountRef, y.AccountRef));

         var invoicesNotesForm = new InvoicesNotesListForm(ctrsByAccountRef);
         invoicesNotesForm.ShowDialog();

         var operation = new GenerateLastMonthInvoices(
            contracId => invoicesNotesForm.AllNotes.Find(notes => notes.ContractID == contracId).Notes, 
            ctrsByAccountRef);
         operation.DBChanged += (cc) => hDB.NeedReload = true;

         var progressForm = new GenerateLMIProgressForm(operation);
         progressForm.ShowDialog();

      }

      private void btPrintLMI_Click(object sender, EventArgs e)
      {
         if (!new AskDb(hDB.DBCtx).AreInvoicesAlreadyGenerated(DateUtils.LastMonthDate.Year, DateUtils.LastMonthDate.Month))
         {
            UserInteraction.ShowInfo("Invoices for previous month are no yet generated.");
            return;
         }

         if (PdfFilesHelper.IsReportOpened(PrintLMIProgressForm.LmiInvoicesReportAlias))
         {
            UserInteraction.ShowError("Last month invoices report already opened.\nUse it for further operations.");
            return;
         }

         new PrintLMIProgressForm().ShowDialog();
      }

      private void btSaveStocks_Click(object sender, EventArgs e)
      {
         new SaveStockListsProgressForm().ShowDialog();
      }

      private void UpdateInvoicedEmailSettings()
      {
         var settings = repository.Exists<SmtpClientSettings>() ? 
            repository.Get<SmtpClientSettings>() :new SmtpClientSettings();
         var invoiceMailTemplate = repository.Exists<MailTemplate>() ? 
            repository.Get<MailTemplate>() : Fastness.Invoicing.Utils.DefaultInvoiceMailTemplate();

         var mailSettingsForm = new InvoiceMailSettingsForm(settings, invoiceMailTemplate);
         mailSettingsForm.SmtpSettingsChanged += (updatedSettings) =>
         {
            repository.Update<SmtpClientSettings>(updatedSettings);
         };

         mailSettingsForm.MailTemplateChanged += (updatedInvoiceMailTemplate) =>
         {
            repository.Update<MailTemplate>(updatedInvoiceMailTemplate);
         };
         mailSettingsForm.ShowDialog();
      }
      private void btSetInvoiceMailSettings_Click(object sender, EventArgs e)
      {
         UpdateInvoicedEmailSettings();
      }

      private void btMailLMI_Click(object sender, EventArgs e)
      {
         bool mailLMIAllowed = false;
         new ConfirmPasswordBeforeFragileActionForm(
            "Confirm your password to continue invoices mailing",
            passwordOk => mailLMIAllowed = passwordOk).ShowDialog();

         if (!mailLMIAllowed)
         {
            return;
         }
         if (!new AskDb(hDB.DBCtx).AreInvoicesAlreadyGenerated(DateUtils.LastMonthDate.Year, DateUtils.LastMonthDate.Month))
         {
            UserInteraction.ShowInfo("Invoices for previous month are no yet generated.");
            return;
         }

         if (!repository.Exists<SmtpClientSettings>())
         {
            UpdateInvoicedEmailSettings();
         }

         var operation = new EmailLastMonthInvoices(repository.Get<SmtpClientSettings>(), repository.Get<MailTemplate>());
         operation.DBChanged += (cc) => { hDB.NeedReload = true; };

         var sendMailProgressForm = new EmailLMIProgressForm(operation);
         sendMailProgressForm.ShowDialog();
      }

      private void modifyFastnessUserButton_Click(object sender, EventArgs e)
      {
         new ManageUsersForm().ShowDialog();
      }

      private void contractsGrid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
      {
         if (!ctrsGridMan.InRange(e.RowIndex))
         {
            return;
         }

         var ctr = ctrsGridMan.GetValue(e.RowIndex);
         if (ctr.ContractFinished)
         {
            e.CellStyle.BackColor = Color.Gray;
         }
      }

      private void txtSearchDescription_Enter(object sender, EventArgs e)
      {
         txtSearchDescription.Text = "";
      }

      private void txtSearchCuid_Enter(object sender, EventArgs e)
      {
         txtSearchCuid.Text = "";
      }

      private void txtSearchIuid_Enter(object sender, EventArgs e)
      {
         txtSearchIuid.Text = "";
      }

      private void txtSearchLocation_Enter(object sender, EventArgs e)
      {
         txtSearchLocation.Text = "";
      }
   }
}
