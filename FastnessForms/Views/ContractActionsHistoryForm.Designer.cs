﻿namespace FastnessForms.Views
{
   partial class ContractActionsHistoryForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContractActionsHistoryForm));
         this.dgvActionsHistory = new System.Windows.Forms.DataGridView();
         this.groupBox1 = new System.Windows.Forms.GroupBox();
         this.btReport = new System.Windows.Forms.Button();
         this.btShowAll = new System.Windows.Forms.Button();
         this.btShowRange = new System.Windows.Forms.Button();
         this.label2 = new System.Windows.Forms.Label();
         this.label1 = new System.Windows.Forms.Label();
         this.dtpTo = new System.Windows.Forms.DateTimePicker();
         this.dtpFrom = new System.Windows.Forms.DateTimePicker();
         this.groupBox2 = new System.Windows.Forms.GroupBox();
         this.btShowInvoice = new System.Windows.Forms.Button();
         this.dtpInvoice = new System.Windows.Forms.DateTimePicker();
         this.btClose = new System.Windows.Forms.Button();
         ((System.ComponentModel.ISupportInitialize)(this.dgvActionsHistory)).BeginInit();
         this.groupBox1.SuspendLayout();
         this.groupBox2.SuspendLayout();
         this.SuspendLayout();
         // 
         // dgvActionsHistory
         // 
         this.dgvActionsHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.dgvActionsHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
         this.dgvActionsHistory.Location = new System.Drawing.Point(12, 12);
         this.dgvActionsHistory.Name = "dgvActionsHistory";
         this.dgvActionsHistory.ReadOnly = true;
         this.dgvActionsHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
         this.dgvActionsHistory.Size = new System.Drawing.Size(805, 264);
         this.dgvActionsHistory.TabIndex = 0;
         this.dgvActionsHistory.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvActionsHistory_CellContentDoubleClick);
         // 
         // groupBox1
         // 
         this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.groupBox1.Controls.Add(this.btReport);
         this.groupBox1.Controls.Add(this.btShowAll);
         this.groupBox1.Controls.Add(this.btShowRange);
         this.groupBox1.Controls.Add(this.label2);
         this.groupBox1.Controls.Add(this.label1);
         this.groupBox1.Controls.Add(this.dtpTo);
         this.groupBox1.Controls.Add(this.dtpFrom);
         this.groupBox1.Location = new System.Drawing.Point(114, 292);
         this.groupBox1.Name = "groupBox1";
         this.groupBox1.Size = new System.Drawing.Size(445, 80);
         this.groupBox1.TabIndex = 1;
         this.groupBox1.TabStop = false;
         this.groupBox1.Text = "Show actions";
         // 
         // btReport
         // 
         this.btReport.Location = new System.Drawing.Point(362, 25);
         this.btReport.Name = "btReport";
         this.btReport.Size = new System.Drawing.Size(77, 34);
         this.btReport.TabIndex = 8;
         this.btReport.Text = "Show report";
         this.btReport.UseVisualStyleBackColor = true;
         this.btReport.Click += new System.EventHandler(this.btReport_Click);
         // 
         // btShowAll
         // 
         this.btShowAll.Location = new System.Drawing.Point(268, 25);
         this.btShowAll.Name = "btShowAll";
         this.btShowAll.Size = new System.Drawing.Size(77, 34);
         this.btShowAll.TabIndex = 7;
         this.btShowAll.Text = "Show all";
         this.btShowAll.UseVisualStyleBackColor = true;
         this.btShowAll.Click += new System.EventHandler(this.btShowAll_Click);
         // 
         // btShowRange
         // 
         this.btShowRange.Location = new System.Drawing.Point(185, 25);
         this.btShowRange.Name = "btShowRange";
         this.btShowRange.Size = new System.Drawing.Size(77, 34);
         this.btShowRange.TabIndex = 6;
         this.btShowRange.Text = "Show range";
         this.btShowRange.UseVisualStyleBackColor = true;
         this.btShowRange.Click += new System.EventHandler(this.btShowRange_Click);
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(8, 51);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(19, 13);
         this.label2.TabIndex = 5;
         this.label2.Text = "to:";
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(8, 25);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(30, 13);
         this.label1.TabIndex = 4;
         this.label1.Text = "from:";
         // 
         // dtpTo
         // 
         this.dtpTo.Location = new System.Drawing.Point(44, 45);
         this.dtpTo.Name = "dtpTo";
         this.dtpTo.Size = new System.Drawing.Size(135, 20);
         this.dtpTo.TabIndex = 3;
         // 
         // dtpFrom
         // 
         this.dtpFrom.Location = new System.Drawing.Point(44, 19);
         this.dtpFrom.Name = "dtpFrom";
         this.dtpFrom.Size = new System.Drawing.Size(135, 20);
         this.dtpFrom.TabIndex = 2;
         // 
         // groupBox2
         // 
         this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.groupBox2.Controls.Add(this.btShowInvoice);
         this.groupBox2.Controls.Add(this.dtpInvoice);
         this.groupBox2.Location = new System.Drawing.Point(576, 292);
         this.groupBox2.Name = "groupBox2";
         this.groupBox2.Size = new System.Drawing.Size(241, 80);
         this.groupBox2.TabIndex = 2;
         this.groupBox2.TabStop = false;
         this.groupBox2.Text = "Show invoice";
         // 
         // btShowInvoice
         // 
         this.btShowInvoice.Location = new System.Drawing.Point(152, 25);
         this.btShowInvoice.Name = "btShowInvoice";
         this.btShowInvoice.Size = new System.Drawing.Size(77, 34);
         this.btShowInvoice.TabIndex = 6;
         this.btShowInvoice.Text = "Show";
         this.btShowInvoice.UseVisualStyleBackColor = true;
         this.btShowInvoice.Click += new System.EventHandler(this.btShowInvoice_Click);
         // 
         // dtpInvoice
         // 
         this.dtpInvoice.Location = new System.Drawing.Point(6, 30);
         this.dtpInvoice.Name = "dtpInvoice";
         this.dtpInvoice.Size = new System.Drawing.Size(135, 20);
         this.dtpInvoice.TabIndex = 2;
         // 
         // btClose
         // 
         this.btClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.btClose.Location = new System.Drawing.Point(12, 317);
         this.btClose.Name = "btClose";
         this.btClose.Size = new System.Drawing.Size(77, 34);
         this.btClose.TabIndex = 7;
         this.btClose.Text = "Close";
         this.btClose.UseVisualStyleBackColor = true;
         this.btClose.Click += new System.EventHandler(this.btClose_Click);
         // 
         // ContractActionsHistoryForm
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(829, 382);
         this.Controls.Add(this.btClose);
         this.Controls.Add(this.groupBox2);
         this.Controls.Add(this.groupBox1);
         this.Controls.Add(this.dgvActionsHistory);
         this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
         this.MinimumSize = new System.Drawing.Size(845, 420);
         this.Name = "ContractActionsHistoryForm";
         this.Text = "ClientActionsHistoryForm";
         ((System.ComponentModel.ISupportInitialize)(this.dgvActionsHistory)).EndInit();
         this.groupBox1.ResumeLayout(false);
         this.groupBox1.PerformLayout();
         this.groupBox2.ResumeLayout(false);
         this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.DataGridView dgvActionsHistory;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.Button btShowAll;
      private System.Windows.Forms.Button btShowRange;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.DateTimePicker dtpTo;
      private System.Windows.Forms.DateTimePicker dtpFrom;
      private System.Windows.Forms.GroupBox groupBox2;
      private System.Windows.Forms.Button btShowInvoice;
      private System.Windows.Forms.DateTimePicker dtpInvoice;
      private System.Windows.Forms.Button btClose;
      private System.Windows.Forms.Button btReport;
   }
}