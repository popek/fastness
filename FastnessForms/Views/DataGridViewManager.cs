﻿using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using System.Reflection;
using System;
using System.Collections;
using FastnessForms.ThirdParty;

namespace FastnessForms.Views
{
   public class DataGridViewManager<T> where T : class
   {
      protected bool isSortable;
      protected DataGridView dgv;
      protected bool[] propertyVisible;

      protected List<T> currentContent = new List<T>();

      protected List<T> wholeContent = new List<T>();

      protected PropertyInfo[] Props { get { return typeof(T).GetProperties(); } }

      private int SelectedRowIndex
      {
         get
         {
            return dgv.SelectedRows.Count > 0 ? dgv.SelectedRows[0].Index : -1;
         }
         set
         {
            dgv.Rows[value].Selected = true;
         }
      }
      private int CurrentRowIndex { get { return Math.Max(SelectedRowIndex, 0); } }

      public virtual T CurrentValue { get { return currentContent.Count > 0 ? currentContent[CurrentRowIndex] : null; } }

      public int FirstDisplayedScrollingRowIndex
      {
        get { return dgv.FirstDisplayedScrollingRowIndex;  }
        set { dgv.FirstDisplayedScrollingRowIndex = value; }
      }
      public List<T> Content { get { return currentContent; } }

      public string LastRefreshContext { get; private set; }

      public Dictionary<string, string> ColumnName2Caption  { get; set; }
      public Dictionary<string, int> ColumnName2DisplayIndex { get; set; }

      public bool SortableOnColumnHeaderClick 
      { 
         set 
         {
            isSortable = value;
            ReloadAndUpdateColumns();
         }
      }

      public DataGridViewManager(DataGridView dgv, int[] paramsIndexesToHide)
      {
         this.isSortable = false;
         this.dgv = dgv;
         propertyVisible = Enumerable.Repeat<bool>(true, Props.Length).ToArray();
         foreach (var paramToHideInde in paramsIndexesToHide)
         {
            propertyVisible[paramToHideInde] = false;
         }
      }
      public void Refresh(List<T> objects, string context)
      {
         LastRefreshContext = context;
         Refresh(objects);
      }
      public void Refresh(List<T> objects)
      {
         wholeContent = objects;
         currentContent = wholeContent;
         ReloadAndUpdateColumns();
      }

      private ContextMenuStrip copyGridCellContextMenu = null;
      private ContextMenuStrip CopyGridCellContextMenu
      {
         get
         {
            if(copyGridCellContextMenu == null)
            {
               var copyCellMenuItem = new ToolStripMenuItem();
               copyCellMenuItem.Size = new System.Drawing.Size(102, 22);
               copyCellMenuItem.Text = "&Copy";
               copyCellMenuItem.Click += (s, e) =>
               {
                  if (dgv.SelectedRows.Count > 0 && currentColIndex > -1)
                  {
                     var value = dgv.SelectedRows[0].Cells[currentColIndex].Value;
                     if (value != null && !string.IsNullOrEmpty(value.ToString()))
                     {
                        Clipboard.SetText(value.ToString());
                     }
                  }
               };
               copyGridCellContextMenu = new ContextMenuStrip();
               copyGridCellContextMenu.Size = new System.Drawing.Size(103, 26);
               copyGridCellContextMenu.Items.AddRange(new ToolStripItem[] {
                  copyCellMenuItem});
            }
            return copyGridCellContextMenu;
         }
      }

      int currentColIndex = -1;
      private void SelectCellOnRightClickEventHandler(object sender, MouseEventArgs e)
      {
         var hti = dgv.HitTest(e.X, e.Y);
         currentColIndex = hti.ColumnIndex;
         if (hti.RowIndex > -1 && hti.ColumnIndex > -1)
         {
            dgv.Rows[hti.RowIndex].Selected = true;
         }
      }
      private void ContextMenuNeeded(object sender, DataGridViewCellContextMenuStripNeededEventArgs e)
      {
         if (e.RowIndex > -1 && e.ColumnIndex > -1)
         {
            e.ContextMenuStrip = CopyGridCellContextMenu;
         }
         else
         {
            e.ContextMenuStrip = null;
         }
      }

      public bool AllowCopyCellContentContextMenu
      {
         set
         {
            if(!value)
            {
               dgv.MouseDown -= SelectCellOnRightClickEventHandler;
               dgv.CellContextMenuStripNeeded -= ContextMenuNeeded;
               dgv.ContextMenuStrip = null;
               currentColIndex = -1;
               return;
            }

            // right click selects cell
            dgv.MouseDown += SelectCellOnRightClickEventHandler;
            // context menu is displayed
            //dgv.ContextMenuStrip = CreateCopyGridCellContextMenu();
            dgv.CellContextMenuStripNeeded += ContextMenuNeeded;
         }
      }

      // returns true if there is at least one filtered record
      public bool FilterOut(Func<T, bool> filter)
      {
         if (wholeContent.Count == 0)
         {
            return false;
         }

         currentContent = wholeContent.Where(row => filter(row)).ToList<T>();

         ReloadAndUpdateColumns();

         return currentContent.Count > 0;
      }

      // returns true if selection had been found
      public bool SelectFirst(Func<T, bool> predicate)
      {
         return SelectRow(0, predicate);
      }
      public bool SelectNext(Func<T, bool> predicate)
      {
         return SelectRow(Math.Max(SelectedRowIndex + 1, 0), predicate);
      }
      private bool SelectRow(int startIndex, Func<T, bool> predicate)
      {
         if (currentContent.Count == 0)
         {
            return false;
         }

         int index = currentContent.FindIndex(startIndex, row => predicate(row));
         // if start was not at the begining then start search from the begining
         if (index == -1 && startIndex > 0)
         {
            index = currentContent.FindIndex(0, row => predicate(row));
         }
         if(index == -1)
         {
            return false;
         }

         // scroll in a way that, first visible row is fourth one before selected one
         dgv.FirstDisplayedScrollingRowIndex = Math.Max(index, 0);
         SelectedRowIndex = index;
         return true;
      }

      public void LoadWholeContent()
      {
         Refresh(wholeContent);
      }

      public bool InRange(int rowIndex)
      {
         return rowIndex < currentContent.Count;
      }

      public T GetValue(int rowIndex)
      {
         return currentContent[rowIndex];
      }

      private void ReloadAndUpdateColumns()
      {
         Reload();
         UpdateColumnsDetails();
      }
      protected virtual void Reload()
      {
         if(isSortable)
         {
            dgv.DataSource = new SortableBindingList<T>(currentContent);
         }
         else
         {
            dgv.DataSource = currentContent;
         }
      }
      private void UpdateColumnsDetails()
      {
         for (var i = 0; i < propertyVisible.Length; ++i)
         {
            var col = dgv.Columns[i];

            col.Visible = propertyVisible[i];

            if (ColumnName2Caption != null)
            {
               string caption;
               if (ColumnName2Caption.TryGetValue(col.Name, out caption))
               {
                  col.HeaderText = caption;
               }            
            }
            if (ColumnName2DisplayIndex != null)
            {
               int displayIndex;
               if (ColumnName2DisplayIndex.TryGetValue(col.Name, out displayIndex))
               {
                  col.DisplayIndex = displayIndex;
               }
            }
         }
      }
   }
}
