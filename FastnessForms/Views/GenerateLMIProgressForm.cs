﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Fastness.BusinessOperations;

namespace FastnessForms.Views
{
   public partial class GenerateLMIProgressForm : Form
   {
      GenerateLastMonthInvoices operation;

      public GenerateLMIProgressForm(GenerateLastMonthInvoices operation)
      {
         InitializeComponent();
         this.operation = operation;
         this.operation.ProgressChanged += (progress, status) =>
         {
            asyncLMIGenerator.ReportProgress(progress, status);
         };
      }

      private void btClose_Click(object sender, EventArgs e)
      {
         Close();
      }

      private void asyncLMIGenerator_DoWork(object sender, DoWorkEventArgs e)
      {
         operation.Run();
      }

      private void asyncLMIGenerator_ProgressChanged(object sender, ProgressChangedEventArgs e)
      {
         pbProgressInfo.Value = e.ProgressPercentage;

         if (e.UserState != null)
         {
            pbProgressInfo.Value = 0;
            pbProgressInfo.Minimum = 0;
            pbProgressInfo.Maximum = (int)e.UserState;
            return;
         }

         if (pbProgressInfo.Value == pbProgressInfo.Maximum - 1) // all invoices are generated
         {
            lbInfo.Text = "Invoices generated. Saving invoices history into database...";
            return;
         }
         
         if (pbProgressInfo.Value < pbProgressInfo.Maximum - 1)
         {
            lbInfo.Text = string.Format("Last month invoices generation in progress... ({0}/{1})",
               pbProgressInfo.Value, pbProgressInfo.Maximum - 1); // -1 since the last step is saving invoice history in DB
         }
      }

      private void asyncLMIGenerator_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
      {
         lbInfo.Text = "Last month invoices generation in completed.";
         btClose.Enabled = true;

         if (operation.Error != null)
         {
            UserInteraction.ShowError("Error occured during L.M.I generation:\n\n{0}\n\nInvoices were not generated.", operation.Error.Message);
         }
      }

      private void GenerateLMIProgressForm_Load(object sender, EventArgs e)
      {
         lbInfo.Text = "Last month invoices generation in progress...";
         this.btClose.Enabled = false;

         this.asyncLMIGenerator.RunWorkerAsync();
      }
   }
}
