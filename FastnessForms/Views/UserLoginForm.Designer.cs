﻿namespace FastnessForms.Views
{
   partial class UserLoginForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserLoginForm));
         this.txtLogin = new System.Windows.Forms.TextBox();
         this.txtPassword = new System.Windows.Forms.MaskedTextBox();
         this.label1 = new System.Windows.Forms.Label();
         this.label2 = new System.Windows.Forms.Label();
         this.cbSaveCredentials = new System.Windows.Forms.CheckBox();
         this.label3 = new System.Windows.Forms.Label();
         this.btLogin = new System.Windows.Forms.Button();
         this.btExit = new System.Windows.Forms.Button();
         this.SuspendLayout();
         // 
         // txtLogin
         // 
         this.txtLogin.Location = new System.Drawing.Point(115, 12);
         this.txtLogin.Name = "txtLogin";
         this.txtLogin.Size = new System.Drawing.Size(216, 20);
         this.txtLogin.TabIndex = 0;
         // 
         // txtPassword
         // 
         this.txtPassword.Location = new System.Drawing.Point(115, 38);
         this.txtPassword.Name = "txtPassword";
         this.txtPassword.PasswordChar = '*';
         this.txtPassword.Size = new System.Drawing.Size(216, 20);
         this.txtPassword.TabIndex = 1;
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(11, 15);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(61, 13);
         this.label1.TabIndex = 2;
         this.label1.Text = "User name:";
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(12, 41);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(56, 13);
         this.label2.TabIndex = 3;
         this.label2.Text = "Password:";
         // 
         // cbSaveCredentials
         // 
         this.cbSaveCredentials.AutoSize = true;
         this.cbSaveCredentials.Location = new System.Drawing.Point(115, 64);
         this.cbSaveCredentials.Name = "cbSaveCredentials";
         this.cbSaveCredentials.Size = new System.Drawing.Size(15, 14);
         this.cbSaveCredentials.TabIndex = 2;
         this.cbSaveCredentials.UseVisualStyleBackColor = true;
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Location = new System.Drawing.Point(11, 65);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(89, 13);
         this.label3.TabIndex = 5;
         this.label3.Text = "Save credentials:";
         // 
         // btLogin
         // 
         this.btLogin.Location = new System.Drawing.Point(175, 93);
         this.btLogin.Name = "btLogin";
         this.btLogin.Size = new System.Drawing.Size(75, 23);
         this.btLogin.TabIndex = 3;
         this.btLogin.Text = "Login";
         this.btLogin.UseVisualStyleBackColor = true;
         this.btLogin.Click += new System.EventHandler(this.btLogin_Click);
         // 
         // btExit
         // 
         this.btExit.Location = new System.Drawing.Point(256, 93);
         this.btExit.Name = "btExit";
         this.btExit.Size = new System.Drawing.Size(75, 23);
         this.btExit.TabIndex = 4;
         this.btExit.Text = "Exit";
         this.btExit.UseVisualStyleBackColor = true;
         this.btExit.Click += new System.EventHandler(this.btExit_Click);
         // 
         // UserLoginForm
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(343, 128);
         this.Controls.Add(this.btExit);
         this.Controls.Add(this.btLogin);
         this.Controls.Add(this.label3);
         this.Controls.Add(this.cbSaveCredentials);
         this.Controls.Add(this.label2);
         this.Controls.Add(this.label1);
         this.Controls.Add(this.txtPassword);
         this.Controls.Add(this.txtLogin);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
         this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
         this.MaximizeBox = false;
         this.MinimizeBox = false;
         this.Name = "UserLoginForm";
         this.Text = "Login to Fastness System";
         this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.UserLoginForm_FormClosed);
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.TextBox txtLogin;
      private System.Windows.Forms.MaskedTextBox txtPassword;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.CheckBox cbSaveCredentials;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.Button btLogin;
      private System.Windows.Forms.Button btExit;
   }
}