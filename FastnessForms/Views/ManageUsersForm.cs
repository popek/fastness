﻿using Fastness.Common;
using Fastness.DataModel;
using Fastness.DataModel.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Fastness.DataModel.Utils;

namespace FastnessForms.Views
{

   public partial class ManageUsersForm : Form
   {
      public class NameAndRole
      {
         public string Name { get; set; }
         public string Role { get; set; }

         public NameAndRole(string name, int role)
         {
            Name = name;
            if (role == User.ADMIN)
               Role = "admin";
            else if (role == User.WRITER)
               Role = "read-write";
            else
               Role = "read-only";
         }
      };

      DataGridViewManager<NameAndRole> userListMan;
      ModifyFastnessUser.Context context;

      public ManageUsersForm()
      {
         InitializeComponent();
         UpdateFormContext();

         userListMan = new DataGridViewManager<NameAndRole>(dgvUsersList, new int[] { });

         if (!LoggedUserUtils.IsAdmin())
         {
            rbAddNewUser.Enabled = false;
            rbRemoveOtherUser.Enabled = false;
            cbRole.Enabled = false;
         }

         UpdateList();
      }

      private void rbEditCurrentUser_CheckedChanged(object sender, EventArgs e)
      {
         UpdateFormContext();
      }
      private void rbAddNewUser_CheckedChanged(object sender, EventArgs e)
      {
         UpdateFormContext();
      }
      private void rbRemoveOtherUser_CheckedChanged(object sender, EventArgs e)
      {
         UpdateFormContext();
      }
      private void UpdateFormContext()
      {
         txtUserName.Text = "";
         txtPassword.Text = "";
         txtPassword.Enabled = true;
         txtPasswordConfirm.Text = "";
         txtPasswordConfirm.Enabled = true;
         cbRole.SelectedIndex = User.READER;
         cbRole.Enabled = true;

         btRemoveUser.Enabled = false;
         btSave.Enabled = true;

         if (rbEditCurrentUser.Checked)
         {
            context = ModifyFastnessUser.Context.ATTACH_EXISTING_USER;
            txtUserName.Text = LoggedUserUtils.UserName;
            cbRole.SelectedIndex = LoggedUserUtils.UserRole;
         }
         else if (rbAddNewUser.Checked)
         {
            context = ModifyFastnessUser.Context.ADD_NEW_USER;
         }
         else if (rbRemoveOtherUser.Checked)
         {
            context = ModifyFastnessUser.Context.REMOVE_USER;
            btRemoveUser.Enabled = true;
            btSave.Enabled = false;

            txtUserName.Text = userListMan.CurrentValue != null ? userListMan.CurrentValue.Name : "";
            txtPassword.Enabled = false;
            txtPasswordConfirm.Enabled = false;
            cbRole.Enabled = false;
         }
      }

      private void UpdateList()
      {
         using (var ctx = new fastnessEntities())
         {
            var usersView = ctx.User.ToList<User>().Select(u => new NameAndRole(u.Login, u.Role)).ToList<NameAndRole>();
            userListMan.Refresh(usersView);
         }
      }

      private void btRemoveUser_Click(object sender, EventArgs e)
      {
         btSave_Click(sender, e); //the samve procedure
      }

      private void btSave_Click(object sender, EventArgs e)
      {
         bool success = false;
         switch (context)
         { 
            case ModifyFastnessUser.Context.ADD_NEW_USER:
               success = AddNewUser();
               break;
            case ModifyFastnessUser.Context.REMOVE_USER:
               success = RemoveUser();
               break;
            case ModifyFastnessUser.Context.ATTACH_EXISTING_USER:
               success = EditCurrentUser();
               break;
         }
         if (success)
         {
            UpdateList();
         }
      }

      private void tbCancel_Click(object sender, EventArgs e)
      {
         Close();
      }

      private bool AddNewUser()
      {
         if (!UserAndPasswordFilledProperly())
         {
            return false;
         }
         using (var ctx = new fastnessEntities())
         {
            if (UserAlreadyExists(ctx))
            {
               return false;
            }
         }

         ModifyDb.RunTransaction(dbCtx => new ModifyFastnessUser(
            ModifyFastnessUser.Context.ADD_NEW_USER,
               new Fastness.DataModel.User
               {
                  Login = txtUserName.Text,
                  Password = PasswordUtils.CalculateHash(txtPassword.Text),
                  Role = cbRole.SelectedIndex
               }, 
               dbCtx).Run());

         UserInteraction.ShowInfo("User '{0}' added succesfully.", txtUserName.Text);
         return true;
      }
      private bool RemoveUser()
      {
         if (LoggedUserUtils.LoginMatch(txtUserName.Text))
         {
            UserInteraction.ShowError("You can not remove your user!");
            txtUserName.Focus();
            return false;
         }
         using (var ctx = new fastnessEntities())
         {
            if (ctx.User.Count(u => u.Login == txtUserName.Text) == 0)
            {
               UserInteraction.ShowError(
                  "User '{0}' does not exists. Only existing user can be removed!", 
                  txtUserName.Text);
               txtUserName.Focus();
               return false;
            }
         }
         ModifyDb.RunTransaction(dbCtx => new ModifyFastnessUser(
            ModifyFastnessUser.Context.REMOVE_USER,
            dbCtx.User.Where(u => u.Login == txtUserName.Text).First(),
            dbCtx).Run());
         UserInteraction.ShowInfo("User '{0}' removed succesfully.", txtUserName.Text);
         return true;
      }
      private bool EditCurrentUser()
      {
         if (!UserAndPasswordFilledProperly())
         {
            return false;
         }
         User currentUser = null;
         using (var ctx = new fastnessEntities())
         {
            currentUser = ctx.User.Where(u => u.Login == LoggedUserUtils.UserName).FirstOrDefault();

            var loginChanged = LoggedUserUtils.UserName != txtUserName.Text;
            if(loginChanged && UserAlreadyExists(ctx))
            {
               return false;
            }

            currentUser.Login = txtUserName.Text;
            currentUser.Password = PasswordUtils.CalculateHash(txtPassword.Text);
            currentUser.Role = cbRole.SelectedIndex;
         }
         ModifyDb.RunTransaction(dbCtx => new ModifyFastnessUser(
            ModifyFastnessUser.Context.ATTACH_EXISTING_USER, 
            currentUser, 
            dbCtx).Run());
         LoggedUserUtils.SetLoggedUser(currentUser);

         UserInteraction.ShowInfo("Current user edited succesfully.");
         return true;
      }

      private bool UserAlreadyExists(fastnessEntities ctx)
      {
         var user = ctx.User.Where(u => u.Login == txtUserName.Text).FirstOrDefault();
         if (user != null)
         {
            UserInteraction.ShowError(
               "User '{0}' already exists. Pick login which does not exist yet!", 
               txtUserName.Text);
            txtUserName.Focus();
            return true;
         }
         return false;
      }

      private bool UserAndPasswordFilledProperly()
      {
         if (txtUserName.IsTextEmpty("User name"))
         {
            return false;
         }
         if (txtPassword.IsTextEmpty("User password"))
         {
            return false;
         }
         if (txtPassword.Text != txtPasswordConfirm.Text)
         {
            UserInteraction.ShowError("Password confirmation does not match entered password!");
            txtPasswordConfirm.Focus();
            return false;
         }
         return true;
      }

      private void dgvUsersList_SelectionChanged(object sender, EventArgs e)
      {
         if (userListMan.CurrentValue != null)
         {
            UpdateFormContext();
         }
      }

   }
}
