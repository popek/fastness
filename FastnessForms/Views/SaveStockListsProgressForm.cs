﻿using Fastness.Common;
using ExportToExcel;
using Fastness.DataModel;
using Fastness.DataModel.Utils;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace FastnessForms.Views
{
   public partial class SaveStockListsProgressForm : Form
   {
      static readonly ILog log = LogManager.GetLogger(typeof(SaveStockListsProgressForm));

      private string ErrorMessage { get; set; }

      public SaveStockListsProgressForm()
      {
         InitializeComponent();
      }
      private void asyncSaver_DoWork(object sender, DoWorkEventArgs e)
      {
         try
         {
            DoWork();
         }
         catch (Exception exc)
         {
            log.ErrorFormat("exception during saving stock list (did you forgot to close all stocklists files?): {0}", exc);
            ErrorMessage = string.Format("Exception during savin stock lists:\n\n{0}", exc);
         }
      }

      private void DoWork()
      {
         log.Info("saving stocks list to files");

         CurrentDirUtils.ResetSubdir("stocks");

         using(var dbCtx = new fastnessEntities())
         {
            var ctrs = new FetchFromDb(dbCtx).AvailableContracts();

            asyncSaver.ReportProgress(0, ctrs.Count());

            int counter = 1;
            foreach (var ctr in ctrs)
            {
               bool fileCreated = CreateExcelFile.CreateExcelDocument(
                  XlsStockListRecord.FromItems(new FetchFromDb(dbCtx).InStockContractItems(ctr)), 
                  StockListFilePath(ctr));

               if (!fileCreated)
               {
                  log.ErrorFormat(string.Format("failed to save stock list file for {0}", ctr.LogDesc()));

                  ErrorMessage = string.Format("Failed to save stock list file for '{0}'.\nIf file is opened, close it and generate stock lists one more time. Otherwise contact fastness application provider)", ctr.Name);
                  return;
               }

               asyncSaver.ReportProgress(counter++);
            }
         }

         log.Info("saving stocks list to files done");
      }

      private void asyncSaver_ProgressChanged(object sender, ProgressChangedEventArgs e)
      {
         pbProgressInfo.Value = e.ProgressPercentage;

         if (e.UserState != null)
         {
            pbProgressInfo.Value = 0;
            pbProgressInfo.Minimum = 0;
            pbProgressInfo.Maximum = (int)e.UserState;
            return;
         }

         lbInfo.Text = string.Format("Saving stock lists in progress... ({0}/{1})",
            pbProgressInfo.Value, pbProgressInfo.Maximum);
      }

      private void asyncSaver_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
      {
         if (ErrorMessage != null)
         {
            lbInfo.Text = "Saving stock lists failed.";
            UserInteraction.ShowError(ErrorMessage);
         }
         else
         {
            lbInfo.Text = "Saving stock lists completed.";
         }
         btClose.Enabled = true;
      }

      private void SaveStockListsrogressForm_Load(object sender, EventArgs e)
      {
         lbInfo.Text = "Saving stock lists in progress...";

         btClose.Enabled = false;

         asyncSaver.RunWorkerAsync();
      }

      private string StockListFilePath(Contract ctr)
      {
         var filename = string.Format("{0}_{1}_{2}.xlsx", RemoveForbiddenChars(ctr.Name), ctr.ContractID, RemoveForbiddenChars(ctr.AccountRef));
         return CurrentDirUtils.GenerateFilePath("stocks", filename);
      }
      private string RemoveForbiddenChars(string text)
      {
         StringBuilder sb = new StringBuilder(text.Length);
         foreach(var letter in text)
         {
            if(letter == '*' ||
               letter == '/' ||
               letter == '\\' ||
               letter == '?' ||
               letter == ',' ||
               letter == '.' ||
               letter == '\'' ||
               Path.GetInvalidFileNameChars().Any(invalidChar => letter == invalidChar))
            {
               sb.Append(' ');
               continue;
            }
            sb.Append(letter);
         }
         return sb.ToString();
      }

      private void btClose_Click(object sender, EventArgs e)
      {
         Close();
      }
   }

   internal class XlsStockListRecord
   {
      public int OrderNo { get; set; }
      public string IUID { get; set; }
      public string CIUID { get; set; }
      public string Description { get; set; }
      public string ConsignmentDate { get; set; }
      public string DestructionDate { get; set; }
      public string Location { get; set; }

      public static List<XlsStockListRecord> FromItems(IEnumerable<ItemDetail> items)
      {
         var result = new List<XlsStockListRecord>();
         int counter = 1;
         foreach (var item in items)
         {
            result.Add(new XlsStockListRecord
            {
               OrderNo = counter++,
               IUID = item.ItemNumber,
               CIUID = item.ClientItemRef,
               Description = item.Description,
               ConsignmentDate = DateUtils.ToString(item.DateRecieved),
               DestructionDate = item.DestructionDate,
               Location = item.Location
            });
         }
         return result;
      }

   }

}
