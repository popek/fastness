﻿using Fastness.DataModel;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;

using Fastness.Common;
using Fastness.DataModel.Utils;
using System;


namespace FastnessForms.Views
{
   public static class FastnessUI
   {
      public static DataGridViewManager<ActionHistory> 
         CreateActionHistoryGridManager(DataGridView dgv)
      {
         /*
          *   0 ActionHistoryID
              1 ItemNumber
              2 Description
              3 Date
              4 Notes
              5 Charge
              6 DateCompleted
              7 InvoiceNumber
              8 AuthorisedBy
              9 Location
              10 Percentage
              11 LastAmended
              12 ContractActionID
          * 
          * some columns will be hidden
          */

         var gridMan = new DataGridViewManager<ActionHistory>(
            dgv,
            new int[] { 0, 10, 12 });

         gridMan.ColumnName2Caption = new Dictionary<string, string> 
         {
            {"Description", "Action"},
            {"ItemNumber", "IUID"},
            {"Date", "Date Actioned"},
            {"DateCompleted", "Date Completed"}
         };
         gridMan.ColumnName2DisplayIndex = new Dictionary<string, int> { { "DateCompleted", 3 } };

         return gridMan;
      }
   }
}
