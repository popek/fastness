﻿namespace FastnessForms.Views
{
   partial class InvoicesNotesListForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.dgvNotes = new System.Windows.Forms.DataGridView();
         this.btContinue = new System.Windows.Forms.Button();
         ((System.ComponentModel.ISupportInitialize)(this.dgvNotes)).BeginInit();
         this.SuspendLayout();
         // 
         // dgvNotes
         // 
         this.dgvNotes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.dgvNotes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
         this.dgvNotes.Location = new System.Drawing.Point(12, 12);
         this.dgvNotes.Name = "dgvNotes";
         this.dgvNotes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
         this.dgvNotes.Size = new System.Drawing.Size(376, 366);
         this.dgvNotes.TabIndex = 0;
         this.dgvNotes.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvNotes_CellMouseDoubleClick);
         // 
         // btContinue
         // 
         this.btContinue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.btContinue.Location = new System.Drawing.Point(12, 384);
         this.btContinue.Name = "btContinue";
         this.btContinue.Size = new System.Drawing.Size(73, 25);
         this.btContinue.TabIndex = 1;
         this.btContinue.Text = "OK";
         this.btContinue.UseVisualStyleBackColor = true;
         this.btContinue.Click += new System.EventHandler(this.btContinue_Click);
         // 
         // InvoicesNotesListForm
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(400, 421);
         this.ControlBox = false;
         this.Controls.Add(this.btContinue);
         this.Controls.Add(this.dgvNotes);
         this.Name = "InvoicesNotesListForm";
         this.Text = "Notes for invoices";
         this.Load += new System.EventHandler(this.InvoicesNotesListForm_Load);
         ((System.ComponentModel.ISupportInitialize)(this.dgvNotes)).EndInit();
         this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.DataGridView dgvNotes;
      private System.Windows.Forms.Button btContinue;
   }
}