﻿using Fastness.Common;
using System;
using System.Windows.Forms;

namespace FastnessForms.Views
{
   public partial class InvoiceNotesForm : Form
   {
      InvoiceNotes notes;

      public InvoiceNotesForm(InvoiceNotes notes)
      {
         InitializeComponent();

         this.notes = notes;
         txtNotes.Text = notes.Notes;
         Text = string.Format("Invoice {0}/{1} notes for '{2}'", DateUtils.LastMonthDate.Year, DateUtils.LastMonthDate.Month, notes.Name);
      }

      private void btOk_Click(object sender, EventArgs e)
      {
         var lines = txtNotes.Text.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

         if (lines.Length > 4)
         {
            UserInteraction.ShowError("Invoice note has to much lines.\nInvoice note can have up to 4 lines");
            return;
         }
         if (txtNotes.Text.Length > 250)
         {
            UserInteraction.ShowError("Invoice note is to long.\nInvoice node can have up to 250 characters");
            return;
         }

         notes.Notes = txtNotes.Text;
         Close();
      }
   }
}
