﻿using Fastness.DataModel;
using Fastness.DataModel.Transaction;
using Fastness.DataModel.Utils;
using System;
using System.Windows.Forms;

namespace FastnessForms.Views
{
   public partial class ActionDestroyForm : Form
   {
      ActionContext actionCtx;
      ActionFormContext formCtx;
      Action<FastnessActionContext, string> itemSaver;

      fastnessEntities dbCtx = new fastnessEntities();

      ItemDetail fetchedItem = null;
      TextBoxChangesTracker itemNumberTrack = null;

      private bool ReadyToDestroy
      { 
         set 
         {
            this.txtCharge.Enabled = false; // charge is always disabled (explicitly enabled by pressing button)
            this.dtpDestroy.Enabled = value;
            this.txtAuthoriziedBy.Enabled = value;
            this.txtNotes.Enabled = value;
            this.btDestroy.Enabled = value;
            this.btShow.Enabled = !value;
            this.btAllowChargeChange.Enabled = value;
         } 
      }

      public ActionDestroyForm(ActionContext actionCtx, ActionFormContext formCtx, Action<FastnessActionContext, string> itemSaver)
      {
         this.actionCtx = actionCtx;
         this.formCtx = formCtx;

         this.itemSaver = itemSaver;

         InitializeComponent();

         this.Text = this.actionCtx.FormCaption;

         this.txtItemNumber.AddKeyPressWithTextLengthLimitAndIntegerValue(DBFieldsConstraint.ITEM_NUMBER_LENGTH);
         this.txtConfirmItemNumber.AddKeyPressWithTextLengthLimitAndIntegerValue(DBFieldsConstraint.ITEM_NUMBER_LENGTH);
         
         this.txtCharge.AddKeyPressWithNumericValue();

         this.txtNotes.AddKeyPressWithTextLengthLimit(DBFieldsConstraint.ACTION_HISTORY_NOTES_MAX_LENGHT);

         this.txtAuthoriziedBy.AddKeyPressWithTextLengthLimit(DBFieldsConstraint.ACTION_HISTORY_AUTHORIZED_BY_MAX_LENGHT);

         this.txtCharge.Text = actionCtx.CtrAction.Charge.ToString();
         this.dtpDestroy.Value = this.formCtx.LastActionDate;
         this.txtAuthoriziedBy.Text = this.formCtx.LastActionAuthorizer;

         this.txtClientItemReference.Enabled = false;
         this.txtDescription.Enabled = false;
         this.txtType.Enabled = false;
         this.txtPercentage.Enabled = false;
         this.txtLocation.Enabled = false;
         this.btAllowChargeChange.Enabled = false;
         this.txtCharge.Enabled = false;

         itemNumberTrack = new TextBoxChangesTracker(this.txtItemNumber, (itemNumberChanged) =>
         {
            ReadyToDestroy = !itemNumberChanged;
         });

         ReadyToDestroy = false;
      }

      private bool FormDataValid()
      {
         if (!txtItemNumber.IsTextLenthExactlyTheSame(DBFieldsConstraint.ITEM_NUMBER_LENGTH, "Box number"))
         {
            return false;
         }

         if (!txtItemNumber.IsDigitsOnlyText("Box number"))
         {
            return false;               

         }
         if (!new AskDb(dbCtx).ItemAlreadyExists(txtItemNumber.Text))
         {
            txtItemNumber.NotifyTextError("Box with this number does not exists!");
            return false;
         }
         if (txtItemNumber.Text != txtConfirmItemNumber.Text)
         {
            txtConfirmItemNumber.NotifyTextError("Box Number and confirmed box number are different!");
            return false;
         }

         if (!txtCharge.IsValidNumericValueFormat("Charge"))
         {
            return false;
         }

         if (!dtpDestroy.IsActionDateConstraintsValid(formCtx.LastActionDate, actionCtx.CtrAction.Description))
         {
            return false;
         }
         var takenFromClientAt = fetchedItem.DateReturned ?? fetchedItem.DateRecieved;
         if (dtpDestroy.Value < takenFromClientAt)
         {
            UserInteraction.ShowError(
               "Destroy date '{0}' is before the box had been put in stock ({1}).\nDestroy can take place after the date box had been put to in stock", 
               dtpDestroy.Value.ToShortDateString(), takenFromClientAt.Value.ToShortDateString());
            dtpDestroy.Focus();
            return false;
         }

         if (txtNotes.IsTextToLong(DBFieldsConstraint.ACTION_HISTORY_NOTES_MAX_LENGHT, "notes"))
         {
            return false;
         }
         if (txtAuthoriziedBy.IsTextToLong(DBFieldsConstraint.ACTION_HISTORY_AUTHORIZED_BY_MAX_LENGHT, "Authorization name"))
         {
            return false;
         }
         
         return true;
      }

      private void btDestroy_Click(object sender, EventArgs e)
      {
         if (!FormDataValid())
         {
            return;
         }

         itemSaver(
            new FastnessActionContext(
               fetchedItem,
               actionCtx.Ctr,
               actionCtx.CtrAction,
               dtpDestroy.Value,
               Double.Parse(this.txtCharge.Text),
               txtAuthoriziedBy.Text),
            txtNotes.Text);

         formCtx.Update(txtAuthoriziedBy.Text, dtpDestroy.Value);

         Close();
      }

      private void btClose_Click(object sender, EventArgs e)
      {
         Close();
      }

      private void btShow_Click(object sender, EventArgs e)
      {
         var validateFormBeforeShow = new Func<bool>(() =>
         {
            if (!txtItemNumber.IsTextLenthExactlyTheSame(DBFieldsConstraint.ITEM_NUMBER_LENGTH, "Box number"))
            {
               return false;
            }
            if (!txtItemNumber.IsDigitsOnlyText("Box number"))
            {
               return false;
            }
            if (!new AskDb(dbCtx).ItemAlreadyExists(this.txtItemNumber.Text))
            {
               this.txtItemNumber.NotifyTextError("Box with this number does not exists!");
               return false;
            }
            if (this.txtItemNumber.Text != this.txtConfirmItemNumber.Text)
            {
               this.txtConfirmItemNumber.NotifyTextError("Box Number and confirmed box number are different!");
               return false;
            }
            return true;
         });

         if (!validateFormBeforeShow())
         {
            return;
         }

         fetchedItem = new FetchFromDb(dbCtx).ItemBasedOnItemNo(this.txtItemNumber.Text);

         if (fetchedItem.ContractID != actionCtx.Ctr.ContractID)
         {
            UserInteraction.ShowError(
               "Box '{0}' does not belong to '{1}'.\nYou must choose box owned by '{1}'", 
               fetchedItem.ItemNumber, actionCtx.Ctr.Name);
            fetchedItem = null;
            return;
         }
         if(fetchedItem.Destroyed ?? false)
         {
            UserInteraction.ShowError(
               "Box '{0}' has been destroyed.\nYou must choose box which still exists.", 
               fetchedItem.ItemNumber);
            fetchedItem = null;
            return;
         }
         if (!(fetchedItem.InStock ?? true))
         {
            UserInteraction.ShowError("Box '{0}' is out of stock.\nYou must choose box which is still in stock.", 
               fetchedItem.ItemNumber);
            fetchedItem = null;
            return;
         }

         this.txtClientItemReference.Text = fetchedItem.ClientItemRef;
         this.txtDescription.Text = fetchedItem.Description;
         this.txtType.Text = fetchedItem.Type;
         this.txtPercentage.Text = fetchedItem.Percentage.ToString();
         this.txtLocation.Text = fetchedItem.Location;

         itemNumberTrack.TrackChanges = true;
         ReadyToDestroy = true;

         dtpDestroy.Focus();
      }

      private void btAllowChargeChange_Click(object sender, EventArgs e)
      {
         txtCharge.Enabled = true;
      }
   }
}
