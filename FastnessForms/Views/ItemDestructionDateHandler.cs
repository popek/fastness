﻿using System;
using System.Globalization;
using System.Windows.Forms;
using Fastness.DataModel;

namespace FastnessForms.Views
{
   public class ItemDestructionDateHandler
   {
      TextBoxBase destructionDate;

      public ItemDestructionDateHandler(TextBoxBase destructionDate)
      {
         this.destructionDate = destructionDate;
      }

      public bool IsDestructionDateValid()
      {
         string dummy;
         if (TryParseDestructionDate(out dummy))
            return true;

         destructionDate.NotifyTextError("Destruction date is invalid!");
         return false;
      }
      public string GetDestructionDateString()
      {
         string result;
         TryParseDestructionDate(out result);
         return result;
      }

      private bool TryParseDestructionDate(out string destructionDateText)
      {
         if (string.IsNullOrEmpty(destructionDate.Text.Trim()))
         {
            destructionDateText = string.Empty;
            return true;
         }

         if(TryParseDestructionDate(
            out destructionDateText, 
            ItemDetail.DestructionDateFormatDDMMYYYY()))
         {
            return true;
         }
         if (TryParseDestructionDate(
            out destructionDateText, 
            ItemDetail.DestructionDateFormatMMYYYY()))
         {
            return true;
         }
         if (TryParseDestructionDate(
            out destructionDateText,
            ItemDetail.DestructionDateFormatYYYY()))
         {
            return true;
         }

         destructionDateText = string.Empty;
         return false;
      }
      private bool TryParseDestructionDate(out string destructionDateText, string format)
      {
         DateTime result;
         if (DateTime.TryParseExact(
            destructionDate.Text.Trim(),
            format,
            CultureInfo.InvariantCulture,
            DateTimeStyles.None,
            out result))
         {
            destructionDateText = result.ToString(format);
            return true;
         }
         destructionDateText = string.Empty;
         return false;
      }
   }
}
