﻿using Fastness.Common;
using Fastness.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FastnessForms.Views
{
   public partial class UserLoginForm : Form
   {
      XmlObjectsRepository repository = new XmlObjectsRepository(CurrentDirUtils.GetSubdirPath("auxiliary_configs"));
      bool cryptedPasswordFromSavedObjectOn = false;
      Action<Fastness.DataModel.User> loggedUserCollector;

      Fastness.DataModel.User loggedUser;

      public UserLoginForm(Action<Fastness.DataModel.User> loggedUserCollector)
      {
         InitializeComponent();
         
         this.loggedUserCollector = loggedUserCollector;

         if (repository.Exists<Fastness.DataModel.User>())
         {
            var savedUser = repository.Get<Fastness.DataModel.User>();
            txtLogin.Text = savedUser.Login;
            txtPassword.Text = savedUser.Password;
            cbSaveCredentials.Checked = true;

            cryptedPasswordFromSavedObjectOn = true;
            txtPassword.TextChanged += (s, e) => { cryptedPasswordFromSavedObjectOn = false; }; 

            btLogin.Focus();
         }
         else
         {
            txtLogin.Text = "";
            txtPassword.Text = "";
            cbSaveCredentials.Checked = false;

            txtLogin.Focus();
         }
      }

      private void btExit_Click(object sender, EventArgs e)
      {
         Close();
      }

      private void btLogin_Click(object sender, EventArgs e)
      {
         if (!ValidateFormData())
         {
            return;
         }
         
         btLogin.Enabled = false;
         btExit.Enabled = false;

         using (var ctx = new fastnessEntities())
         {
            var user = ctx.User.Where(u => u.Login == txtLogin.Text).FirstOrDefault();
            if (user == null)
            {
               txtLogin.NotifyTextError(string.Format("Invalid user name. User '{0}' does not exist!", txtLogin.Text));
               return;
            }

            bool passwordOk = cryptedPasswordFromSavedObjectOn ?
               user.Password == txtPassword.Text :
               user.IsValidPassword(txtPassword.Text);
            if (!passwordOk)
            {
               txtPassword.NotifyTextError(string.Format("Invalid password. Provided password does not match user '{0}' password!", txtLogin.Text));
               btLogin.Enabled = true;
               btExit.Enabled = true;
               return;
            }
            loggedUser = user;
         }

         if (cbSaveCredentials.Checked)
         {
            repository.Update<Fastness.DataModel.User>(loggedUser);
         }
         Close();
      }

      private bool ValidateFormData()
      {
         if (txtLogin.IsTextEmpty("user name"))
         {
            return false;
         }
         if (txtPassword.IsTextEmpty("password"))
         {
            return false;
         }
         return true;
      }

      private void UserLoginForm_FormClosed(object sender, FormClosedEventArgs e)
      {
         loggedUserCollector(loggedUser);
      }
   }
}
