﻿namespace FastnessForms.Views
{
   partial class PrintLMIProgressForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrintLMIProgressForm));
         this.asyncLMIPrinter = new System.ComponentModel.BackgroundWorker();
         this.pbProgressInfo = new System.Windows.Forms.ProgressBar();
         this.lbInfo = new System.Windows.Forms.Label();
         this.SuspendLayout();
         // 
         // asyncLMIPrinter
         // 
         this.asyncLMIPrinter.WorkerReportsProgress = true;
         this.asyncLMIPrinter.DoWork += new System.ComponentModel.DoWorkEventHandler(this.asyncLMIPrinter_DoWork);
         this.asyncLMIPrinter.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.asyncLMIPrinter_ProgressChanged);
         this.asyncLMIPrinter.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.asyncLMIPrinter_RunWorkerCompleted);
         // 
         // pbProgressInfo
         // 
         this.pbProgressInfo.Location = new System.Drawing.Point(12, 38);
         this.pbProgressInfo.Name = "pbProgressInfo";
         this.pbProgressInfo.Size = new System.Drawing.Size(521, 23);
         this.pbProgressInfo.TabIndex = 0;
         // 
         // lbInfo
         // 
         this.lbInfo.BackColor = System.Drawing.SystemColors.Control;
         this.lbInfo.Cursor = System.Windows.Forms.Cursors.Default;
         this.lbInfo.ForeColor = System.Drawing.SystemColors.ControlText;
         this.lbInfo.Location = new System.Drawing.Point(12, 9);
         this.lbInfo.Name = "lbInfo";
         this.lbInfo.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.lbInfo.Size = new System.Drawing.Size(521, 26);
         this.lbInfo.TabIndex = 12;
         this.lbInfo.Text = "lbInfo";
         // 
         // PrintLMIProgressForm
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(545, 72);
         this.ControlBox = false;
         this.Controls.Add(this.lbInfo);
         this.Controls.Add(this.pbProgressInfo);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
         this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
         this.Name = "PrintLMIProgressForm";
         this.Text = "Printing last month invoices";
         this.Load += new System.EventHandler(this.PrintLMIProgressForm_Load);
         this.ResumeLayout(false);

      }

      #endregion

      private System.ComponentModel.BackgroundWorker asyncLMIPrinter;
      private System.Windows.Forms.ProgressBar pbProgressInfo;
      public System.Windows.Forms.Label lbInfo;
   }
}