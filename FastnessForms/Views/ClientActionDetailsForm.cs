﻿using Fastness.DataModel;
using Fastness.DataModel.Transaction;
using System;
using System.Windows.Forms;
using Fastness.DataModel.Utils;

namespace FastnessForms.Views
{
   public partial class ClientActionDetailsForm : Form
   {
      ContractAction ctrAction;
      Action requestCtrActionChanged;

      bool chargeChanged = false;
      bool percentFreeChanged = false;

      bool UnsavedChangesOnBoard 
      {
         get
         {
            return chargeChanged || percentFreeChanged;
         }
      }

      public ClientActionDetailsForm(ContractAction ctrAction, Action requestCtrActionChanged)
      {
         this.ctrAction = ctrAction;
         this.requestCtrActionChanged = requestCtrActionChanged;
         InitializeComponent();

         this.txtCharge.AddKeyPressWithNumericValue();
         this.txtPerFree.AddKeyPressWithNumericValue();

         this.btSaveChanges.Enabled = LoggedUserUtils.CanEditDBContent();
      }

      private void ClientActionDetailsForm_Load(object sender, EventArgs e)
      {
         lbAuxiliaryInfo.Text = string.Format("Code: {0}\nDescription: {1}", ctrAction.Code, ctrAction.Description);
         txtCharge.Text = ctrAction.Charge.ToString();
         txtPerFree.Text = ctrAction.PerFreeAction.ToString();

         this.txtCharge.AddTextChangeHandler(() => ctrAction.Charge.ToString(), (textChanged) =>
         {
            chargeChanged = textChanged;
            HandleChange();
         });

         this.txtPerFree.AddTextChangeHandler(() => ctrAction.PerFreeAction.ToString(), (textChanged) =>
         {
            percentFreeChanged = textChanged;
            HandleChange();
         });

         this.optSpecificClientOnly.Checked = true;
         HandleChange();
      }

      private void HandleChange()
      {
         if (!LoggedUserUtils.CanEditDBContent())
         {
            this.btSaveChanges.Enabled = false;
            return;
         }
         this.btSaveChanges.Enabled = UnsavedChangesOnBoard;
      }

      private bool CheckFormData()
      {
         return txtCharge.IsValidNumericValueFormat("Charge") && txtPerFree.IsValidNumericValueInRange("Percents Free", 0, 100);
      }

      private void btSaveChanges_Click(object sender, EventArgs e)
      {
         if (!CheckFormData())
         {
            return;
         }
         if (!UserInteraction.AskUser("Do you want to save changes?"))
         {
            return;
         }

         if (optSpecificClientOnly.Checked)
         {
            RunUpdate(UpdateFastnessAction.Context.UPDATE_SINGLE_CONTRACT_ACTION);
            requestCtrActionChanged();
         }
         else if (optClientsAndService.Checked)
         {
            RunUpdate(UpdateFastnessAction.Context.UPDATE_SERVICE_ACTION_AND_ALL_CONTRACT_ACTIONS);
            requestCtrActionChanged();
         }
         else if (optServiceOnly.Checked)
         {
            RunUpdate(UpdateFastnessAction.Context.UPDATE_SERVICE_ACTION);
         }
         else
         {
            throw new Exception("ClientActionDetailForm has no option checked, which indicates and error!");
         }
         UserInteraction.ShowInfo(
            "New charge (percent free) for {0} action saved succesfully.", 
            ctrAction.Code);
         Close();
      }

      private void RunUpdate(UpdateFastnessAction.Context fasntessActionUpdateContext)
      {
         ModifyDb.RunTransaction(dbCtx => new UpdateFastnessAction(
            ctrAction,
            Double.Parse(this.txtCharge.Text),
            Double.Parse(this.txtPerFree.Text),
            fasntessActionUpdateContext,
            dbCtx).Run());
      }

      private void btClose_Click(object sender, EventArgs e)
      {
         if (LoggedUserUtils.CanEditDBContent())
         {
            if (UnsavedChangesOnBoard &&
                !UserInteraction.CanContinue("Unsaved changes will be lost.\nDo you want to close form and loose all changes in action?"))
            {
               return;
            }
         }
         Close();
      }
   }
}
