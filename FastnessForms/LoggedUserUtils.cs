﻿
using Fastness.Common;
namespace FastnessForms
{
   static class LoggedUserUtils
   {
      private static Fastness.DataModel.User LoggedUser { get; set; }

      public static string UserName { get { return LoggedUser.Login; } }
      public static int UserRole { get { return LoggedUser.Role; } }

      public static void SetLoggedUser(Fastness.DataModel.User loggedUser)
      {
         LoggedUserUtils.LoggedUser = loggedUser;
      }

      public static bool CanEditDBContent()
      {
         return LoggedUser.Role == Fastness.DataModel.User.ADMIN || LoggedUser.Role == Fastness.DataModel.User.WRITER;
      }
      public static bool IsAdmin()
      {
         return LoggedUser.Role == Fastness.DataModel.User.ADMIN;
      }

      public static bool IsValidPassword(string plain)
      {
         return LoggedUser.IsValidPassword(plain);
      }
      public static bool LoginMatch(string loginToCheck)
      {
         return LoggedUser.Login == loginToCheck;
      }
   }
}
