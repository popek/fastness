﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace FastnessForms
{
   class Names
   {
      public static readonly string SESSION_FILE_NAME = "session.xml";
      public static readonly string ROOT = "SessionState";
      public static readonly string CONTRACTS_GRID_WIDHTS = "ContractsGridColumnsWidths";

      public static string SessionFilePath { get { return Path.Combine(Directory.GetCurrentDirectory(), Names.SESSION_FILE_NAME); } }
   }

   public interface ISessionStateBuilder
   {
      ISessionStateBuilder WithContractsGrid(DataGridView grid);
   
   }

   public class SessionStateLoader : ISessionStateBuilder
   {
      XElement stateNode;

      public SessionStateLoader()
      {
         this.stateNode = XElement.Load(Names.SessionFilePath);
      }

      public ISessionStateBuilder WithContractsGrid(DataGridView grid)
      {
         var widthsNode = stateNode.Element(Names.CONTRACTS_GRID_WIDHTS);

         foreach (DataGridViewColumn col in grid.Columns)
         {
            col.Width = Convert.ToInt32(widthsNode.Attribute(col.Name).Value);
         }
         return this;
      }
   }

   public class SessionStateSaver : ISessionStateBuilder
   {
      XElement stateNode = new XElement(Names.ROOT);

      public ISessionStateBuilder WithContractsGrid(DataGridView contractsGrid)
      {
         var widthsNode = new XElement(Names.CONTRACTS_GRID_WIDHTS);
         foreach (DataGridViewColumn col in contractsGrid.Columns)
         {
            widthsNode.SetAttributeValue(col.Name, col.Width);
         }
         stateNode.Add(widthsNode);

         return this;
      }

      public void Save()
      {
         stateNode.Save(Names.SessionFilePath);
      }
   }
}
