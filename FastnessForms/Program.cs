﻿using log4net.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Reflection;

namespace FastnessForms
{

   static class Program
   {
      // based on: http://stackoverflow.com/questions/6392031/how-to-check-if-another-instance-of-the-application-is-running answer 8
      public static bool AlreadyRunning()
      {
         var current = Process.GetCurrentProcess();
         var processes = Process.GetProcessesByName(current.ProcessName);

         //Loop through the running processes in with the same name 
         foreach (var process in Process.GetProcessesByName(current.ProcessName))
         {
            //Ignore the current process 
            if (process.Id != current.Id)
            {
               //Make sure that the process is running from the exe file. 
               if (Assembly.GetExecutingAssembly().Location.
                    Replace("/", "\\") == current.MainModule.FileName)

               {
                  //Return the other process instance.  
                  return true;

               }
            }
         }
         //No other instance was found, return null.  
         return false;
      }

      /// <summary>
      /// The main entry point for the application.
      /// </summary>
      [STAThread]
      static void Main()
      {
         if(AlreadyRunning())
         {
            Views.UserInteraction.ShowInfo("Fastness application is already running.\nUse apllication which is already running instead of new one.");
            return;
         }
         XmlConfigurator.Configure();

         Application.EnableVisualStyles();
         Application.SetCompatibleTextRenderingDefault(false);

         Fastness.DataModel.User user = null;
         new Views.UserLoginForm(loggedUser =>
         {
            user = loggedUser;
         })
         .ShowDialog();
         
         if (user == null)
         {
            return;
         }

         LoggedUserUtils.SetLoggedUser(user);

         Application.Run(new Views.MainScreen());
      }
   }
}
