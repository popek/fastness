﻿using Fastness.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastnessForms.FuncTests
{
   public class TestActionsDetails
   {
      public string Description { get; set; }
      public int ActionsDuringMonthCount { get; set; }
   }

   public class TestContractDetails
   {
      public string AccountRef { get; set; }
      public string Name { get; set; }
      public double FixedFee { get; set; }
      public int FixedUnits { get; set; }
   }

   public class TestDataInfo
   {
      public int ItemsAtTheBeginingOfTheMonth { get; set; }
      public List<TestActionsDetails> TestActions { get; set; }
      public List<TestContractDetails> TestContracts { get; set; }
      
      public TestDataInfo()
      {
         this.ItemsAtTheBeginingOfTheMonth = 30;

         using (var ctx = new fastnessEntities())
         {
            this.TestActions = 
            (from a in ctx.ActionTemplate
             where a.ServiceLevel == "deep"
             select new TestActionsDetails { Description = a.Description, ActionsDuringMonthCount = 10 })
            .ToList<TestActionsDetails>();
         }
         int auxCnt = 0;
         foreach (var ta in this.TestActions)
         {
            ta.ActionsDuringMonthCount += auxCnt++;
         }
         
         this.TestContracts = new List<TestContractDetails>();
         for (int i = 1; i < 11; ++i)
         {
            this.TestContracts.Add(new TestContractDetails
            {
               AccountRef = string.Format("A_R_{0}", i),
               Name = string.Format("company C{0} ltd", i),
               FixedFee = 13 + i,
               FixedUnits = 4 + i
            });         
         }
      }
   }
}
