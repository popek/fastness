﻿using Fastness.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FastnessForms.FuncTests
{
   public partial class TestInvoiceDataSetupForm : Form
   {
      TestDataInfo tdi;

      public TestInvoiceDataSetupForm(TestDataInfo tdi)
      {
         this.tdi = tdi;
         
         InitializeComponent();
      }

      private void TestInvoiceSetupForm_Load(object sender, EventArgs e)
      {
         this.testDataView.DataSource = this.tdi.TestActions;

         this.testContractsView.DataSource = this.tdi.TestContracts;

         this.itemsAtBeginingOfMonth.Value = tdi.ItemsAtTheBeginingOfTheMonth;
      }

      private void setupButton_Click(object sender, EventArgs e)
      {
         this.tdi.ItemsAtTheBeginingOfTheMonth = (int)this.itemsAtBeginingOfMonth.Value;

         Close();
      }
   }
}
