﻿using Fastness.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastnessForms.FuncTests
{
   public class InvoiceTestDataReloader
   {
      InvoiceTestDataLoader tdl;

      public InvoiceTestDataReloader(TestDataInfo tdi, DateTime invoiceDate)
      {
         tdl = new InvoiceTestDataLoader(tdi, invoiceDate);
      }

      public void Reload()
      {
         Clean();
         tdl.SetupTestData();
      }

      void Clean()
      {
         using (var ctx = new fastnessEntities())
         {
            ctx.Contract.RemoveRange(ctx.Contract);

            ctx.ContractAction.RemoveRange(ctx.ContractAction);

            ctx.ItemDetail.RemoveRange(ctx.ItemDetail);

            ctx.ActionHistory.RemoveRange(ctx.ActionHistory);

            ctx.InvoiceHistory.RemoveRange(ctx.InvoiceHistory);

            var miscParams = ctx.MiscParams.First();
            miscParams.NextInvoiceNo = 1;
            miscParams.VAT = 0.23;
            ctx.MiscParams.Attach(miscParams);
            ctx.SetAsModified(miscParams);

            ctx.SaveChanges();
         }
      }
   }
}
