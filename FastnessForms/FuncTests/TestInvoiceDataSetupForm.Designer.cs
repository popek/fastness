﻿namespace FastnessForms.FuncTests
{
   partial class TestInvoiceDataSetupForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.testDataView = new System.Windows.Forms.DataGridView();
         this.setupButton = new System.Windows.Forms.Button();
         this.itemsAtBeginingOfMonth = new System.Windows.Forms.NumericUpDown();
         this.testContractsView = new System.Windows.Forms.DataGridView();
         this.label1 = new System.Windows.Forms.Label();
         this.label2 = new System.Windows.Forms.Label();
         this.label3 = new System.Windows.Forms.Label();
         ((System.ComponentModel.ISupportInitialize)(this.testDataView)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.itemsAtBeginingOfMonth)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.testContractsView)).BeginInit();
         this.SuspendLayout();
         // 
         // testDataView
         // 
         this.testDataView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
         this.testDataView.Location = new System.Drawing.Point(15, 291);
         this.testDataView.Name = "testDataView";
         this.testDataView.Size = new System.Drawing.Size(453, 210);
         this.testDataView.TabIndex = 0;
         // 
         // setupButton
         // 
         this.setupButton.Location = new System.Drawing.Point(384, 530);
         this.setupButton.Name = "setupButton";
         this.setupButton.Size = new System.Drawing.Size(81, 35);
         this.setupButton.TabIndex = 1;
         this.setupButton.Text = "close";
         this.setupButton.UseVisualStyleBackColor = true;
         this.setupButton.Click += new System.EventHandler(this.setupButton_Click);
         // 
         // itemsAtBeginingOfMonth
         // 
         this.itemsAtBeginingOfMonth.Location = new System.Drawing.Point(15, 546);
         this.itemsAtBeginingOfMonth.Name = "itemsAtBeginingOfMonth";
         this.itemsAtBeginingOfMonth.Size = new System.Drawing.Size(73, 20);
         this.itemsAtBeginingOfMonth.TabIndex = 2;
         // 
         // testContractsView
         // 
         this.testContractsView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
         this.testContractsView.Location = new System.Drawing.Point(12, 25);
         this.testContractsView.Name = "testContractsView";
         this.testContractsView.Size = new System.Drawing.Size(453, 210);
         this.testContractsView.TabIndex = 3;
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(12, 9);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(90, 13);
         this.label1.TabIndex = 4;
         this.label1.Text = "test clients details";
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(12, 530);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(148, 13);
         this.label2.TabIndex = 5;
         this.label2.Text = "items at the begining of month";
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Location = new System.Drawing.Point(12, 275);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(102, 13);
         this.label3.TabIndex = 6;
         this.label3.Text = "client actions details";
         // 
         // TestInvoiceDataSetupForm
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(482, 579);
         this.Controls.Add(this.label3);
         this.Controls.Add(this.label2);
         this.Controls.Add(this.label1);
         this.Controls.Add(this.testContractsView);
         this.Controls.Add(this.itemsAtBeginingOfMonth);
         this.Controls.Add(this.setupButton);
         this.Controls.Add(this.testDataView);
         this.Name = "TestInvoiceDataSetupForm";
         this.Text = "TestInvoiceSetupForm";
         this.Load += new System.EventHandler(this.TestInvoiceSetupForm_Load);
         ((System.ComponentModel.ISupportInitialize)(this.testDataView)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.itemsAtBeginingOfMonth)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.testContractsView)).EndInit();
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.DataGridView testDataView;
      private System.Windows.Forms.Button setupButton;
      private System.Windows.Forms.NumericUpDown itemsAtBeginingOfMonth;
      private System.Windows.Forms.DataGridView testContractsView;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label3;
   }
}