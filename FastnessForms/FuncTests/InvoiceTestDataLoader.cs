﻿using Fastness.Common;
using Fastness.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FastnessForms.FuncTests
{
   public class InvoiceTestDataLoader
   {
      private TestDataInfo tdi;

      private DateTime invoiceDate;
      private Random rnd = new Random();

      int nextItemId = 0;

      List<ContractAction> _consignmentActions;
      List<ContractAction> ConsignmentActions 
      { 
         get 
         {
            if (_consignmentActions == null)
            {
               using (var ctx = new fastnessEntities())
               {
                  _consignmentActions = (from ca in ctx.ContractAction
                                         where ca.Description == ActionUtils.ACTION_DESCRIPTION_CONSIGNMENT
                                         select ca).ToList<ContractAction>();
               }
            }
            return _consignmentActions;
         } 
      }
      List<ContractAction> _retrieveActions;
      List<ContractAction> RetrieveActions
      {
         get
         {
            if (_retrieveActions == null)
            {
               using (var ctx = new fastnessEntities())
               {
                  _retrieveActions = (from ca in ctx.ContractAction
                                      where ca.Description == ActionUtils.ACTION_DESCRIPTION_RETRIEVAL
                                      select ca).ToList<ContractAction>();
               }
            }
            return _retrieveActions;
         }
      }

      public InvoiceTestDataLoader(TestDataInfo tdi, DateTime invoiceDate)
      {
         this.tdi = tdi;
         this.invoiceDate = invoiceDate;
      }

      public void SetupTestData()
      {
         foreach (var ctr in LoadContracts())
         {
            AddTestItemsForContract(ctr);

            AddTestActionsForContract(ctr);
         }
      }
      private DateTime PrevMonthConsignDate
      { 
         get 
         { 
            return new DateTime(
               invoiceDate.Month == 1 ? invoiceDate.Year -1 : invoiceDate.Year, 
               invoiceDate.Month == 1 ? 12 : invoiceDate.Month - 1,
               rnd.Next() % 27 + 1); 
         } 
      }
      private DateTime ActionDateCompleted 
      { 
         get 
         { 
            return new DateTime(
               invoiceDate.Year, 
               invoiceDate.Month,
               rnd.Next() % 27 + 1); 
         } 
      }

      private void AddInitialConsignmentHistoryEntry(ItemDetail item, fastnessEntities ctx)
      {
         var conAction = ConsignmentActions.Find(ca => ca.ContractID == item.ContractID);
         ctx.ActionHistory.Add(new ActionHistory
         {
            ItemNumber = item.ItemNumber,
            Description = conAction.Description,
            Date = item.DateRecieved,
            Charge = conAction.Charge,
            DateCompleted = item.DateRecieved,
            Percentage = item.Percentage,
            Location = item.Location,
            ContractActionID = conAction.ContractActionID
         });
      }
      private void AddInitialRetrieveHistoryEntry(ItemDetail item, fastnessEntities ctx)
      {
         var retAction = RetrieveActions.Find(ca => ca.ContractID == item.ContractID);
         ctx.ActionHistory.Add(new ActionHistory
         {
            ItemNumber = item.ItemNumber,
            Description = retAction.Description,
            Date = item.DateRecieved, // set as being retrieved the same day it has been consigned
            Charge = retAction.Charge,
            DateCompleted = item.DateRecieved,
            Percentage = item.Percentage,
            Location = item.Location,
            ContractActionID = retAction.ContractActionID
         });
      }

      private void AddTestItemsForContract(Contract ctr)
      {
         using (var ctx = new fastnessEntities())
         {
            for (var i = 0; i < this.tdi.ItemsAtTheBeginingOfTheMonth; ++i)
            {
               var item = CreateTestItemForContract(ctr, ctx);
               AddInitialConsignmentHistoryEntry(item, ctx);
            }

            ctx.SaveChanges();
         }
      }

      private void AddTestActionsForContract(Contract ctr)
      {
         using (var ctx = new fastnessEntities())
         {
            foreach (var ta in this.tdi.TestActions)
            {
               var ctrAction = (from ca in ctx.ContractAction
                                where ca.ContractID == ctr.ContractID && ca.Description == ta.Description
                                select ca)
                               .FirstOrDefault();

               var ctrItem = (from item in ctx.ItemDetail
                              where item.ContractID == ctr.ContractID
                              select item)
                             .FirstOrDefault();

               for (var i = 0; i < ta.ActionsDuringMonthCount; ++i)
               {
                  AddContractAction(ctx, ctr, ctrAction, ctrItem);
               }
            }

            ctx.SaveChanges();
         }
      }

      private void AddContractAction(fastnessEntities ctx, Contract ctr, ContractAction ctrAction, ItemDetail ctrItem)
      {
         var itemForAction = ctrItem;

         var actionDate = ActionDateCompleted;

         if (ctrAction.Description == ActionUtils.ACTION_DESCRIPTION_CONSIGNMENT)
         {
            itemForAction = CreateTestItemForContract(ctr, item => 
            {
               item.DateRecieved = actionDate;
            }, ctx);
         }
         else if (ctrAction.Description == ActionUtils.ACTION_DESCRIPTION_RECONSIGNMENT)
         {
            itemForAction = CreateTestItemForContract(ctr, item => 
            {
               item.DateReturned = actionDate;
            }, ctx);
            AddInitialConsignmentHistoryEntry(itemForAction, ctx); // if we are reconsigning then it must have a consign action before, and ...
            AddInitialRetrieveHistoryEntry(itemForAction, ctx); // retrieved action before
         }
         else if (ctrAction.Description == ActionUtils.ACTION_DESCRIPTION_SHREADING)
         {
            itemForAction = CreateTestItemForContract(ctr, item =>
            {
               item.Location = "";
               item.DateDestroyed = actionDate;
               item.InStock = false;
               item.Destroyed = true;
            }, ctx);
            AddInitialConsignmentHistoryEntry(itemForAction, ctx); // if we are shreading then it must have a consign action before
         }
         else if (ctrAction.Description == ActionUtils.ACTION_DESCRIPTION_RETRIEVAL ||
                  ctrAction.Description == ActionUtils.ACTION_DESCRIPTION_ONSITERETRIEVAL ||
                  ctrAction.Description == ActionUtils.ACTION_DESCRIPTION_EMERETRIEVAL)
         {
            itemForAction = CreateTestItemForContract(ctr, item =>
            {
               item.Location = "";
               item.DateRetrieved = actionDate;
               item.InStock = false;
            }, ctx);
            AddInitialConsignmentHistoryEntry(itemForAction, ctx); // if we are retrieving then it must have a consign action before
         }

         // add action if item is defined (items at the begining of month is > 0), or if this is "no item" action
         if (itemForAction != null ||
            ActionUtils.IsGenericWithoutItem(ctrAction.Code))
         {
            var itemNo = "0";
            var location = "";
            if (!ActionUtils.IsGenericWithoutItem(ctrAction.Code))
            {
               itemNo = itemForAction.ItemNumber;
               location = itemForAction.Location;
            }

            ctx.ActionHistory.Add(new ActionHistory
            {
               ItemNumber = itemNo,
               Description = ctrAction.Description,
               Date = actionDate,
               Charge = ctrAction.Charge,
               DateCompleted = actionDate,
               Percentage = 100,
               Location = location,
               ContractActionID = ctrAction.ContractActionID
            });
         }
      }

      private ItemDetail CreateTestItemForContract(Contract ctr, fastnessEntities ctx)
      {
         return CreateTestItemForContract(ctr, item => { }, ctx);
      }

      private ItemDetail CreateTestItemForContract(Contract ctr, Action<ItemDetail> itemVisitor, fastnessEntities ctx)
      {
         ItemDetail item = null;
         nextItemId++;
         item = new ItemDetail
         {
            Location = nextItemId.ToString("D8"),
            ContractID = ctr.ContractID,
            ItemNumber = string.Format("{0:D3}{1:D5}", ctr.ContractID, nextItemId),
            
            DateRecieved = PrevMonthConsignDate,
            InStock = true,
            Percentage = 100
         };

         itemVisitor(item);

         ctx.ItemDetail.Add(item);

         return item;
      }


      private List<Contract> LoadContracts()
      {
         using (var ctx = new fastnessEntities())
         {
            var nextCtrId = ctx.Contract.Count();

            var slTab = new string[] {"deep", "standard", "shallow"};

            foreach (var testCtr in this.tdi.TestContracts)
            {
               var a = "";
               for (var i = 0; i < 6; ++i)
               {
                  a += string.Format("inv.addrr. {1} ({0}){2}", nextCtrId, i, Environment.NewLine);
               }

               var aa = "";
               if (nextCtrId % 2 > 0)
               {
                  for (var i = 0; i < 5; ++i)
                  {
                     aa += string.Format("cl.addrr. {1} ({0}){2}", nextCtrId, i, Environment.NewLine);
                  }
               }

               var returnedThisMonth = this.tdi.TestActions.Sum(ta => {
                  if (ta.Description == ActionUtils.ACTION_DESCRIPTION_RETRIEVAL ||
                      ta.Description == ActionUtils.ACTION_DESCRIPTION_ONSITERETRIEVAL ||
                      ta.Description == ActionUtils.ACTION_DESCRIPTION_EMERETRIEVAL)
                     return ta.ActionsDuringMonthCount;
                  else
                     return 0;
               });
               var reconThisMonth = this.tdi.TestActions.Find(ta => ta.Description == ActionUtils.ACTION_DESCRIPTION_RECONSIGNMENT).ActionsDuringMonthCount;
               var destroyedThisMonth = this.tdi.TestActions.Find(ta => ta.Description == ActionUtils.ACTION_DESCRIPTION_SHREADING).ActionsDuringMonthCount;

               var newCtr = new Contract
               {
                  ContractID = nextCtrId,
                  AccountRef = testCtr.AccountRef.Substring(0, Math.Min(testCtr.AccountRef.Length, 8)),
                  Name = testCtr.Name,
                  Description = string.Format("some description for {0}", testCtr.Name),
                  ServiceLevel = slTab[nextCtrId % slTab.Length],
                  ConLastMonth = this.tdi.ItemsAtTheBeginingOfTheMonth + returnedThisMonth + destroyedThisMonth, // because on each retrieve, destroy additional item is generated so additional initial consignment is generated (on recon, there is initial con and initiali retrieve so it does not affect items count)
                  ConThisMonth = this.tdi.TestActions.Find(ta => ta.Description == ActionUtils.ACTION_DESCRIPTION_CONSIGNMENT).ActionsDuringMonthCount,
                  ReturnedThisMonth = returnedThisMonth,
                  ReConThisMonth = reconThisMonth,
                  DestroyedThisMonth = destroyedThisMonth,
                  FixedFee = testCtr.FixedFee,
                  FixedUnits = testCtr.FixedUnits,
                  FixedUnitsToDate = 0,
                  ContractFinished = false,
                  Address = a,
                  AuxiliaryAddress = aa,
                  EmailAddress = string.Format("dummy{0}@.mail.com", nextCtrId)
               };
               newCtr.Total = newCtr.ConLastMonth + newCtr.ConThisMonth + newCtr.ReConThisMonth - newCtr.ReturnedThisMonth - newCtr.DestroyedThisMonth;

               ctx.Contract.Add(newCtr);

               AddContractActions(newCtr, ctx);

               ++nextCtrId;
            }

            ctx.SaveChanges();
            return ctx.Contract.ToList<Contract>();
         }
         throw new Exception("something wrong while loading contarcts!");
      }

      private void AddContractActions(Contract ctr, fastnessEntities ctx)
      {
         var nextCtrActionId = ctx.ContractAction.Count();

         var actions = (from a in ctx.ActionTemplate
                        where a.ServiceLevel == ctr.ServiceLevel
                        select a);

         foreach (var a in actions)
         {
            ctx.ContractAction.Add(new ContractAction
            {
               ContractActionID = nextCtrActionId++,
               Description = a.Description,
               Code = a.Code,
               Charge = a.Charge,
               PerFreeAction = a.PerFreeAction,
               ContractID = ctr.ContractID,
            });
         }
      }
   }
}
