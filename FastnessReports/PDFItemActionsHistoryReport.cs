﻿using Fastness.Common;
using Fastness.DataModel;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;

namespace FastnessReports
{

   public class PDFItemActionsHistoryReport : PDFReportBase
   {
      //
      private class PDFReportSettings
      {
         public float PagetHeaderHeight { get { return 50; } }
         public float PagetFooterHeight { get { return 50; } }

         public float PageHorzMargin { get { return 50; } }

         public Font FooterFont { get; private set; }
         public Font TitleFont { get; private set; }
         public Font EntriesBoldFont { get; private set; }
         public Font EntriesFont { get; private set; }

         public float TitleToItemsSpace { get { return 100; } }

         public PDFReportSettings()
         {
            this.FooterFont = FontFactory.GetFont("Arial", 8, Font.NORMAL);
            this.TitleFont = FontFactory.GetFont("Arial", 20, Font.BOLD);
            this.EntriesBoldFont = FontFactory.GetFont("Arial", 10, Font.BOLD);
            this.EntriesFont = FontFactory.GetFont("Arial", 10, Font.NORMAL);
         }
      }
      //
      private class PageEventHelper : PdfPageEventHelper
      {
         PDFReportSettings settings;

         public PageEventHelper(PDFReportSettings settings)
         {
            this.settings = settings;
         }

         public override void OnEndPage(PdfWriter writer, Document document)
         {
            base.OnEndPage(writer, document);

            var rect = iTextSharp.text.PageSize.A4;
            var xPos = (rect.Left + rect.Right) / 2;

            var yPos = 14 + settings.FooterFont.Size;

            var pageNo = new Phrase(string.Format("{0}", writer.PageNumber), settings.FooterFont);

            ColumnText.ShowTextAligned(writer.DirectContent, Element.ALIGN_CENTER, pageNo, xPos, yPos, 0);
         }
      }

      PDFReportSettings settings;

      List<ActionHistory> actions;
      string headerText;

      public PDFItemActionsHistoryReport(List<ActionHistory> actions, string reportFilePath, string headerText)
      {
         this.actions = actions;
         this.headerText = headerText;

         settings = new PDFReportSettings();

         doc = new Document(iTextSharp.text.PageSize.A4,
            settings.PageHorzMargin,
            settings.PageHorzMargin,
            settings.PagetHeaderHeight,
            settings.PagetFooterHeight);

         writer = PdfWriter.GetInstance(doc, new FileStream(reportFilePath, FileMode.Create));
         writer.PageEvent = new PageEventHelper(settings);

         doc.Open();
      }

      public void Print()
      {
         AddHeader();
         AddActionsList();

         doc.Close();
      }
      void AddHeader()
      {
         var paragraph = new Paragraph(headerText, settings.TitleFont)
         {
            Alignment = Element.ALIGN_CENTER,
         };
         paragraph.SpacingAfter = settings.TitleToItemsSpace;
         doc.Add(paragraph);
      }
      void AddActionsList()
      {
         var itemsTable = new PdfPTable(4) { HorizontalAlignment = Element.ALIGN_CENTER };
         itemsTable.SpacingBefore = 0;
         itemsTable.SpacingAfter = 0;
         itemsTable.DefaultCell.Border = 0;
         itemsTable.DefaultCell.PaddingTop = 8;
         itemsTable.SetWidths(new int[] { 40, 180, 60, 90 });
         
         Action<string> addHeaderCell = (cellText) =>
         {
            itemsTable.AddCell(new PdfPCell(new Phrase(cellText, settings.EntriesBoldFont))
            {
               HorizontalAlignment = Element.ALIGN_LEFT,
               Border = 0,
               PaddingBottom = 14
            });
         };

         addHeaderCell("Index");
         addHeaderCell("Description");
         addHeaderCell("Charge");
         addHeaderCell("Date completed");

         var currencyFormatter = new CurrencyFormatter();
         int index = 1;
         foreach (var action in actions)
         {
            itemsTable.AddCell(new Phrase(index.ToString(), settings.EntriesFont));

            itemsTable.AddCell(new Phrase(action.Description, settings.EntriesFont));
            itemsTable.AddCell(new Phrase(currencyFormatter.Format(action.Charge), settings.EntriesFont));
            itemsTable.AddCell(new Phrase(DateUtils.ToString(action.DateCompleted), settings.EntriesFont));
            ++index;
         }
         doc.Add(itemsTable);
      }
   }
}
