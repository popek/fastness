﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;

namespace FastnessReports
{
   public class PDFReportBase : IDisposable
   {
      protected Document doc;
      protected PdfWriter writer;

      public void Dispose()
      {
         doc.Close();
         doc.Dispose();
      }
   }
}
