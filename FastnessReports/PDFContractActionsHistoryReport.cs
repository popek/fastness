﻿using Fastness.Common;
using Fastness.DataModel;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;

namespace FastnessReports
{

   public class PDFContractActionsHistoryReport : PDFReportBase
   {
      //
      private class PDFReportSettings
      {
         public float PagetHeaderHeight { get { return 50; } }
         public float PagetFooterHeight { get { return 50; } }

         public float PageHorzMargin { get { return 50; } }

         public Font FooterFont { get; private set; }
         public Font TitleFont { get; private set; }
         public Font EntriesBoldFont { get; private set; }
         public Font EntriesFont { get; private set; }

         public float TitleToItemsSpace { get { return 100; } }

         public PDFReportSettings()
         {
            this.FooterFont = FontFactory.GetFont("Arial", 8, Font.NORMAL);
            this.TitleFont = FontFactory.GetFont("Arial", 20, Font.BOLD);
            this.EntriesBoldFont = FontFactory.GetFont("Arial", 10, Font.BOLD);
            this.EntriesFont = FontFactory.GetFont("Arial", 10, Font.NORMAL);
         }
      }
      //
      private class PageEventHelper : PdfPageEventHelper
      {
         PDFReportSettings settings;

         public PageEventHelper(PDFReportSettings settings)
         {
            this.settings = settings;
         }

         public override void OnEndPage(PdfWriter writer, Document document)
         {
            base.OnEndPage(writer, document);

            var rect = iTextSharp.text.PageSize.A4;
            var xPos = (rect.Left + rect.Right) / 2;

            var yPos = 14 + settings.FooterFont.Size;

            var pageNo = new Phrase(string.Format("{0}", writer.PageNumber), settings.FooterFont);

            ColumnText.ShowTextAligned(writer.DirectContent, Element.ALIGN_CENTER, pageNo, xPos, yPos, 0);
         }
      }


      PDFReportSettings settings;

      List<ActionHistory> actions;
      Func<string, string> iuid2cuid;      
      string headerText;

      public PDFContractActionsHistoryReport(
         List<ActionHistory> actions, 
         Func<string, string> iuid2cuid, 
         string reportFilePath, 
         string headerText)
      {
         this.actions = actions;
         this.iuid2cuid = iuid2cuid;
         this.headerText = headerText;

         settings = new PDFReportSettings();

         doc = new Document(iTextSharp.text.PageSize.A4,
            settings.PageHorzMargin,
            settings.PageHorzMargin,
            settings.PagetHeaderHeight,
            settings.PagetFooterHeight);

         writer = PdfWriter.GetInstance(doc, new FileStream(reportFilePath, FileMode.Create));
         writer.PageEvent = new PageEventHelper(settings);

         doc.Open();
      }

      public void Print()
      {
         AddHeader();
         AddActionsList();

         doc.Close();
      }
      void AddHeader()
      {
         var paragraph = new Paragraph(headerText, settings.TitleFont)
         {
            Alignment = Element.ALIGN_CENTER,
         };
         paragraph.SpacingAfter = settings.TitleToItemsSpace;
         doc.Add(paragraph);
      }
      void AddActionsList()
      {
         var itemsTable = new PdfPTable(6) { HorizontalAlignment = Element.ALIGN_CENTER };
         itemsTable.SpacingBefore = 0;
         itemsTable.SpacingAfter = 0;
         itemsTable.DefaultCell.Border = 0;
         itemsTable.DefaultCell.PaddingTop = 8;
         itemsTable.SetWidths(new int[] { 40, 60, 90, 100, 60, 90 });
         
         Action<string> addHeaderCell = (cellText) =>
         {
            itemsTable.AddCell(new PdfPCell(new Phrase(cellText, settings.EntriesBoldFont))
            {
               HorizontalAlignment = Element.ALIGN_LEFT,
               Border = 0,
               PaddingBottom = 14
            });
         };

         addHeaderCell("Index");
         addHeaderCell("IUID");
         addHeaderCell("CUID");
         addHeaderCell("Action");
         addHeaderCell("Charge");
         addHeaderCell("Date");

         var currencyFormatter = new CurrencyFormatter();
         int index = 1;

         var addCell = new Action<string>(cellText => itemsTable.AddCell(new Phrase(cellText, settings.EntriesFont)));
         foreach (var action in actions)
         {
            addCell(index.ToString());

            if (action.ItemNumber == ActionUtils.ITEM_NUMBER_FOR_NO_ITEM_ACTION_HITORY)
            {
               addCell("");
               addCell("");
            }
            else 
            {
               addCell(action.ItemNumber);
               addCell(iuid2cuid(action.ItemNumber));
            }
            
            addCell(action.Description);
            addCell(currencyFormatter.Format(action.Charge));
            addCell(DateUtils.ToString(action.DateCompleted));

            ++index;
         }
         doc.Add(itemsTable);
      }
   }
}
