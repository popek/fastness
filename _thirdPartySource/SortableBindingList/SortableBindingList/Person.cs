using System;

namespace Be.Timvw.Demo.SortableBindingList
{
    public class Person
    {
        private int id;
        private string firstName;
        private string lastName;
        private DateTime birthday;
        private double score;

        public Person(int id, string firstName, string lastName, DateTime birthday, double score)
        {
            this.id = id;
            this.firstName = firstName;
            this.LastName = lastName;
            this.birthday = birthday;
            this.score = score;
        }

        public int Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        public string Firstname
        {
            get { return this.firstName; }
            set { this.firstName = value; }
        }

        public string LastName
        {
            get { return this.lastName; }
            set { this.lastName = value; }
        }

        public DateTime Birthday
        {
            get { return this.birthday; }
            set { this.birthday = value; }
        }

        public double Score
        {
            get { return this.score; }
            set { this.score = value; }
        }
    }
}
