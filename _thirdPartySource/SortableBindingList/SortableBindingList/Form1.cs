using System;
using System.Collections.Generic;
using System.Windows.Forms;

using Be.Timvw.Framework.ComponentModel;

namespace Be.Timvw.Demo.SortableBindingList
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            this.dataGridView1.AutoGenerateColumns = false;

            this.ColumnId.DataPropertyName = "Id";
            this.ColumnFirstName.DataPropertyName = "FirstName";
            this.ColumnLastName.DataPropertyName = "LastName";
            this.ColumnBirthday.DataPropertyName = "Birthday";
            this.ColumnScore.DataPropertyName = "Score";

            List<Person> list = new List<Person>();
            list.Add(new Person(1, "Tim", "4", new DateTime(1980, 4, 30), 100.1));
            list.Add(new Person(2, "Amy", "2", new DateTime(1983, 1, 1), 200.2));
            list.Add(new Person(3, "Sarah", "3", new DateTime(1984, 1, 24), 300.3));
            list.Add(new Person(4, "Mike", "1", new DateTime(1988, 3, 21), 400.4));

            SortableBindingList<Person> persons = new SortableBindingList<Person>(list);

            this.dataGridView1.DataSource = persons;
        }
    }
}