﻿using Fastness.DataModel;
using oldDbModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace portDB
{
   public class MiscConverter : TableConverter
   {
      // IMPORTANT: tbSterowanie is not loadable in code! (no primary key). Set next invoice no by hand
      protected override void ConvertInternal(fastnessEntities ctx, fastnessEntitiesOld ctxOld)
      {
         var nextInvoiceNo = (ctxOld.tbInvoiceHistory.ToList().LastOrDefault().invoiceNumber??0) + 1;

         var miscPar = new MiscParams { VAT = (double)(ctxOld.tbVAT.ToList().LastOrDefault().VAT ?? 0) / 10000, NextInvoiceNo = nextInvoiceNo };
         ctx.MiscParams.Add(miscPar);
      }
   }

}
