﻿using Fastness.Common;
using Fastness.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace portDB
{
   public class Program
   {
      static XmlObjectsRepository repository_ = new XmlObjectsRepository(CurrentDirUtils.GetSubdirPath("auxiliary_settings"));
      public static XmlObjectsRepository Repo { get { return repository_; } }

      static void Main(string[] args)
      {
         if (!Repo.Exists<ConstSettingsToPort>())
         {
            Repo.Update<ConstSettingsToPort>(new ConstSettingsToPort { 
               MailForInvoices = ""});
         }

         using (var ctx = new fastnessEntities())
         {
            ctx.Database.ExecuteSqlCommand("delete from Contract;");
            ctx.Database.ExecuteSqlCommand("delete from ActionTemplate;");
            ctx.Database.ExecuteSqlCommand("delete from InvoiceHistory;");
            ctx.Database.ExecuteSqlCommand("delete from MiscParams;");
            ctx.Database.ExecuteSqlCommand("delete from ContractAction;");
            ctx.Database.ExecuteSqlCommand("delete from ActionHistory;");
            ctx.Database.ExecuteSqlCommand("delete from ItemDetail;");

            ctx.Database.ExecuteSqlCommand("DBCC CHECKIDENT('ActionTemplate',RESEED,0);");
            ctx.Database.ExecuteSqlCommand("DBCC CHECKIDENT('InvoiceHistory',RESEED,0);");
            ctx.Database.ExecuteSqlCommand("DBCC CHECKIDENT('ContractAction',RESEED,0);");
            ctx.Database.ExecuteSqlCommand("DBCC CHECKIDENT('ActionHistory',RESEED,0);");
            ctx.Database.ExecuteSqlCommand("DBCC CHECKIDENT('ItemDetail',RESEED,0);");
         }

         foreach (var c in new List<TableConverter>{
            new ContractsConverter(),
            new ActionTypeConverter(),
            new ClientActionConverter(),
            new MadeActionsConverter(),
            new ItemConverter(),
            new InvoiceHistoryConverter(),
            new MiscConverter()
         })
         {
            c.Convert();
         }
      }
   }
}
