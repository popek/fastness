﻿using Fastness.DataModel;
using oldDbModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace portDB
{
   public class ContractsConverter : TableConverter
   {
      protected override void ConvertInternal(fastnessEntities ctx, fastnessEntitiesOld ctxOld)
      {
         // Pam reported that old DB has discrepancy (items total amount in contract stat does not match items count from items table). 
         // At this point there is no other way then manually fix such errors by modifying items total count in contract stats
         var manualDiscrepancyFixer = new Func<int, contract, int>((oldTotalCount, oldCtr) =>
         {
            if (oldCtr.ContractID == 67) // CP1
               return oldTotalCount + 2;
            else if (oldCtr.ContractID == 111) // OSL1
               return oldTotalCount + 1;
            else
               return oldTotalCount;
         });

         ctx.Contract.AddRange(ctxOld.contract.ToList().Select(oldCtr => new Fastness.DataModel.Contract 
            {
               ContractID = oldCtr.ContractID,
               AccountRef = oldCtr.AccountRef,
               Name = oldCtr.Name,
               ClientInvoiceRef = oldCtr.ClientInvoiceRef,
               ServiceLevel = oldCtr.ServiceLevel,
               ConLastMonth = manualDiscrepancyFixer((int)(oldCtr.ConLastMonth??0), oldCtr),
               ConThisMonth = (int)(oldCtr.ConThisMonth??0),
               ReturnedThisMonth = (int)(oldCtr.ReturnedThisMonth??0),
               ReConThisMonth = (int)(oldCtr.ReConThisMonth??0),
               DestroyedThisMonth = (int)(oldCtr.DestroyedThisMonth??0),
               Total = manualDiscrepancyFixer((int)(oldCtr.Total??0), oldCtr),
               FixedFee = (int)(oldCtr.FixedFee??0),
               FixedUnits = (int)(oldCtr.FixedUnits??0),
               FixedUnitsToDate = (int)(oldCtr.FixedUnitsToDate??0),
               ContractFinished = oldCtr.ContractFinished ?? false,
               Description = oldCtr.Description,
               Address = String.Join(
                  Environment.NewLine,
                  new string[] {
                     oldCtr.ClientAddress1, 
                     oldCtr.ClientAddress2, 
                     oldCtr.ClientAddress3, 
                     oldCtr.ClientAddress4, 
                     oldCtr.ClientAddress5, 
                     oldCtr.ClientAddress6}),
               AuxiliaryAddress = oldCtr.AuxiliaryAddress,
               EmailAddress = Program.Repo.Get<ConstSettingsToPort>().MailForInvoices
        
            }));

      }
   }

}
