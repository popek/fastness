﻿using System.Xml.Serialization;

namespace portDB
{
   // class containing auxiliary settings
   [XmlRoot("ConstSettingsToPort")]
   public class ConstSettingsToPort
   {
      [XmlAttribute("MailForInvoices")]
      public string MailForInvoices { get; set; }
   }
}
