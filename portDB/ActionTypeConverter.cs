﻿using Fastness.DataModel;
using oldDbModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace portDB
{
   public class ActionTypeConverter : TableConverter
   {
      protected override void ConvertInternal(fastnessEntities ctx, fastnessEntitiesOld ctxOld)
      {
         ctx.ActionTemplate.AddRange(ctxOld.actionType.ToList().Select(a => new Fastness.DataModel.ActionTemplate 
         {
            //ActionTemplateID = a.actionID, // not needed since DB will generate it
            ServiceLevel = a.ServiceLevel,
            Code = a.Code,
            Description = a.Description,
            Note = a.note,
            Charge = Helper.RoundCharge(a.charge ?? 0),
            PerFreeAction = a.PerFreeAction??0
         }));
      }
   }

}
