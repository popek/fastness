﻿using Fastness.DataModel;
using oldDbModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace portDB
{
   public class MadeActionsConverter : TableConverter
   {
      protected override void ConvertInternal(fastnessEntities ctx, fastnessEntitiesOld ctxOld)
      {
         var ctr2ctrActions = new Dictionary<int, List<ContractAction>>();

         // map new DB contract actions
         foreach (var ctrAction in ctx.ContractAction)
         { 
            if(ctr2ctrActions.Where(kv => kv.Key == ctrAction.ContractID).Count() == 0)
            {
               ctr2ctrActions[ctrAction.ContractID] = (from ca in ctx.ContractAction where ca.ContractID == ctrAction.ContractID select ca).ToList();
            }
         }

         var madeAction2contractAction = new Dictionary<int, long>();
         
         // action with such description is corrupted action which was amended manualy!
         var oldMadeActions = ctxOld.tbMadeActions.Where(ma => ma.Description != "***** Action Deleted *****").ToList();

         foreach(var madeAction in oldMadeActions)
         {

            // error catched during porting latest version of db. Single action which contract should be 34 had ctr id 0. Why? don't know.
            if (madeAction.ContractID.HasValue && madeAction.ContractID.Value == 0)
            {
               madeAction.ContractID = 34;
            }

            var ctrActions = ctr2ctrActions[madeAction.ContractID??0];

            var newDbContractAction = 
               ctrActions
               .Where(ca => (ca.Description == madeAction.Description || 
                             ca.Code.Substring(2) == madeAction.Type.Substring(2)))
               .FirstOrDefault();

            if (newDbContractAction == null)
            {
               throw new Exception(String.Format("contract action not found in new DB! (description: {0})", madeAction.Description));
            }

            if (madeAction.Type != newDbContractAction.Code)
            {
               Console.WriteLine("inconsistency! (code MUST be the same: {0} != {1})", madeAction.Type, newDbContractAction.Code);
               //throw new Exception("inconsistency! (code MUST be the same)");
            }
            madeAction2contractAction.Add(madeAction.ActionID, newDbContractAction.ContractActionID);
         }

         ctx.ActionHistory.AddRange(oldMadeActions.ToList().Select(a => 
            {
               var newActionHistory = new Fastness.DataModel.ActionHistory
               {
                  //ActionHistoryID = a.ActionID, // not needed since DB generates it
                  ItemNumber = a.ItemID == null ? "0" : a.ItemID,
                  Description = a.Description,
                  Date = a.Date,
                  DateCompleted = a.DateCompleted,
                  Notes = a.Notes == null ? "" : a.Notes,
                  Charge = Helper.RoundCharge(a.Charge ?? 0),
                  InvoiceNumber = a.InvoiceNumber,
                  AuthorisedBy = a.AuthorisedBy == null ? "" : a.AuthorisedBy,
                  Location = a.Location == null ? "" : a.Location,
                  Percentage = a.Percentage,
                  LastAmended = a.LastAmended,
                  ContractActionID = madeAction2contractAction[a.ActionID],
                  Invoiced = a.Invoiced??false
               };

               correct3DigitsConReconChargeForAugustAndLaterIfRequired(newActionHistory, a);
               return newActionHistory;
            }));
      }

      void correct3DigitsConReconChargeForAugustAndLaterIfRequired(ActionHistory ah, tbMadeActions oldAction)
      {
         if (oldAction.ContractID == 67 ||   // CP1
             oldAction.ContractID == 105 ||  // CP-FIN 
             oldAction.ContractID == 109)    // CP-HR
         {
            var actionDate = (oldAction.Date??new DateTime());
            if ((actionDate.Year == 2015 && actionDate.Month >= 8) || actionDate.Year > 2015)
            {
               if (oldAction.Type == "DP-COL" || oldAction.Type == "DP-STORE")
               {
                  ah.Charge = 0.155; // change from 0.16 to 0.155
               }
            }
         }
      }
   }

}
