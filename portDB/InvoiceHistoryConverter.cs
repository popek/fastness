﻿using Fastness.Common;
using Fastness.DataModel;
using Fastness.Invoicing;
using oldDbModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace portDB
{
   public class InvoiceHistoryConverter : TableConverter
   {
      private double GetVAT(int year, fastnessEntitiesOld ctxOld)
      {
         // entires in reverse order
         var vatEntries = ctxOld.tbVAT.ToList().OrderBy(v => v.ID_FirstYear).Reverse().ToList();
         foreach (var oldVatEntry in vatEntries)
         {
            if (year >= oldVatEntry.ID_FirstYear)
            {
               return (double)(oldVatEntry.VAT ?? 0) / 10000;
            }
         }
         return (double)(vatEntries.Last().VAT ?? 0) / 10000;
      }


      protected override void ConvertInternal(fastnessEntities ctx, fastnessEntitiesOld ctxOld)
      {
         var oldInvoiceEntries = 
            (from oi in ctxOld.tbInvoiceHistory
               group oi by oi.invoiceNumber into oiEntries
               select new { InvoiceNo = oiEntries.Key.Value, Entries = oiEntries.AsEnumerable() })
            .ToList()
            .Select(entryExt => entryExt.Entries.Aggregate(null, (Invoice invoice, tbInvoiceHistory entry) =>
            {
               if (invoice == null)
               {
                  invoice = new Invoice();
                  invoice.ContractID = entry.contractID ?? 0;
                  invoice.Date = new DateTime(entry.Year ?? 0, entry.Month ?? 0, DateTime.DaysInMonth(entry.Year ?? 0, entry.Month ?? 0));
                  invoice.InvoiceNo = entry.invoiceNumber ?? 0;
                  invoice.Notes = entry.notes;
               }

               if (invoice.ContractID != entry.contractID)
               {
                  System.Console.WriteLine("invoice history error: invoice no: {0}, exists for 2 contracts {1} {2}", entry.invoiceNumber, invoice.ContractID, entry.contractID);
                  return invoice;
                  //throw new Exception("suppose to be the same contract when invoice entires area from single invoice!");
               }
               if (invoice.Notes != entry.notes)
               {
                  throw new Exception("suppose to be the same notes when invoice entires area from single invoice!");
               }

               invoice.Entries.Add(new Invoice.Entry
               {
                  Description = entry.typeOfAction,
                  Quantity = entry.amountOfActions??0,
                  Charge = entry.chargeForAllActions??0
               });

               return invoice;
            }));

         var collector = new InvoiceHistoryCollector();
         foreach (var i in oldInvoiceEntries)
         {
            i.SetupTotalChargeAndTax(GetVAT(i.Date.Year, ctxOld));
            collector.Collect(i);
         }
         ctx.InvoiceHistory.AddRange(collector.Entries);
      }
   }

}
