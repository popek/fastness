﻿using Fastness.DataModel;
using oldDbModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace portDB
{
   public class ItemConverter : TableConverter
   {
      private string ConvertDestructionDate(string oldDestructionDate)
      {
         if (string.IsNullOrEmpty(oldDestructionDate))
         {
            return oldDestructionDate;
         }

         var dt = new DateTime();
         if (DateTime.TryParseExact(oldDestructionDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt))
         {
            return dt.ToString(Fastness.DataModel.ItemDetail.DestructionDateFormatDDMMYYYY());
         }
         if (DateTime.TryParseExact(oldDestructionDate, "yyyy-MM", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt))
         {
            return dt.ToString(Fastness.DataModel.ItemDetail.DestructionDateFormatMMYYYY());
         }
         if (DateTime.TryParseExact(oldDestructionDate, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt))
         {
            return dt.ToString(Fastness.DataModel.ItemDetail.DestructionDateFormatYYYY());
         }

         Console.WriteLine("old destruction date: {0} is invalid!", oldDestructionDate);
         return oldDestructionDate;
      }

      protected override void ConvertInternal(fastnessEntities ctx, fastnessEntitiesOld ctxOld)
      {
         ctx.ItemDetail.AddRange(ctxOld.ItemDetail.ToList().Select(a => new Fastness.DataModel.ItemDetail
         {
            //ItemID = a.ItemID, // not needed since DB will add it
            ItemNumber = a.ItemNumber,
            ContractID = a.ContractID??0,
            ClientItemRef = a.ClientItemRef,
            Description = a.Description,
            DestructionDate = ConvertDestructionDate(a.DestructionDate),
            Type = a.Typpe,
            Percentage = a.Percentage.HasValue ? new System.Nullable<int>((int)a.Percentage.Value) : null,
            DateRecieved = a.DateRecieved,
            DateDestroyed = a.DateDestroyed,
            DateRetrieved = a.DateRetrieved,
            DateReturned = a.DateReturned,
            Destroyed = a.Destroyed,
            Location = a.Location,
            InStock =  a.InStock
         }));
      }
   }

}
