﻿using Fastness.DataModel;
using oldDbModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace portDB
{
   public abstract class TableConverter
   {
      public void Convert()
      {
         var name = this.GetType().Name;
         Console.WriteLine(string.Format(">>>> About to convert {0}", name));

         using (var ctxOld = new fastnessEntitiesOld())
         {
            using (var ctx = new fastnessEntities())
            {
               ctx.Configuration.AutoDetectChangesEnabled = false;

               ConvertInternal(ctx, ctxOld);

               ctx.Configuration.AutoDetectChangesEnabled = true;

               ctx.ForceDetectChanges();
               ctx.SaveChanges();
            }
         }
      }

      protected abstract void ConvertInternal(fastnessEntities ctc, fastnessEntitiesOld ctxOld);
   }
}
