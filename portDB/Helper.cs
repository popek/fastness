﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace portDB
{
   public static class Helper
   {
      public static double RoundCharge(double charge)
      {
         var two = Math.Round(charge, 2);
         var three = Math.Round(charge, 3);

         return Math.Min(two, three);
      }
   }
}
