﻿using Fastness.DataModel;
using oldDbModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace portDB
{
   public class ClientActionConverter : TableConverter
   {
      protected override void ConvertInternal(fastnessEntities ctx, fastnessEntitiesOld ctxOld)
      {
         ctx.ContractAction.AddRange(ctxOld.clientAction.ToList().Select(a => new Fastness.DataModel.ContractAction 
         {
            //ContractActionID = a.clientActionID, // not important since DB will generate this number
            Description = a.Description,
            Code = a.Code,
            Charge = Helper.RoundCharge(a.charge ?? 0),
            PerFreeAction = a.PerFreeAction??0,
            ContractID = a.clientID??0
         }));
      }
   }

}
