﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.events;

public partial class CreatePDFWithFormFields : System.Web.UI.Page
{
    protected void btnCreatePDF_Click(object sender, EventArgs e)
    {
        // Create a Document object
        var document = new Document(PageSize.A4, 50, 50, 25, 25);

        // Create a new PdfWrite object, writing the output to the file ~/PDFTemplate/SimpleFormFieldDemo.pdf
        var output = new FileStream(Server.MapPath("~/PDFTemplate/SimpleFormFieldDemo.pdf"), FileMode.Create);
        var writer = PdfWriter.GetInstance(document, output);

        // Open the Document for writing
        document.Open();

        var table = new PdfPTable(2);
        table.DefaultCell.Border = 0;
        

        // *** Name row...
        table.AddCell("Enter your name:");
        var nameField = new TextField(writer, new Rectangle(0, 0, 200, 10), "name");
        nameField.BackgroundColor = Color.YELLOW;

        var nameCell = new PdfPCell();
        nameCell.CellEvent = new FieldPositioningEvents(writer, nameField.GetTextField());
        table.AddCell(nameCell);
        // ****************

        // *** Age row...
        table.AddCell("Enter your age:");
        var ageField = new TextField(writer, new Rectangle(0, 0, 50, 10), "age");
        ageField.BackgroundColor = Color.YELLOW;

        var ageCell = new PdfPCell();
        ageCell.CellEvent = new FieldPositioningEvents(writer, ageField.GetTextField());
        table.AddCell(ageCell);
        // ****************

        document.Add(table);

        document.Close();

        pnlViewPDF.Visible = true;
        ClientScript.RegisterStartupScript(this.GetType(), "blah", "alert('PDF created!');", true);
    }
}