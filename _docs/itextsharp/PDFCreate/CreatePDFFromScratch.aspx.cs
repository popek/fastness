﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

public partial class CreatePDFFromScratch : System.Web.UI.Page
{
    protected void btnCreatePDF_Click(object sender, EventArgs e)
    {
        // Create a Document object
        var document = new Document(PageSize.A4, 50, 50, 25, 25);

        // Create a new PdfWrite object, writing the output to a MemoryStream
        var output = new MemoryStream();
        var writer = PdfWriter.GetInstance(document, output);

        // Open the Document for writing
        document.Open();

        // First, create our fonts... (For more on working w/fonts in iTextSharp, see: http://www.mikesdotnetting.com/Article/81/iTextSharp-Working-with-Fonts
        var titleFont = FontFactory.GetFont("Arial", 18, Font.BOLD);
        var subTitleFont = FontFactory.GetFont("Arial", 14, Font.BOLD);
        var boldTableFont = FontFactory.GetFont("Arial", 12, Font.BOLD);
        var endingMessageFont = FontFactory.GetFont("Arial", 10, Font.ITALIC);
        var bodyFont = FontFactory.GetFont("Arial", 12, Font.NORMAL);

        // Add the "Northwind Traders Receipt" title
        document.Add(new Paragraph("Northwind Traders Receipt", titleFont));

        // Now add the "Thank you for shopping at Northwind Traders. Your order details are below." message
        document.Add(new Paragraph("Thank you for shopping at Northwind Traders. Your order details are below.", bodyFont));
        document.Add(Chunk.NEWLINE);

        // Add the "Order Information" subtitle
        document.Add(new Paragraph("Order Information", subTitleFont));

        // Create the Order Information table - see http://www.mikesdotnetting.com/Article/86/iTextSharp-Introducing-Tables for more info
        var orderInfoTable = new PdfPTable(2);
        orderInfoTable.HorizontalAlignment = 0;
        orderInfoTable.SpacingBefore = 10;
        orderInfoTable.SpacingAfter = 10;
        orderInfoTable.DefaultCell.Border = 0;

        orderInfoTable.SetWidths(new int[] { 1, 4 });
        orderInfoTable.AddCell(new Phrase("Order:", boldTableFont));
        orderInfoTable.AddCell(txtOrderID.Text);
        orderInfoTable.AddCell(new Phrase("Price:", boldTableFont));
        orderInfoTable.AddCell(Convert.ToDecimal(txtTotalPrice.Text).ToString("c"));

        document.Add(orderInfoTable);



        // Add the "Items In Your Order" subtitle
        document.Add(new Paragraph("Items In Your Order", subTitleFont));

        // Create the Order Details table
        var orderDetailsTable = new PdfPTable(3);
        orderDetailsTable.HorizontalAlignment = 0;
        orderDetailsTable.SpacingBefore = 10;
        orderDetailsTable.SpacingAfter = 35;
        orderDetailsTable.DefaultCell.Border = 0;

        orderDetailsTable.AddCell(new Phrase("Item #:", boldTableFont));
        orderDetailsTable.AddCell(new Phrase("Item Name:", boldTableFont));
        orderDetailsTable.AddCell(new Phrase("Qty:", boldTableFont));

        foreach (System.Web.UI.WebControls.ListItem item in cblItemsPurchased.Items)
            if (item.Selected)
            {
                // Each CheckBoxList item has a value of ITEMNAME|ITEM#|QTY, so we split on | and pull these values out...
                var pieces = item.Value.Split("|".ToCharArray());
                orderDetailsTable.AddCell(pieces[1]);
                orderDetailsTable.AddCell(pieces[0]);
                orderDetailsTable.AddCell(pieces[2]);
            }

        document.Add(orderDetailsTable);


        // Add ending message
        var endingMessage = new Paragraph("Thank you for your business! If you have any questions about your order, please contact us at 800-555-NORTH.", endingMessageFont);
        endingMessage.SetAlignment("Center");
        document.Add(endingMessage);

        // Finally, add an image in the upper right corner
        var logo = iTextSharp.text.Image.GetInstance(Server.MapPath("~/Images/4guysfromrolla.gif"));
        logo.SetAbsolutePosition(440, 800);
        document.Add(logo);

        document.Close();

        Response.ContentType = "application/pdf";
        Response.AddHeader("Content-Disposition", string.Format("attachment;filename=Receipt-{0}.pdf", txtOrderID.Text));
        Response.BinaryWrite(output.ToArray());
    }
}