﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2>Creating PDFs with iTextSharp</h2>
    <p>
        This demo shows how to use ASP.NET and <a href="http://sourceforge.net/projects/itextsharp/">iTextSharp</a> to programmatically 
        create PDF documents from an ASP.NET website.
    </p>
    <ul>
        <li><a href="CreatePDFFromScratch.aspx"><b>Programmatically Create a PDF</b></a> - this demo shows how to programmatically create a PDF using
        iTextSharp.</li>
        <li><a href="ConvertHTMLtoPDF.aspx"><b>Convert HTML to PDF</b></a> - this demo shows how to use iTextSharp's HTML --&gt; PDF translation capabilities
        to create a PDF from an HTML document.</li>
        <li><a href="CreatePDFWithFormFields.aspx"><b>Create a PDF with Form Fields</b></a> - this demo shows how to create a new PDF from scratch
        that contains form fields. These form fields can then be read and populated using the code examined in the article
        <a href="http://www.4guysfromrolla.com/articles/030211-1.aspx">Filling in PDF Forms with ASP.NET and iTextSharp</a>.</li>
    </ul>
    <p>
        Happy Programming!
    </p>
    <blockquote>
        <b>Scott Mitchell</b><br />
        <a href="mailto:mitchell@4guysfromrolla.com">mitchell@4guysfromrolla.com</a><br />
        <a href="http://www.4guysfromrolla.com/ScottMitchell.shtml">www.4guysfromrolla.com/ScottMitchell</a><br />
        <a href="http://scottonwriting.net/sowBlog/">My Blog</a>
    </blockquote>
</asp:Content>

