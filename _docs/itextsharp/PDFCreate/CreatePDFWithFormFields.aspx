﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="CreatePDFWithFormFields.aspx.cs" Inherits="CreatePDFWithFormFields" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1>Create a PDF With Form Fields</h1>
    <p>
        The following demo shows how to programmatically create a PDF that contains form fields using iTextSharp.
        A PDF with form fields can have these fields dynamically filled, which we examined in
        <a href="http://www.4guysfromrolla.com/articles/030211-1.aspx">Filling in PDF Forms with ASP.NET and iTextSharp</a>.
    </p>
    <p>
        Clicking the "Create the PDF!" button below will create a new PDF in the folder <code>~/PDFTemplate</code> named
        <code>SimpleFormFieldDemo.pdf</code>. Download the sample application from 
        <i>Filling in PDF Forms with ASP.NET and iTextSharp</i> to see how to programmatically read (and populate) the form
        fields in this PDF!
    </p>
    <p>
        <asp:Button ID="btnCreatePDF" runat="server" Text="Create the PDF!" 
            onclick="btnCreatePDF_Click" />
    </p>
    <asp:Panel runat="server" ID="pnlViewPDF" Visible="false">
        <asp:HyperLink ID="lnkPDF" runat="server" 
            NavigateUrl="~/PDFTemplate/SimpleFormFieldDemo.pdf" Target="_blank">View <code>SimpleFormFieldDemo.pdf<code></code></code></asp:HyperLink>
    </asp:Panel>
</asp:Content>

