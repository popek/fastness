//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace oldDbModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class clientAction
    {
        public int clientActionID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public Nullable<float> charge { get; set; }
        public Nullable<int> actionID { get; set; }
        public Nullable<int> clientID { get; set; }
        public string ServiceLevel { get; set; }
        public Nullable<float> PerFreeAction { get; set; }
        public string note { get; set; }
        public byte[] SSMA_TimeStamp { get; set; }
    }
}
