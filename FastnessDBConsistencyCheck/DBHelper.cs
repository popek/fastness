﻿using System;
using Fastness.DataModel;

namespace FastnessDBConsistencyCheck
{
   public class DBHelper
   {
      static public void WithFreshDb(Action<fastnessEntities> action)
      {
         using (var dbCtx = new fastnessEntities())
         {
            dbCtx.Configuration.AutoDetectChangesEnabled = false;
            action(dbCtx);
         }
      }
      static public T WithFreshDb<T>(Func<fastnessEntities, T> func)
      {
         using (var dbCtx = new fastnessEntities())
         {
            dbCtx.Configuration.AutoDetectChangesEnabled = false;
            return func(dbCtx);
         }
      }
   }
}
