﻿using System;
using System.Text;
using Fastness.Common;
using Fastness.DataModel;
using FastnessDBConsistencyCheck.Checker;
using log4net;

namespace FastnessDBConsistencyCheck
{
   class Program
   {
      private static readonly string REPOSITORY_SUBDIR = "configs";
      static readonly ILog log = LogManager.GetLogger(typeof(Program));

      static void Main(string[] args)
      {
         log4net.Config.XmlConfigurator.Configure();

         var start = DateTime.Now;

         var repository = new XmlObjectsRepository(CurrentDirUtils.GetSubdirPath(REPOSITORY_SUBDIR));
         if (!repository.Exists<FastnessDBConsistencyCheckSettings>())
         {
            throw new InvalidOperationException("Missing consistency checker config object");
         }

         var checkers = new ConsistencyChecker[] 
         {
            new ItemConsistencyChecker(),
            new ItemsCountConsistencyChecker()
         };

         var sb = new StringBuilder();
         foreach (var checker in checkers)
         {
            checker.ErrorHandler = (format, sargs) =>
            {
               sb.AppendFormat(format, sargs);
               sb.Append("\n");
            };
            checker.Run();
         }
         var checkTime = DateTime.Now - start;
         if(sb.Length > 0)
         {
            sb.AppendFormat("\n\nchecking db consistency took {0}\n", checkTime);
            SendDbConsistencyErrorReport(sb.ToString(), repository.Get<FastnessDBConsistencyCheckSettings>());
         }
         else
         {
            log.InfoFormat("there is no Db consistency error. Check took {0}", checkTime);
         }
      }

      public static void SendDbConsistencyErrorReport(string report, FastnessDBConsistencyCheckSettings settings)
      {
         var mailSubject = "Fastness DB consistency error report";
         SmtpClientSettings smtpSettings = null;
         try
         {
            var repository = new XmlObjectsRepository(CurrentDirUtils.GetSubdirPath(REPOSITORY_SUBDIR));
            if (repository.Exists<SmtpClientSettings>())
            {
               smtpSettings = repository.Get<SmtpClientSettings>();
            }
            else
            {
               log.ErrorFormat("smtp client file does not exist. Db consistency error report won't be send.\nReport: {0}",
                  report);
               return;
            }
         }
         catch (Exception e)
         {
            log.ErrorFormat("failed to load smtp client settings {0}", e);
            return;
         }

         try
         {
            log.Debug("trying to send mail....");

            using (var sender = new MailSender(smtpSettings))
            {
               sender.SendMail(
                  settings.SenderAddress,
                  settings.DestinationAddresses,
                  settings.CCAddresses,
                  mailSubject,
                  report,
                  null);
            }
         }
         catch (Exception mailSendError)
         {
            log.ErrorFormat("sending mail failed. Reason:\n {0}.\nDb consistency error report wasn't send.\nReport:\n {1}",
               mailSendError.Message, report);
         }
      }
   }
}
