﻿using System.Xml.Serialization;

namespace FastnessDBConsistencyCheck
{
   [XmlRoot("FastnessDBConsistencyCheckSettings")]
   public class FastnessDBConsistencyCheckSettings
   {
      [XmlElement("SenderAddress")]
      public string SenderAddress { get; set; }

      [XmlArray("DestinationAddresses")]
      public string[] DestinationAddresses { get; set; }

      [XmlArray("CCAddresses")]
      public string[] CCAddresses { get; set; }

   }
}
