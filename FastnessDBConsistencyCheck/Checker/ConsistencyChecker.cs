﻿using Fastness.DataModel;

namespace FastnessDBConsistencyCheck.Checker
{
   public abstract class ConsistencyChecker
   {
      public DbConsistencyErrorEventHandler ErrorHandler { protected get; set; }

      public abstract void Run();
   }
}
