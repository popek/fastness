﻿using System.Text;
using Fastness.DataModel;

namespace FastnessDBConsistencyCheck.Checker
{
   class ItemConsistencyChecker : ConsistencyChecker
   {
      public override void Run()
      {
         DBHelper.WithFreshDb(dbCtx => RunInternal(dbCtx));
      }
      private void RunInternal(fastnessEntities dbCtx)
      {
         foreach(var item in dbCtx.ItemDetail)
         {
            var sb = new StringBuilder();

            if(item.InStock ?? true)
            {
               if(item.DateRetrieved.HasValue)
               {
                  sb.AppendFormat("- in-stock item has retrieved date {0}\n", item.DateRetrieved.Value);
               }
               if (item.DateDestroyed.HasValue)
               {
                  sb.AppendFormat("- in-stock item has destroyed date {0}\n", item.DateDestroyed.Value);
               }
               if(item.Destroyed ?? false)
               {
                  sb.Append("- in-stock item is marked as destroyed\n");
               }
            }
            else
            {
               if (item.DateReturned.HasValue)
               {
                  sb.AppendFormat("- out-of-stock item has reconsign date {0}\n", item.DateReturned.Value);
               }
            }

            if(item.DateReturned.HasValue && item.DateRetrieved.HasValue)
            {
               sb.AppendFormat("- item has recon({0}), and retrieve({1}) dates set\n", item.DateReturned.Value, item.DateRetrieved.Value);
            }
            if (item.DateReturned.HasValue && item.DateDestroyed.HasValue)
            {
               sb.AppendFormat("- item has recon({0}), and destroy({1}) dates set\n", item.DateReturned.Value, item.DateDestroyed.Value);
            }
            if (item.DateRetrieved.HasValue && item.DateDestroyed.HasValue)
            {
               sb.AppendFormat("- item has retrieve({0}), and destroy({1}) dates set\n", item.DateRetrieved.Value, item.DateDestroyed.Value);
            }
            if (item.DateDestroyed.HasValue && !(item.Destroyed ?? false))
            {
               sb.AppendFormat("- item is not destroyed, and has destroy({0}) date set\n", item.DateDestroyed.Value);
            }
            if(sb.Length > 0)
            {
               ErrorHandler("[{0}]:\n{1}", item.ItemNumber, sb.ToString());
            }
         }
      }
   }
}
