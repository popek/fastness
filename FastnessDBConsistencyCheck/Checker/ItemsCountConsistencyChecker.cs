﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fastness.Common;
using Fastness.DataModel;
using Fastness.DataModel.Utils;

namespace FastnessDBConsistencyCheck.Checker
{
   public class ItemsCountConsistencyChecker : ConsistencyChecker
   {
      private DateTime dateWithMonthBeingChecked;

      public override void Run()
      {
         var lastMonth = DateUtils.LastMonthDate;
         var lastMonthInvoicesAlreadyGenerated = DBHelper.WithFreshDb(dbCtx =>
            {
               return new AskDb(dbCtx).AreInvoicesAlreadyGenerated(lastMonth.Year, lastMonth.Month);
            });

         // when last month invoice is not generated then contract table stats. shows still last month stats 
         // so they need to be checked against last month actions in actions history
         dateWithMonthBeingChecked = lastMonthInvoicesAlreadyGenerated
            ? DateTime.Today
            : lastMonth;

         foreach (var ctr in DBHelper.WithFreshDb(dbCtx => { return dbCtx.Contract.Where(ctr => !ctr.ContractFinished).ToArray(); }))
         {
            DBHelper.WithFreshDb(dbCtx => CheckContract(ctr, dbCtx));
         }
      }

      private void CheckContract(Contract ctr, fastnessEntities dbCtx)
      {
         var sb = new StringBuilder();

         var itemsCount = new FetchFromDb(dbCtx).InStockContractItems(ctr).Count();
         if (itemsCount != ctr.Total)
         {
            sb.AppendFormat("- in-stock items count({0}) does not match 'total'({1}) in contract\n",
               itemsCount, ctr.Total);
         }

         var ctrHistory = from ah in dbCtx.ActionHistory
                          join ca in dbCtx.ContractAction on ah.ContractActionID equals ca.ContractActionID
                          where ah.DateCompleted >= FirstDay && ah.DateCompleted <= LastDay && ca.ContractID == ctr.ContractID
                          select new
                          {
                             ContractActionID = ca.ContractActionID,
                             Description = ca.Description
                          };

         var stats = (from ah in ctrHistory
                     group ah by ah.ContractActionID into actionsGroup
                     select new
                     {
                        Description = actionsGroup.FirstOrDefault().Description,
                        Amount = actionsGroup.Count()
                     }).ToArray();

         var actionsCount = new Func<string, int>(description => 
         {
            return (from stat in stats where stat.Description == description select stat.Amount).FirstOrDefault();
         });

         var conCount = actionsCount(ActionUtils.ACTION_DESCRIPTION_CONSIGNMENT);
         if(conCount != ctr.ConThisMonth)
         {
            sb.AppendFormat("- consigned this month({0}) does not match 'consigned this month'({1}) in contract\n",
               conCount, ctr.ConThisMonth);
         }

         var reconCount = actionsCount(ActionUtils.ACTION_DESCRIPTION_RECONSIGNMENT);
         if (reconCount != ctr.ReConThisMonth)
         {
            sb.AppendFormat("- reconsigned this month({0}) does not match 'reconsigned this month'({1}) in contract\n",
               reconCount, ctr.ReConThisMonth);
         }

         var retCount = actionsCount(ActionUtils.ACTION_DESCRIPTION_RETRIEVAL) +
                        actionsCount(ActionUtils.ACTION_DESCRIPTION_EMERETRIEVAL) +
                        actionsCount(ActionUtils.ACTION_DESCRIPTION_ONSITERETRIEVAL);
         if (retCount != ctr.ReturnedThisMonth)
         {
            sb.AppendFormat("- returned this month({0}) does not match 'returned this month'({1}) in contract\n",
               retCount, ctr.ReturnedThisMonth);
         }

         var desCount = actionsCount(ActionUtils.ACTION_DESCRIPTION_SHREADING);
         if (desCount != ctr.DestroyedThisMonth)
         {
            sb.AppendFormat("- destroyed this month({0}) does not match 'destroyed this month'({1}) in contract\n",
               desCount, ctr.DestroyedThisMonth); 
         }

         var itemsAtTheBeginingOfMonth = itemsCount - conCount - reconCount + retCount + desCount;
         if (itemsAtTheBeginingOfMonth != ctr.ConLastMonth)
         {
            sb.AppendFormat("- items at the begining of month({0}) does not match 'consigned lasth month'({1}) in contract\n",
               itemsAtTheBeginingOfMonth, ctr.ConLastMonth);
         }
         if (sb.Length > 0)
         {
            ErrorHandler("[{0}]: {1}", ctr.AccountRef, sb.ToString());
         }
      }

      private DateTime LastDay
      {
         get
         {
            return new DateTime(
               dateWithMonthBeingChecked.Year, 
               dateWithMonthBeingChecked.Month, 
               DateTime.DaysInMonth(dateWithMonthBeingChecked.Year, dateWithMonthBeingChecked.Month));
         }
      }
      private DateTime FirstDay
      {
         get
         {
            return new DateTime(
               dateWithMonthBeingChecked.Year, 
               dateWithMonthBeingChecked.Month, 
               1);
         }
      }
   }
}
