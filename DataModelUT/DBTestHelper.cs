﻿using Fastness.DataModel;
using Moq;
using System.Collections.Generic;
using System.Data.Entity;

namespace DataModelUT
{
   //
   public class Backend
   {
      public List<Contract> ctrs = new List<Contract>();
      public List<ItemDetail> items = new List<ItemDetail>();
      public List<ActionHistory> actionsHistory = new List<ActionHistory>();
      public List<ContractAction> ctrActions = new List<ContractAction>();
      public List<ActionTemplate> actionTemplate = new List<ActionTemplate>();
      public List<MiscParams> miscParams = new List<MiscParams>();
      public List<InvoiceHistory> invoicesHistory = new List<InvoiceHistory>();

      public Backend WithContract(Contract ctr)
      {
         ctr.ContractID = ctrs.Count;
         ctrs.Add(ctr);
         return this;
      }
      public Backend WithContractAction(ContractAction ctrAction)
      {
         ctrAction.ContractActionID = ctrActions.Count;
         ctrActions.Add(ctrAction);
         return this;
      }
      public Backend WithActionTemplate(ActionTemplate at)
      {
         at.ActionTemplateID = actionTemplate.Count;
         actionTemplate.Add(at);
         return this;
      }
      public Backend WithItem(ItemDetail item)
      {
         item.ItemID = items.Count;
         items.Add(item);
         return this;
      }
      public Backend WithActionHistory(ActionHistory ah)
      {
         ah.ActionHistoryID = actionsHistory.Count;
         actionsHistory.Add(ah);
         return this;
      }
      public Backend WithMiscParam(MiscParams mp)
      {
         mp.MiscParamsID = miscParams.Count;
         miscParams.Add(mp);
         return this;
      }
      public Backend WithInvoiceHistory(InvoiceHistory ih)
      {
         ih.InvoiceHistoryID = invoicesHistory.Count;
         invoicesHistory.Add(ih);
         return this;
      }
      public DBMocks buildAllMocks()
      {
         return DBTestHelper.GetDBMocks(ctrs, actionTemplate, ctrActions, items, actionsHistory, miscParams, invoicesHistory);
      }
   }

   //
   public class DBMocks
   { 
      public Mock<DbSet<Contract>> ContractMock = new Mock<DbSet<Contract>>();
      public Mock<DbSet<ItemDetail>> ItemDetailMock = new Mock<DbSet<ItemDetail>>();
      public Mock<DbSet<ActionHistory>> ActionHistoryMock = new Mock<DbSet<ActionHistory>>();
      public Mock<DbSet<ContractAction>> ContractActionMock = new Mock<DbSet<ContractAction>>();
      public Mock<DbSet<ActionTemplate>> ActionTemplateMock = new Mock<DbSet<ActionTemplate>>();
      public Mock<DbSet<MiscParams>> MiscParamsMock = new Mock<DbSet<MiscParams>>();
      public Mock<DbSet<InvoiceHistory>> InvoiceHistoryMock = new Mock<DbSet<InvoiceHistory>>();

      public Mock<fastnessEntities> CtxMock = new Mock<fastnessEntities>();

      public DBMocks()
      {
         CtxMock.Setup(c => c.Contract).Returns(ContractMock.Object);
         CtxMock.Setup(c => c.ItemDetail).Returns(ItemDetailMock.Object);
         CtxMock.Setup(c => c.ActionHistory).Returns(ActionHistoryMock.Object);
         CtxMock.Setup(c => c.ContractAction).Returns(ContractActionMock.Object);
         CtxMock.Setup(c => c.ActionTemplate).Returns(ActionTemplateMock.Object);
         CtxMock.Setup(c => c.MiscParams).Returns(MiscParamsMock.Object);
         CtxMock.Setup(c => c.InvoiceHistory).Returns(InvoiceHistoryMock.Object);

         //CtxMock.Setup(c => c.SaveChanges());
         //CtxMock.Setup(c => c.SetAsModified(It.IsAny<object>()));
      }
   }

   //
   public class DBTestHelper
   {
      static public DBMocks GetDBMocks(
         List<Contract> contractsList,
         List<ActionTemplate> actionsList,
         List<ContractAction> contractsActionsList,
         List<ItemDetail> itemsList,
         List<ActionHistory> actionsHistory,
         
         List<MiscParams> miscParams,
         List<InvoiceHistory> invoicesHistory)
      {
         var dbMocks = new DBMocks();

         dbMocks.ContractMock.SetupData(contractsList);
         dbMocks.ItemDetailMock.SetupData(itemsList);
         dbMocks.ActionHistoryMock.SetupData(actionsHistory);
         dbMocks.ActionTemplateMock.SetupData(actionsList);
         dbMocks.ContractActionMock.SetupData(contractsActionsList);
         dbMocks.MiscParamsMock.SetupData(miscParams);
         dbMocks.InvoiceHistoryMock.SetupData(invoicesHistory);

         return dbMocks;
      }

   }
}
