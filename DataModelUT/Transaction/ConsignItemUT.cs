﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fastness.DataModel;
using Fastness.Common;
using Fastness.DataModel.Transaction;
using Fastness.DataModel.Exceptions;

namespace DataModelUT.Transaction
{
   [TestClass]
   public class ConsignItemUT
   {
      [TestMethod]
      public void consign_item_OK()
      {
         // GIVEN
         int conLastMonth_before = 5;
         int conThisMonth_before = 8;
         int reConThisMonth_before = 2;
         int returnedThisMonth_before = 0;
         int destroyedThisMonth_before = 0;
         int total_before = 15;
         var ctr = new Contract() 
         {
            ContractID = 22,

            ConLastMonth = conLastMonth_before,
            ConThisMonth = conThisMonth_before,
            ReConThisMonth = reConThisMonth_before,
            ReturnedThisMonth = returnedThisMonth_before,
            DestroyedThisMonth = destroyedThisMonth_before,
            Total = total_before
         };
         var ctrAction = new ContractAction 
         {
            ContractActionID = 123,
            Description = ActionUtils.ACTION_DESCRIPTION_CONSIGNMENT,
            Charge = 88,
            ContractID = ctr.ContractID, 
            PerFreeAction = 13
         };

         var newItem = new ItemDetail
         {
            ItemNumber = "11111111",
            ContractID = ctr.ContractID,
            ClientItemRef = "someRef",
            Description = "some description",
            DestructionDate = "2012-11",
            Type = "dummyType",
            Percentage = 111,
            Location = "12345678",
            InStock = false
         };
         var itemBefore = TestHelper.DeepCopy<ItemDetail>(newItem);

         var expectedCharge = 1234.33;
         var expectedAuthor = "some dummy consign authorization";
         var expectedDateOfAction = new DateTime(2012, 11, 1);
         var exptectedActionHistory = new ActionHistory
         {
            ItemNumber = newItem.ItemNumber,
            Description = ActionUtils.ACTION_DESCRIPTION_CONSIGNMENT,
            Notes = "",
            Charge = expectedCharge,
            Date = DateTime.Today,
            DateCompleted = expectedDateOfAction,
            AuthorisedBy = expectedAuthor,
            Location = newItem.Location,
            Percentage = newItem.Percentage,
            ContractActionID = ctrAction.ContractActionID
         };

         var backend = new Backend();
         var dbMocks = backend.WithContract(ctr).WithItem(new ItemDetail { ItemID = 23 }).buildAllMocks();

         var ctx = new FastnessActionContext(newItem, ctr, ctrAction, expectedDateOfAction, expectedCharge, expectedAuthor);
         ctx.AttachDbContext(dbMocks.CtxMock.Object);
         var target = new ConsignItem(ctx);
         
         //WHEN
         target.Run();

         //THEN
         Assert.AreEqual(backend.items.Count, 2);
         Assert.AreEqual(backend.items[1], newItem);
         Assert.AreEqual(backend.items[1].DateRecieved, expectedDateOfAction);
         Assert.IsTrue(backend.items[1].InStock ?? true);
         Assert.IsTrue(TestHelper.AreValuesEqual<ItemDetail>(backend.items[1], itemBefore, new string[] { "InStock", "InStockEOM", "ItemID", "DateRecieved" }), "item details which supposed to stay unchanged, were changed!");
         dbMocks.ItemDetailMock.Verify(set => set.Add(backend.items[1]), Moq.Times.Once());

         Assert.AreEqual(ctr.ConLastMonth, conLastMonth_before);
         Assert.AreEqual(ctr.ConThisMonth, conThisMonth_before + 1);
         Assert.AreEqual(ctr.ReConThisMonth, reConThisMonth_before);
         Assert.AreEqual(ctr.ReturnedThisMonth, returnedThisMonth_before);
         Assert.AreEqual(ctr.DestroyedThisMonth, destroyedThisMonth_before);
         Assert.AreEqual(ctr.Total, total_before + 1);
         dbMocks.ContractMock.Verify(set => set.Attach(ctr), Moq.Times.Once());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(ctr), Moq.Times.Once());

         Assert.IsTrue(TestHelper.AreValuesEqual<ActionHistory>(backend.actionsHistory[0], exptectedActionHistory, new string[] { "ActionHistoryID" }), "action history entry is not as expected one!");
         dbMocks.ActionHistoryMock.Verify(set => set.Add(backend.actionsHistory[0]), Moq.Times.Once());

         dbMocks.CtxMock.Verify(db => db.SaveChanges(), Moq.Times.Once());

      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void consign_item_ERROR_empty_item_number()
      {
         // GIVEN
         var newItem = new ItemDetail
         {
            ItemNumber = "",
            Location = "123456789"
         };

         var backend = new Backend();
         var dbMocks = backend.buildAllMocks();

         var ctx = new FastnessActionContext(newItem, null, null, DateTime.Today, 0, "");
         ctx.AttachDbContext(dbMocks.CtxMock.Object);
         var target = new ConsignItem(ctx);

         //WHEN
         target.Run();
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void consign_item_ERROR_empty_location_number()
      {
         // GIVEN
         var newItem = new ItemDetail
         {
            ItemNumber = "123456789",
            Location = ""
         };

         var backend = new Backend();
         var dbMocks = backend.buildAllMocks();

         var ctx = new FastnessActionContext(newItem, null, null, DateTime.Today, 0, "");
         ctx.AttachDbContext(dbMocks.CtxMock.Object);
         var target = new ConsignItem(ctx);

         //WHEN
         target.Run();
      }
      [TestMethod]
      [ExpectedException(typeof(DataConstraintBrokenException))]
      public void consign_item_ERROR_item_number_which_already_exists()
      {
         string alreadyExistingItemNumber = "12345678";
         // GIVEN
         var newItem = new ItemDetail
         {
            ItemNumber = alreadyExistingItemNumber,
            Location = "123456789"
         };

         var backend = new Backend().WithItem(new ItemDetail { ItemNumber = alreadyExistingItemNumber });
         var dbMocks = backend.buildAllMocks();

         var ctx = new FastnessActionContext(newItem, null, null, DateTime.Today, 0, "");
         ctx.AttachDbContext(dbMocks.CtxMock.Object);
         var target = new ConsignItem(ctx);

         //WHEN
         target.Run();
      }
      [TestMethod]
      [ExpectedException(typeof(DataConstraintBrokenException))]
      public void consign_item_ERROR_when_item_has_history_entries()
      {
         var itemNumberForWhichThereAreHistoryEntriesAlready = "12345678";
         // GIVEN
         var newItem = new ItemDetail
         {
            ItemNumber = itemNumberForWhichThereAreHistoryEntriesAlready,
            Location = "123456789"
         };

         var backend = new Backend().WithActionHistory(new ActionHistory { ItemNumber = itemNumberForWhichThereAreHistoryEntriesAlready });
         var dbMocks = backend.buildAllMocks();

         var ctx = new FastnessActionContext(newItem, null, null, DateTime.Today, 0, "");
         ctx.AttachDbContext(dbMocks.CtxMock.Object);
         var target = new ConsignItem(ctx);

         //WHEN
         target.Run();
      }
   }
}
