﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fastness.DataModel;
using Fastness.DataModel.Transaction;

namespace DataModelUT.Transaction
{
   [TestClass]
   public class GenericActionWithoutItemUT
   {
      [TestMethod]
      public void generic_no_item_action_OK()
      {
         // GIVEN
         int conLastMonth_before = 5;
         int conThisMonth_before = 8;
         int reConThisMonth_before = 2;
         int returnedThisMonth_before = 0;
         int destroyedThisMonth_before = 0;
         int total_before = 15;
         var ctr = new Contract()
         {
            ContractID = 22,

            ConLastMonth = conLastMonth_before,
            ConThisMonth = conThisMonth_before,
            ReConThisMonth = reConThisMonth_before,
            ReturnedThisMonth = returnedThisMonth_before,
            DestroyedThisMonth = destroyedThisMonth_before,
            Total = total_before
         };
         var ctrAction = new ContractAction
         {
            ContractActionID = 123,
            Description = "dummy description",
            Charge = 88,
            ContractID = ctr.ContractID,
            PerFreeAction = 13
         };

         var expectedCharge = 1234.33;
         var expectedAuthor = "some dummy consign authorization";
         var expectedDateOfAction = new DateTime(2012, 11, 1);
         var expectedNnotes = "some dummy notes";
         var exptectedActionHistory = new ActionHistory
         {
            ItemNumber = "0",
            Description = ctrAction.Description,
            Notes = expectedNnotes,
            Charge = expectedCharge,
            Date = DateTime.Today,
            DateCompleted = expectedDateOfAction,
            AuthorisedBy = expectedAuthor,
            Location = "",
            Percentage = null,
            ContractActionID = ctrAction.ContractActionID
         };

         var backend = new Backend();
         var dbMocks = backend.buildAllMocks();

         var ctx = new FastnessActionContext(null, ctr, ctrAction, expectedDateOfAction, expectedCharge, expectedAuthor);
         ctx.AttachDbContext(dbMocks.CtxMock.Object);

         var target = new GenericActionWithoutItem(ctx, expectedNnotes);

         //WHEN
         target.Run();

         //THEN
         Assert.AreEqual(ctr.ConLastMonth, conLastMonth_before);
         Assert.AreEqual(ctr.ConThisMonth, conThisMonth_before);
         Assert.AreEqual(ctr.ReConThisMonth, reConThisMonth_before);
         Assert.AreEqual(ctr.ReturnedThisMonth, returnedThisMonth_before);
         Assert.AreEqual(ctr.DestroyedThisMonth, destroyedThisMonth_before);
         Assert.AreEqual(ctr.Total, total_before);
         dbMocks.ContractMock.Verify(set => set.Attach(Moq.It.IsAny<Contract>()), Moq.Times.Never());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(Moq.It.IsAny<Contract>()), Moq.Times.Never());

         Assert.IsTrue(TestHelper.AreValuesEqual<ActionHistory>(backend.actionsHistory[0], exptectedActionHistory, new string[] { "ActionHistoryID" }), "action history entry is not as expected one!");
         dbMocks.ActionHistoryMock.Verify(set => set.Add(Moq.It.Is<ActionHistory>(ah =>
            ah.ItemNumber == "0" &&
            ah.Description == ctx.CtrAction.Description &&
            ah.Charge == ctx.Charge &&
            ah.Date == DateTime.Today &&
            ah.DateCompleted == ctx.Date &&
            ah.AuthorisedBy == ctx.Author &&
            ah.ContractActionID == ctx.CtrAction.ContractActionID &&

            ah.Notes == expectedNnotes &&
            ah.Location == "" &&
            ah.Percentage == null
         )), Moq.Times.Once());

         dbMocks.CtxMock.Verify(db => db.SaveChanges(), Moq.Times.Once());
      }
   }
}
