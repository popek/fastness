﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fastness.DataModel;
using Fastness.DataModel.Transaction;
using Fastness.DataModel.Exceptions;

namespace DataModelUT.Transaction
{
   [TestClass]
   public class GenericActionOnItemUT
   {
      [TestMethod]
      public void generic_item_action_OK()
      {
         // GIVEN
         int conLastMonth_before = 5;
         int conThisMonth_before = 8;
         int reConThisMonth_before = 2;
         int returnedThisMonth_before = 0;
         int destroyedThisMonth_before = 0;
         int total_before = 15;
         var ctr = new Contract()
         {
            ContractID = 22,

            ConLastMonth = conLastMonth_before,
            ConThisMonth = conThisMonth_before,
            ReConThisMonth = reConThisMonth_before,
            ReturnedThisMonth = returnedThisMonth_before,
            DestroyedThisMonth = destroyedThisMonth_before,
            Total = total_before
         };
         var ctrAction = new ContractAction
         {
            ContractActionID = 123,
            Description = "dummy description",
            Charge = 88,
            ContractID = ctr.ContractID,
            PerFreeAction = 13
         };

         var item = new ItemDetail
         {
            ItemNumber = "11111111",
            ContractID = ctr.ContractID,
            ClientItemRef = "someRef",
            Description = "some description",
            DestructionDate = "2012-11",
            DateRecieved = new DateTime(2012, 1, 1),
            Type = "dummyType",
            Percentage = 111,
            Location = "12345678",
         };
         var itemBefore = TestHelper.DeepCopy<ItemDetail>(item);

         var expectedCharge = 1234.33;
         var expectedAuthor = "some dummy consign authorization";
         var expectedDateOfAction = new DateTime(2012, 11, 1);
         var expectedNnotes = "some dummy notes";
         var exptectedActionHistory = new ActionHistory
         {
            ItemNumber = item.ItemNumber,
            Description = ctrAction.Description,
            Notes = expectedNnotes,
            Charge = expectedCharge,
            Date = DateTime.Today,
            DateCompleted = expectedDateOfAction,
            AuthorisedBy = expectedAuthor,
            Location = item.Location,
            Percentage = null,
            ContractActionID = ctrAction.ContractActionID
         };

         var backend = new Backend();
         var dbMocks = backend.WithContract(ctr).WithItem(item).buildAllMocks();

         var ctx = new FastnessActionContext(item, ctr, ctrAction, expectedDateOfAction, expectedCharge, expectedAuthor);
         ctx.AttachDbContext(dbMocks.CtxMock.Object);
         var target = new GenericActionOnItem(ctx, expectedNnotes);

         //WHEN
         target.Run();

         //THEN
         Assert.AreEqual(backend.items[0], item);
         Assert.IsTrue(TestHelper.AreValuesEqual<ItemDetail>(backend.items[0], itemBefore, new string[] { }), "item details which supposed to stay unchanged, were changed!");
         dbMocks.ItemDetailMock.Verify(set => set.Attach(Moq.It.IsAny<ItemDetail>()), Moq.Times.Never());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(Moq.It.IsAny<ItemDetail>()), Moq.Times.Never());

         Assert.AreEqual(ctr.ConLastMonth, conLastMonth_before);
         Assert.AreEqual(ctr.ConThisMonth, conThisMonth_before);
         Assert.AreEqual(ctr.ReConThisMonth, reConThisMonth_before);
         Assert.AreEqual(ctr.ReturnedThisMonth, returnedThisMonth_before);
         Assert.AreEqual(ctr.DestroyedThisMonth, destroyedThisMonth_before);
         Assert.AreEqual(ctr.Total, total_before);
         Assert.IsTrue(TestHelper.AreValuesEqual<ActionHistory>(backend.actionsHistory[0], exptectedActionHistory, new string[] { "ActionHistoryID" }), "action history entry is not as expected one!");
         dbMocks.ContractMock.Verify(set => set.Attach(Moq.It.IsAny<Contract>()), Moq.Times.Never());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(Moq.It.IsAny<Contract>()), Moq.Times.Never());

         dbMocks.ActionHistoryMock.Verify(set => set.Add(Moq.It.Is<ActionHistory>(ah => 
            ah.ItemNumber == item.ItemNumber &&
            ah.Description == ctx.CtrAction.Description &&
            ah.Charge == ctx.Charge &&
            ah.Date == DateTime.Today &&
            ah.DateCompleted == ctx.Date &&
            ah.AuthorisedBy == ctx.Author &&
            ah.ContractActionID == ctx.CtrAction.ContractActionID &&

            ah.Notes == expectedNnotes &&
            ah.Location == item.Location &&
            ah.Percentage == null
         )), Moq.Times.Once());

         dbMocks.CtxMock.Verify(db => db.SaveChanges(), Moq.Times.Once());
      }

      [TestMethod]
      [ExpectedException(typeof(DataDoesNotExistException))]
      public void generic_item_action_ERROR_when_item_is_not_in_db_yet()
      {
         // GIVEN
         var item = new ItemDetail
         {
            ItemNumber = "12345678"
         };

         var backend = new Backend();
         var dbMocks = backend.buildAllMocks();

         var ctx = new FastnessActionContext(item, null, new ContractAction { Description = "dummy" }, DateTime.Today, 0, "");
         ctx.AttachDbContext(dbMocks.CtxMock.Object);
         var target = new GenericActionOnItem(ctx, "dummy note");

         //WHEN
         target.Run();
      }
      [TestMethod]
      [ExpectedException(typeof(DataNotUniqueException))]
      public void generic_item_action_ERROR_when_db_item_is_not_uniqe_which_indicates_on_db_constraint_error()
      {
         // GIVEN
         var notUniqueItemNo = "12345678";
         var item = new ItemDetail
         {
            ItemNumber = notUniqueItemNo,
         };

         var backend = new Backend();
         var dbMocks = backend
            .WithItem(item)
            .WithItem(new ItemDetail { ItemNumber = notUniqueItemNo })
            .buildAllMocks();

         var ctx = new FastnessActionContext(item, null, new ContractAction { Description = "dummy" }, DateTime.Today, 0, "");
         ctx.AttachDbContext(dbMocks.CtxMock.Object);
         var target = new GenericActionOnItem(ctx, "dummy note");

         //WHEN
         target.Run();
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void generic_item_action_ERROR_when_item_marked_as_out_of_stock()
      {
         // GIVEN
         var item = new ItemDetail
         {
            ItemNumber = "123456789",
            InStock = false
         };

         var backend = new Backend();
         var dbMocks = backend.WithItem(item).buildAllMocks();

         var ctx = new FastnessActionContext(item, null, new ContractAction { Description = "dummy" }, DateTime.Today, 0, "");
         ctx.AttachDbContext(dbMocks.CtxMock.Object);
         var target = new GenericActionOnItem(ctx, "some dummy note");

         //WHEN
         target.Run();
      }

   }
}
