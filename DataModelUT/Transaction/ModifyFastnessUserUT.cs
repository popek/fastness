﻿using System;
using Fastness.DataModel.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DataModelUT.Transaction
{
   [TestClass]
   public class ModifyFastnessUserUT
   {
      [TestMethod]
      [ExpectedException(typeof(DataConstraintBrokenException))]
      public void add_new_user_success_when_user_with_the_same_login_does_not_exits()
      {
         throw new NotImplementedException();
      }
      [TestMethod]
      public void add_new_user_fails_when_user_with_the_same_login_already_exists()
      {
         throw new NotImplementedException();
      }
      [TestMethod]
      public void remove_existing_user_success_when_user_exists()
      {
         throw new NotImplementedException();
      }
      [TestMethod]
      [ExpectedException(typeof(DataConstraintBrokenException))]
      public void remove_existing_user_fails_when_user_with_does_not_exists_already()
      {
         throw new NotImplementedException();
      }
      [TestMethod]
      public void modify_existing_user_success()
      {
         throw new NotImplementedException();
      }
   }
}
