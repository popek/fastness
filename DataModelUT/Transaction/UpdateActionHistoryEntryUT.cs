﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fastness.DataModel;
using Fastness.Common;
using Fastness.DataModel.Transaction;
using Fastness.DataModel.Exceptions;

namespace DataModelUT.Transaction
{
   [TestClass]
   public class UpdateActionHistoryEntryUT
   {
      [TestMethod]
      public void testPositive_when_updating_consignment()
      {
         // GIVEN
         var consignedItem = new ItemDetail
         {
            DateRecieved = DateTime.Today - new TimeSpan(2, 0, 0, 0)
         };
         var consignHistoryEntry = new ActionHistory 
         { 
            Description = ActionUtils.ACTION_DESCRIPTION_CONSIGNMENT,

            Location = "12345678",
            Charge = 2.3,
            DateCompleted = consignedItem.DateRecieved,
            AuthorisedBy = null,
            Notes = "some notes at the begining"
         };

         string expectedLocation = "87654321";
         double expectedCharge = 88;
         DateTime expectedDate = DateTime.Today;
         string expectedAuthorizedBy = "some authorizer";
         string expectedNotes = "notes which were changed";

         var backend = new Backend();
         var dbMocks = backend
            .WithActionHistory(consignHistoryEntry)
            .WithItem(consignedItem)
            .buildAllMocks();

         var target = new UpdateActionHistoryEntry(consignHistoryEntry, expectedLocation, expectedCharge, expectedDate, expectedAuthorizedBy, expectedNotes, "DP-STORE", dbMocks.CtxMock.Object);

         // WHEN
         target.Run();

         // THEN
         Assert.AreEqual(expectedLocation, backend.items[0].Location);
         Assert.AreEqual(expectedDate, backend.items[0].DateRecieved);
         dbMocks.ItemDetailMock.Verify(set => set.Attach(backend.items[0]), Moq.Times.Once());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(backend.items[0]), Moq.Times.Once());

         Assert.AreEqual(expectedLocation, backend.actionsHistory[0].Location);
         Assert.AreEqual(expectedDate, backend.actionsHistory[0].DateCompleted);
         Assert.AreEqual(expectedCharge, backend.actionsHistory[0].Charge);
         Assert.AreEqual(expectedAuthorizedBy, backend.actionsHistory[0].AuthorisedBy);
         Assert.AreEqual(expectedNotes, backend.actionsHistory[0].Notes);
         dbMocks.ActionHistoryMock.Verify(set => set.Attach(backend.actionsHistory[0]), Moq.Times.Once());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(backend.actionsHistory[0]), Moq.Times.Once());

         dbMocks.CtxMock.Verify(db => db.SaveChanges(), Moq.Times.Once());
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void testNegative_when_updating_consignment_with_location_but_item_already_returned()
      {
         // GIVEN
         var consignedItem = new ItemDetail
         {
            DateRecieved = DateTime.Today - new TimeSpan(2, 0, 0, 0),
            DateRetrieved = DateTime.Today - new TimeSpan(1, 0, 0, 0)
         };
         var consignHistoryEntry = new ActionHistory
         {
            Description = ActionUtils.ACTION_DESCRIPTION_CONSIGNMENT,

            Location = "12345678",
            Charge = 2.3,
            DateCompleted = consignedItem.DateRecieved,
            AuthorisedBy = null,
            Notes = "some notes at the begining"
         };

         string expectedLocation = "87654321";

         var backend = new Backend();
         var dbMocks = backend
            .WithActionHistory(consignHistoryEntry)
            .WithItem(consignedItem)
            .buildAllMocks();

         var target = new UpdateActionHistoryEntry(consignHistoryEntry, expectedLocation, 0, DateTime.Today, "", "", "DP-STORE", dbMocks.CtxMock.Object);

         // WHEN
         target.Run();
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void testNegative_when_updating_consignment_with_location_but_item_already_reconsigned()
      {
         // GIVEN
         var consignedItem = new ItemDetail
         {
            DateRecieved = DateTime.Today - new TimeSpan(2, 0, 0, 0),
            DateReturned = DateTime.Today - new TimeSpan(1, 0, 0, 0)
         };
         var consignHistoryEntry = new ActionHistory
         {
            Description = ActionUtils.ACTION_DESCRIPTION_CONSIGNMENT,

            Location = "12345678",
            Charge = 2.3,
            DateCompleted = consignedItem.DateRecieved,
            AuthorisedBy = null,
            Notes = "some notes at the begining"
         };

         string expectedLocation = "87654321";

         var backend = new Backend();
         var dbMocks = backend
            .WithActionHistory(consignHistoryEntry)
            .WithItem(consignedItem)
            .buildAllMocks();

         var target = new UpdateActionHistoryEntry(consignHistoryEntry, expectedLocation, 0, DateTime.Today, "", "", "DP-STORE", dbMocks.CtxMock.Object);

         // WHEN
         target.Run();
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void testNegative_when_updating_consignment_with_location_but_item_already_destroyed()
      {
         // GIVEN
         var consignedItem = new ItemDetail
         {
            DateRecieved = DateTime.Today - new TimeSpan(2, 0, 0, 0),
            DateDestroyed = DateTime.Today - new TimeSpan(1, 0, 0, 0)
         };
         var consignHistoryEntry = new ActionHistory
         {
            Description = ActionUtils.ACTION_DESCRIPTION_CONSIGNMENT,

            Location = "12345678",
            Charge = 2.3,
            DateCompleted = consignedItem.DateRecieved,
            AuthorisedBy = null,
            Notes = "some notes at the begining"
         };

         string expectedLocation = "87654321";

         var backend = new Backend();
         var dbMocks = backend
            .WithActionHistory(consignHistoryEntry)
            .WithItem(consignedItem)
            .buildAllMocks();

         var target = new UpdateActionHistoryEntry(consignHistoryEntry, expectedLocation, 0, DateTime.Today, "", "", "DP-STORE", dbMocks.CtxMock.Object);

         // WHEN
         target.Run();
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void testNegative_when_updating_consignment_with_location_which_is_already_in_use()
      {
         // GIVEN
         var consignedItem = new ItemDetail
         {
            ItemNumber = "11111111",
            DateRecieved = DateTime.Today - new TimeSpan(2, 0, 0, 0),
         };
         var consignHistoryEntry = new ActionHistory
         {
            Description = ActionUtils.ACTION_DESCRIPTION_CONSIGNMENT,

            ItemNumber = consignedItem.ItemNumber,
            Location = "12345678",
            Charge = 2.3,
            DateCompleted = consignedItem.DateRecieved,
            AuthorisedBy = null,
            Notes = "some notes at the begining"
         };

         string expectedLocation = "87654321";

         var itemWhichOccupiesExpectedLocation = new ItemDetail { ItemNumber = "22222222", Location = expectedLocation };
         
         var backend = new Backend();
         var dbMocks = backend
            .WithActionHistory(consignHistoryEntry)
            .WithItem(consignedItem)
            .WithItem(itemWhichOccupiesExpectedLocation)
            .buildAllMocks();

         var target = new UpdateActionHistoryEntry(consignHistoryEntry, expectedLocation, 0, DateTime.Today, "", "", "DP-STORE", dbMocks.CtxMock.Object);

         // WHEN
         target.Run();
      }

      [TestMethod]
      public void testPositive_when_updating_reconsignment()
      {
         // GIVEN
         var reconsignedItem = new ItemDetail
         {
            DateReturned = DateTime.Today - new TimeSpan(2, 0, 0, 0)
         };
         var reconsignHistoryEntry = new ActionHistory
         {
            Description = ActionUtils.ACTION_DESCRIPTION_RECONSIGNMENT,

            Location = "12345678",
            Charge = 2.3,
            DateCompleted = reconsignedItem.DateReturned,
            AuthorisedBy = null,
            Notes = "some notes at the begining"
         };

         string expectedLocation = "87654321";
         double expectedCharge = 88;
         DateTime expectedDate = DateTime.Today;
         string expectedAuthorizedBy = "some authorizer";
         string expectedNotes = "notes which were changed";

         var backend = new Backend();
         var dbMocks = backend
            .WithActionHistory(reconsignHistoryEntry)
            .WithItem(reconsignedItem)
            .buildAllMocks();

         var target = new UpdateActionHistoryEntry(reconsignHistoryEntry, expectedLocation, expectedCharge, expectedDate, expectedAuthorizedBy, expectedNotes, "DP-COL", dbMocks.CtxMock.Object);

         // WHEN
         target.Run();

         // THEN
         Assert.AreEqual(expectedLocation, backend.items[0].Location);
         Assert.AreEqual(expectedDate, backend.items[0].DateReturned);
         dbMocks.ItemDetailMock.Verify(set => set.Attach(backend.items[0]), Moq.Times.Once());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(backend.items[0]), Moq.Times.Once());

         Assert.AreEqual(expectedLocation, backend.actionsHistory[0].Location);
         Assert.AreEqual(expectedDate, backend.actionsHistory[0].DateCompleted);
         Assert.AreEqual(expectedCharge, backend.actionsHistory[0].Charge);
         Assert.AreEqual(expectedAuthorizedBy, backend.actionsHistory[0].AuthorisedBy);
         Assert.AreEqual(expectedNotes, backend.actionsHistory[0].Notes);
         dbMocks.ActionHistoryMock.Verify(set => set.Attach(backend.actionsHistory[0]), Moq.Times.Once());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(backend.actionsHistory[0]), Moq.Times.Once());

         dbMocks.CtxMock.Verify(db => db.SaveChanges(), Moq.Times.Once());
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void testNegative_when_updating_reconsignment_with_location_but_item_already_returned()
      {
         // GIVEN
         var reconsignedItem = new ItemDetail
         {
            DateReturned = DateTime.Today - new TimeSpan(2, 0, 0, 0),
            DateRetrieved = DateTime.Today - new TimeSpan(1, 0, 0, 0)
         };
         var reconsignHistoryEntry = new ActionHistory
         {
            Description = ActionUtils.ACTION_DESCRIPTION_RECONSIGNMENT,

            Location = "12345678",
            Charge = 2.3,
            DateCompleted = reconsignedItem.DateReturned,
            AuthorisedBy = null,
            Notes = "some notes at the begining"
         };

         string expectedLocation = "87654321";

         var backend = new Backend();
         var dbMocks = backend
            .WithActionHistory(reconsignHistoryEntry)
            .WithItem(reconsignedItem)
            .buildAllMocks();

         var target = new UpdateActionHistoryEntry(reconsignHistoryEntry, expectedLocation, 0, DateTime.Today, "", "", "DP-COL", dbMocks.CtxMock.Object);

         // WHEN
         target.Run();
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void testNegative_when_updating_location_on_not_last_reconsignment()
      {
         // GIVEN
         var item = new ItemDetail
         {
            ItemNumber = "11111111",
            DateReturned = DateTime.Today - new TimeSpan(1, 0, 0, 0)
         };
         var firstReconsignHistoryEntry = new ActionHistory
         {
            Description = ActionUtils.ACTION_DESCRIPTION_RECONSIGNMENT,

            ItemNumber = item.ItemNumber,
            Location = "12345678",
            Charge = 2.3,
            DateCompleted = item.DateReturned,
            AuthorisedBy = null,
            Notes = "some notes at the begining"
         };
         var firstRetrieveHistoryEntry = new ActionHistory
         {
            Description = ActionUtils.ACTION_DESCRIPTION_RETRIEVAL,

            ItemNumber = item.ItemNumber,
            Location = "12345678",
            Charge = 2.3,
            DateCompleted = firstReconsignHistoryEntry.DateCompleted,
            AuthorisedBy = null,
            Notes = "some notes at the begining"
         };
         var lastReconsignHistoryEntry = new ActionHistory
         {
            Description = ActionUtils.ACTION_DESCRIPTION_RECONSIGNMENT,

            ItemNumber = item.ItemNumber,
            Location = "12345678",
            Charge = 2.3,
            DateCompleted = firstRetrieveHistoryEntry.DateCompleted,
            AuthorisedBy = null,
            Notes = "some notes at the begining"
         };

         string expectedLocation = "87654321";

         var backend = new Backend();
         var dbMocks = backend
            .WithActionHistory(firstReconsignHistoryEntry)
            .WithActionHistory(firstRetrieveHistoryEntry)
            .WithActionHistory(lastReconsignHistoryEntry)
            .WithItem(item)
            .buildAllMocks();

         var target = new UpdateActionHistoryEntry(firstReconsignHistoryEntry, expectedLocation, 0, DateTime.Today, "", "", "DP-COL", dbMocks.CtxMock.Object);

         // WHEN
         target.Run();
      }
      [TestMethod]
      public void testPositive_when_updating_location_on_last_reconsignment()
      {
         // GIVEN
         var item = new ItemDetail
         {
            ItemNumber = "11111111",
            DateReturned = DateTime.Today - new TimeSpan(1, 0, 0, 0)
         };
         var firstReconsignHistoryEntry = new ActionHistory
         {
            Description = ActionUtils.ACTION_DESCRIPTION_RECONSIGNMENT,

            ItemNumber = item.ItemNumber,
            Location = "12345678",
            Charge = 2.3,
            DateCompleted = item.DateReturned,
            AuthorisedBy = null,
            Notes = "some notes at the begining"
         };
         var firstRetrieveHistoryEntry = new ActionHistory
         {
            Description = ActionUtils.ACTION_DESCRIPTION_RETRIEVAL,

            ItemNumber = item.ItemNumber,
            Location = "12345678",
            Charge = 2.3,
            DateCompleted = firstReconsignHistoryEntry.DateCompleted,
            AuthorisedBy = null,
            Notes = "some notes at the begining"
         };
         var lastReconsignHistoryEntry = new ActionHistory
         {
            Description = ActionUtils.ACTION_DESCRIPTION_RECONSIGNMENT,

            ItemNumber = item.ItemNumber,
            Location = "12345678",
            Charge = 2.3,
            DateCompleted = firstRetrieveHistoryEntry.DateCompleted,
            AuthorisedBy = null,
            Notes = "some notes at the begining"
         };

         string expectedLocation = "87654321";

         var backend = new Backend();
         var dbMocks = backend
            .WithActionHistory(firstReconsignHistoryEntry)
            .WithActionHistory(firstRetrieveHistoryEntry)
            .WithActionHistory(lastReconsignHistoryEntry)
            .WithItem(item)
            .buildAllMocks();

         var target = new UpdateActionHistoryEntry(lastReconsignHistoryEntry, expectedLocation, 0, DateTime.Today, "", "", "DP-COL", dbMocks.CtxMock.Object);

         // WHEN
         target.Run();

         // THEN
         Assert.AreEqual(expectedLocation, item.Location);
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void testNegative_when_updating_reconsignment_with_location_but_item_already_destroyed()
      {
         // GIVEN
         var reconsignedItem = new ItemDetail
         {
            DateReturned = DateTime.Today - new TimeSpan(2, 0, 0, 0),
            DateDestroyed = DateTime.Today - new TimeSpan(1, 0, 0, 0)
         };
         var reconsignHistoryEntry = new ActionHistory
         {
            Description = ActionUtils.ACTION_DESCRIPTION_RECONSIGNMENT,

            Location = "12345678",
            Charge = 2.3,
            DateCompleted = reconsignedItem.DateReturned,
            AuthorisedBy = null,
            Notes = "some notes at the begining"
         };

         string expectedLocation = "87654321";

         var backend = new Backend();
         var dbMocks = backend
            .WithActionHistory(reconsignHistoryEntry)
            .WithItem(reconsignedItem)
            .buildAllMocks();

         var target = new UpdateActionHistoryEntry(reconsignHistoryEntry, expectedLocation, 0, DateTime.Today, "", "", "DP-COL", dbMocks.CtxMock.Object);

         // WHEN
         target.Run();
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void testNegative_when_updating_reconsignment_with_location_which_is_already_in_use()
      {
         // GIVEN
         var reconsignedItem = new ItemDetail
         {
            ItemNumber = "11111111",
            DateReturned = DateTime.Today - new TimeSpan(2, 0, 0, 0),
         };
         var reconsignHistoryEntry = new ActionHistory
         {
            Description = ActionUtils.ACTION_DESCRIPTION_CONSIGNMENT,

            ItemNumber = reconsignedItem.ItemNumber,
            Location = "12345678",
            Charge = 2.3,
            DateCompleted = reconsignedItem.DateReturned,
            AuthorisedBy = null,
            Notes = "some notes at the begining"
         };

         string expectedLocation = "87654321";

         var itemWhichOccupiesExpectedLocation = new ItemDetail { ItemNumber = "22222222", Location = expectedLocation };

         var backend = new Backend();
         var dbMocks = backend
            .WithActionHistory(reconsignHistoryEntry)
            .WithItem(reconsignedItem)
            .WithItem(itemWhichOccupiesExpectedLocation)
            .buildAllMocks();

         var target = new UpdateActionHistoryEntry(reconsignHistoryEntry, expectedLocation, 0, DateTime.Today, "", "", "DP-COL", dbMocks.CtxMock.Object);

         // WHEN
         target.Run();
      }


      private void testPositive_when_updating_some_kind_of_retrieval(string retrieveActionCode)
      {
         // GIVEN
         var retrievedItem = new ItemDetail
         {
            DateRetrieved = DateTime.Today - new TimeSpan(2, 0, 0, 0)
         };
         var retrieveHistoryEntry = new ActionHistory
         {
            Description = "NOTIMPORTANT",

            Location = "",
            Charge = 2.3,
            DateCompleted = retrievedItem.DateRetrieved,
            AuthorisedBy = null,
            Notes = "some notes at the begining"
         };

         double expectedCharge = 88;
         DateTime expectedDate = DateTime.Today;
         string expectedAuthorizedBy = "some authorizer";
         string expectedNotes = "notes which were changed";

         var backend = new Backend();
         var dbMocks = backend
            .WithActionHistory(retrieveHistoryEntry)
            .WithItem(retrievedItem)
            .buildAllMocks();

         var target = new UpdateActionHistoryEntry(retrieveHistoryEntry, "", expectedCharge, expectedDate, expectedAuthorizedBy, expectedNotes, retrieveActionCode, dbMocks.CtxMock.Object);

         // WHEN
         target.Run();

         // THEN
         Assert.AreEqual(expectedDate, backend.items[0].DateRetrieved);
         dbMocks.ItemDetailMock.Verify(set => set.Attach(backend.items[0]), Moq.Times.Once());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(backend.items[0]), Moq.Times.Once());

         Assert.AreEqual("", backend.actionsHistory[0].Location);
         Assert.AreEqual(expectedDate, backend.actionsHistory[0].DateCompleted);
         Assert.AreEqual(expectedCharge, backend.actionsHistory[0].Charge);
         Assert.AreEqual(expectedAuthorizedBy, backend.actionsHistory[0].AuthorisedBy);
         Assert.AreEqual(expectedNotes, backend.actionsHistory[0].Notes);
         dbMocks.ActionHistoryMock.Verify(set => set.Attach(backend.actionsHistory[0]), Moq.Times.Once());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(backend.actionsHistory[0]), Moq.Times.Once());

         dbMocks.CtxMock.Verify(db => db.SaveChanges(), Moq.Times.Once());
      }
      [TestMethod]
      public void testPositive_when_updating_retrieval()
      {
         testPositive_when_updating_some_kind_of_retrieval("DP-RETRIEVE");
      }
      [TestMethod]
      public void testPositive_when_updating_emergency_retrieval()
      {
         testPositive_when_updating_some_kind_of_retrieval("DP-EME-RET");
      }
      [TestMethod]
      public void testPositive_when_updating_onsite_retrieval()
      {
         testPositive_when_updating_some_kind_of_retrieval("DP-ONSITE");
      }

      [TestMethod]
      public void testPositive_when_updating_shreading()
      {
         // GIVEN
         var destroyedItem = new ItemDetail
         {
            DateDestroyed = DateTime.Today - new TimeSpan(2, 0, 0, 0)
         };
         var shreadingHistoryEntry = new ActionHistory
         {
            Description = ActionUtils.ACTION_DESCRIPTION_SHREADING,

            Location = "",
            Charge = 2.3,
            DateCompleted = destroyedItem.DateDestroyed,
            AuthorisedBy = null,
            Notes = "some notes at the begining"
         };

         double expectedCharge = 88;
         DateTime expectedDate = DateTime.Today;
         string expectedAuthorizedBy = "some authorizer";
         string expectedNotes = "notes which were changed";

         var backend = new Backend();
         var dbMocks = backend
            .WithActionHistory(shreadingHistoryEntry)
            .WithItem(destroyedItem)
            .buildAllMocks();

         var target = new UpdateActionHistoryEntry(shreadingHistoryEntry, "", expectedCharge, expectedDate, expectedAuthorizedBy, expectedNotes, "DP-SHRED", dbMocks.CtxMock.Object);

         // WHEN
         target.Run();

         // THEN
         Assert.AreEqual(expectedDate, backend.items[0].DateDestroyed);
         dbMocks.ItemDetailMock.Verify(set => set.Attach(backend.items[0]), Moq.Times.Once());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(backend.items[0]), Moq.Times.Once());

         Assert.AreEqual("", backend.actionsHistory[0].Location);
         Assert.AreEqual(expectedDate, backend.actionsHistory[0].DateCompleted);
         Assert.AreEqual(expectedCharge, backend.actionsHistory[0].Charge);
         Assert.AreEqual(expectedAuthorizedBy, backend.actionsHistory[0].AuthorisedBy);
         Assert.AreEqual(expectedNotes, backend.actionsHistory[0].Notes);
         dbMocks.ActionHistoryMock.Verify(set => set.Attach(backend.actionsHistory[0]), Moq.Times.Once());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(backend.actionsHistory[0]), Moq.Times.Once());

         dbMocks.CtxMock.Verify(db => db.SaveChanges(), Moq.Times.Once());
      }

      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void testNegative_when_updating_location_of_NO_con_or_recon_action()
      {
         // GIVEN
         var historyEntry = new ActionHistory
         {
            Description = "NOTIMPORTANT",

            Location = "",
            Charge = 2.3,
            DateCompleted = DateTime.Today - new TimeSpan(2, 0, 0, 0),
            AuthorisedBy = null,
            Notes = "some notes at the begining"
         };

         string expectedLocation = "87654321";

         var backend = new Backend();
         var dbMocks = backend
            .WithActionHistory(historyEntry)
            .buildAllMocks();

         var target = new UpdateActionHistoryEntry(historyEntry, expectedLocation, 0, DateTime.Today, "", "", "DP-DUMMY", dbMocks.CtxMock.Object);

         // WHEN
         target.Run();
      }

      [TestMethod]
      public void testPositive_update_last_amend_date()
      {
         // GIVEN
         var historyEntry = new ActionHistory
         {
            Description = "NOTIMPORTANT",
            Location = "", 

            LastAmended = null
         };

         var backend = new Backend();
         var dbMocks = backend
            .WithActionHistory(historyEntry)
            .buildAllMocks();

         var target = new UpdateActionHistoryEntry(historyEntry, "", 0, DateTime.Today - new TimeSpan(5, 0, 0, 0), "", "", "DP-ANY", dbMocks.CtxMock.Object);

         // WHEN
         target.Run();

         // THEN
         Assert.AreEqual(DateTime.Today, backend.actionsHistory[0].LastAmended);
         dbMocks.ActionHistoryMock.Verify(set => set.Attach(backend.actionsHistory[0]), Moq.Times.Once());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(backend.actionsHistory[0]), Moq.Times.Once());

         dbMocks.CtxMock.Verify(db => db.SaveChanges(), Moq.Times.Once());
      }

   }
}
