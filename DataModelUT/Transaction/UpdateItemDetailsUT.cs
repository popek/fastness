﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fastness.DataModel;
using Fastness.DataModel.Transaction;
using Fastness.DataModel.Exceptions;

namespace DataModelUT.Transaction
{
   [TestClass]
   public class UpdateItemDetailsUT
   {
      [TestMethod]
      public void testPositive()
      {
         // GIVEN
         int newPercentage = 23;
         string newDescription = "description after change";
         string newCuid = "cuid after change";
         string newDestructionDate = "2013-03";

         var itemToUpdate = new ItemDetail 
         { 
            ItemNumber = "12345678",
            Location = "87654321",
            
            Percentage = 75,
            Description = "desc before",
            ClientItemRef = "cuid before",
            DestructionDate = "2012-02"            
         };

         var itemActionWithStandardPercentage = new ActionHistory
         {
            ItemNumber = itemToUpdate.ItemNumber,
            Percentage = itemToUpdate.Percentage,
            LastAmended = null
         };
         var itemActionWithNullMeaning100Percentage = new ActionHistory
         {
            ItemNumber = itemToUpdate.ItemNumber,
            Percentage = null,
            LastAmended = null
         };
         var itemActionWithZeroPercentage = new ActionHistory
         {
            ItemNumber = itemToUpdate.ItemNumber,
            Percentage = 0,
            LastAmended = null
         };


         var backend = new Backend();
         var dbMocks = backend
            .WithItem(itemToUpdate)
            .WithActionHistory(new ActionHistory 
            {
               ItemNumber = itemToUpdate.ItemNumber,
               Percentage = 50,
               LastAmended = null
            })
            .WithActionHistory(itemActionWithStandardPercentage)
            .WithActionHistory(itemActionWithNullMeaning100Percentage)
            .WithActionHistory(itemActionWithZeroPercentage)
            .buildAllMocks();

         var target = new UpdateItemDetails(itemToUpdate, newPercentage, newDestructionDate, newDescription, newCuid, dbMocks.CtxMock.Object);

         // WHEN
         target.Run();

         // THEN
         Assert.AreEqual(newPercentage, backend.items[0].Percentage);
         Assert.AreEqual(newDescription, backend.items[0].Description);
         Assert.AreEqual(newDestructionDate, backend.items[0].DestructionDate);
         Assert.AreEqual(newCuid, backend.items[0].ClientItemRef);
         dbMocks.ItemDetailMock.Verify(set => set.Attach(itemToUpdate), Moq.Times.Once());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(itemToUpdate), Moq.Times.Once());

         Assert.AreEqual(newPercentage, itemActionWithStandardPercentage.Percentage);
         Assert.AreEqual(DateTime.Today, itemActionWithStandardPercentage.LastAmended);
         dbMocks.ActionHistoryMock.Verify(set => set.Attach(itemActionWithStandardPercentage), Moq.Times.Once());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(itemActionWithStandardPercentage), Moq.Times.Once());

         Assert.AreEqual(newPercentage, itemActionWithNullMeaning100Percentage.Percentage);
         Assert.AreEqual(DateTime.Today, itemActionWithNullMeaning100Percentage.LastAmended);
         dbMocks.ActionHistoryMock.Verify(set => set.Attach(itemActionWithNullMeaning100Percentage), Moq.Times.Once());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(itemActionWithNullMeaning100Percentage), Moq.Times.Once());

         Assert.AreEqual(0, itemActionWithZeroPercentage.Percentage);
         Assert.AreEqual(null, itemActionWithZeroPercentage.LastAmended);
         dbMocks.ActionHistoryMock.Verify(set => set.Attach(itemActionWithZeroPercentage), Moq.Times.Never());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(itemActionWithZeroPercentage), Moq.Times.Never());

         dbMocks.CtxMock.Verify(db => db.SaveChanges(), Moq.Times.Once());
      }

      [TestMethod]
      [ExpectedException(typeof(DataDoesNotExistException))]
      public void throw_when_item_to_update_not_in_DB()
      {
         // GIVEN
         var itemToUpdateAbsentFromDB = new ItemDetail
         {
            ItemNumber = "12345678",
            Location = "87654321",

            Percentage = 75,
            Description = "desc before",
            DestructionDate = "2012-02"
         };
         var someItemInDB = new ItemDetail
         {
            ItemNumber = "0011223344",
            Location = "44332211",

            Percentage = 75,
            Description = "desc before",
            DestructionDate = "2012-02"
         };

         var backend = new Backend();
         var dbMocks = backend.WithItem(someItemInDB).buildAllMocks();

         var target = new UpdateItemDetails(itemToUpdateAbsentFromDB, 0, "", "", "", dbMocks.CtxMock.Object);

         // WHEN
         target.Run();
      }

      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void throw_when_item_business_rule_broken()
      {
         // GIVEN
         var itemWithoutItemNumber = new ItemDetail
         {
            ItemNumber = null,
            Location = "87654321",

            Percentage = 75,
            Description = "desc before",
            DestructionDate = "2012-02"
         };

         var backend = new Backend();
         var dbMocks = backend.WithItem(itemWithoutItemNumber).buildAllMocks();

         var target = new UpdateItemDetails(itemWithoutItemNumber, 0, "", "", "", dbMocks.CtxMock.Object);

         // WHEN
         target.Run();
      }

   }
}
