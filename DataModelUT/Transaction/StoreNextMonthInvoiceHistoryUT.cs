﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fastness.DataModel;
using Fastness.DataModel.Transaction;
using System.Collections.Generic;
using Fastness.DataModel.Exceptions;
using System;

namespace DataModelUT.Transaction
{
   [TestClass]
   public class StoreNextMonthInvoiceHistoryUT
   {
      [TestMethod]
      public void testPositive()
      {
         // WHEN
         int month = 6;
         int year = 2015;

         var someExistingInvoiceHistoryEntryForOtherMonth1 = new InvoiceHistory
         {
            Month = month - 1,
            Year = year,
            InvoiceNo = 1,
            ContractID = 4,
            Details = "dummy details",
         };
         var someExistingInvoiceHistoryEntryForOtherMonth2 = new InvoiceHistory
         {
            Month = month - 1,
            Year = year,
            InvoiceNo = 2,
            ContractID = 5,
            Details = "dummy details of other inv.",
         };

         var newInvoiceHistoryEntry1 = new InvoiceHistory
         {
            Month = month,
            Year = year,
            InvoiceNo = 5,
            ContractID = 4,
            Details = "dummy details",
         };
         var newInvoiceHistoryEntry2 = new InvoiceHistory
         {
            Month = month,
            Year = year,
            InvoiceNo = 6,
            ContractID = 5,
            Details = "dummy details in second entry",
         };
         var entriesToAdd = new List<InvoiceHistory> { newInvoiceHistoryEntry1, newInvoiceHistoryEntry2 };

         var nextInvoiceNoBefore = 111;
         var oneAndOnlyMiscParam = new MiscParams { NextInvoiceNo = nextInvoiceNoBefore };

         // contract stats suppose to be reseted
         var tatalInCtr1 = 999;
         var contractMatchingInvoice1 = new Contract 
         { 
            ContractID = newInvoiceHistoryEntry1.ContractID,
            ConLastMonth = 0,
            ConThisMonth = 33,
            ReturnedThisMonth = 3,
            ReConThisMonth = 5,
            DestroyedThisMonth = 4,
            Total = tatalInCtr1
         };
         var tatalInCtr2 = 99;
         var contractMatchingInvoice2 = new Contract
         {
            ContractID = newInvoiceHistoryEntry2.ContractID,
            ConLastMonth = 0,
            ConThisMonth = 33,
            ReturnedThisMonth = 3,
            ReConThisMonth = 5,
            DestroyedThisMonth = 4,
            Total = tatalInCtr2
         };

         var backend = new Backend();
         var dbMocks = backend
            .WithContract(contractMatchingInvoice1)
            .WithContract(contractMatchingInvoice2)
            .WithInvoiceHistory(someExistingInvoiceHistoryEntryForOtherMonth1)
            .WithInvoiceHistory(someExistingInvoiceHistoryEntryForOtherMonth2)
            .WithMiscParam(oneAndOnlyMiscParam)
            .buildAllMocks();

         var target = new StoreNextMonthInvoiceHistory(entriesToAdd, dbMocks.CtxMock.Object);

         // WHEN
         target.Run();

         // THEN
         Assert.AreEqual(4, backend.invoicesHistory.Count);
         Assert.IsTrue(TestHelper.AreValuesEqual(newInvoiceHistoryEntry1, backend.invoicesHistory[2], new string[] { "InvoiceHistoryID" }));
         Assert.IsTrue(TestHelper.AreValuesEqual(newInvoiceHistoryEntry2, backend.invoicesHistory[3], new string[] { "InvoiceHistoryID" }));

         var assertContract = new Action<Contract, int>((ctrToCheck, expectedTotal) =>
         {
            Assert.AreEqual(ctrToCheck.ConLastMonth, expectedTotal);
            Assert.AreEqual(ctrToCheck.ConThisMonth, 0);
            Assert.AreEqual(ctrToCheck.ReturnedThisMonth, 0);
            Assert.AreEqual(ctrToCheck.ReConThisMonth, 0);
            Assert.AreEqual(ctrToCheck.DestroyedThisMonth, 0);
            Assert.AreEqual(ctrToCheck.Total, expectedTotal);

            dbMocks.CtxMock.Verify(db => db.SetAsModified(ctrToCheck), Moq.Times.Once());
         });
         assertContract(backend.ctrs[0], tatalInCtr1);
         assertContract(backend.ctrs[1], tatalInCtr2);


         dbMocks.InvoiceHistoryMock.Verify(s => s.AddRange(entriesToAdd), Moq.Times.Once());

         Assert.AreEqual(oneAndOnlyMiscParam.NextInvoiceNo, nextInvoiceNoBefore + entriesToAdd.Count);
         dbMocks.CtxMock.Verify(db => db.SetAsModified(oneAndOnlyMiscParam), Moq.Times.Once());

         dbMocks.CtxMock.Verify(ctx => ctx.SaveChanges());
      }

      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void testNegative_when_invoices_does_not_have_the_same_yearMonth()
      {
         // WHEN
         int month = 6;
         int year = 2015;

         var newInvoiceHistoryEntry = new InvoiceHistory
         {
            Month = month,
            Year = year,
            InvoiceNo = 5,
            ContractID = 4,
            Details = "dummy details",
         };
         var newInvoiceHistoryEntryWithDifferentYearMonth = new InvoiceHistory
         {
            Month = month + 1,
            Year = year,
            InvoiceNo = 6,
            ContractID = 5,
            Details = "dummy details in second entry",
         };
         var backend = new Backend();
         var dbMocks = backend
            .buildAllMocks();

         var target = new StoreNextMonthInvoiceHistory(new List<InvoiceHistory> { newInvoiceHistoryEntry, newInvoiceHistoryEntryWithDifferentYearMonth }, dbMocks.CtxMock.Object);

         // WHEN
         target.Run();
      }

      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void testNegative_when_there_are_already_invoices_whith_the_same_yearMonth_in_DB()
      {
         // WHEN
         int month = 6;
         int year = 2015;

         var someExistingInvoiceHistoryEntryWithTheSameYearMonth = new InvoiceHistory
         {
            Month = month,
            Year = year,
            InvoiceNo = 61,
            ContractID = 8,
            Details = "dummy details ddddaaaabbbb",
         };

         var newInvoiceHistoryEntry = new InvoiceHistory
         {
            Month = month,
            Year = year,
            InvoiceNo = 5,
            ContractID = 4,
            Details = "dummy details",
         };

         var backend = new Backend();
         var dbMocks = backend
            .WithInvoiceHistory(someExistingInvoiceHistoryEntryWithTheSameYearMonth)
            .buildAllMocks();

         var target = new StoreNextMonthInvoiceHistory(new List<InvoiceHistory> { newInvoiceHistoryEntry }, dbMocks.CtxMock.Object);

         // WHEN
         target.Run();
      }

      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void testNegative_when_more_then_one_MiscParams_record_in_db()
      {
         // WHEN
         int month = 6;
         int year = 2015;

         var entriesToAdd = new List<InvoiceHistory>
         {
            new InvoiceHistory
            {
               Month = month,
               Year = year,
               InvoiceNo = 5,
               ContractID = 4,
               Details = "dummy details",
            }
         };
         var contractMatchingInvoice = new Contract
         {
            ContractID = entriesToAdd[0].ContractID,
            ConLastMonth = 0,
            ConThisMonth = 33,
            ReturnedThisMonth = 3,
            ReConThisMonth = 5,
            DestroyedThisMonth = 4,
            Total = 999
         };

         var backend = new Backend();
         var dbMocks = backend
            .WithContract(contractMatchingInvoice)
            .WithMiscParam(new MiscParams { NextInvoiceNo = 453 })
            .WithMiscParam(new MiscParams { NextInvoiceNo = 456 })
            .buildAllMocks();

         var target = new StoreNextMonthInvoiceHistory(entriesToAdd, dbMocks.CtxMock.Object);

         // WHEN
         target.Run();
      }
   }
}
