﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fastness.DataModel;
using Fastness.Common;
using Fastness.DataModel.Transaction;
using Fastness.DataModel.Exceptions;

namespace DataModelUT.Transaction
{
   [TestClass]
   public class UpdateFastnessActionUT
   {
      [TestMethod]
      public void update_single_contract_action_only()
      {
         // GIVEN
         double expectedCharge = 2345;
         double expectedPercentsFree = 23;

         var ctr = new Contract() 
         {
            ContractID = 22,
         };
         var ctrAction = new ContractAction 
         {
            ContractID = ctr.ContractID,
            Description = "dummy action",
            Code = "dummy code",

            Charge = 100,
            PerFreeAction = 13
         };
         var ctrActionBefore = TestHelper.DeepCopy<ContractAction>(ctrAction);

         var backend = new Backend();
         var dbMocks = backend.WithContract(ctr).WithContractAction(ctrAction).buildAllMocks();

         var target = new UpdateFastnessAction(ctrAction, expectedCharge, expectedPercentsFree, UpdateFastnessAction.Context.UPDATE_SINGLE_CONTRACT_ACTION, dbMocks.CtxMock.Object);
         
         //WHEN
         target.Run();

         //THEN
         Assert.AreEqual(backend.ctrActions.Count, 1);
         Assert.AreEqual(backend.ctrActions[0].Code, ctrActionBefore.Code);
         Assert.AreEqual(backend.ctrActions[0].Description, ctrActionBefore.Description);
         Assert.AreEqual(backend.ctrActions[0].Charge, expectedCharge);
         Assert.AreEqual(backend.ctrActions[0].PerFreeAction, expectedPercentsFree);
         dbMocks.ContractActionMock.Verify(set => set.Attach(ctrAction), Moq.Times.Once());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(ctrAction), Moq.Times.Once());

         dbMocks.ActionTemplateMock.Verify(set => set.Attach(Moq.It.IsAny<ActionTemplate>()), Moq.Times.Never());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(Moq.It.IsAny<ActionTemplate>()), Moq.Times.Never());

         dbMocks.CtxMock.Verify(db => db.SaveChanges(), Moq.Times.Once());
      }
      [TestMethod]
      public void update_template_action_only()
      {
         // GIVEN
         double expectedCharge = 2345;
         double expectedPercentsFree = 23;

         var ctr = new Contract()
         {
            ContractID = 22,
         };
         var at = new ActionTemplate
         {
            Description = "dummy action",
            Code = "dummy code",
            Charge = 11,
            PerFreeAction = 88
         };
         var ctrAction = new ContractAction
         {
            ContractID = ctr.ContractID,
            Description = at.Description,
            Code = at.Code,

            Charge = 100,
            PerFreeAction = 13
         };

         var ctrActionBefore = TestHelper.DeepCopy<ContractAction>(ctrAction);
         var atBefore = TestHelper.DeepCopy<ActionTemplate>(at);

         var backend = new Backend();
         var dbMocks = backend.
            WithContract(ctr).
            WithContractAction(ctrAction).
            WithActionTemplate(at).
            buildAllMocks();

         var target = new UpdateFastnessAction(ctrAction, expectedCharge, expectedPercentsFree, UpdateFastnessAction.Context.UPDATE_SERVICE_ACTION, dbMocks.CtxMock.Object);

         //WHEN
         target.Run();

         //THEN
         Assert.AreEqual(backend.ctrActions.Count, 1);
         Assert.AreEqual(backend.ctrActions[0].Code, ctrActionBefore.Code);
         Assert.AreEqual(backend.ctrActions[0].Description, ctrActionBefore.Description);
         Assert.AreEqual(backend.ctrActions[0].Charge, ctrActionBefore.Charge);
         Assert.AreEqual(backend.ctrActions[0].PerFreeAction, ctrActionBefore.PerFreeAction);
         dbMocks.ContractActionMock.Verify(set => set.Attach(Moq.It.IsAny<ContractAction>()), Moq.Times.Never());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(Moq.It.IsAny<ContractAction>()), Moq.Times.Never());

         Assert.AreEqual(backend.actionTemplate.Count, 1);
         Assert.AreEqual(backend.actionTemplate[0].Code, atBefore.Code);
         Assert.AreEqual(backend.actionTemplate[0].Description, atBefore.Description);
         Assert.AreEqual(backend.actionTemplate[0].Charge, expectedCharge);
         Assert.AreEqual(backend.actionTemplate[0].PerFreeAction, expectedPercentsFree);
         dbMocks.ActionTemplateMock.Verify(set => set.Attach(backend.actionTemplate[0]), Moq.Times.Once());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(backend.actionTemplate[0]), Moq.Times.Once());

         dbMocks.CtxMock.Verify(db => db.SaveChanges(), Moq.Times.Once());

      }
      [TestMethod]
      public void update_template_action_and_action_in_all_contracts()
      {
         // GIVEN
         double expectedCharge = 2345;
         double expectedPercentsFree = 23;

         var ctr1 = new Contract()
         {
            ContractID = 22,
         };
         var ctr2 = new Contract()
         {
            ContractID = 34,
         };
         var at = new ActionTemplate
         {
            Description = "dummy action",
            Code = "dummy code",
            Charge = 11,
            PerFreeAction = 88
         };
         var ctrAction1 = new ContractAction
         {
            ContractID = ctr1.ContractID,
            Description = at.Description,
            Code = at.Code,

            Charge = 100,
            PerFreeAction = 13
         };
         var ctrAction2 = new ContractAction
         {
            ContractID = ctr2.ContractID,
            Description = at.Description,
            Code = at.Code,

            Charge = 130,
            PerFreeAction = 3
         };

         var atBefore = TestHelper.DeepCopy<ActionTemplate>(at);

         var backend = new Backend();
         var dbMocks = backend.
            WithContract(ctr1).
            WithContract(ctr2).
            WithContractAction(ctrAction1).
            WithContractAction(ctrAction2).
            WithActionTemplate(at).
            buildAllMocks();

         var target = new UpdateFastnessAction(ctrAction1, expectedCharge, expectedPercentsFree, UpdateFastnessAction.Context.UPDATE_SERVICE_ACTION_AND_ALL_CONTRACT_ACTIONS, dbMocks.CtxMock.Object);

         //WHEN
         target.Run();

         //THEN
         Assert.AreEqual(backend.ctrActions.Count, 2);

         Assert.AreEqual(backend.ctrActions[0].Code, at.Code);
         Assert.AreEqual(backend.ctrActions[0].Description, at.Description);
         Assert.AreEqual(backend.ctrActions[0].Charge, expectedCharge);
         Assert.AreEqual(backend.ctrActions[0].PerFreeAction, expectedPercentsFree);
         dbMocks.ContractActionMock.Verify(set => set.Attach(backend.ctrActions[0]), Moq.Times.Once());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(backend.ctrActions[0]), Moq.Times.Once());

         Assert.AreEqual(backend.ctrActions[1].Code, at.Code);
         Assert.AreEqual(backend.ctrActions[1].Description, at.Description);
         Assert.AreEqual(backend.ctrActions[1].Charge, expectedCharge);
         Assert.AreEqual(backend.ctrActions[1].PerFreeAction, expectedPercentsFree);
         dbMocks.ContractActionMock.Verify(set => set.Attach(backend.ctrActions[1]), Moq.Times.Once());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(backend.ctrActions[1]), Moq.Times.Once());


         Assert.AreEqual(backend.actionTemplate.Count, 1);
         Assert.AreEqual(backend.actionTemplate[0].Code, atBefore.Code);
         Assert.AreEqual(backend.actionTemplate[0].Description, atBefore.Description);
         Assert.AreEqual(backend.actionTemplate[0].Charge, expectedCharge);
         Assert.AreEqual(backend.actionTemplate[0].PerFreeAction, expectedPercentsFree);
         dbMocks.ActionTemplateMock.Verify(set => set.Attach(backend.actionTemplate[0]), Moq.Times.Once());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(backend.actionTemplate[0]), Moq.Times.Once());

         dbMocks.CtxMock.Verify(db => db.SaveChanges(), Moq.Times.Once());
      }

      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void throw_when_trying_to_update_template_action_and_all_contract_actions_with_percents_free_more_then_100_percents()
      {
         throw_when_trying_to_update_with_percents_free_more_then_100_percents(UpdateFastnessAction.Context.UPDATE_SERVICE_ACTION_AND_ALL_CONTRACT_ACTIONS);
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void throw_when_trying_to_update_template_action_only_with_percents_free_more_then_100_percents()
      {
         throw_when_trying_to_update_with_percents_free_more_then_100_percents(UpdateFastnessAction.Context.UPDATE_SERVICE_ACTION);
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void throw_when_trying_to_update_contract_action_with_percents_free_more_then_100_percents()
      {
         throw_when_trying_to_update_with_percents_free_more_then_100_percents(UpdateFastnessAction.Context.UPDATE_SINGLE_CONTRACT_ACTION);
      }

      private void throw_when_trying_to_update_with_percents_free_more_then_100_percents(UpdateFastnessAction.Context ctx)
      {
         // GIVEN
         double invalidPercentsFreeValue = 101;

         var ctr = new Contract()
         {
            ContractID = 22,
         };
         var ctrAction = new ContractAction
         {
            ContractID = ctr.ContractID,
            Description = "dummy action",
            Code = "dummy code",

            Charge = 100,
            PerFreeAction = 13
         };
         var backend = new Backend();
         var dbMocks = backend.WithContract(ctr).WithContractAction(ctrAction).buildAllMocks();

         var target = new UpdateFastnessAction(ctrAction, 22, invalidPercentsFreeValue, ctx, dbMocks.CtxMock.Object);

         //WHEN
         target.Run();
      }

   }
}
