﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fastness.DataModel;
using Fastness.DataModel.Transaction;

namespace DataModelUT.Transaction
{
   [TestClass]
   public class UpdateEmailedInvoiceDetailsUT
   {
      [TestMethod]
      public void test_lastSendInvoice_is_updated_in_contracts_for_which_invoices_were_send()
      {
         // GIVEN
         var dateOfEmailedInvoices = new DateTime(2015, 8, 3);
         var contractForWhichInvoicesWereSendIds = new int[] { 4, 7 };

         var backend = new Backend();
         var totalContractsCount = 10;
         while (totalContractsCount-- > 0)
         {
            backend.WithContract(new Contract());
         }
         var dbMocks = backend.buildAllMocks();

         var target = new UpdateEmailedInvoiceDetails(contractForWhichInvoicesWereSendIds.ToList<int>(), dateOfEmailedInvoices, dbMocks.CtxMock.Object);

         // WHEN
         target.Run();

         // THEN
         Assert.IsTrue(
            backend.ctrs.Where(c => contractForWhichInvoicesWereSendIds.Contains(c.ContractID))
            .All(c => c.LastSendInvoice == dateOfEmailedInvoices)
            , "not all contracts for which invoices were send have new LastSendInvoice!");

         foreach (var ctr in backend.ctrs.Where(c => contractForWhichInvoicesWereSendIds.Contains(c.ContractID)))
         {
            dbMocks.ContractMock.Verify(set => set.Attach(ctr), Moq.Times.Once(),
            "contract for which invoice email was send, hasn't been attached to DB!");

            dbMocks.CtxMock.Verify(db => db.SetAsModified(ctr), Moq.Times.Once(),
            "contract for which invoice email was send, was not set as modified!");
         }
         dbMocks.CtxMock.Verify(db => db.SaveChanges(), Moq.Times.Once(),
         "transaction were not saved(populated) into DB!");
      }
      [TestMethod]
      public void test_lastSendInvoice_is_not_changed_in_contracts_for_which_invoices_were_NOT_send()
      {
         // GIVEN
         var dateOfEmailedInvoices = new DateTime(2015, 8, 3);
         var contractForWhichInvoicesWereSendIds = new int[] { 4, 7 };

         var backend = new Backend();
         var totalContractsCount = 10;
         while (totalContractsCount-- > 0)
         {
            backend.WithContract(new Contract());
         }
         var dbMocks = backend.buildAllMocks();

         var target = new UpdateEmailedInvoiceDetails(contractForWhichInvoicesWereSendIds.ToList<int>(), dateOfEmailedInvoices, dbMocks.CtxMock.Object);

         // WHEN
         target.Run();

         Assert.IsTrue(
            backend.ctrs.Where(c => !contractForWhichInvoicesWereSendIds.Contains(c.ContractID))
            .All(c => c.LastSendInvoice == null),
            "some contracts for which invoices were NOT send have new LastSendInvoice date!");

         foreach (var ctr in backend.ctrs.Where(c => !contractForWhichInvoicesWereSendIds.Contains(c.ContractID)))
         {
            dbMocks.ContractMock.Verify(set => set.Attach(ctr), Moq.Times.Never(),
            "contract for which invoice email was NOT send, was attached to DB!");

            dbMocks.CtxMock.Verify(db => db.SetAsModified(ctr), Moq.Times.Never(),
            "contract for which invoice email was NOT send, was set as modified!");
         }
      }
   }
}
