﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fastness.DataModel;
using Fastness.Common;
using Fastness.DataModel.Transaction;
using Fastness.DataModel.Exceptions;

namespace DataModelUT.Transaction
{
   [TestClass]
   public class ReconsignItemUT
   {
      [TestMethod]
      public void reconsign_item_OK()
      {
         // GIVEN
         int conLastMonth_before = 5;
         int conThisMonth_before = 8;
         int reConThisMonth_before = 2;
         int returnedThisMonth_before = 0;
         int destroyedThisMonth_before = 0;
         int total_before = 15;
         var ctr = new Contract()
         {
            ContractID = 22,

            ConLastMonth = conLastMonth_before,
            ConThisMonth = conThisMonth_before,
            ReConThisMonth = reConThisMonth_before,
            ReturnedThisMonth = returnedThisMonth_before,
            DestroyedThisMonth = destroyedThisMonth_before,
            Total = total_before
         };
         var ctrAction = new ContractAction
         {
            ContractActionID = 123,
            Description = ActionUtils.ACTION_DESCRIPTION_RECONSIGNMENT,
            Charge = 88,
            ContractID = ctr.ContractID,
            PerFreeAction = 13
         };

         var itemToReconsign = new ItemDetail
         {
            ItemNumber = "11111111",
            ContractID = ctr.ContractID,
            ClientItemRef = "someRef",
            Description = "some description",
            DestructionDate = "2012-11",
            Type = "dummyType",
            Percentage = 111,
            Location = "",
            DateRecieved = new DateTime(2011, 1, 1),
            DateRetrieved = new DateTime(2011, 11, 1),
            InStock = false
         };
         var itemBefore = TestHelper.DeepCopy<ItemDetail>(itemToReconsign);

         var expectedCharge = 1234.33;
         var expectedAuthor = "some dummy consign authorization";
         var expectedDateOfAction = new DateTime(2012, 11, 1);
         var expectedLocation = "12345678";
         var exptectedActionHistory = new ActionHistory
         {
            ItemNumber = itemToReconsign.ItemNumber,
            Description = ActionUtils.ACTION_DESCRIPTION_RECONSIGNMENT,
            Notes = "",
            Charge = expectedCharge,
            Date = DateTime.Today,
            DateCompleted = expectedDateOfAction,
            AuthorisedBy = expectedAuthor,
            Location = expectedLocation,
            Percentage = itemToReconsign.Percentage,
            ContractActionID = ctrAction.ContractActionID
         };

         var backend = new Backend();
         var dbMocks = backend.WithContract(ctr).WithItem(itemToReconsign).buildAllMocks();

         var ctx = new FastnessActionContext(itemToReconsign, ctr, ctrAction, expectedDateOfAction, expectedCharge, expectedAuthor);
         ctx.AttachDbContext(dbMocks.CtxMock.Object);
         var target = new ReconsignItem(ctx, expectedLocation);

         //WHEN
         target.Run();

         //THEN
         Assert.AreEqual(backend.items.Count, 1);
         Assert.AreEqual(backend.items[0], itemToReconsign);
         Assert.AreEqual(backend.items[0].DateReturned, expectedDateOfAction);
         Assert.IsTrue(TestHelper.AreValuesEqual<ItemDetail>(backend.items[0], itemBefore, new string[] { "InStock", "InStockEOM", "ItemID", "DateReturned", "DateRetrieved", "Location" }), "item details which supposed to stay unchanged, were changed!");
         Assert.IsTrue(backend.items[0].InStock ?? true);
         Assert.IsNull(backend.items[0].DateRetrieved);
         Assert.AreEqual(backend.items[0].Location, expectedLocation);
         dbMocks.ItemDetailMock.Verify(set => set.Attach(backend.items[0]), Moq.Times.Once());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(backend.items[0]), Moq.Times.Once());

         Assert.AreEqual(ctr.ConLastMonth, conLastMonth_before);
         Assert.AreEqual(ctr.ConThisMonth, conThisMonth_before);
         Assert.AreEqual(ctr.ReConThisMonth, reConThisMonth_before + 1);
         Assert.AreEqual(ctr.ReturnedThisMonth, returnedThisMonth_before);
         Assert.AreEqual(ctr.DestroyedThisMonth, destroyedThisMonth_before);
         Assert.AreEqual(ctr.Total, total_before + 1);
         dbMocks.ContractMock.Verify(set => set.Attach(ctr), Moq.Times.Once());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(ctr), Moq.Times.Once());

         Assert.IsTrue(TestHelper.AreValuesEqual<ActionHistory>(backend.actionsHistory[0], exptectedActionHistory, new string[] { "ActionHistoryID" }), "action history entry is not as expected one!");
         dbMocks.ActionHistoryMock.Verify(set => set.Add(backend.actionsHistory[0]), Moq.Times.Once());

         dbMocks.CtxMock.Verify(db => db.SaveChanges(), Moq.Times.Once());
      }
      [TestMethod]
      [ExpectedException(typeof(DataDoesNotExistException))]
      public void reconsign_item_ERROR_when_item_is_not_in_db_yet()
      {
         // GIVEN
         var itemToReconsign = new ItemDetail
         {
            ItemNumber = "12345678",
            Location = "",
         };

         var backend = new Backend();
         var dbMocks = backend.buildAllMocks();

         var ctx = new FastnessActionContext(itemToReconsign, null, null, DateTime.Today, 0, "");
         ctx.AttachDbContext(dbMocks.CtxMock.Object);
         var target = new ReconsignItem(ctx, "12345678");

         //WHEN
         target.Run();
      }
      [TestMethod]
      [ExpectedException(typeof(DataNotUniqueException))]
      public void reconsign_item_ERROR_when_db_item_is_not_uniqe_which_indicates_on_db_constraint_error()
      {
         // GIVEN
         var notUniqueItemNo = "12345678";
         var itemToReconsign = new ItemDetail
         {
            ItemNumber = notUniqueItemNo,
            Location = ""
         };

         var backend = new Backend();
         var dbMocks = backend
            .WithItem(itemToReconsign)
            .WithItem(new ItemDetail { ItemNumber = notUniqueItemNo })
            .buildAllMocks();

         var ctx = new FastnessActionContext(itemToReconsign, null, null, DateTime.Today, 0, "");
         ctx.AttachDbContext(dbMocks.CtxMock.Object);
         var target = new ReconsignItem(ctx, "12345678");

         //WHEN
         target.Run();
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void reconsign_item_ERROR_when_item_marked_as_in_stock_already()
      {
         // GIVEN
         var itemToReconsign = new ItemDetail
         {
            ItemNumber = "123456789",
            Location = "",
            InStock = true
         };

         var backend = new Backend();
         var dbMocks = backend.WithItem(itemToReconsign).buildAllMocks();

         var ctx = new FastnessActionContext(itemToReconsign, null, null, DateTime.Today, 0, "");
         ctx.AttachDbContext(dbMocks.CtxMock.Object);
         var target = new ReconsignItem(ctx, "12345678");

         //WHEN
         target.Run();
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void reconsign_item_ERROR_when_item_has_location_already()
      {
         // GIVEN
         var itemToReconsign = new ItemDetail
         {
            ItemNumber = "123456789",
            Location = "87654321",
            InStock = false
         };

         var backend = new Backend();
         var dbMocks = backend.WithItem(itemToReconsign).buildAllMocks();

         var ctx = new FastnessActionContext(itemToReconsign, null, null, DateTime.Today, 0, "");
         ctx.AttachDbContext(dbMocks.CtxMock.Object);
         var target = new ReconsignItem(ctx, "12345678");

         //WHEN
         target.Run();
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void reconsign_item_ERROR_when_item_retrieved_date_is_null()
      {
         // GIVEN
         var itemToReconsign = new ItemDetail
         {
            ItemNumber = "123456789",
            Location = "",
            InStock = false,
            DateRetrieved = null
         };

         var backend = new Backend();
         var dbMocks = backend.WithItem(itemToReconsign).buildAllMocks();

         var ctx = new FastnessActionContext(itemToReconsign, null, null, DateTime.Today, 0, "");
         ctx.AttachDbContext(dbMocks.CtxMock.Object);
         var target = new ReconsignItem(ctx, "12345678");

         //WHEN
         target.Run();
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void reconsign_item_ERROR_when_item_destroy_date_is_set()
      {
         // GIVEN
         var itemToReconsign = new ItemDetail
         {
            ItemNumber = "123456789",
            Location = "",
            InStock = false,
            DateRetrieved = new DateTime(2012, 1, 1),
            DateDestroyed = new DateTime(2013, 1, 1)
         };

         var backend = new Backend();
         var dbMocks = backend.WithItem(itemToReconsign).buildAllMocks();

         var ctx = new FastnessActionContext(itemToReconsign, null, null, DateTime.Today, 0, "");
         ctx.AttachDbContext(dbMocks.CtxMock.Object);
         var target = new ReconsignItem(ctx, "12345678");

         //WHEN
         target.Run();
      }
   }
}
