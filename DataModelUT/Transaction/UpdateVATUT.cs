﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fastness.DataModel;
using Fastness.DataModel.Transaction;
using Fastness.DataModel.Exceptions;

namespace DataModelUT.Transaction
{
   [TestClass]
   public class UpdateVATUT
   {
      [TestMethod]
      public void testPositive()
      {
         // GIVEN
         double expectedVat = 0.22;

         var backend = new Backend();
         var dbMocks = backend
            .WithMiscParam(new MiscParams { VAT = 0.14})
            .buildAllMocks();

         var target = new UpdateVAT(expectedVat, dbMocks.CtxMock.Object);

         // WHEN
         target.Run();

         // THEN
         Assert.AreEqual(expectedVat, backend.miscParams[0].VAT);
         dbMocks.MiscParamsMock.Verify(set => set.Attach(backend.miscParams[0]), Moq.Times.Once());
         dbMocks.CtxMock.Verify(db => db.SetAsModified(backend.miscParams[0]), Moq.Times.Once());

         dbMocks.CtxMock.Verify(db => db.SaveChanges(), Moq.Times.Once());
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void testNegative_when_new_value_is_grater_then_100_percents()
      {
         // GIVEN
         double newToLargetVatValue = 1.1;

         var backend = new Backend();
         var dbMocks = backend
            .WithMiscParam(new MiscParams { VAT = .23 })
            .buildAllMocks();

         var target = new UpdateVAT(newToLargetVatValue, dbMocks.CtxMock.Object);

         // WHEN
         target.Run();
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void testNegative_when_more_then_one_MiscParams_record_in_db()
      {
         // GIVEN
         double newVatValue = 0.1;

         var backend = new Backend();
         var dbMocks = backend
            .WithMiscParam(new MiscParams { VAT = .23 })
            .WithMiscParam(new MiscParams { VAT = .43 })
            .buildAllMocks();

         var target = new UpdateVAT(newVatValue, dbMocks.CtxMock.Object);

         // WHEN
         target.Run();
      }

   }
}
