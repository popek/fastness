﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModelUT
{
   public class TestHelper
   {
      static public T DeepCopy<T>(T src)
      {
         var dest = Activator.CreateInstance<T>();
         var type = typeof(T);

         foreach (var prop in type.GetProperties())
         {
            prop.SetValue(dest, prop.GetValue(src));
         }
         foreach (var field in type.GetFields())
         {
            field.SetValue(dest, field.GetValue(src));
         }
         return dest;
      }

      static public bool AreValuesEqual<T>(T l, T r, string[] propsFieldsToSkip)
      {
         var type = typeof(T);

         foreach (var prop in type.GetProperties())
         {
            if (propsFieldsToSkip.Contains(prop.Name))
            {
               continue;
            }
            if (!Object.Equals(prop.GetValue(l), prop.GetValue(r)))
            {
               return false;
            }
         }
         foreach (var field in type.GetFields())
         {
            if (propsFieldsToSkip.Contains(field.Name))
            {
               continue;
            }
            if (!Object.Equals(field.GetValue(l), field.GetValue(r)))
            {
               return false;
            }
         }
         return true;
      }
   }
}
