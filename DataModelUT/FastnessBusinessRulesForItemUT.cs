﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fastness.DataModel;
using Fastness.DataModel.Utils;
using Fastness.DataModel.Exceptions;

namespace DataModelUT
{
   [TestClass]
   public class FastnessBusinessRulesForItemUT
   {
      private void AssertNoThrow(Action a)
      {
         try
         {
            a();
         }
         catch (Exception e)
         {
            Assert.Fail(string.Format("error: exception ({0}) should NOT happen! ", e));
         }
      }


      private ItemDetail CreateJustConsignedItem()
      {
         return new ItemDetail
         {
            ItemNumber = "10101010",
            DateRecieved = DateTime.Today,
            DateDestroyed = null,
            DateRetrieved = null,
            DateReturned = null,
            Destroyed = null,
            Location = "12345678",
            InStock = true
         };
      }
      private ItemDetail CreateReConsignedItem()
      {
         var target = CreateJustConsignedItem();
         target.DateReturned = DateTime.Today;
         return target;
      }
      private ItemDetail CreateRetrievedItem()
      {
         var target = CreateJustConsignedItem();
         target.DateRetrieved = DateTime.Today;
         target.InStock = false;
         target.Location = "";
         return target;
      }
      private ItemDetail CreateDestroyedItem()
      {
         var target = CreateJustConsignedItem();
         target.DateDestroyed = DateTime.Today;
         target.Destroyed = true;
         target.InStock = false;
         target.Location = "";
         return target;
      }

      [TestMethod]
      public void item_consigned_OK()
      {
         var target = CreateJustConsignedItem();
         AssertNoThrow(() => 
         {
            FastnessBusinessRules.ValidateItemCosistency(target);
         });
      }
      [TestMethod]
      public void item_reConsigned_OK()
      {
         var target = CreateReConsignedItem();
         AssertNoThrow(() =>
         {
            FastnessBusinessRules.ValidateItemCosistency(target);
         });
      }
      [TestMethod]
      public void item_retrieved_OK()
      {
         var target = CreateRetrievedItem();
         AssertNoThrow(() =>
         {
            FastnessBusinessRules.ValidateItemCosistency(target);
         });
      }
      [TestMethod]
      public void item_destroyed_OK()
      {
         var target = CreateDestroyedItem();
         AssertNoThrow(() =>
         {
            FastnessBusinessRules.ValidateItemCosistency(target);
         });
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void item_consigned_ERROR_no_consign_date()
      {
         //GIVEN
         var target = CreateJustConsignedItem();
         target.DateRecieved = null;
         //WHEN
         FastnessBusinessRules.ValidateItemCosistency(target);
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void item_consigned_ERROR_invalid_item_number()
      {
         //GIVEN
         var target = CreateJustConsignedItem();
         target.ItemNumber = "22";
         //WHEN
         FastnessBusinessRules.ValidateItemCosistency(target);
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void item_consigned_ERROR_destroy_date_exists()
      {
         //GIVEN
         var target = CreateJustConsignedItem();
         target.DateDestroyed = DateTime.Today;
         //WHEN
         FastnessBusinessRules.ValidateItemCosistency(target);
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void item_consigned_ERROR_destroy_flag_true()
      {
         //GIVEN
         var target = CreateJustConsignedItem();
         target.Destroyed = true;
         //WHEN
         FastnessBusinessRules.ValidateItemCosistency(target);
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void item_consigned_ERROR_retrieve_date_exists()
      {
         //GIVEN
         var target = CreateJustConsignedItem();
         target.DateRetrieved = DateTime.Today;
         //WHEN
         FastnessBusinessRules.ValidateItemCosistency(target);
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void item_consigned_ERROR_inStock_flag_is_false()
      {
         //GIVEN
         var target = CreateJustConsignedItem();
         target.InStock = false;
         //WHEN
         FastnessBusinessRules.ValidateItemCosistency(target);
      }

      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void item_reconsigned_ERROR_destroy_date_exists()
      {
         //GIVEN
         var target = CreateReConsignedItem();
         target.DateDestroyed = DateTime.Today;
         //WHEN
         FastnessBusinessRules.ValidateItemCosistency(target);
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void item_reconsigned_ERROR_destroy_flag_true()
      {
         //GIVEN
         var target = CreateReConsignedItem();
         target.Destroyed = true;
         //WHEN
         FastnessBusinessRules.ValidateItemCosistency(target);
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void item_reconsigned_ERROR_retrieve_date_exists()
      {
         //GIVEN
         var target = CreateReConsignedItem();
         target.DateRetrieved = DateTime.Today;
         //WHEN
         FastnessBusinessRules.ValidateItemCosistency(target);
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void item_reconsigned_ERROR_inStock_flag_is_false()
      {
         //GIVEN
         var target = CreateReConsignedItem();
         target.InStock = false;
         //WHEN
         FastnessBusinessRules.ValidateItemCosistency(target);
      }

      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void item_retrieved_ERROR_destroy_date_exists()
      {
         //GIVEN
         var target = CreateRetrievedItem();
         target.DateDestroyed = DateTime.Today;
         //WHEN
         FastnessBusinessRules.ValidateItemCosistency(target);
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void item_retrieved_ERROR_destroy_flag_true()
      {
         //GIVEN
         var target = CreateRetrievedItem();
         target.Destroyed = true;
         //WHEN
         FastnessBusinessRules.ValidateItemCosistency(target);
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void item_retrieved_ERROR_reconsign_date_exists()
      {
         //GIVEN
         var target = CreateRetrievedItem();
         target.DateReturned = DateTime.Today;
         //WHEN
         FastnessBusinessRules.ValidateItemCosistency(target);
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void item_retrieved_ERROR_inStock_flag_is_true()
      {
         //GIVEN
         var target = CreateRetrievedItem();
         target.InStock = true;
         //WHEN
         FastnessBusinessRules.ValidateItemCosistency(target);
      }

      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void item_destroyed_ERROR_destroy_flag_is_false()
      {
         //GIVEN
         var target = CreateDestroyedItem();
         target.Destroyed = false;
         //WHEN
         FastnessBusinessRules.ValidateItemCosistency(target);
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void item_destroyed_ERROR_reconsigned_date_exists()
      {
         //GIVEN
         var target = CreateDestroyedItem();
         target.DateReturned = DateTime.Today;
         //WHEN
         FastnessBusinessRules.ValidateItemCosistency(target);
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void item_destroyed_ERROR_retrieved_date_exists()
      {
         //GIVEN
         var target = CreateDestroyedItem();
         target.DateRetrieved = DateTime.Today;
         //WHEN
         FastnessBusinessRules.ValidateItemCosistency(target);
      }
      [TestMethod]
      [ExpectedException(typeof(BusinessRuleException))]
      public void item_destroyed_ERROR_inStock_flag_is_true()
      {
         //GIVEN
         var target = CreateDestroyedItem();
         target.InStock = true;
         //WHEN
         FastnessBusinessRules.ValidateItemCosistency(target);
      }
   }
}
