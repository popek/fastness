﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fastness.DataModel.Utils;
using System.Linq;
using System;

namespace DataModelUT
{
   [TestClass]
   public class MiscUT
   {
      [TestMethod]
      public void FixAddress_remove_blank_lines()
      {
         var target =
@"line 1

line 3

line5

";
         var expected =
@"line 1
line 3
line5";
         var result = Misc.FixAddress(target);

         Assert.AreEqual(expected, result);         
      }
      [TestMethod]
      public void FixAddress_trim_white_spaces()
      {
         var target =
@"   line 1   
  line 2   
      line 3
  line 4    ";
         var expected =
@"line 1
line 2
line 3
line 4";
         var result = Misc.FixAddress(target);

         Assert.AreEqual(expected, result);
      }
      [TestMethod]
      public void FixAddress_recognize_as_blank_address()
      {
         var target =
@"   
  
      
     ";
         var expected = "";
         var result = Misc.FixAddress(target);

         Assert.AreEqual(expected, result);
      }

      [TestMethod]
      public void ValidateAddress_report_to_long()
      {
         var maxCharsCount = 10;

         var target =
@"line 1

line 3

line5

";
         var result = Misc.ValidateAddress(target, maxCharsCount, 12);

         Assert.AreEqual(Misc.AddressStatus.ADDRESS_TO_LONG, result);
      }

      [TestMethod]
      public void ValidateAddress_report_empty()
      {
         var target =
@"   
  
      
     ";
         var result = Misc.ValidateAddress(target, 10, 12);

         Assert.AreEqual(Misc.AddressStatus.ADDRESS_IS_EMPTY, result);
      }

      [TestMethod]
      public void ValidateAddress_report_to_much_lines()
      {
         var maxLinesCount = 4;

         var target =
@"line 1

line 3

line5

   fdsajfd    

fdsaf d 

";
         var result = Misc.ValidateAddress(target, 200, maxLinesCount);

         Assert.AreEqual(Misc.AddressStatus.ADDRESS_TO_MUCH_LINES, result);
      }

      [TestMethod]
      public void EmailAddressesListToArray_remove_blank_lines()
      {
         var target =
@"mail.1@ble.com

mail.3@blah.pl

mail.rup.pup@erfds-ld.com

";
         string[] expected = { "mail.1@ble.com", "mail.3@blah.pl", "mail.rup.pup@erfds-ld.com" };
         var result = Misc.EmailAddressesListToArray(target);

         Assert.IsTrue(expected.SequenceEqual(result));
      }
      [TestMethod]
      public void EmailAddressesListToArray_trim_white_spaces()
      {
         var target =
@"   mail.1@ble.com   
  mail.3@blah.pl   
      mail.rup.pup@erfds-ld.com  ";
         string[] expected = { "mail.1@ble.com", "mail.3@blah.pl", "mail.rup.pup@erfds-ld.com" };
         var result = Misc.EmailAddressesListToArray(target);

         Assert.IsTrue(expected.SequenceEqual(result));
      }

   }
}
