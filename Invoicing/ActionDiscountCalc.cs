﻿using Fastness.Common;
using Fastness.DataModel;
using log4net;
using System;
using System.Linq;

namespace Fastness.Invoicing
{
   public class ActionDiscountCalc
   {
      protected double discountInPercents;

      public ActionDiscountCalc(double discountInPercents)
      {
         this.discountInPercents = discountInPercents;
      }

      public virtual double ApplyDiscount(double originalCharge)
      {
         return originalCharge * (100 - discountInPercents) / 100;
      }
   }

   public class RetrtieveActionDiscountCalc : ActionDiscountCalc
   {
      static readonly ILog log = LogManager.GetLogger(typeof(RetrtieveActionDiscountCalc));

      private int retrievesCount;
      private int amountOfItems;

      public RetrtieveActionDiscountCalc(double discountInPercents, int retrievesCount, int amountOfItems)
         : base(discountInPercents)
      {
         this.retrievesCount = retrievesCount;
         this.amountOfItems = amountOfItems;
      }

      public override double ApplyDiscount(double originalCharge)
      {
         var noChargeRetrievsCount = (int)(discountInPercents * amountOfItems / 100);

         log.DebugFormat("calculating retrieve actions discount (discount[%]: {0}, retrievs count: {1}, free retrievs count: {2})", 
                         discountInPercents, retrievesCount, noChargeRetrievsCount);

         if(noChargeRetrievsCount >= retrievesCount)
         {
            return 0;
         }
         return originalCharge * (retrievesCount - noChargeRetrievsCount)/retrievesCount;
      }
   }
}
