﻿using System;
using System.Linq;
using Fastness.DataModel;
using Fastness.DataModel.Utils;
using Fastness.Common;
using log4net;
using Fastness.DataModel.Exceptions;

namespace Fastness.Invoicing
{
   //
   public interface IInvoiceGenerator
   {
      Invoice Generate(DateTime monthAndYear, int invoiceNo);
   }

   //
   class InStockItemsStatsManager
   {
      public ItemsAndPercentsCount ConsignedCount { get; private set; }
      public ItemsAndPercentsCount ReconsignedCount { get; private set; }
      
      public ItemsAndPercentsCount RetrievedCount { get; private set; }
      public ItemsAndPercentsCount ShrededCount { get; private set; }

      public InStockItemsStatsManager()
      {
         ConsignedCount = new ItemsAndPercentsCount();
         ReconsignedCount = new ItemsAndPercentsCount();
         RetrievedCount = new ItemsAndPercentsCount();
         ShrededCount = new ItemsAndPercentsCount();
      }
      public void UpdateStats(string actionCode, int actionsCount, int precentsCount)
      {
         ItemsAndPercentsCount target = null;
         if (ActionUtils.IsConsignment(actionCode))
         {
            target = ConsignedCount;
         }
         else if (ActionUtils.IsReconsignment(actionCode))
         {
            target = ReconsignedCount;
         }
         else if (ActionUtils.IsAnyKindOfRetrieval(actionCode))
         {
            target = RetrievedCount;
         }
         else if (ActionUtils.IsShreading(actionCode))
         {
            target = ShrededCount;
         }

         if(target != null)
         {
            target.ItemsCount += actionsCount;
            target.ItemsPercentsCount += precentsCount;
         }
      }

      public ItemsAndPercentsCount CalculateItemsAtTheBeginingOfMonth(ItemsAndPercentsCount itemsAtTheEndOfMonthCount)
      {
         return new ItemsAndPercentsCount
         {
            ItemsCount = itemsAtTheEndOfMonthCount.ItemsCount 
                         - ConsignedCount.ItemsCount
                         - ReconsignedCount.ItemsCount 
                         + RetrievedCount.ItemsCount 
                         + ShrededCount.ItemsCount,
            ItemsPercentsCount = itemsAtTheEndOfMonthCount.ItemsPercentsCount 
                                 - ConsignedCount.ItemsPercentsCount 
                                 - ReconsignedCount.ItemsPercentsCount
                                 + RetrievedCount.ItemsPercentsCount
                                 + ShrededCount.ItemsPercentsCount
         };
      }
   }

   //
   class ConAndReconDiscountCalc
   {
      static readonly ILog log = LogManager.GetLogger(typeof(ConAndReconDiscountCalc));

      Invoice.Entry conEntry;
      Invoice.Entry reconEntry;

      InStockItemsStatsManager stats;

      double itemCharge;

      public ConAndReconDiscountCalc(InStockItemsStatsManager stats, double itemCharge)
      {
         this.stats = stats;
         this.itemCharge = itemCharge;
      }

      public void NotifyEntry(string actionCode, Invoice.Entry entry)
      {
         if (ActionUtils.IsConsignment(actionCode))
         {
            if (conEntry != null)
            {
               throw new InvalidOperationException("registering consignment for the second time indicates an error!");
            }
            conEntry = entry;
         }

         if (ActionUtils.IsReconsignment(actionCode))
         {
            if (reconEntry != null)
            {
               throw new InvalidOperationException("registering re-consignment for the second time indicates an error!");
            }
            reconEntry = entry;
         }
      }

      public void ApplyDiscount(ItemsAndPercentsCount itemsToBeDiscounted)
      {
         HandleEntryDiscount(conEntry, stats.ConsignedCount, itemsToBeDiscounted, itemCharge);
         HandleEntryDiscount(reconEntry, stats.ReconsignedCount, itemsToBeDiscounted, itemCharge);
      }

      static void HandleEntryDiscount(Invoice.Entry entry, ItemsAndPercentsCount entryItems, ItemsAndPercentsCount itemsToBeDiscounted, double itemCharge)
      {
         if (entry != null)
         {
            log.DebugFormat("before applying discount for '{0}': quantity={1}, charge={2:0.000}. items to be discounted: {3}({4})", entry.Description, entry.Quantity, entry.Charge, itemsToBeDiscounted.ItemsCount, itemsToBeDiscounted.ItemsPercentsCount);

            if (entryItems.ItemsCount >= itemsToBeDiscounted.ItemsCount)
            {
               entry.Quantity = entryItems.ItemsCount - itemsToBeDiscounted.ItemsCount;
               itemsToBeDiscounted.ItemsCount = 0;
            }
            else
            {
               itemsToBeDiscounted.ItemsCount -= entryItems.ItemsCount;
               entry.Quantity = 0;
            }

            if (entryItems.ItemsPercentsCount >= itemsToBeDiscounted.ItemsPercentsCount)
            {
               // TODO: most probably need to be corrected to entry.Charge -= itemCharge * (double)itemsToBeDiscounted.ItemsPercentsCount / 100
               entry.Charge = itemCharge * (double)(entryItems.ItemsPercentsCount - itemsToBeDiscounted.ItemsPercentsCount) / 100 ;
               itemsToBeDiscounted.ItemsPercentsCount = 0;
            }
            else
            {
               entry.Charge = 0;
               itemsToBeDiscounted.ItemsPercentsCount -= entryItems.ItemsPercentsCount;
            }

            log.DebugFormat("after applying discount for '{0}': quantity={1}, charge={2:0.000}. items to be discounted: {3}({4})", entry.Description, entry.Quantity, entry.Charge, itemsToBeDiscounted.ItemsCount, itemsToBeDiscounted.ItemsPercentsCount);
         }
      }
   }

   public class InvoiceGeneratorForContract : IInvoiceGenerator
   {
      static readonly ILog log = LogManager.GetLogger(typeof(InvoiceGeneratorForContract));

      double vat;
      Contract ctr;
      fastnessEntities dbCtx;

      ContractInfoProvider ctrInfoProv;
      InStockItemsStatsManager itemsDuringMonthStats;
      ItemsAndPercentsCount itemsInStockOnTheEndOfMonth;

      public InvoiceGeneratorForContract(double vat, Contract ctr, fastnessEntities dbCtx)
      {
         this.vat = vat;
         this.ctr = ctr;
         this.dbCtx = dbCtx;

         this.ctrInfoProv = new ContractInfoProvider(dbCtx);
         this.itemsDuringMonthStats = new InStockItemsStatsManager();
         this.itemsInStockOnTheEndOfMonth = new ItemsAndPercentsCount();
      }

      public Invoice Generate(DateTime monthAndYear, int invoiceNo)
      {
         log.InfoFormat("generating invoice for contract {0}", ctr.LogDesc());

         var first = FirstDay(monthAndYear);
         var last = LastDay(monthAndYear);

         itemsInStockOnTheEndOfMonth = ctrInfoProv.GetContractItemsCount(ctr.ContractID);

         log.DebugFormat("items in stock EOM: {0}({1})", itemsInStockOnTheEndOfMonth.ItemsCount, itemsInStockOnTheEndOfMonth.ItemsPercentsCount);

         var priceForItem = ctrInfoProv.GetContractItemPrice(ctr.ContractID);
         var conAndReconDiscountCalc = new ConAndReconDiscountCalc(this.itemsDuringMonthStats, priceForItem);

         var invoice = new Invoice
         {
            ContractID = ctr.ContractID,
            InvoiceReference = ctr.ClientInvoiceRef,
            Date = last,
            InvoiceAddress = ctr.Address,
            ClientAddress = ctr.AuxiliaryAddress,
            InvoiceNo = invoiceNo
         };

         var ctrHistory =  from ah in dbCtx.ActionHistory
                           join ca in dbCtx.ContractAction on ah.ContractActionID equals ca.ContractActionID
                           where ah.DateCompleted >= first && ah.DateCompleted <= last && ca.ContractID == ctr.ContractID
                           select new 
                           {
                              ContractActionID = ah.ContractActionID,

                              Charge = ah.Charge,
                              Percentage = ah.Percentage
                           };
            
         var actionsHistoryStats =  
                           from ah in ctrHistory
                           group ah by ah.ContractActionID into actionsGroup
                           select new
                           {
                              ContractActionID = actionsGroup.FirstOrDefault().ContractActionID,

                              Amount = actionsGroup.Count(),
                              ChargeAll = actionsGroup.Sum(a => a.Charge),
                              PercAll = actionsGroup.Sum(a => a.Percentage??0),
                           };

         var contractActionsCache = (from ca in dbCtx.ContractAction
                                     where ca.ContractID == ctr.ContractID
                                     select ca).ToList<ContractAction>();

         foreach (var stats in actionsHistoryStats)
         {
            var result = from ca in contractActionsCache where ca.ContractActionID == stats.ContractActionID select ca;
            if (result.Count() != 1)
            {
               throw new BusinessRuleException(string.Format("ContarctAction table error: action with ID: {0} is not on contract {1} list!", stats.ContractActionID, ctr.ContractID));
            }
            var ctrAction = result.First();

            var chargeCalc = CreateActionChargCalc(ctrAction.Code, ctrAction.PerFreeAction, stats.Amount, itemsInStockOnTheEndOfMonth.ItemsCount);

            var entry = new Invoice.Entry 
            {
               Description = ctrAction.Description,
               Charge = chargeCalc.ApplyDiscount(stats.ChargeAll),
               Quantity = stats.Amount 
            };

            log.DebugFormat("entry: [desc:'{0}' quant:{1} charge:{2}(org. charge:{3})]", entry.Description, entry.Quantity, entry.Charge, stats.ChargeAll);

            invoice.Entries.Add(entry);

            itemsDuringMonthStats.UpdateStats(ctrAction.Code, stats.Amount, stats.PercAll);

            conAndReconDiscountCalc.NotifyEntry(ctrAction.Code, entry);
         }

         invoice.Entries.Add(CreateItemsChargeEntry(itemsInStockOnTheEndOfMonth, priceForItem, conAndReconDiscountCalc.ApplyDiscount));

         AppendFixedUnitChargeEntryIfNecessary(invoice);

         invoice.SetupTotalChargeAndTax(vat);

         log.DebugFormat("invoice tax (based on total charge: {0}, and VAT: {1}% is {2}", invoice.TotalNetto, vat, invoice.Tax);

         return invoice;
      }

      private Invoice.Entry CreateItemsChargeEntry(ItemsAndPercentsCount itemsInStockOnTheEndOfMonth, double priceForItem, Action<ItemsAndPercentsCount> conOrReconEntriesAdjuster)
      {
         var fixedUnits = ctr.FixedUnits;
         var fixedUnitsPercentsCount = fixedUnits * 100;
         var fixedFee = ctr.FixedFee;
         var itemsAtTheBeginingOfMonth = itemsDuringMonthStats.CalculateItemsAtTheBeginingOfMonth(itemsInStockOnTheEndOfMonth);

         var itemsChargeInvoiceEntry = new Invoice.Entry { Description = InvoiceAdditionalData.ItemsChargeEntryCaption(fixedFee) };

         itemsChargeInvoiceEntry.Quantity = Math.Max(itemsAtTheBeginingOfMonth.ItemsCount - fixedUnits, 0);

         var itemsPercentsToBePaidFor = itemsAtTheBeginingOfMonth.ItemsPercentsCount - fixedUnitsPercentsCount;

         if (itemsPercentsToBePaidFor >= 0)
         {
            itemsChargeInvoiceEntry.Charge = priceForItem * (double)(itemsAtTheBeginingOfMonth.ItemsPercentsCount - fixedUnitsPercentsCount) / 100;

            log.DebugFormat("items percentage amount exceeds fixed units percentage amount (with {0}%). Extra charge is {1:0.00}", itemsPercentsToBePaidFor, itemsChargeInvoiceEntry.Charge);
         }
         else 
         {
            itemsChargeInvoiceEntry.Charge = 0;

            conOrReconEntriesAdjuster(
               new ItemsAndPercentsCount { 
                  ItemsCount = fixedUnits > itemsAtTheBeginingOfMonth.ItemsCount ? fixedUnits - itemsAtTheBeginingOfMonth.ItemsCount : 0, 
                  ItemsPercentsCount = -itemsPercentsToBePaidFor 
               }); // '-' since we want positive value of items (in percents) which charge must be taken back from con. or recon.

            log.DebugFormat("items percentage is less the fixed units percentage. The difference charge (for {0}% of items) will be substracted from cons. or recons.");
         }

         return itemsChargeInvoiceEntry;
      }

      private void AppendFixedUnitChargeEntryIfNecessary(Invoice invoice)
      {
         if (MathUtils.AreEqual(ctr.FixedFee, 0.0))
         {
            log.DebugFormat("fixed fee is 0. Fixed units invoice entry won't be on invoice");
            return;
         }

         var entry = new Invoice.Entry
         {
            Description = InvoiceAdditionalData.FixedUnitsEntryCaption,
            Charge = ctr.FixedFee,
            Quantity = ctr.FixedUnits
         };
         invoice.Entries.Add(entry);

         log.DebugFormat("fixed units invoice entry: amount: {0}, charge:{1}", entry.Quantity, entry.Charge);
      }

      private DateTime LastDay(DateTime date)
      {
         return new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
      }

      private DateTime FirstDay(DateTime date)
      {
         return new DateTime(date.Year, date.Month, 1);
      }


      private ActionDiscountCalc CreateActionChargCalc(string actionCode, double percentsFree, int amountOfActions, int endOfMonthItemsCount)
      {
         if (ActionUtils.IsRetrieval(actionCode))
         {
            return new RetrtieveActionDiscountCalc(percentsFree, amountOfActions, endOfMonthItemsCount);
         }
         else
         {
            return new ActionDiscountCalc(percentsFree);
         }
      }
   }
}
