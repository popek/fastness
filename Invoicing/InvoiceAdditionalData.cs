﻿
namespace Fastness.Invoicing
{
   public class InvoiceAdditionalData
   {
      private static string[] reportFooter = new string[]
      {
         "P.O. Box 101, Wigton, CA7 5YA",
         "Tel: 016973 52496 Fax: 016973 51030",
         "VAT Registration No: 772 5386 04",
         "Registered Number: 4116235"
      };

      public static string[] ReportFooter { get { return reportFooter; } }

      public static string FixedUnitsEntryCaption { get { return "Minimum Charge"; } }

      public static string ZeroFeeItemsChargeEntryCaption { get { return "Units at the beginning of the month"; } }
      public static string NonZeroFeeItemsChargeEntryCaption { get { return "Additional Units at the beginning of the month"; } }

      public static string ItemsChargeEntryCaption(double fixedFee)
      {
         return fixedFee == 0 ? ZeroFeeItemsChargeEntryCaption : NonZeroFeeItemsChargeEntryCaption;
      }
      
   }
}
