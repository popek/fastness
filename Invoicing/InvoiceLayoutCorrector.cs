﻿using Fastness.Common;
using System;
using System.Collections.Generic;

namespace Fastness.Invoicing
{
   // 
   public class InvoiceLayoutCorrector
   {
      Invoice.Entry fixedUnits;
      Invoice.Entry atBeginingOfMonthUnits;
      SortedDictionary<int, Invoice.Entry> index2entry = new SortedDictionary<int, Invoice.Entry>();

      public void CorrectLayout(Invoice invoice)
      {
         foreach (var entry in invoice.Entries)
         {
            Process(entry);
         }
         invoice.Entries.Clear();

         if (fixedUnits != null)
         {
            invoice.Entries.Add(fixedUnits);
         }
         if (atBeginingOfMonthUnits != null)
         {
            invoice.Entries.Add(atBeginingOfMonthUnits);
         }
         foreach (var indexAndEntry in index2entry)
         {
            invoice.Entries.Add(indexAndEntry.Value);
         }
      }

      private void Process(Invoice.Entry entry)
      {
         int auxiliaryActionInfoIndex = FastnessActionsList.FixedList.FindIndex(fah => fah.LongName == entry.Description);
         if (auxiliaryActionInfoIndex > -1)
         {
            index2entry[FastnessActionsList.FixedList[auxiliaryActionInfoIndex].IndexOnInvoice] = entry;
         }
         else if (entry.Description == InvoiceAdditionalData.FixedUnitsEntryCaption)
         {
            fixedUnits = entry;
         }
         else if (entry.Description == InvoiceAdditionalData.ZeroFeeItemsChargeEntryCaption ||
                  entry.Description == InvoiceAdditionalData.NonZeroFeeItemsChargeEntryCaption)
         {
            atBeginingOfMonthUnits = entry;
         }
         else
         {
            throw new Exception(string.Format("Don't know how to handle '{0}' invoice entry!", entry.Description));
         }
      }
   }
}
