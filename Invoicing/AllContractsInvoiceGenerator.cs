﻿using Fastness.DataModel;
using Fastness.DataModel.Utils;
using Fastness.Invoicing.Report;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Fastness.Invoicing
{
   public class AllContractsInvoiceGenerator
   {
      static readonly ILog log = LogManager.GetLogger(typeof(AllContractsInvoiceGenerator));

      public event InvoiceHandler NextInvoiceGenerated;

      fastnessEntities dbCtx;
      IEnumerable<Contract> contracts;

      public AllContractsInvoiceGenerator(fastnessEntities dbCtx, IEnumerable<Contract> contracts)
      {
         this.dbCtx = dbCtx;
         this.contracts = contracts;

         NextInvoiceGenerated += invoice =>
         {
            new InvoiceLayoutCorrector().CorrectLayout(invoice);
         };
      }

      public void Generate(DateTime monthAndYear)
      {
         log.InfoFormat("+++ generating all invoices for month {0}/{1}", monthAndYear.Month, monthAndYear.Year);

         var nextInvoiceIndex = new FetchFromDb(dbCtx).NextInvoiceNo();
         
         foreach (var ctr in contracts)
         {
            var invoice = CreateInvoiceGenerator(monthAndYear, ctr, dbCtx).Generate(monthAndYear, nextInvoiceIndex);

            NextInvoiceGenerated(invoice);

            ++nextInvoiceIndex;
         }

         log.InfoFormat("+++ invoices generation for month {0}/{1} finished", monthAndYear.Month, monthAndYear.Year);
      }

      private IInvoiceGenerator CreateInvoiceGenerator(DateTime monthAndYear, Contract ctr, fastnessEntities dbCtx)
      {
         return new InvoiceGeneratorForContract(Misc.CurrentVAT, ctr, dbCtx);
      }
   }
}
