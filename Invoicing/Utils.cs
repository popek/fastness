﻿using Fastness.Common;
using System;

namespace Fastness.Invoicing
{
   public class Utils
   {
      static public MailTemplate DefaultInvoiceMailTemplate()
      {
         return new MailTemplate
         {
            Sender = "",
            SubjectTemplate = "Invoice for $DATE",
            BodyTemplate =
@"Dear $CUSTOMER,

invoice for $DATE is in attachement

regards
Fastness Team
"
         };
      }
      static public string StringFromTemplate(string template, string contractName, DateTime invoiceDate)
      {
         return template.Replace("$CUSTOMER", contractName).Replace("$DATE", DateUtils.ToMonthYearString(invoiceDate));
      }
   }
}
