﻿using Fastness.DataModel;
using log4net;
using System;
using Fastness.DataModel.Utils;


namespace Fastness.Invoicing
{
   public class InvoiceHistoryEntryDeserializer
   {
      static readonly ILog log = LogManager.GetLogger(typeof(InvoiceHistoryEntryDeserializer));

      fastnessEntities dbCtx;

      public InvoiceHistoryEntryDeserializer(fastnessEntities dbCtx)
      {
         this.dbCtx = dbCtx;
      }

      public Invoice Deserialize(InvoiceHistory ih)
      {
         log.DebugFormat("deserializing {0} invoice history entry", ih.LogDesc());

         var invoice = new InvoiceHistoryDetailsHandler().FromDetails(ih.Details);

         invoice.Date = new DateTime(ih.Year, ih.Month, DateTime.DaysInMonth(ih.Year, ih.Month));
         invoice.InvoiceNo = ih.InvoiceNo;

         var ctr = new FetchFromDb(dbCtx).ContractBasedOnContractId(ih.ContractID);

         invoice.ContractID = ctr.ContractID;
         invoice.InvoiceReference = ctr.ClientInvoiceRef;
         invoice.ClientAddress = ctr.AuxiliaryAddress;
         invoice.InvoiceAddress = ctr.Address;

         return invoice;
      }
   }
}
