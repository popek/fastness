﻿using Fastness.DataModel;
using Fastness.DataModel.Exceptions;
using Fastness.DataModel.Utils;
using System;
using System.Text;
using System.Globalization;

namespace Fastness.Invoicing
{
   public class InvoiceHistoryDetailsHandler
   {
      public string ToDetails(Invoice invoice)
      {
         var culture = new CultureInfo("en-GB");
         var sb = new StringBuilder(DBFieldsConstraint.INVOICE_HISTORY_DETAILS_MAX_LENGHT);

         sb.AppendFormat(culture, "{0:0.000};{1:0.000}\n", invoice.TotalNetto, invoice.Tax);
         foreach (var e in invoice.Entries)
         {
            sb.AppendFormat(culture, "{0:0.000};{1};{2}\n", e.Charge, e.Quantity, e.Description);
         }
         sb.AppendFormat(culture, "\n{0}", invoice.Notes);

         return sb.ToString();
      }

      public Invoice FromDetails(string invoiceHistoryEntry)
      {
         var culture = new CultureInfo("en-GB");

         var lines = invoiceHistoryEntry.Split('\n');

         if (lines.Length < 1)
         {
            throw new BusinessRuleException(string.Format("malformed invoice history entry details ({0})", invoiceHistoryEntry));
         }

         var invoice = new Invoice();

         var totalAndTax = lines[0].Split(';');
         invoice.TotalNetto = Convert.ToDouble(totalAndTax[0], culture);
         invoice.Tax = Convert.ToDouble(totalAndTax[1], culture);

         int index = 1;
         while (!string.IsNullOrEmpty(lines[index]))
         {
            var entryValues = lines[index].Split(';');
            invoice.Entries.Add(new Invoice.Entry
            {
               Charge = Convert.ToDouble(entryValues[0], culture),
               Quantity = Convert.ToInt32(entryValues[1], culture),
               Description = entryValues[2]
            });
            ++index;
         }

         // combine lines of notes 
         ++index;
         var sb = new StringBuilder();
         while (index < lines.Length-1)
         {
            sb.AppendFormat("{0}\n", lines[index]);
            ++index;
         }
         sb.Append(lines[index]);

         invoice.Notes = sb.ToString();

         return invoice;
      }
   }
}
