﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Linq;

namespace Fastness.Invoicing
{
   [XmlRoot("I")]
   public class Invoice
   {
      [XmlRoot]
      public class Entry
      {
         [XmlAttribute("d")]
         public string Description { get; set; }

         [XmlAttribute("q")]
         public int Quantity { get; set; }

         [XmlIgnore]
         public double Charge { get; set; }
      }

      [XmlIgnore]
      public string InvoiceAddress { get; set; }

      [XmlIgnore]
      public string ClientAddress { get; set; }

      [XmlAttribute("no")]
      public int InvoiceNo { get; set; }

      [XmlIgnore]
      public DateTime Date { get; set; }

      [XmlAttribute("ctrId")]
      public int ContractID { get; set; }

      [XmlAttribute("invRef")]
      public string InvoiceReference { get; set; }

      [XmlElement]
      public string Notes;

      [XmlArrayItem("E")]
      public List<Entry> Entries { get; set; }

      [XmlIgnore]
      public double TotalNetto { get; set; }

      [XmlIgnore]
      public double Tax { get; set; }

      public Invoice()
      {
         this.Entries = new List<Entry>();
      }

      public void SetupTotalChargeAndTax(double vat)
      {
         TotalNetto = Entries.Sum(entry => entry.Charge);
         Tax = vat * TotalNetto;
      }
   }
}
