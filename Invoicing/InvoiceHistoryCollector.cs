﻿using Fastness.DataModel;
using System.Collections.Generic;

namespace Fastness.Invoicing
{
   public class InvoiceHistoryCollector
   {
      public List<InvoiceHistory> Entries { get; private set; }
      
      public InvoiceHistoryCollector()
      {
         Entries = new List<InvoiceHistory>();
      }

      public void Collect(Invoice invoice)
      {
         Entries.Add(new InvoiceHistory
         {
            Month = invoice.Date.Month,
            Year = invoice.Date.Year,
            ContractID = invoice.ContractID,
            InvoiceNo = invoice.InvoiceNo,
            Details = new InvoiceHistoryDetailsHandler().ToDetails(invoice)
         });
      }
   }
}
