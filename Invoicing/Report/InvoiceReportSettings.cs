﻿using iTextSharp.text;

namespace Fastness.Invoicing.Report
{
   public class InvoiceReportSettings
   {
      public float PagetHeaderHeight { get {return 50; } }
      public float PagetFooterHeight { get { return 95; } }

      public float PageHorzMargin { get { return 15; } }

      public Font TitleFont { get; private set; }

      public Font HeaderBoldFont { get; private set; }
      public Font HeaderFont { get; private set; }

      public Font EntriesBoldFont { get; private set; }
      public Font EntriesFont { get; private set; }

      public Font FooterFontBig { get; private set; }
      public Font FooterFontSmall { get; private set; }

      public Font NotestFont { get; private set; }

      public float TitleToDetailsSpace { get { return 85; } }
      public float InBetweenDetailsSpace { get { return 10; } }
      public float DetailsToEntriesSpace { get { return 40; } }
      public float EntriesToSummarySpace { get { return 40; } }
      public float SummaryToNotesSpace { get { return 30; } }


      public InvoiceReportSettings()
      {
         this.TitleFont = FontFactory.GetFont("Arial", 22, Font.BOLD);

         this.HeaderBoldFont = FontFactory.GetFont("Arial", 11, Font.BOLD);
         this.HeaderFont = FontFactory.GetFont("Arial", 11, Font.NORMAL);

         this.EntriesBoldFont = FontFactory.GetFont("Arial", 11, Font.BOLD);
         this.EntriesFont = FontFactory.GetFont("Arial", 11, Font.NORMAL);

         this.FooterFontBig = FontFactory.GetFont("Arial", 8, Font.NORMAL);
         this.FooterFontSmall = FontFactory.GetFont("Arial", 6, Font.NORMAL);

         this.NotestFont = FontFactory.GetFont("Arial", 10, Font.ITALIC);
      }
   }
}
