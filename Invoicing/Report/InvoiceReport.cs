﻿using Fastness.Common;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.IO;

namespace Fastness.Invoicing.Report
{
   class PageFooterHandler : PdfPageEventHelper
   {
      InvoiceReportSettings settings;

      public PageFooterHandler(InvoiceReportSettings settings)
      {
         this.settings = settings;
      }

      public override void OnEndPage(PdfWriter writer, Document document)
      {
         base.OnEndPage(writer, document);

         var rect = iTextSharp.text.PageSize.A4;

         var line4 = new Phrase(InvoiceAdditionalData.ReportFooter[3], settings.FooterFontSmall);
         var line4Ypos = 14 + settings.FooterFontSmall.Size;

         var line3 = new Phrase(InvoiceAdditionalData.ReportFooter[2], settings.FooterFontSmall);
         var line3Ypos = line4Ypos + settings.FooterFontSmall.Size + 2;

         var line2 = new Phrase(InvoiceAdditionalData.ReportFooter[1], settings.FooterFontBig);
         var line2Ypos = line3Ypos + settings.FooterFontBig.Size + 2;

         var line1 = new Phrase(InvoiceAdditionalData.ReportFooter[0], settings.FooterFontBig);
         var line1Ypos = line2Ypos + settings.FooterFontBig.Size + 2;
         
         var xPos = (rect.Left + rect.Right) / 2;
         
         ColumnText.ShowTextAligned(writer.DirectContent, Element.ALIGN_CENTER, line1, xPos, line1Ypos, 0);
         ColumnText.ShowTextAligned(writer.DirectContent, Element.ALIGN_CENTER, line2, xPos, line2Ypos, 0);
         ColumnText.ShowTextAligned(writer.DirectContent, Element.ALIGN_CENTER, line3, xPos, line3Ypos, 0);
         ColumnText.ShowTextAligned(writer.DirectContent, Element.ALIGN_CENTER, line4, xPos, line4Ypos, 0);
      }
   }


   public interface IInvoiceReport
   {
      void AddInvoice(Invoice invoice);
   }

   public class PDFInvoiceReport : IInvoiceReport, IDisposable
   {
      InvoiceReportSettings setting;
      Document doc;
      PdfWriter writer;
      ICurrencyFormatter currencyFormatter;

      public PDFInvoiceReport(string reportFilePath)
      {
         currencyFormatter = new CurrencyFormatter();

         setting = new InvoiceReportSettings();

         doc = new Document(iTextSharp.text.PageSize.A4,
            setting.PageHorzMargin,
            setting.PageHorzMargin,
            setting.PagetHeaderHeight,
            setting.PagetFooterHeight);

         writer = PdfWriter.GetInstance(doc, new FileStream(reportFilePath, FileMode.Create));
         writer.PageEvent = new PageFooterHandler(this.setting);
         doc.Open();
      }

      public void AddInvoice(Invoice invoice)
      {
         if (!doc.NewPage())
         {
            throw new InvalidOperationException("could not add new page for next invoice report!");
         }

         AddTileAndLogo();
         AddGeneralInfo(invoice);
         AddInvoiceReference(invoice);
         AddEntries(invoice);
         AddSummary(invoice);
         AddNotes(invoice);
      }

      public void Dispose()
      {
         doc.Close();
         doc.Dispose();
      }

      void AddTileAndLogo()
      {
         var paragraph = new Paragraph("INVOICE", setting.TitleFont)
         {
            Alignment = Element.ALIGN_CENTER,
         };
         paragraph.SpacingAfter = setting.TitleToDetailsSpace;
         doc.Add(paragraph);

         var logo = iTextSharp.text.Image.GetInstance(Resources.logo, System.Drawing.Imaging.ImageFormat.Png);
         logo.SetAbsolutePosition(440, 690);
         doc.Add(logo);
      }

      void AddGeneralInfo(Invoice invoice)
      {
         var createAddressTable = new Func<PdfPTable>(() =>
         {
            var addressTab = new PdfPTable(3)
            {
               HorizontalAlignment = Element.ALIGN_CENTER            
            };
            addressTab.HorizontalAlignment = Element.ALIGN_CENTER;
            addressTab.DefaultCell.Border = 0;
            addressTab.DefaultCell.PaddingTop = 5;
            addressTab.SetWidths(new int[] { 370, 110, 90 });
            addressTab.SpacingAfter = setting.InBetweenDetailsSpace;
            return addressTab;
         });
         var resolveLine = new Func<int, string[], string>((index, lines) => 
         { 
            return index < lines.Length ? lines[index] : string.Empty; 
         });
         var createCell = new Func<string, Font, PdfPCell>((text, font) =>
         {
            var phrase = font == null ? new Phrase(text) : new Phrase(text, font);
            return new PdfPCell(phrase)
            {
               HorizontalAlignment = Element.ALIGN_LEFT,
               VerticalAlignment = Element.ALIGN_TOP,
               Border = 0
            };
         });

         if (!string.IsNullOrEmpty(invoice.ClientAddress))
         {
            AddAddressTableWithGeneralInfo(createAddressTable(), "Client Address", createCell, resolveLine, invoice);
            AddAddressTable(createAddressTable(), "Invoice Address", createCell, resolveLine, invoice);
         }
         else
         {
            AddAddressTableWithGeneralInfo(createAddressTable(), "Invoice Address", createCell, resolveLine, invoice);
         }
      }
      void AddAddressTableWithGeneralInfo(
         PdfPTable table, 
         string addressHeader,
         Func<string, Font, PdfPCell> createCell, 
         Func<int, string[], string> resolveLine,
         Invoice invoice)
      {
         table.AddCell(createCell(addressHeader, setting.HeaderBoldFont));
         table.AddCell(createCell("", null));
         table.AddCell(createCell("", null));

         var generalInfo = new Tuple<string, string>[] {
            new Tuple<string, string>("Invoice Number", invoice.InvoiceNo.ToString()),
            new Tuple<string, string>("Invoice Date", invoice.Date.ToString("dd\\/MM\\/yy")),
            new Tuple<string, string>("Contract ID", invoice.ContractID.ToString())
         };

         string[] invoiceAddressLines = invoice.InvoiceAddress.Split('\n');

         int lineNo = 0;
         for (; lineNo < 3; ++lineNo)
         {
            table.AddCell(createCell(resolveLine(lineNo, invoiceAddressLines), setting.HeaderFont));
            table.AddCell(createCell(generalInfo[lineNo].Item1, setting.HeaderFont));
            table.AddCell(new PdfPCell(new Phrase(generalInfo[lineNo].Item2, setting.HeaderFont))
            {
               HorizontalAlignment = Element.ALIGN_RIGHT,
               VerticalAlignment = Element.ALIGN_TOP,
               Border = 0
            });
         }
         for (; lineNo < invoiceAddressLines.Length; ++lineNo)
         {
            table.AddCell(createCell(resolveLine(lineNo, invoiceAddressLines), setting.HeaderFont));
            table.AddCell(createCell("", null));
            table.AddCell(createCell("", null));
         }
         doc.Add(table);
      }

      void AddAddressTable(
         PdfPTable table,
         string addressHeader,
         Func<string, Font, PdfPCell> createCell,
         Func<int, string[], string> resolveLine,
         Invoice invoice)
      {
         string[] clientAddressLines = invoice.ClientAddress.Split('\n');
         table.AddCell(createCell(addressHeader, setting.HeaderBoldFont));
         table.AddCell(createCell("", null));
         table.AddCell(createCell("", null));

         int lineNo = 0;
         for (; lineNo < clientAddressLines.Length; ++lineNo)
         {
            table.AddCell(createCell(resolveLine(lineNo, clientAddressLines), setting.HeaderFont));
            table.AddCell(createCell("", null));
            table.AddCell(createCell("", null));
         }
         doc.Add(table);
      }

      void AddInvoiceReference(Invoice invoice)
      {
         if (string.IsNullOrEmpty(invoice.InvoiceReference))
         {
            return;
         }

         var invoiceRefTab = new PdfPTable(2)
         {
            HorizontalAlignment = Element.ALIGN_CENTER
         };

         invoiceRefTab.DefaultCell.Border = 0;
         invoiceRefTab.DefaultCell.PaddingTop = 5;
         invoiceRefTab.SetWidths(new int[] { 100, 360 });
         invoiceRefTab.SpacingAfter = setting.DetailsToEntriesSpace;

         invoiceRefTab.AddCell(new Phrase("Invoice reference:", setting.HeaderBoldFont));
         invoiceRefTab.AddCell(new PdfPCell(new Phrase(invoice.InvoiceReference, setting.HeaderFont)) 
         {
            HorizontalAlignment = Element.ALIGN_LEFT,
            VerticalAlignment = Element.ALIGN_BOTTOM,
            Border = 0
         });

         doc.Add(invoiceRefTab);
      }

      void AddEntries(Invoice invoice)
      {
         var charges = new PdfPTable(3) { HorizontalAlignment = Element.ALIGN_CENTER };
         charges.SpacingBefore = 0;
         charges.SpacingAfter = 0;
         charges.DefaultCell.Border = 0;
         charges.DefaultCell.PaddingTop = 8;
         charges.SetWidths(new int[] { 180, 60, 40 });

         charges.SpacingAfter = setting.EntriesToSummarySpace;

         Action<string, int> addHeaderCell = (cellText, align) =>
         {
            charges.AddCell(new PdfPCell(new Phrase(cellText, setting.EntriesBoldFont))
            {
               HorizontalAlignment = align,
               Border = 0,
               PaddingBottom = 14
            });
         };

         addHeaderCell("Action", Element.ALIGN_LEFT);
         addHeaderCell("Quantity", Element.ALIGN_RIGHT);
         addHeaderCell(currencyFormatter.Symbol, Element.ALIGN_RIGHT);

         foreach (var entry in invoice.Entries)
         {
            charges.AddCell(new Phrase(entry.Description, setting.EntriesFont));
            charges.AddCell(new PdfPCell(new Phrase(entry.Quantity.ToString(), setting.EntriesFont))
            {
               HorizontalAlignment = Element.ALIGN_RIGHT,
               VerticalAlignment = Element.ALIGN_BOTTOM,
               Border = 0
            });
            charges.AddCell(new PdfPCell(new Phrase(ChargeToString(entry.Charge), setting.EntriesFont))
            {
               HorizontalAlignment = Element.ALIGN_RIGHT,
               VerticalAlignment = Element.ALIGN_BOTTOM,
               Border = 0
            });
         }
         doc.Add(charges);
      }

      void AddSummary(Invoice invoice)
      {
         var total = new PdfPTable(3) { HorizontalAlignment = Element.ALIGN_CENTER };
         total.DefaultCell.Border = 0;
         total.DefaultCell.PaddingTop = 8;
         total.SetWidths(new int[] { 180, 60, 40 });
         total.SpacingAfter = setting.SummaryToNotesSpace;
         
         Action<string, double> addRow = (caption, value) =>
         {
            total.AddCell(new Phrase("", setting.EntriesFont));
            total.AddCell(new Phrase(caption, setting.EntriesFont));
            total.AddCell(new PdfPCell(new Phrase(ChargeToString(value), setting.EntriesFont))
            {
               HorizontalAlignment = Element.ALIGN_RIGHT,
               VerticalAlignment = Element.ALIGN_BOTTOM,
               Border = 0
            });
         };

         addRow("Net Amount:", invoice.TotalNetto);
         addRow("VAT Amount:", invoice.Tax);
         addRow("Invoice Total:", 
            currencyFormatter.RoundUp(invoice.TotalNetto) +
            currencyFormatter.RoundUp(invoice.Tax));

         doc.Add(total);
      }

      void AddNotes(Invoice invoice)
      {
         if (string.IsNullOrEmpty(invoice.Notes))
         {
            return;
         }

         var notes = new PdfPTable(3) { HorizontalAlignment = Element.ALIGN_CENTER };
         notes.DefaultCell.Border = 0;
         notes.DefaultCell.PaddingTop = 8;
         notes.SetWidths(new int[] { 180, 60, 40 });

         notes.AddCell(new Phrase("Notes:", setting.HeaderBoldFont));
         notes.AddCell(new Phrase(""));
         notes.AddCell(new Phrase(""));

         notes.AddCell(new PdfPCell(new Phrase(invoice.Notes, setting.NotestFont))
         {
            HorizontalAlignment = Element.ALIGN_LEFT,
            VerticalAlignment = Element.ALIGN_BOTTOM,
            Border = 0
         });
         notes.AddCell(new Phrase(""));
         notes.AddCell(new Phrase(""));

         doc.Add(notes);
      }

      string ChargeToString(double charge)
      {
         return currencyFormatter.Format(charge);
      }
   }
}
