/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [A].[ItemActionID]
      ,[A].[ItemID]
      ,[A].[Description]
      ,[A].[Date]
      ,[A].[Notes]
      ,[A].[Charge]
      ,[A].[Completed]
      ,[A].[DateCompleted]
      ,[A].[Invoiced]
      ,[A].[InvoiceNumber]
      ,[A].[AuthorisedBy]
      ,[A].[Location]
      ,[A].[Percentage]
      ,[A].[LastAmended]
	  ,[Item].[ContractID]
  FROM [fastness].[dbo].[ItemDetail] as Item
  inner join [fastness].[dbo].[ItemAction] as A on [Item].[ItemNumber] = [A].[ItemID]
  where [Item].[ContractID] = 8 and [A].[Date] >= '2003-02-01' and [A].[Date] <= '2003-02-28'
  